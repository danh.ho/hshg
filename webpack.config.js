const { addTailwindPlugin } = require("@ngneat/tailwind");
const tailwindConfig = require("./tailwind.config.js");
const tailwindTruncate = require('@tailwindcss/line-clamp');
module.exports = (config) => {
    addTailwindPlugin({
        webpackConfig: config,
        tailwindConfig,
        tailwindTruncate,
        patchComponentsStyles: true
    });
    return config;
};

// module.exports = {
//     module: {
//       rules: [
//         {
//           test: /\.css$/,
//           loader: "postcss-loader",
//           options: {
//             postcssOptions: {
//               ident: "postcss",
//               plugins: () => [require("tailwindcss"), require("autoprefixer")],
//             },
//           },
//         },
//       ],
//     },
//   }