// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL: "https://hshg.nng.bz/api/graphql/",
  SITE_ID: "3",
  facebookAppId: '176819761283428',
  facebookRedirectURL: 'https://dev.hshg.nng.bz/api/login/facebook/',
  // gmailAppId: '647795121279-dq0aehot18kkkghnl036h23n1ues5l1n.apps.googleusercontent.com',
  gmailAppId: '639384541101-rbcnmu6dtecoti9mijpq7o2ps5vshcek.apps.googleusercontent.com',
  // gmailRedirectURL: 'https://abd.nng.bz/api/profile/google/',
  gmailRedirectURL: 'https://dev.hshg.nng.bz/api/profile/google/',
  gmailScope: 'https%3A//www.googleapis.com/auth/userinfo.profile openid https%3A//www.googleapis.com/auth/userinfo.email',
  // vnpayRedirectURL: 'http://localhost:4202/success/',
  vnpayRedirectURL: "https://dev.hshg.nng.bz/success/",
  API_VNPAY: "https://dev.hshg.nng.bz/api/order/vnpay/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
