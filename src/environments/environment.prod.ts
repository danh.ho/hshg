export const environment = {
  production: true,
  API_URL: "/api/graphql/",
  SITE_ID: "7",
  facebookAppId: '176819761283428',
  facebookRedirectURL: '/api/login/facebook/',
  gmailAppId: '639384541101-rbcnmu6dtecoti9mijpq7o2ps5vshcek.apps.googleusercontent.com',
  gmailRedirectURL: '/api/profile/google/',
  gmailScope: 'https%3A//www.googleapis.com/auth/userinfo.profile openid https%3A//www.googleapis.com/auth/userinfo.email',
  vnpayRedirectURL: "/success/",
  API_VNPAY: "/api/order/vnpay/",
};
