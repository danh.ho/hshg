import { AuthState } from 'src/app/shared/store/auth';
import { CmsState } from 'src/app/shared/store/cms';
import { FlatpageState } from 'src/app/shared/store/flatpage';
import { ProductState } from 'src/app/shared/store/product';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { CmsNewsState } from 'src/app/shared/store/tip-article'
import { ProductForBuyState } from './category/store/product.state';
import { OrderCartState } from './checkout/store';
import { HistoryState } from './my-account/components/order-history/store';
import { AddressState } from './my-account/components/profile-address/store';
import { ProfileState } from './my-account/shared/states';
import { ProductDetailForBuyState } from './product-detail/store';
import { BranchState } from './shared/store/branch/branch.state';


const productStates = [ProductForBuyState];

export const appState = [
    ...productStates,
    AddressState,
    ProfileState,
    HistoryState,
    ShoppingCartState,
    CmsState,
    ProductState,
    CmsNewsState,
    FlatpageState,
    OrderCartState,
    AuthState,
    ProductDetailForBuyState,
    BranchState
];
export const appStoredState = [AuthState, ShoppingCartState];
