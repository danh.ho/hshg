import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LikeProduct } from 'src/app/product-detail/store';
import { DialogLoginComponent } from 'src/app/shared/components/dialog-login/dialog-login.component';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { CmsNewsState, GetNews, GetPromotion, LikeNews, LikeNewsSuccessfully } from 'src/app/shared/store/tip-article';

@Component({
    selector: 'tdl-app-artile-listing',
    templateUrl: './artile-listing.component.html',
    styleUrls: ['./artile-listing.component.scss'],
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArtileListingComponent {
    // @Input() contentItems: Array<NewsNode>;
    paginatorLength: number = 0;
    private _isLogin: boolean;
    private _subscription = new Subscription();
    formGroup: FormGroup;
    contentItems = [];
    indexNews;
    after = -1;
    totalCount = 0;
    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        navSpeed: 700,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        navText: ['', ''],
        responsive: {
          0: {
            items: 1
          },
          425: {
            items: 2
          },
          768: {
            items: 3
          },
          1024: {
            items: 3
          }
        },
        nav: false,
        autoWidth: true
    }
    constructor(
      private _router: Router,
      public modalController: ModalController,
      private _store: Store,
      private _actions: Actions,
      private _fb: FormBuilder,
      private _activatedRoute: ActivatedRoute,
    ) { 
      this.formGroup = this._fb.group({
        like: this._fb.array([]),
      })
    }
    replaceBlank = /\s/g;

    ngOnInit(): void {
      this._subscription.add(this._store.select(CmsNewsState.getPromotion)
      .subscribe(res => {
        if(res){
          // this.contentItems = res
          const { edges, totalCount }= res
          edges?.forEach(ele => {
            this.contentItems.push(ele.node)
          })
          console.log(this.contentItems);
          
          this.totalCount = totalCount
          this.formGroup.removeControl('like');
          this.formGroup.addControl('like', this._fb.array([]));
          for (let i = 0; i < this.contentItems?.length; i++) {
              this.likes.push(
                  this._fb.group({
                      likeCount: this.contentItems[i]?.likeCount
                  })
              )
          }
        }
      }))
      this._subscription.add(
        this._store.select(AuthState.isLogin).pipe(
          tap(isLogin => this._isLogin = isLogin)
        ).subscribe(() => {
        })
      );
      this._subscription.add(
        this._actions
          .pipe(
            ofActionSuccessful(LikeNewsSuccessfully),
          )
          .subscribe((res) => {
            console.log(res)
            this.likes.at(this.indexNews).patchValue({
              likeCount: res?.payload?.news?.likeCount
          })
            // this._store.dispatch([new GetNews({categoryName: 'Tin tức', orderBy:'-created', first:3})]);
          })
      );
    }
    ngAfterViewInit(): void {
      this._store.dispatch([new GetPromotion({categoryName: 'Khuyến mãi', orderBy:'-created', first:3})]);
    }
    goToPromotionDetail(data){
      if(data?.url){
        window.location.href = data?.url
      }
    }
    seeMore(){
      this.after +=3;
      this._store.dispatch([new GetPromotion({categoryName: 'Khuyến mãi', orderBy:'-created',
                                         first:3, after: this.after.toString()})]);
    }
    async openDialog() {
      const modal = await this.modalController.create({
        component: DialogLoginComponent,
        cssClass: 'dialog-login',
        componentProps: {
          title: 'khuyến mãi'
        }
      });
      await modal.present();
      const { data } = await modal.onWillDismiss();
      if (data.dismissed == true) {
        this._router.navigate(['/auth/login'])
      }
    }
    likeProduct(newsId, index){
      if (!this._isLogin) {
        this.openDialog()
      } else {
        this.indexNews = index
        this._store.dispatch(new LikeNews({ newsId }));
      }
    }
    get likes(){
      return this.formGroup.get('like') as FormArray
    }
}
