import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, Select, Actions, ofActionSuccessful } from '@ngxs/store';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { GetNewsDetailByID, CmsNewsState, LikeNewsSuccessfully, LikeNews } from 'src/app/shared/store/tip-article';
import { Observable, Subscription } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { IonContent, ModalController } from '@ionic/angular';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { DialogLoginComponent } from 'src/app/shared/components/dialog-login/dialog-login.component';

@Component({
    templateUrl: './news-detail.component.html',
    styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit,OnDestroy {
    @Select(CmsNewsState.getNewsNode) newsNode$: Observable<NewsNode>;
    @ViewChild(IonContent) contentPage: IonContent ;
    isShowBackToTop = false;
    cartItemsNo: number;
    id;
    _isLogin;
    formGroup: FormGroup;
    indexNews;
    likeCountNews = 0;
    slide = 3;
    private _subscription = new Subscription();
    constructor(
      private _activatedRoute: ActivatedRoute,
      private _store: Store,
      private _actions: Actions,
      private _router: Router,
      public modalController: ModalController,
      private _fb: FormBuilder,
      private breakpointObserver: BreakpointObserver,
    ) {
      this.formGroup = this._fb.group({
        like: this._fb.array([]),
      })
    }

    ngOnInit(): void {
        this._subscription.add(this._activatedRoute.paramMap.pipe(
            map(params => params.get('id')),
            tap(id => this.id = id),
            switchMap(id => this._store.dispatch(new GetNewsDetailByID({id, amountNews:4})))
        ).subscribe());
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                this._isLogin = isLogin
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
        this._subscription.add(this._store.select(CmsNewsState.getNewsNode)
        .subscribe(res => {
          if(res){
            this.likeCountNews = res?.likeCount
            this.formGroup.removeControl('like');
            this.formGroup.addControl('like', this._fb.array([]));
            for (let i = 0; i < res?.newsRelated?.length; i++) {
                this.likes.push(
                    this._fb.group({
                        likeCount: res?.newsRelated[i]?.likeCount
                    })
                )
            }
          }
        }))
        this._subscription.add(
          this._actions
            .pipe(
              ofActionSuccessful(LikeNewsSuccessfully),
            )
            .subscribe((res) => {
              if(res?.payload?.news?.id == this.id){
                this.likeCountNews = res?.payload?.news?.likeCount
              } else {
                this.likes.at(this.indexNews).patchValue({
                  likeCount: res?.payload?.news?.likeCount
                })
              }
            })
        );
        this._subscription.add(this.breakpointObserver.observe([Breakpoints.Handset]).pipe(
          tap(state => {
              if (state.matches) {
                this.slide = 4
              } else {
                this.slide = 3
              }
          })
      ).subscribe());
    }
    scrollToTop() {    
        this.contentPage.scrollToTop(700);
    }
    logScrolling(event){
        if(event.detail.scrollTop > 100){
          this.isShowBackToTop = true;
        } else {
          this.isShowBackToTop = false;
        }
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
    async openDialog() {
      const modal = await this.modalController.create({
        component: DialogLoginComponent,
        cssClass: 'dialog-login',
        componentProps: {
          title: 'tin tức'
        }
      });
      await modal.present();
      const { data } = await modal.onWillDismiss();
      if (data.dismissed == true) {
        this._router.navigate(['/auth/login'])
      }
    }
    likeProduct(newsId, index){
      if (!this._isLogin) {
        this.openDialog()
      } else {
        this._store.dispatch(new LikeNews({ newsId }));
        this.indexNews = index
      }
    }
    get likes(){
      return this.formGroup.get('like') as FormArray
    }
}
