import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'tdl-app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

    constructor(
        private _activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

}
