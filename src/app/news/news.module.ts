import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { NewsListingComponent } from './pages/news-listing/news-listing.component';
import { NewsDetailComponent } from './pages/news-detail/news-detail.component';
import { BannerSideComponent } from './components/banner-side/banner-side.component';
import { TrainningComponent } from './components/training/training.component';
import { ArtileListingComponent } from './components/artile-listing/artile-listing.component';
import { NewsContentComponent } from './components/news-content/news-content.component';
import { EventListingComponent } from './pages/event-listing/event-listing.component';
import { BeautyTipsListingComponent } from './pages/beauty-tips-listing/beauty-tips-listing.component';
import { MatCarouselModule } from '@ngbmodule/material-carousel';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SharedModule } from '../shared/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { IonicModule } from '@ionic/angular';


@NgModule({
    declarations: [NewsComponent, NewsListingComponent, NewsDetailComponent, BannerSideComponent, TrainningComponent, ArtileListingComponent, NewsContentComponent, EventListingComponent, BeautyTipsListingComponent],
    imports: [
        CommonModule,
        NewsRoutingModule,
        SharedModule,
        MatCarouselModule.forRoot(),
        NgxUsefulSwiperModule,
        NgxGalleryModule,
        CarouselModule, 
        IonicModule
    ]
})
export class NewsModule { }
