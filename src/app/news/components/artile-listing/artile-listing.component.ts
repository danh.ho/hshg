import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { DialogLoginComponent } from 'src/app/shared/components/dialog-login/dialog-login.component';
import { NewsNode, NewsNodeConnection } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { CmsNewsState, GetNews, LikeNews, LikeNewsSuccessfully } from 'src/app/shared/store/tip-article';

@Component({
    selector: 'tdl-app-artile-listing',
    templateUrl: './artile-listing.component.html',
    styleUrls: ['./artile-listing.component.scss'],
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArtileListingComponent {
    paginatorLength: number = 0;
    lenSlice = 3
    totalCount = 0;
    after = -1;
    cartItemsNo: number;
    private _subscription = new Subscription();
    _isLogin: boolean;
    contentItems = [];
    formGroup: FormGroup;
    indexNews;
    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        navSpeed: 700,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        navText: ['', ''],
        responsive: {
          0: {
            items: 1
          },
          425: {
            items: 2
          },
          768: {
            items: 3
          },
          1024: {
            items: 4
          }
        },
        nav: false,
        autoWidth: true
    }
    constructor(
      private _router: Router,
      private _store: Store,
      private _actions: Actions,
      public modalController: ModalController,
      private _fb: FormBuilder,
    ) { 
      this.formGroup = this._fb.group({
        like: this._fb.array([]),
      })
    }
    replaceBlank = /\s/g;

    ngOnInit(): void {
        this._subscription.add(
          this._store.select(AuthState.isLogin).pipe(
            switchMap((isLogin) => {
              this._isLogin = isLogin
              if (isLogin) {
                return this._store.select(ShoppingCartState.getMemberCart)
              } else {
                return this._store.select(ShoppingCartState.getGuestCart)
              }
            })
          ).subscribe(cart => {
            this.cartItemsNo = cart.length;
      }));
      this._subscription.add(this._store.select(CmsNewsState.getNewsNodeConnection)
      .subscribe(res => {
        if(res){
          const { edges, totalCount }= res
          edges?.forEach(ele => {
            this.contentItems.push(ele.node)
          })
          this.totalCount = totalCount
          this.formGroup.removeControl('like');
          this.formGroup.addControl('like', this._fb.array([]));
          for (let i = 0; i < this.contentItems?.length; i++) {
              this.likes.push(
                  this._fb.group({
                      likeCount: this.contentItems[i]?.likeCount
                  })
              )
          }
        }
      }))
      this._subscription.add(
        this._actions
          .pipe(
            ofActionSuccessful(LikeNewsSuccessfully),
          )
          .subscribe((res) => {
            this.likes.at(this.indexNews).patchValue({
              likeCount: res?.payload?.news?.likeCount
          })
            // this._store.dispatch([new GetNews({categoryName: 'Tin tức', orderBy:'-created', first:3})]);
          })
      );
    }

    goToPromotionDetail(data){
      if(data?.url){
        window.location.href = data?.url
      }
  }
  ngAfterViewInit(): void {
    this._store.dispatch([new GetNews({categoryName: 'Tin tức', orderBy:'-created', first:3})]);
  }
  seeMore(){
    this.lenSlice += 3;
    this.after +=3;
    this._store.dispatch([new GetNews({categoryName: 'Tin tức', orderBy:'-created',
                                       first:3, after: this.after.toString()})]);
  }
  async openDialog() {
    const modal = await this.modalController.create({
      component: DialogLoginComponent,
      cssClass: 'dialog-login',
      componentProps: {
        title: 'tin tức'
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.dismissed == true) {
      this._router.navigate(['/auth/login'])
    }
  }
  likeProduct(newsId, index){
    if (!this._isLogin) {
      this.openDialog()
    } else {
      this.indexNews = index
      this._store.dispatch(new LikeNews({ newsId }));
      
    }
  }
  get likes(){
    return this.formGroup.get('like') as FormArray
}
}
