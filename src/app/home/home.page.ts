import { ChangeDetectorRef, Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { LoadCategoryList, LoadProductsForBuy, ProductForBuyState } from '../category/store';
import { CmsNewsState, GetNews } from '../shared/store/tip-article';
import { Observable } from 'rxjs'
import { CmsState, GetBannerList, GetCustomerReviews, GetFeedbacks, GetGroupBanner, GetOurPartners } from '../shared/store/cms';
import { NAVBAR_MENU } from '../header/menu';
import { OnDestroy } from '@angular/core';
import { GetCollectionList, GetFlashSale, GetProductList, GetProductListByCollection, LoadProductsWithVariantAll, LoadProductsWithVariantHomePage, ProductState } from '../shared/store/product';
import { BannerGroupNode, BannerNode, ByProductAndVariant, CustomerReviewNode, NewsNode, OurPartnerNode, ProductForBuyNode, ProductForBuyNodeConnection, ProductImageNode } from '../shared/models/graphql.models';
import { IonContent, IonSlides, ModalController } from '@ionic/angular';
import { SubcribleModalComponent } from '../shared/components/subcrible-modal/subcrible-modal.component';
import { first, map, switchMap, tap } from 'rxjs/operators';
import { AddItemToGuestBag, AddItemToMemberShoppingCart, GetMemberShoppingCart, ShoppingCartState } from '../shared/store/shopping-cart';
import { AuthState } from '../shared/store/auth';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { MatDialog } from '@angular/material/dialog';
import { VideoProductDetailComponent } from '../shared/components/video-product-detail/video-product-detail.component';
import { SelectBranchComponent } from '../shared/components/select-branch/select-branch.component';
import { DomSanitizer } from '@angular/platform-browser';
type NodeConnection = ProductForBuyNodeConnection;
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  @Output() actionScroll = new EventEmitter();
  @ViewChild('grid') ele: ElementRef;
  @ViewChild('product') eleProduct: ElementRef;
  private _subscription = new Subscription();
  @Select(CmsNewsState.getNews) news$: Observable<Array<NewsNode>>;
  @Select(CmsState.getBannerList) bannerList$: Observable<Array<BannerNode>>;
  @Select(ProductState.getBestSellerProducts) product$: Observable<Array<ProductForBuyNode>>;
  @ViewChild('slider')  slides: IonSlides;
  @ViewChild(IonContent) content: IonContent ;
  swipeNext(){
    this.slides.slideNext();
  }
  swipePrev(){
    this.slides.slidePrev();
  }
  navbar = NAVBAR_MENU;
  bannerList;
  items;
  subBanner;
  popupBanner;
  categories = []
  collections = []
  private fragment: string;
  private _isLogin: boolean;
  productImageList: ProductImageNode[];
  public dataProduct;
  public totalProduct = 0;
  isShowBackToTop = false;
  selectedProduct = null;
  cartItemsNo: number;
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 5000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
    },
    nav: false,
    // dotsClass: 'owl-dots'
  }
  productOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    autoplayHoverPause: false,
    autoplaySpeed: 700,
    autoplayTimeout: 1000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
    },
    nav: false,
    // dotsClass: 'owl-dots'
  }
  slideBannerOpts = {
    initialSlide: 0,
    speed: 400,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
      // pauseOnMouseEnter: true,
    },
    loop: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      type: "bullets",
    },

  };

  constructor(private activatedRoute: ActivatedRoute,
    private _store: Store,
    private _cdRef: ChangeDetectorRef,
    public modalController: ModalController,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private sanitizer : DomSanitizer
  ) {


  }
  // async presentModal() {
  //   const modal = await this.modalController.create({
  //     component: SelectBranchComponent,
  //     cssClass: 'my-custom-class',
  //     backdropDismiss:false
  //   });
  //   await modal.present();
  // }
  ngOnInit() {
    // this.loadDataForBuy();
    // this.presentModal();
    this.loadCategories();
    this._store.dispatch(new GetCollectionList())
    this._subscription.add(this._store.select(ProductState.getCollectionNodeConnection).subscribe(
      res => {
        if(res){
          this.collections = res
        }
      }
    ))
    if(this.collections.length>0){
          this._store.dispatch(new LoadProductsWithVariantHomePage({ collectionId: this.collections[0]?.id, first: 8 }))
    }
    this._subscription.add(this._store.select(CmsState.getBannerList)
      .subscribe(
      ));
    // setTimeout(() => {
    //   this.presentModal();
    // }, 5000);

    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        tap(isLogin => {
          if (isLogin) {
            this._isLogin = true;
          } else {
            this._isLogin = false;
          }
        })
      ).subscribe());
      this._subscription.add(
        this._store.select(AuthState.isLogin).pipe(
          switchMap((isLogin) => {
            if (isLogin) {
              return this._store.select(ShoppingCartState.getMemberCart)
            } else {
              return this._store.select(ShoppingCartState.getGuestCart)
            }
          })
        ).subscribe(cart => {
          this.cartItemsNo = cart.length;
    }));
  }
  scrollToTop() {    
    this.content.scrollToTop(700);
  }
  logScrolling(event){
    if(event.detail.scrollTop > 100){
      this.isShowBackToTop = true;
    } else {
      this.isShowBackToTop = false;
    }
  }
  ngAfterViewInit() {
    document.body.style.display = "initial";
    setTimeout(() => {
      this._store.dispatch(new GetBannerList());
      // this._store.dispatch(new GetGroupBanner());
      this._subscription.add(this._store
        .select(ProductState.getProductWithVariantHomePage)
        .subscribe(res => {
          if (res) {
            this.dataProduct = res;
          }
        }
      ));
      this._subscription.add(this._store.select(CmsState.getBannerGroup)
        .subscribe(res => {
          if (res) {
            const main = res.filter(ele => ele.itemCode == 'main')
            this.bannerList = main[0]?.banner.edges;
          }
        }
        ));
    }, 600);
  }


  public loadDataForBuy(): void {
  }
  loadCategories() {
    this._subscription.add(this._store.select(ProductForBuyState.getCategories).subscribe(
      (res) => {
        if (res)
          this.categories = res?.edges;
      }
    ))
  }
  href(url: string) {
    window.open(url, "_blank")
  }

  addToCart(item: ByProductAndVariant) {
    // let params = {
    //   product: item.id,
    //   quantity: 1
    // }
    const quantity = 1;
    if (this._isLogin) {
      //this.addToCartLogin(params);
      this._store.dispatch(new AddItemToMemberShoppingCart({ product: item.product, quantity }))
      this._store.dispatch(new GetMemberShoppingCart)
    } else {
      this._store.dispatch(new AddItemToGuestBag({ product: item.product, quantity }));
      // this.addToCartNotLogin(item)
    }
  }
  onTabChanged(event: MatTabChangeEvent): void {
    const index = event.index
    this.dataProduct = []
    this._store.dispatch(new LoadProductsWithVariantHomePage({ collectionId: this.collections[index]?.id, first: 8 }))

    // if (event.tab.textLabel == "TẤT CẢ" || event.tab.textLabel == "ALL") {
    //     this._router.navigate(['/product']);
    // }
    
  }
  
  popupVideo(item){
    const dialogRef = this.dialog.open(VideoProductDetailComponent, {
      width: '900px',
      height: 'auto',
      panelClass: 'dialog-video',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){

      }
    });
  }
  over(item){
    this.selectedProduct = item
  }
  out(){
    this.selectedProduct = null;
  }
  showGIF(value){
    if(this.selectedProduct == value){
      return true;
    } 
    return false;
  }
  showProductList(value){
    if(this.selectedProduct == value){
      return true;
    } 
    return false;
  }
  ngOnDestroy(): void {
    this._subscription.unsubscribe();

  }

}
