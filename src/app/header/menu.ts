export const NAVBAR_MENU = [
      {
        name: 'Sản phẩm',
        routing: '/category/all',
        id: '1',
        child: [
        ]
      },
      {
        name: 'Khuyến mãi',
        // routing: '/flatpage/aboutus',
        routing: '/promotion',
        id: '3',
      },
      {
        name: 'Dịch vụ khách hàng',
        routing: '/customer-service',
        id: '4',
        child: [
          {
            name: 'Đăng ký thẻ thành viên',
            routing: '/auth/register',
            id: '10',
          },
          {
            name: 'Chính sách quy định chung',
            routing: '/general-policy',
            id: '11',
          },
          {
            name: 'Chính sách khách hàng thân thiết',
            routing: '/flatpage/loyalty-policy',
            id: '12',
          },
          {
            name: 'Chính sách bảo mật thông tin',
            routing: '/flatpage/privacy-policy',
            id: '13',
          },
          {
            name: 'Tra cứu hóa đơn VAT',
            routing: '/flatpage/vat',
            id: '14',
          },
        ]
      },
      
      {
        name: 'Về chúng tôi',
        routing: '/about-us',
        id: '5',
      },
]
