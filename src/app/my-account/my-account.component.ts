import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { Actions, ofActionCompleted, Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { OrderNodeEdge, UserAddressNode } from '../shared/models/graphql.models';
import { AuthState } from '../shared/store/auth';
import { ShoppingCartState } from '../shared/store/shopping-cart';
import { GetOrderList, HistoryState } from './components/order-history/store';
import { AddressState, LoadAddresses } from './components/profile-address/store';

@Component({
    selector: 'cos-frontend-my-account',
    templateUrl: './my-account.component.html',
    styleUrls: ['./my-account.component.scss']
})
export class MyAccountComponent implements OnInit, AfterViewInit {
    profileSelected = true;
    addressSelected = false;
    addressList: UserAddressNode[];
    orders: OrderNodeEdge[];
    _subSink = new SubSink();
    cartItemsNo: number;
    private _subscription = new Subscription();
    selectedTabIndex = 0;
    @ViewChild('tabGroup', {static: false}) tab: MatTabGroup;
    constructor(
        private _router: Router,
        private _store: Store,
        private _actions: Actions,
        private _route: ActivatedRoute,
        private _cdRef: ChangeDetectorRef,
    ) {
        this._route.queryParams.subscribe((queryParams:any) => {
            window.setTimeout(()=>{
                this.selectedTabIndex = parseInt(queryParams?.tab);
                this._cdRef.markForCheck();
             });
        });
    }
    ngAfterViewInit(): void {
        document.body.style.display = "initial";
    }

    ngOnInit(): void {
        if (this._router.url.split("/")[2] === "edit-profile") {
            this.clickProfile()
        } else if (this._router.url.split("/")[2] === "profile-address") {
            this.clickAddress()
        }
        this.addressList = [];
        this._subSink.sink = this._actions
            .pipe<LoadAddresses>(ofActionCompleted(LoadAddresses))
            .pipe(switchMap(() => this._store.select(AddressState.getAddressNode)))
            .subscribe(addresses => {
                this.addressList = addresses;
            })
        this._subSink.sink = this._actions.pipe(
            ofActionCompleted(GetOrderList),
            withLatestFrom(this._store.select(HistoryState.getOrderList)))
            .subscribe(([, orderList]) => {
                this.orders = orderList.edges;
            })
        this.loadData();
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    clickProfile() {
        this.profileSelected = true;
        this.addressSelected = false;
    }
    clickAddress() {
        this.addressSelected = true;
        this.profileSelected = false
    }

    loadData() {
        this._store.dispatch([new LoadAddresses(), new GetOrderList({first: 100,orderBy: '-id',})])
    }

    onTabChanged(event: MatTabChangeEvent): void {
        const index = event.index;
        this._router.navigate(['/my-account/edit-profile'], { queryParams: { tab: index } })
    }
}
