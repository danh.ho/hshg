
import {
    QueryOrderDetailViewArgs,
    QueryOrdersViewArgs,
    ReviewError,
    UserReviewInput,
} from "src/app/shared/models/graphql.models";

const enum Actions {
    ORDER_VIEW = '[ORDER] Get Order List',
    REVIEW = '[ORDER] Review Order',
    REVIEW_SUCCESSFUL = '[ORDER] Review Order Successful',
    REVIEW_FAILED = '[ORDER] Review Order Failed',
    ORDER_DETAIL = '[ORDER] Get Order Detail By Id',
}

export class GetOrderList {
    static readonly type = Actions.ORDER_VIEW;
    constructor(public readonly payload: QueryOrdersViewArgs) { 
    }
}

export class GetOrderDetail {
    static readonly type = Actions.ORDER_DETAIL;
    constructor(public readonly payload: QueryOrderDetailViewArgs) { 
    }
}

export class Review {
    static readonly type = Actions.REVIEW;
    constructor(public readonly payload: UserReviewInput) { }
}
export class ReviewSuccessful {
    static readonly type = Actions.REVIEW_SUCCESSFUL;
    // constructor(public readonly payload: QueryOrdersViewArgs) {}
}
export class ReviewFailed {
    static readonly type = Actions.REVIEW_FAILED;
    constructor(public readonly payload: ReviewError[]) { }
}
