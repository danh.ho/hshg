
import { OrderError, OrderNode, OrderNodeConnection } from "src/app/shared/models/graphql.models";

export interface HistoryStateModel {
    success: boolean;
    errors?: OrderError[];
    orderList?: OrderNodeConnection;
    orderDetail?: OrderNode;
}
export const initialState: HistoryStateModel = {
    success: false,
};
