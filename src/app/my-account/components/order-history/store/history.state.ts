import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { HistoryStateModel, initialState } from './history-state.model';
import {
    GetOrderList,
    Review,
    ReviewFailed,
    ReviewSuccessful,
} from './history.actions';
import { HistoryService } from 'src/app/my-account/shared/services/history.service';
import { MyAccountService } from 'src/app/my-account/my-account.service';
import { GetOrderDetail } from '.';

@State<HistoryStateModel>({
    name: 'history',
    defaults: initialState,
})
@Injectable()
export class HistoryState {
    @Selector()
    static getOrderList({ orderList }: HistoryStateModel) {
        return orderList;
    }

    @Selector()
    static getOrderDetail({ orderDetail }: HistoryStateModel) {
        return orderDetail;
    }

    constructor(
        private _apiService: HistoryService,
        private _apiOrder: MyAccountService
    ) { }
    @Action(GetOrderList, { cancelUncompleted: true })
    getOrderList(
        { patchState }: StateContext<HistoryStateModel>,
        { payload }: GetOrderList
    ) {
        return this._apiService.ordersView(payload).pipe(
            tap(({ data }) => {
                patchState({
                    orderList: data.ordersView,
                    success: true,
                });
            })
        );
    }
    @Action(Review, { cancelUncompleted: true })
    review(context: StateContext<HistoryStateModel>, { payload }: Review) {
        return this._apiService.reviewForProduct({ input: payload }).pipe(
            tap(({ data }) => {
                const { errors, status } = data.reviewForProduct;
                if (status) {
                    return context.dispatch(new ReviewSuccessful());
                }
                return context.dispatch(new ReviewFailed(errors));
            })
        );
    }

    @Action(ReviewSuccessful)
    reviewSuccessful({ patchState }: StateContext<HistoryStateModel>): void {
        patchState({
            success: true,
        });
    }

    @Action(ReviewFailed)
    reviewFailed({ patchState }: StateContext<HistoryStateModel>): void {
        patchState({
            success: false,
        });
    }

    @Action(GetOrderDetail, { cancelUncompleted: true })
    getOrderDeatail(
        { patchState }: StateContext<HistoryStateModel>,
        { payload }: GetOrderDetail
    ) {
        return this._apiOrder.getOrderDetail(payload).pipe(
            tap(({ data }) => {
                patchState({
                    orderDetail: data.orderDetailView,
                });
            })
        );
    }
}
