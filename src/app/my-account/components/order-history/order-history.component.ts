import { Component, Input, OnInit } from '@angular/core';
import { Actions, ofActionCompleted, Store } from '@ngxs/store';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { OrderNodeEdge } from '../../../shared/models/graphql.models';
import { LoadProfile, ProfileState } from '../../shared/states';
import { GetOrderList, HistoryState } from './store';

@Component({
    selector: 'cos-frontend-order-history',
    templateUrl: './order-history.component.html',
    styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
    // orders: OrderNodeEdge[];
    @Input() orders: OrderNodeEdge[];
    contentTpl;
    id: string;
    private _subSink = new SubSink()
    constructor(
        private _store: Store, private _actions: Actions,
    ) {}

    ngOnInit(): void {
        // this.getData();
        // this._registerGetOrderListCompleted();
        console.log(this.orders)
    }

    getData() {
        this.orders = [];
        // const id = this._store.selectSnapshot(ProfileState.getProfileNode).id
        this._store.dispatch(
            new GetOrderList({
                first: 100,
                orderBy: '-id',
            })
        )
    }
    // private _registerGetOrderListCompleted() {
    //     this._subSink.sink = this._actions.pipe(
    //         ofActionCompleted(GetOrderList),
    //         withLatestFrom(this._store.select(HistoryState.getOrderList)))
    //         .subscribe(([, orderList]) => {
    //             this.orders = orderList.edges;
    //         })
    // }
}
