import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofActionDispatched, ofActionSuccessful, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { ChangePasswordInput } from 'src/app/shared/models/graphql.models';
import { NotificationService } from 'src/app/shared/services';
import { MatchValidator, NotMatchValidator } from 'src/app/shared/utils';
import { SubSink } from 'subsink';
import { ChangePassword, ChangePasswordSuccessful, ChangePasswordFailed } from '../../shared/states';

@Component({
    selector: 'cos-frontend-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

    changePasswordFormGroup: FormGroup;
    code: string;
    private _subsink = new SubSink();
    err: any;
    codeAuth: string;
    translate
    constructor(
        private _formBuilder: FormBuilder,
        private _store: Store,
        private _actions: Actions,
        private _spinner: NgxSpinnerService
    ) {

        this._subsink.sink = this._actions
            .pipe(ofActionDispatched(ChangePassword))
            .subscribe(() => { });
    }

    ngOnInit(): void {
        this.changePasswordFormGroup = this._formBuilder.group({
            password: ['', [Validators.required,
            Validators.minLength(6)]],
            newPassword: ['',
                [
                    Validators.required,
                    Validators.minLength(6),
                    NotMatchValidator('password'),
                ]],
            confirmNewPassword: ['',
                [Validators.required,
                MatchValidator('newPassword'),
                Validators.minLength(6),]]
        })

        this._subsink.sink = this._actions
            .pipe(ofActionDispatched(ChangePassword))
            .subscribe(() => { this._spinner.show() });
        this._subsink.sink = this._actions
            .pipe(ofActionSuccessful(ChangePasswordSuccessful))
            .subscribe((res) => {
                this._spinner.hide();
            });
        this._subsink.sink = this._actions
            .pipe(ofActionSuccessful(ChangePasswordFailed))
            .subscribe((err) => {
                this.err = err;
                this.codeAuth = err.payload[0].message;
                this._spinner.hide();
                this.translate = `Mật khẩu cũ sai`;
            });
    }
    changePass() {
        if (this.changePasswordFormGroup.valid) {
            const input: ChangePasswordInput = {
                password: this.changePasswordFormGroup.get('password').value,
                newPassword: this.changePasswordFormGroup.get('newPassword').value,
                confirmPassword: this.changePasswordFormGroup.get('confirmNewPassword').value,
            };
            this._store.dispatch(new ChangePassword(input));
        }
    }
}
