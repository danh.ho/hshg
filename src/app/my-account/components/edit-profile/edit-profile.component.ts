import { ChangeDetectorRef, Component, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofActionDispatched, ofActionSuccessful, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services';
import { AuthState } from 'src/app/shared/store/auth';
import { getDataURLFromFile } from 'src/app/shared/utils';
import { SubSink } from 'subsink';
import { EditProfileFailed, ProfileState } from '../../shared/states';
import { EditProfile, EditProfileSuccessful, LoadProfile } from '../../shared/states';

const acceptedImageTypes = ['image/png', 'image/jpeg'];
@Component({
    selector: 'cos-frontend-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit, OnDestroy {
    formGroup: FormGroup;
    iconUrl: string | ArrayBuffer;
    icon: File;
    codeAuth: string;
    translate
    err: any
    data = {
        avatar: '',
        email: '',
        firstName: '',
        lastName: '',
        id: '0',
        phone: '0',
    };
    private _subscription = new Subscription();
    _subSink = new SubSink();
    private _isLogin: boolean;
    constructor(
        private _store: Store,
        private _cdRef: ChangeDetectorRef,
        private _router: Router,
        private _actions: Actions,
        private _formBuilder: FormBuilder,
        private _notify: NotificationService,
        private _spinner: NgxSpinnerService
    ) { }

    ngOnInit(): void {
        this.formGroup = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            phone: ['', [Validators.required, this.noWhitespaceValidator, Validators.minLength(10), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
            email: ['', [Validators.required, Validators.email]]
        });
        this._store.select(AuthState.getToken).pipe(
            take(1),
            tap(res => {
                if (res) {
                    this._isLogin = true;
                    this.loadDataProfile();
                } else {
                    this._isLogin = false;
                    this._router.navigate(['/auth/login'])
                }
            })
        ).subscribe(() => { });
        this._subSink.sink = this._actions
            .pipe(ofActionDispatched(EditProfile))
            .subscribe(() => this._spinner.show());

        this._subSink.sink = this._actions
            .pipe(ofActionSuccessful(EditProfileSuccessful))
            .subscribe(() => {
                this.translate = ''
                this._spinner.hide();
            });
        this._subSink.sink = this._actions.pipe(ofActionSuccessful(EditProfileFailed)).subscribe((err) => {
            this.err = err
            this.codeAuth = this.err.payload[0].code;
            this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            this._spinner.hide();
        })
    }
    loadDataProfile() {
        this._store.dispatch(new LoadProfile());
        this._subSink.sink = this._store
            .select(ProfileState.getProfileNode)
            .subscribe(this._fillDataSource.bind(this));
    }

    _fillDataSource(nodeConnection: any) {
        if (!nodeConnection) {
            return
        }
        this.data = nodeConnection
        this.iconUrl = this.data.avatar;
        this.formGroup.setValue({
            firstName: this.data.firstName,
            lastName: this.data.lastName,
            phone: this.data.phone,
            email: this.data.email,
        });
        if (this.data.email !== '') {
            this.formGroup.get('email').disable({ onlySelf: true })
        }
        this._cdRef.detectChanges();

    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
        this._subSink.unsubscribe();
    }

    onSubmit(value: any): void {

        if (this.formGroup.valid) {
            if (this.icon) {
                this._store.dispatch(
                    new EditProfile({
                        email: this.formGroup.get('email').value,
                        avatar: this.icon,
                        firstName: this.formGroup.get('firstName').value,
                        lastName: this.formGroup.get('lastName').value,
                        phone: this.formGroup.get('phone').value,
                    })
                );
            } else if (!this.formGroup.get('email').disabled && this.icon) {
                this._store.dispatch(
                    new EditProfile({
                        avatar: this.icon,
                        email: this.formGroup.get('email').value,
                        firstName: this.formGroup.get('firstName').value,
                        lastName: this.formGroup.get('lastName').value,
                        phone: this.formGroup.get('phone').value,
                    })
                )
            } else if (!this.formGroup.get('email').disabled && !this.icon) {
                this._store.dispatch(
                    new EditProfile({
                        email: this.formGroup.get('email').value,
                        firstName: this.formGroup.get('firstName').value,
                        lastName: this.formGroup.get('lastName').value,
                        phone: this.formGroup.get('phone').value,
                    })
                )
            }
            else {
                this._store.dispatch(
                    new EditProfile({
                        firstName: this.formGroup.get('firstName').value,
                        lastName: this.formGroup.get('lastName').value,
                        phone: this.formGroup.get('phone').value,
                    })
                );
            }
        }
    }
    noWhitespaceValidator(control: FormControl) {
        const phoneNum = control.value;
        let isWhitespace = false
        for (let i = 0; i < phoneNum?.length; i++) {
            if (phoneNum.charAt(i) === ' ') {
                isWhitespace = true
            }
        }
        const isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }
    async imageInputChange(event: Event) {
        const { files }  = event.target as HTMLInputElement;
        if (!files) {
            return;
        }
        const file = Array.from(files)[0];
        if (!acceptedImageTypes.includes(file.type)) {
            return;
        }
        this.icon = file;
        const imageDataURL = await getDataURLFromFile(file);
        this.iconUrl = imageDataURL;
    }
}
