import {
    AddAddressInput,
    DeleteAddressInput,
    SetDefaultAddressInput,
    UpdateAddressInput,
    UserAddressNode,
    UserError,
} from '../../../../shared/models/graphql.models';

const enum Actions {
    LOADING = '[Address] Loading',
    LOAD_ADDRESSES = '[Address] Load Addresses',
    LOAD_ADDRESS_BY_ID = '[Address] Load Address by ID',
    CREATE_ADDRESS = '[Address] Create Address',
    CREATE_ADDRESS_SUCCESSFUL = '[Address] Create Address Successfully',
    CREATE_ADDRESS_FAILED = '[Address] Create Address Failed',
    UPDATE_ADDRESS = '[Address] Update Address',
    UPDATE_ADDRESS_SUCCESSFUL = '[Address] Update Address Successfully',
    UPDATE_ADDRESS_FAILED = '[Address] Update Address Failed',
    DELETE_ADDRESS = '[Address] Delete Address',
    DELETE_ADDRESS_SUCCESSFUL = '[Address] Delete Address Successfully',
    DELETE_ADDRESS_FAILED = '[Address] Delete Address Failed',
    SET_DEFAULT_ADDRESS = '[Address] Set Default Address',
    SET_DEFAULT_ADDRESS_SUCCESSFUL = '[Address] Set Default Address Successfully',
    SET_DEFAULT_ADDRESS_FAILED = '[Address] Set Default Address Failed',
}

export class Loading {
    static readonly type = Actions.LOADING;
}

export class LoadAddresses {
    static readonly type = Actions.LOAD_ADDRESSES;
}

export class CreateAddress {
    static readonly type = Actions.CREATE_ADDRESS;
    constructor(public readonly payload: AddAddressInput) { }
}
export class CreateAddressSuccessful {
    static readonly type = Actions.CREATE_ADDRESS_SUCCESSFUL;
    constructor(public readonly payload: UserAddressNode) { }
}
export class CreateAddressFailed {
    static readonly type = Actions.CREATE_ADDRESS_FAILED;
    constructor(public readonly payload: UserError[]) { }
}

export class UpdateAddress {
    static readonly type = Actions.UPDATE_ADDRESS;
    constructor(public readonly payload: UpdateAddressInput) { }
}
export class UpdateAddressSuccessful {
    static readonly type = Actions.UPDATE_ADDRESS_SUCCESSFUL;
    // constructor(public readonly payload: UpdateAddressInput) { }
    constructor(public readonly payload: UserAddressNode) { }
    
}
export class UpdateAddressFailed {
    static readonly type = Actions.UPDATE_ADDRESS_FAILED;
    constructor(public readonly payload: UserError[]) { }
}

export class DeleteAddress {
    static readonly type = Actions.DELETE_ADDRESS;
    constructor(public readonly payload: DeleteAddressInput) { }
}
export class DeleteAddressSuccessful {
    static readonly type = Actions.DELETE_ADDRESS_SUCCESSFUL;
    constructor(public readonly payload: UserAddressNode) { }
}
export class DeleteAddressFailed {
    static readonly type = Actions.DELETE_ADDRESS_FAILED;
    constructor(public readonly payload: UserError[]) { }
}

export class SetDefaultAddress {
    static readonly type = Actions.SET_DEFAULT_ADDRESS;
    constructor(public readonly payload: SetDefaultAddressInput) { }
}
export class SetDefaultAddressSuccessful {
    static readonly type = Actions.SET_DEFAULT_ADDRESS_SUCCESSFUL;
    constructor(public readonly payload: UserError[]) { }
}
export class SetDefaultAddressFailed {
    static readonly type = Actions.SET_DEFAULT_ADDRESS_FAILED;
    constructor(public readonly payload: UserError[]) { }
}
