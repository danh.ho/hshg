import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { take, tap } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services';
import { AddressService } from '../../../shared/services/address.service';
import { AddressStateModel, initialState } from './address-state.model';
import {
    CreateAddress,
    CreateAddressFailed,
    CreateAddressSuccessful,
    DeleteAddress,
    DeleteAddressFailed,
    DeleteAddressSuccessful,
    LoadAddresses,
    SetDefaultAddress,
    SetDefaultAddressFailed,
    SetDefaultAddressSuccessful,
    UpdateAddress,
    UpdateAddressFailed,
    UpdateAddressSuccessful,
} from './address.actions';

@Injectable({ providedIn: 'root' })
@State<AddressStateModel>({
    name: 'address',
    defaults: initialState,
})
export class AddressState {
    @Selector()
    static getAddressNode({ addressNode }: AddressStateModel) {
        return addressNode;
    }
    @Selector()
    static getSelectedAddressNode({ selectedAddressNode }: AddressStateModel) {
        return selectedAddressNode;
    }

    constructor(private _apiService: AddressService,
        private _notify: NotificationService) { }

    @Action(LoadAddresses, { cancelUncompleted: true })
    loadAddresses({ patchState }: StateContext<AddressStateModel>) {
        return this._apiService.getAddressList().pipe(
            tap(({ data }) => {
                patchState({
                    addressNode: data.userAddress,
                });
            })
        );
    }

    @Action(CreateAddress, { cancelUncompleted: true })
    createAddress(
        { dispatch }: StateContext<AddressStateModel>,
        { payload }: CreateAddress
    ) {
        return this._apiService.addAddress(payload).pipe(
            tap(({ data }) => {
                const { errors, status, userAddress } = data.userAddressAdd;
                if (status) {
                    return dispatch(new CreateAddressSuccessful(userAddress));
                }
                return dispatch(new CreateAddressFailed(errors));
            })
        );
    }

    @Action(CreateAddressSuccessful)
    createAddressSuccessfully(
        { patchState }: StateContext<AddressStateModel>,
        { payload }: CreateAddressSuccessful
    ) {
        patchState({ success: true, selectedAddressNode: payload });
        this._notify.success('','Thêm địa chỉ thành công')
    }
    @Action(CreateAddressFailed)
    createAddressFailed(
        { patchState }: StateContext<AddressStateModel>,
        { payload }: CreateAddressFailed
    ) {
        patchState({ success: false, errors: payload });
    }

    @Action(UpdateAddress, { cancelUncompleted: true })
    updateAddress(
        { dispatch }: StateContext<AddressStateModel>,
        { payload }: UpdateAddress
    ) {
        return this._apiService.updateAddress(payload).pipe(
            tap(({ data }) => {
                const { errors, status, userAddress } = data.userAddressUpdate;

                if (status) {
                    return dispatch(new UpdateAddressSuccessful(userAddress));
                }
                return dispatch(new UpdateAddressFailed(errors));
            })
        );
    }

    @Action(UpdateAddressSuccessful)
    updateCategorySuccessfully({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: true });
        this._notify.success('','Chỉnh sửa địa chỉ thành công')
    }
    @Action(UpdateAddressFailed)
    updateCategoryFailed({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: false });
    }

    @Action(DeleteAddress, { cancelUncompleted: true })
    deleteAddress(
        { dispatch }: StateContext<AddressStateModel>,
        { payload }: DeleteAddress
    ) {
        return this._apiService.deleteAddress(payload).pipe(
            tap(({ data }) => {
                const { errors, status, userAddress } = data.userAddressDelete;

                if (status) {
                    return dispatch(new DeleteAddressSuccessful(userAddress));
                }
                return dispatch(new DeleteAddressFailed(errors));
            })
        );
    }

    @Action(DeleteAddressSuccessful)
    deleteAddressSuccessfully({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: true });
        this._notify.success('','Xóa địa chỉ thành công')
    }
    @Action(DeleteAddressFailed)
    deleteAddressFailed({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: false });
    }

    @Action(SetDefaultAddress, { cancelUncompleted: true })
    setDefaultAddress(
        { dispatch }: StateContext<AddressStateModel>,
        { payload }: SetDefaultAddress
    ) {
        return this._apiService.setDefault(payload).pipe(
            tap(({ data }) => {
                const { errors, status } = data.userAddressSetDefault;

                if (status) {
                    return dispatch(new SetDefaultAddressSuccessful(errors));
                }
                return dispatch(new SetDefaultAddressFailed(errors));
            })
        );
    }

    @Action(SetDefaultAddressSuccessful)
    setDefaultSuccessfully({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: true });
    }
    @Action(SetDefaultAddressFailed)
    setDefaultAddressFailed({ patchState }: StateContext<AddressStateModel>) {
        patchState({ success: false });
    }
}
