import { UserAddressNode, UserError } from '../../../../shared/models/graphql.models';

export interface AddressStateModel {
    addressNode?: UserAddressNode[];
    selectedAddressNode?: UserAddressNode;
    errors?: UserError[];
    success: boolean;
}

export const initialState: AddressStateModel = {
    success: false,
};
