import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Actions, ofActionCompleted, Store } from '@ngxs/store';
import { switchMap } from 'rxjs/operators';
import { UserAddressNode } from 'src/app/shared/models/graphql.models';
import { SubSink } from 'subsink';
import { AddressState, LoadAddresses } from './store';
import { CreateAddressModalComponent } from '../create-address-modal/create-address-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { LoadProfile } from '../../shared/states';

@Component({
    selector: 'cos-frontend-profile-address',
    templateUrl: './profile-address.component.html',
    styleUrls: ['./profile-address.component.scss']
})
export class ProfileAddressComponent implements OnInit {
    // addressList: UserAddressNode[];
    _subSink = new SubSink();
    @Input() addressList: UserAddressNode[];
    editing = false;
    constructor(
        private _store: Store,
        private _actions: Actions,
        private _dialog: MatDialog,
    ) { }

    ngOnInit(): void {
        // this.addressList = [];
        // this._subSink.sink = this._actions
        //     .pipe<LoadAddresses>(ofActionCompleted(LoadAddresses))
        //     .pipe(switchMap(() => this._store.select(AddressState.getAddressNode)))
        //     .subscribe(addresses => {
        //         this.addressList = addresses;
        //     })
        // this.loadData()
    }
    isEditing($event) {
        this.editing = $event
    }
    changeEvent($event) {
        this.loadData();
    }
    loadData() {
        this._store.dispatch(new LoadAddresses())
        this._store.dispatch(new LoadProfile());
    }
    openModal() {
        let modalRef = this._dialog.open(CreateAddressModalComponent, {
            panelClass: 'custom-dialog',
        });
        modalRef.afterClosed().subscribe((result) => {
            this.loadData();
        });
    }
}
