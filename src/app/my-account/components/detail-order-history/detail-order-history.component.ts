import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { SubSink } from 'subsink';
import { OrderNode, OrderNodeEdge } from '../../../shared/models/graphql.models';
import { GetOrderDetail, GetOrderList, HistoryState } from '../order-history/store';

@Component({
    selector: 'cos-frontend-detail-order-history',
    templateUrl: './detail-order-history.component.html',
    styleUrls: ['./detail-order-history.component.scss']
})
export class DetailOrderHistoryComponent implements OnInit {
    orders: OrderNodeEdge[];
    orderDetail: OrderNode;
    contentTpl;
    id: string;
    totalDiscount = 0;
    cartItemsNo: number;
    _subscription = new Subscription()
    private _subSink = new SubSink()
    constructor(
        private _store: Store,
        private _route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.id = this._route.snapshot.paramMap.get('orderDetail');
        this.getData();
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    getData() {
        this.orders = [];
        // this._subSink.sink = this._store
        //     .dispatch(
        //         new GetOrderList({
        //             first: 100,
        //             orderBy: '-id',
        //         })
        //     )
        //     .subscribe((data) => {
        //         this.orders = data.history.orderList.edges;
        //         this.orderDetail = this.orders.filter(data => data.node.id == this.id)[0].node
        //     });

        this._store.dispatch(new GetOrderDetail({id: this.id}))
        this._subscription.add(this._store.select(HistoryState.getOrderDetail).subscribe(data => {
            if(data){
                this.orderDetail = data
                this.totalDiscount = data.discount
            }
            // if(data?.orderDiscount && data?.orderDiscount.length > 0){
            //     this.getTotalDiscount(data?.orderDiscount)
            // }
        }))
        
    }

    getTotalDiscount(data){
        this.totalDiscount = 0
        for(let i = 0; i < data.length; i ++){
            this.totalDiscount += data[i].discount
        }
    }
}
