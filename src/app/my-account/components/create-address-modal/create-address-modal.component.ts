import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Actions, ofActionDispatched, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { DistrictNodeConnection, ProvinceNodeConnection, UserAddressNode, WardNodeConnection } from 'src/app/shared/models/graphql.models';
import { NotificationService } from 'src/app/shared/services';
import { SubSink } from 'subsink';
import { LoadDistrict, LoadProvince, LoadWard, ProfileState } from '../../shared/states';
import { CreateAddress, CreateAddressFailed, CreateAddressSuccessful } from '../profile-address/store';

@Component({
    selector: 'cos-frontend-create-address-modal',
    templateUrl: './create-address-modal.component.html',
    styleUrls: ['./create-address-modal.component.scss']
})
export class CreateAddressModalComponent implements OnInit {
    @Select(ProfileState.getNodeConnectionProvince) provinces$: Observable<ProvinceNodeConnection>;
    @Select(ProfileState.getNodeConnectionDistrict) districts$: Observable<DistrictNodeConnection>;
    @Select(ProfileState.getNodeConnectionWard) wards$: Observable<WardNodeConnection>;
    formGroup: FormGroup;
    _subSink = new SubSink();
    constructor(
        private _formBuilder: FormBuilder,
        private _store: Store,
        private _actions: Actions,
        private _notify: NotificationService,
        public _dialogRef: MatDialogRef<CreateAddressModalComponent>,
        private _spinner: NgxSpinnerService,
        @Inject(MAT_DIALOG_DATA) public data: UserAddressNode
    ) { }

    ngOnInit(): void {
        this.formGroup = this._formBuilder.group({
            receiver: ['', Validators.required],
            street: ['', Validators.required],
            district: ['', Validators.required],
            province: ['', Validators.required],
            ward: ['', Validators.required],
            phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
        });

        this._initDataProvince()
    }

    _initDataProvince(){
        this._store.dispatch(new LoadProvince())
    }

    _initDataDistrict(province){
        this._store.dispatch(new LoadDistrict({province}))
    }

    _initDataWard(district){
        this._store.dispatch(new LoadWard({district}))
    }

    changeProvince(event, province){
        if (event.source.selected) {
            this._initDataDistrict(province?.node?.id)
        }
    }

    changeDistrict(event, district){
        if (event.source.selected) {
            this._initDataWard(district?.node?.id)
        }
    }

    dismiss(address?: UserAddressNode) {
        if (address) {
            this.data = address;
        }
        this._dialogRef.close(this.data);
    }
    onSubmit(value: any) {
        if (this.formGroup.valid) {
            this._store.dispatch(new CreateAddress(this.formGroup.value));
            this._dialogRef.close();
        }
    }
}
