import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { LoadProfile, ProfileState } from '../../shared/states';

@Component({
  selector: 'cos-frontend-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
})
export class RankingComponent implements OnInit {
  private _subscription = new Subscription();
  userProfile
  constructor(
    private _store: Store
  ) { }

  ngOnInit() {
    this.loadDataProfile()
  }
  loadDataProfile() {
    this._store.dispatch(new LoadProfile());
    this._subscription.add(this._store.select(ProfileState.getProfileNode).subscribe(
      (res) => {
        if (res)
          this.userProfile = res
      }
    ))
  }
}
