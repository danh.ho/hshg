import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Actions, ofActionCanceled, ofActionDispatched, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { DistrictNodeConnection, ProvinceNodeConnection, UpdateAddressInput, UserAddressNode, WardNodeConnection } from 'src/app/shared/models/graphql.models';
import { NotificationService } from 'src/app/shared/services';
import { SubSink } from 'subsink';
import { LoadDistrict, LoadProfile, LoadProvince, LoadWard, ProfileState } from '../../shared/states';
import { DeleteAddress, DeleteAddressFailed, DeleteAddressSuccessful, SetDefaultAddress, SetDefaultAddressSuccessful, UpdateAddress, UpdateAddressSuccessful } from '../profile-address/store';

@Component({
    selector: 'cos-frontend-profile-address-detail',
    templateUrl: './profile-address-detail.component.html',
    styleUrls: ['./profile-address-detail.component.scss']
})
export class ProfileAddressDetailComponent implements OnInit {
    @Select(ProfileState.getNodeConnectionProvince) provinces$: Observable<ProvinceNodeConnection>;
    @Select(ProfileState.getNodeConnectionDistrict) districts$: Observable<DistrictNodeConnection>;
    @Select(ProfileState.getNodeConnectionWard) wards$: Observable<WardNodeConnection>;
    @Input() address: any;
    @Input() isEditing: boolean;
    @Output() changeEvent = new EventEmitter<boolean>();
    @Output() editing = new EventEmitter<boolean>();
    _subSink = new SubSink();
    isEdit = false;
    formGroup: FormGroup;
    checkCity = true;
    checkDistrict = true;
    checkAddress = true;
    checkPhone = true;
    checkReceiver = true;

    constructor(
        private _store: Store,
        private _actions: Actions,
        private _notify: NotificationService,
        private _formBuilder: FormBuilder,
        private _spinner: NgxSpinnerService
    ) { }

    ngOnInit(): void {
        this.formGroup = this._formBuilder.group({
            receiver: [this.address.receiver, Validators.required],
            street: [this.address.street, Validators.required],
            district: [this.address?.district?.id, Validators.required],
            province: [this.address?.province?.id, Validators.required],
            ward: [this.address?.ward?.id, Validators.required],
            phone: [this.address.phone, [Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
            isDefault: [false]
        })
        this.editing.emit(false);
        this._subSink.sink = this._actions.pipe(ofActionDispatched(SetDefaultAddress)).subscribe(() => this._spinner.show())
        this._subSink.sink = this._actions.pipe(ofActionDispatched(DeleteAddress)).subscribe(() => this._spinner.show())
        this._subSink.sink = this._actions.pipe(ofActionDispatched(UpdateAddress)).subscribe(() => this._spinner.show())
        this._subSink.sink = this._actions
            .pipe(ofActionSuccessful(SetDefaultAddressSuccessful))
            .subscribe(() => {
                this._store.dispatch(new LoadProfile());
                this._spinner.hide();
            });
        this._subSink.sink = this._actions
            .pipe(ofActionSuccessful(DeleteAddressSuccessful))
            .subscribe(() => {
                this._store.dispatch(new LoadProfile());
                this._spinner.hide();

                this.changeEvent.emit();
            });
        this._subSink.sink = this._actions
            .pipe(ofActionSuccessful(UpdateAddressSuccessful))
            .subscribe(() => {
                this._store.dispatch(new LoadProfile());
                this._spinner.hide();
                this.changeEvent.emit();
                this.editing.emit(false)
            });

            this._initDataProvince()
    }

    _initDataProvince(){
        this._store.dispatch(new LoadProvince())
    }

    _initDataDistrict(province){
        this._store.dispatch(new LoadDistrict({province}))
    }

    _initDataWard(district){
        this._store.dispatch(new LoadWard({district}))
    }

    changeProvince(event, province){
        if (event.source.selected) {
            this._initDataDistrict(province?.node?.id)
        }
    }

    changeDistrict(event, district){
        if (event.source.selected) {
            this._initDataWard(district?.node?.id)
        }
    }

    setDefault(id: string) {
        this._store.dispatch(new SetDefaultAddress({ addressId: id }));
    }

    deleteAddress(id: string) {
        this._store.dispatch(new DeleteAddress({ id }))
    }
    changeData() {
        let id = this.address.id;
        this.address = this.formGroup.value;
        this.address.id = id;
        this.address.isDefault = undefined
        this._store.dispatch(new UpdateAddress(this.address));
    }
    onSubmit(value: any) {
        if (this.formGroup.valid) {
            if (this.formGroup.get('isDefault').value === true) {
                this.setDefault(this.address.id);
            }
            this.changeData();
            this.formGroup.get('isDefault').setValue(false);
            this.isEdit = false;
        }
    }

}
