import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import {
    ChangePasswordInput,
    Mutation,
    MutationAuthChangePasswordArgs,
} from 'src/app/shared/models/graphql.models';
import { changePassword } from '../graphql/mutations/profile-update-mutation.service';


@Injectable({
    providedIn: 'root',
})
export class ChangePasswordService {
    constructor(private _apollo: Apollo) { }

    changePassword(input: ChangePasswordInput) {
        return this._apollo.mutate<Mutation, MutationAuthChangePasswordArgs>({
            mutation: changePassword,
            variables: {
                input: input,
            },
        });
    }
}
