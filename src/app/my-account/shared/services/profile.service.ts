import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Query, UserProfile } from 'src/app/shared/models/graphql.models';
import { getProfile } from '../graphql/queries'

@Injectable({
    providedIn: 'root',
})
export class ProfileService {
    constructor(private _apollo: Apollo) { }

    getProfile() {
        return this._apollo.query<Query, UserProfile>({
            query: getProfile,
        });
    }
}
