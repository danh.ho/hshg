import { Injectable } from '@angular/core';
import {
    ChangeProfileInput,
    Mutation,
    MutationUserChangeProfileArgs,
} from '../../../shared/models/graphql.models';
import { Apollo } from 'apollo-angular';
import { changeProfile } from '../graphql/mutations/profile-update-mutation.service';

@Injectable({
    providedIn: 'root',
})
export class ChangeProfileService {
    constructor(private _apollo: Apollo) { }

    changeProfile(input: ChangeProfileInput) {
        return this._apollo.mutate<Mutation, MutationUserChangeProfileArgs>({
            mutation: changeProfile,
            variables: {
                input: input,
            },
        });
    }
}
