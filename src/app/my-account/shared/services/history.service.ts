import { Injectable } from '@angular/core';
import {
    Mutation,
    MutationReviewForProductArgs,
    Query,
    QueryOrdersViewArgs,
} from '../../../shared/models/graphql.models';
import { Apollo } from 'apollo-angular';
import { reviewForProduct } from '../graphql/mutations/profile-update-mutation.service';
import { ordersView } from '../graphql/queries/get-order-history-query';

@Injectable({
    providedIn: 'root',
})
export class HistoryService {
    constructor(private _apollo: Apollo) { }

    ordersView(input: QueryOrdersViewArgs) {
        return this._apollo.query<Query, QueryOrdersViewArgs>({
            query: ordersView,
            variables: input,
        });
    }

    reviewForProduct(input: MutationReviewForProductArgs) {
        return this._apollo.mutate<Mutation, MutationReviewForProductArgs>({
            mutation: reviewForProduct,
            variables: input,
        });
    }
}
