import { Injectable } from '@angular/core';
import {
    AddAddressInput,
    DeleteAddressInput,
    Mutation,
    MutationUserAddressAddArgs,
    MutationUserAddressDeleteArgs,
    MutationUserAddressSetDefaultArgs,
    MutationUserAddressUpdateArgs,
    Query,
    SetDefaultAddressInput,
    UpdateAddressInput,
} from '../../../shared/models/graphql.models';
import { Apollo } from 'apollo-angular';
import {
    addAddress,
    deleteAddress,
    setDefault,
    updateAddress,
} from '../graphql/mutations/profile-update-mutation.service';
import { getAddressList } from '../graphql/queries';

@Injectable({
    providedIn: 'root',
})
export class AddressService {
    constructor(private _apollo: Apollo) { }

    getAddressList() {
        return this._apollo.query<Query>({
            query: getAddressList,
        });
    }

    addAddress(input: AddAddressInput) {
        return this._apollo.mutate<Mutation, MutationUserAddressAddArgs>({
            mutation: addAddress,
            variables: { input },
        });
    }

    updateAddress(input: UpdateAddressInput) {
        return this._apollo.mutate<Mutation, MutationUserAddressUpdateArgs>({
            mutation: updateAddress,
            variables: { input },
        });
    }

    deleteAddress(input: DeleteAddressInput) {
        return this._apollo.mutate<Mutation, MutationUserAddressDeleteArgs>({
            mutation: deleteAddress,
            variables: { input },
        });
    }

    setDefault(input: SetDefaultAddressInput) {
        return this._apollo.mutate<Mutation, MutationUserAddressSetDefaultArgs>({
            mutation: setDefault,
            variables: { input },
        });
    }
}
