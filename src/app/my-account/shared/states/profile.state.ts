import { State, Selector, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { ProfileService } from '../services/profile.service';
import {
    Loading,
    EditProfile,
    EditProfileSuccessful,
    EditProfileFailed,
    ChangePasswordSuccessful,
    ChangePasswordFailed,
    ChangePassword,
    LoadProfile,
    LoadWard,
    LoadProvince,
    LoadDistrict,
} from '../actions/profile.action';
import { ChangeProfileService, } from '../services/change-profile.service';
import { ChangePasswordService } from '../services/change-password.service';
import { ProfileStateModel, initialState } from './profile-state.model';
import { NotificationService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MyAccountService } from '../../my-account.service';

@State<ProfileStateModel>({
    name: 'profile',
    defaults: initialState,
})
@Injectable()
export class ProfileState {
    @Selector()
    static getProfileNode({ userProfile }: ProfileStateModel) {
        return userProfile;
    }
    @Selector()
    static isLoading({ loading }: ProfileStateModel): boolean {
        return loading;
    }

    @Selector()
    static getNodeConnectionProvince({ nodeConnectionProvince }: ProfileStateModel) {
        return nodeConnectionProvince;
    }

    @Selector()
    static getNodeConnectionDistrict({ nodeConnectionDistrict }: ProfileStateModel) {
        return nodeConnectionDistrict;
    }

    @Selector()
    static getNodeConnectionWard({ nodeConnectionWard }: ProfileStateModel) {
        return nodeConnectionWard;
    }


    constructor(
        private _profileService: ProfileService,
        private _changeProfileService: ChangeProfileService,
        private _changePasswordService: ChangePasswordService,
        private _notify: NotificationService,
        private router: Router,
        private _spinner: NgxSpinnerService,
        private _apiService: MyAccountService
    ) { }

    @Action(Loading)
    loading({ patchState }: StateContext<ProfileStateModel>) {
        patchState({ loading: true });
    }

    @Action(LoadProfile, { cancelUncompleted: true })
    getProfile({ patchState }: StateContext<ProfileStateModel>) {
        return this._profileService.getProfile().pipe(
            tap(({ data }) => {
                patchState({
                    userProfile: data.userProfile,
                });
            })
        );
    }

    @Action(EditProfile, { cancelUncompleted: true })
    editProfile(
        context: StateContext<ProfileStateModel>,
        { payload }: EditProfile
    ) {
        return this._changeProfileService.changeProfile(payload).pipe(
            tap(({ data }) => {
                const { errors, user } = data.userChangeProfile;
                if (errors) {
                    return context.dispatch(new EditProfileFailed(errors));
                }
                return context.dispatch(new EditProfileSuccessful(user));
            })
        );
    }

    @Action(EditProfileSuccessful)
    editProfileSuccessful(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: EditProfileSuccessful
    ): void {
        patchState({
            loading: false,
            userProfile: payload,
        });
    }

    @Action(EditProfileFailed)
    editProfileFailed(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: EditProfileFailed
    ): void {
        patchState({
            loading: false,
            errors: payload,
        });
    }

    @Action(ChangePassword, { cancelUncompleted: true })
    changePassword(
        context: StateContext<ProfileStateModel>,
        { payload }: ChangePassword
    ) {
        return this._changePasswordService.changePassword(payload).pipe(
            tap(({ data }) => {

                const { errors } = data.authChangePassword;
                if (errors) {
                    return context.dispatch(new ChangePasswordFailed(errors));
                }
                return context.dispatch(new ChangePasswordSuccessful());
            })
        );
    }

    @Action(ChangePasswordSuccessful)
    changePasswordSuccessful({
        patchState,
    }: StateContext<ProfileStateModel>): void {
        patchState({
            loading: false,
        });
        this._notify.success('', 'Tạo mật khẩu mới thành công');
        this.router.navigate(["/"])
    }

    @Action(ChangePasswordFailed)
    changePasswordFailed(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: ChangePasswordFailed
    ): void {
        patchState({
            loading: false,
            errors: payload,
        });
    }

    @Action(LoadProvince)
    loadProvince(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: LoadProvince
    ) {
        return this._apiService
            .getAllProvince({...payload})
            .pipe(
                tap(({ data }) => {
                    patchState({
                        nodeConnectionProvince: data.provinces,
                    });
                })
            );
    }

    @Action(LoadDistrict)
    loadDistrict(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: LoadDistrict
    ) {
        return this._apiService
            .getAllDistrict({...payload})
            .pipe(
                tap(({ data }) => {
                    patchState({
                        nodeConnectionDistrict: data.districts,
                    });
                })
            );
    }

    @Action(LoadWard)
    loadWard(
        { patchState }: StateContext<ProfileStateModel>,
        { payload }: LoadWard
    ) {
        return this._apiService
        .getAllWard({...payload})
        .pipe(
            tap(({ data }) => {
                patchState({
                    nodeConnectionWard: data.wards,
                });
            })
        );
    }
}
