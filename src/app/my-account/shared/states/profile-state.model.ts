import { DistrictNodeConnection, ProvinceNodeConnection, UserAddressNode, UserError, UserProfile, WardNodeConnection } from 'src/app/shared/models/graphql.models';

export interface ProfileStateModel {
    loading: boolean;
    userProfile?: UserProfile;
    errors?: UserError[];
    nodeConnectionProvince?: ProvinceNodeConnection;
    nodeConnectionDistrict?: DistrictNodeConnection;
    nodeConnectionWard?: WardNodeConnection;
    
}
export const initialState: ProfileStateModel = {
    loading: false,
};