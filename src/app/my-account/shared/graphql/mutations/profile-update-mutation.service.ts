import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationCartUpdateArgs, } from 'src/app/shared/models/graphql.models';

export const changeProfile = gql`
  mutation changeProfile($input: ChangeProfileInput) {
    userChangeProfile(input: $input) {
      status
      errors {
        code
        field
        message
      }
      user {
        avatar
        firstName
        lastName
        phone
        email
      }
    }
  }
`;

export const addAddress = gql`
  mutation addAddress($input: AddAddressInput!) {
    userAddressAdd(input: $input) {
      status
      errors {
        code
        field
        message
      }
      userAddress {
        formatAddress
        id
        isDefault
        phone
        receiver
        street
        ward {
          code
          id
          nameWard
        }
        district {
          code
          id
          nameDistrict
        }
        province {
          code
          id
          nameProvince
        }
      }
    }
  }
`;

export const updateAddress = gql`
  mutation updateAddress($input: UpdateAddressInput!) {
    userAddressUpdate(input: $input) {
      status
      errors {
        code
        field
        message
      }
      userAddress {
        formatAddress
        id
        isDefault
        phone
        receiver
        street
        ward {
          code
          id
          nameWard
        }
        district {
          code
          id
          nameDistrict
        }
        province {
          code
          id
          nameProvince
        }
      }
    }
  }
`;

export const deleteAddress = gql`
  mutation deleteAddress($input: DeleteAddressInput!) {
    userAddressDelete(input: $input) {
      status
      errors {
        code
        field
        message
      }
      userAddress {
        formatAddress
        id
        isDefault
        phone
        receiver
        street
        ward {
          code
          id
          nameWard
        }
        district {
          code
          id
          nameDistrict
        }
        province {
          code
          id
          nameProvince
        }
      }
    }
  }
`;

export const setDefault = gql`
  mutation setDefault($input: SetDefaultAddressInput) {
    userAddressSetDefault(input: $input) {
      errors {
        message
        field
        code
      }
      status
    }
  }
`;
export const changePassword = gql`
  mutation authChangePassword($input: ChangePasswordInput!) {
    authChangePassword(input: $input) {
      status
      errors {
        message
        field
      }
    }
  }
`;

export const reviewForProduct = gql`
  mutation ReviewForProductMutation($input: UserReviewInput!) {
    reviewForProduct(input: $input) {
      status
      review {
        id
        totalRate
      }
      errors {
        code
        field
        message
      }
    }
  }
`;

@Injectable({
    providedIn: 'root'
})
export class ProfileUpdateMutation extends Mutation<ParentMutation, MutationCartUpdateArgs> {
    document = changeProfile;
}

