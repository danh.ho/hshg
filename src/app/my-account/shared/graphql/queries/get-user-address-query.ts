import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { UserAddressNode } from 'src/app/shared/models/graphql.models';

const getUserProfile = gql`
    query {
        __typename
        userAddress {
            city
            phone
            street
            receiver
            id
            district
            isDefault
        }
    }`;

@Injectable({ providedIn: 'root' })
export class GetUserAddress extends Query<UserAddressNode> {
  document = getUserProfile;
}