import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QueryProvincesArgs } from 'src/app/shared/models/graphql.models';

const getAllProvince = gql`
query GetProvince(
    $before: String,
    $after: String,
    $first: Int,
    $last: Int,
){
    provinces(
        before: $before,
        after: $after,
        first: $first,
        last: $last,
    ) {
        edges {
          node {
            code
            id
            nameProvince
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
  }
}
`;

@Injectable({ providedIn: 'root' })
export class GetAllProvincesQuery extends Query<ParentQuery, QueryProvincesArgs> {
  document = getAllProvince;
}
