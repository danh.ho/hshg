import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {   Query as BaseQuery, QueryOrderDetailViewArgs } from 'src/app/shared/models/graphql.models';

const getOderById = gql`
  query GetOrderById($id: ID!) {
    orderDetailView(id: $id) {
        created
        deposit
        id
        orderNote
        orderStatus
        orderAddress {
          formatAddress
          phone
          receiver
          street
          ward {
            code
            id
            nameWard
          }
          district {
            code
            id
            nameDistrict
          }
          province {
            code
            id
            nameProvince
          }
          addressNote
        }
        totalAmount
        discount
        paymentStatus
        paymentMethod {
          name
          description
        }
        details {
          discount
          product {
            id
            name
            price
            productType
            sku
            thumbnail
            visibility
          }
          isReviewed
          id
          productName
          promotionCode
          quantity
          unitPrice
        }
        orderDiscount {
            description
            discount
            discountType
            id
            unitDiscount
        }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetOrderByIdQuery extends Query<
BaseQuery,
QueryOrderDetailViewArgs
> {
  document = getOderById;
}
