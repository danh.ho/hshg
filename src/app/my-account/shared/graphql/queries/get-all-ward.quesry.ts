import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QueryWardsArgs } from 'src/app/shared/models/graphql.models';

const getAllWard = gql`
query GetWard(
    $before: String,
    $after: String,
    $first: Int,
    $last: Int,
    $district: String
){
    wards(
        before: $before,
        after: $after,
        first: $first,
        last: $last,
        district: $district
    ) {
        edges {
          node {
            code
            id
            nameWard
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllWardsQuery extends Query<ParentQuery, QueryWardsArgs> {
  document = getAllWard;
}
