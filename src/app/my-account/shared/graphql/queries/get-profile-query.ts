import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { UserProfile } from 'src/app/shared/models/graphql.models';

export const getProfile = gql`
    query {
        __typename
        userProfile {
          address {
            formatAddress
            id
            isDefault
            phone
            receiver
            street
            ward {
              code
              id
              nameWard
            }
            district {
              code
              id
              nameDistrict
            }
            province {
              code
              id
              nameProvince
            }
          }
          avatar
          code
          email
          firstName
          id
          lastName
          phone
          bonusPoint
          rank
        }
    }`;

export const getAddressList = gql`
  query userAddress {
    userAddress {
      formatAddress
        id
        isDefault
        phone
        receiver
        street
        ward {
          code
          id
          nameWard
        }
        district {
          code
          id
          nameDistrict
        }
        province {
          code
          id
          nameProvince
        }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetProfile extends Query<UserProfile> {
    document = getProfile;
}