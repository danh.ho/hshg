import { gql } from "@apollo/client/core";

export const ordersView = gql`
query ordersView(
  $after: String
  $before: String
  $first: Int
  $orderStatus: String
  $totalAmount: Float
  $paymentMethod: ID
  $createdBy: String
  $product: String
  $orderBy: String
  $customer: ID
) {
  ordersView(
    after: $after
    before: $before
    first: $first
    orderStatus: $orderStatus
    totalAmount: $totalAmount
    paymentMethod: $paymentMethod
    createdBy: $createdBy
    product: $product
    orderBy: $orderBy
    customer:$customer
  ) {
    edges {
      cursor
      node {
        created
        deposit
        id
        orderNote
        orderStatus
        orderAddress {
          formatAddress
          phone
          receiver
          street
          ward {
            code
            id
            nameWard
          }
          district {
            code
            id
            nameDistrict
          }
          province {
            code
            id
            nameProvince
          }
          addressNote
        }
        totalAmount
        paymentStatus
        paymentMethod {
          name
          description
        }
        details {
          discount
          product {
            id
            name
            price
            productType
            sku
            thumbnail
            visibility
          }
          isReviewed
          id
          productName
          promotionCode
          quantity
          unitPrice
        }
      }
    }
    totalCount
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
  }
}
`;
