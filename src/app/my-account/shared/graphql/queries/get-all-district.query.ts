import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QueryDistrictsArgs } from 'src/app/shared/models/graphql.models';

const getAllDistrict = gql`
query GetDistrict(
    $before: String,
    $after: String,
    $first: Int,
    $last: Int,
    $province: String
){
    districts(
        before: $before,
        after: $after,
        first: $first,
        last: $last,
        province: $province
    ) {
        edges {
          node {
            code
            id
            nameDistrict
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllDistrictsQuery extends Query<ParentQuery, QueryDistrictsArgs> {
  document = getAllDistrict;
}
