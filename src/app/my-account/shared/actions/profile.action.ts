import { ChangePasswordInput, ChangeProfileInput, QueryDistrictsArgs, QueryProvincesArgs, QueryWardsArgs, UserError, UserProfile, UserReviewInput } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    LOADING = '[Profile] Loading',
    GET_PROFILE = '[Profile] Get Profile',
    LOAD_PROFILE = '[Profile] Load Profile',
    EDIT_PROFILE = '[Profile] Edit Profile',
    EDIT_PROFILE_SUCCESSFUL = '[Profile] Edit Profile Successful',
    EDIT_PROFILE_FAILED = '[Profile] Edit Profile Failed',
    CHANGE_PASSWORD = "[Profile] Change Password",
    CHANGE_PASSWORD_SUCCESSFUL = "[Profile] Change Password Successful",
    CHANGE_PASSWORD_FAILED = "[Profile] Change Password Failed",
    LOAD_ALL_PROVINCE = '[List All Provinces] List All Provinces',
    LOAD_ALL_DISTRICT = '[List All District] List All District',
    LOAD_ALL_WARD = '[List All Ward] List All Ward',
}
export class Loading {
    static readonly type = ACTIONS.LOADING;
}

export class LoadProfile {
    static readonly type = ACTIONS.LOAD_PROFILE;
    constructor(public readonly payload?: UserProfile) { }
}
export class GetProfile {
    static readonly type = ACTIONS.GET_PROFILE;
    constructor(public readonly payload?: UserProfile) { }
}

export class EditProfile {
    static readonly type = ACTIONS.EDIT_PROFILE;
    constructor(public readonly payload: ChangeProfileInput) { }
}

export class EditProfileSuccessful {
    static readonly type = ACTIONS.EDIT_PROFILE_SUCCESSFUL;
    constructor(public readonly payload: UserProfile) { }
}
export class EditProfileFailed {
    static readonly type = ACTIONS.EDIT_PROFILE_FAILED;
    constructor(public readonly payload: UserError[]) { }
}

export class ChangePassword {
    static readonly type = ACTIONS.CHANGE_PASSWORD;
    constructor(public readonly payload: ChangePasswordInput) { }
}

export class ChangePasswordSuccessful {
    static readonly type = ACTIONS.CHANGE_PASSWORD_SUCCESSFUL;
}
export class ChangePasswordFailed {
    static readonly type = ACTIONS.CHANGE_PASSWORD_FAILED;
    constructor(public readonly payload: UserError[]) { }
}

export class LoadProvince{
    static readonly type = ACTIONS.LOAD_ALL_PROVINCE;
    constructor(public readonly payload?: QueryProvincesArgs) {
    }
}

export class LoadDistrict{
    static readonly type = ACTIONS.LOAD_ALL_DISTRICT;
    constructor(public readonly payload?: QueryDistrictsArgs) {
    }
}

export class LoadWard{
    static readonly type = ACTIONS.LOAD_ALL_WARD;
    constructor(public readonly payload?: QueryWardsArgs) {
    }
}