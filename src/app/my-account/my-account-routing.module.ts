import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MyAccountComponent } from './my-account.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ProfileAddressComponent } from './components/profile-address/profile-address.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';
import { DetailOrderHistoryComponent } from './components/detail-order-history/detail-order-history.component';

const routes: Routes = [
    {
        path: '',
        component: MyAccountComponent,
        children: [
            {
                path: 'edit-profile',
                component: EditProfileComponent,
            },
            {
                path: 'profile-address',
                component: ProfileAddressComponent
            },
            {
                path: '',
                redirectTo: 'edit-profile',
                // pathMatch: 'full'
            }
        ]
    },
    {
        path: 'change-password',
        component: ChangePasswordComponent
    },
    // {
    //     path: 'order-history',
    //     component: OrderHistoryComponent
    // },
    {
        path: 'order-history/:orderDetail',
        component: DetailOrderHistoryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MyAccountRoutingModule { }
