import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyAccountComponent } from './my-account.component';
import { MyAccountRoutingModule } from './my-account-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ProfileAddressComponent } from './components/profile-address/profile-address.component';
import { ProfileAddressDetailComponent } from './components/profile-address-detail/profile-address-detail.component';
import { CreateAddressModalComponent } from './components/create-address-modal/create-address-modal.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { OrderHistoryComponent } from './components/order-history/order-history.component';
import { DetailOrderHistoryComponent } from './components/detail-order-history/detail-order-history.component';
import { SharedModule } from '../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { RankingComponent } from './components/ranking/ranking.component';



@NgModule({
    declarations: [MyAccountComponent, EditProfileComponent, ProfileAddressComponent, ProfileAddressDetailComponent, CreateAddressModalComponent, ChangePasswordComponent,
         OrderHistoryComponent, DetailOrderHistoryComponent, RankingComponent],
    imports: [
        CommonModule,
        MyAccountRoutingModule,
        FormsModule,
        MatButtonModule,
        ReactiveFormsModule,
        SharedModule,
        IonicModule
    ]
})
export class MyAccountModule { }
