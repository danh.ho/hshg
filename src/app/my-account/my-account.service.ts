import { Injectable } from '@angular/core';
import { QueryDistrictsArgs, QueryOrderDetailViewArgs, QueryProvincesArgs, QueryWardsArgs, UserAddressNode, UserProfile } from '../shared/models/graphql.models';
import { GetProfile } from './shared/graphql/queries';
import { GetAllDistrictsQuery } from './shared/graphql/queries/get-all-district.query';
import { GetAllProvincesQuery } from './shared/graphql/queries/get-all-province.query';
import { GetAllWardsQuery } from './shared/graphql/queries/get-all-ward.quesry';
import { GetOrderByIdQuery } from './shared/graphql/queries/get-order-detail-view.query';
import { GetUserAddress } from './shared/graphql/queries/get-user-address-query';

@Injectable({
    providedIn: 'root'
})
export class MyAccountService {

    constructor(
        private _getProfile: GetProfile,
        private _getUserAddress: GetUserAddress,
        private _getAllProvince: GetAllProvincesQuery,
        private _getAllDistrict: GetAllDistrictsQuery,
        private _getAllWard: GetAllWardsQuery,
        private _getOrderDetail: GetOrderByIdQuery
    ) { }

    getAllCart(args: UserProfile) {
        return this._getProfile.watch(args).valueChanges;
    }
    getUserAddress(args: UserAddressNode) {
        return this._getUserAddress.watch(args).valueChanges;
    }

    getAllProvince(args?: QueryProvincesArgs) {
        return this._getAllProvince.watch(args).valueChanges;
    }

    getAllDistrict(args?: QueryDistrictsArgs) {
        return this._getAllDistrict.watch(args).valueChanges;
    }

    getAllWard(args?: QueryWardsArgs) {
        return this._getAllWard.watch(args).valueChanges;
    }

    getOrderDetail(args?: QueryOrderDetailViewArgs) {
        return this._getOrderDetail.watch(args).valueChanges;
    }
}
