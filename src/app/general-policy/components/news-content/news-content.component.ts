import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Subscription,Observable } from 'rxjs';
import { FeedbackNode, NewsNode  } from 'src/app/shared/models/graphql.models';
import { CmsState, GetBannerList, GetFeedbacks } from 'src/app/shared/store/cms';


@Component({
    selector: 'tdl-app-news-content',
    templateUrl: './news-content.component.html',
    styleUrls: ['./news-content.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsContentComponent implements OnInit, OnChanges {
    @Select(CmsState.getFeedBacks) feedbacks$: Observable<Array<FeedbackNode>>;
    @Input() content: NewsNode;
    description;

    private _subscription = new Subscription();
    constructor(
        private _router: Router,
        private _store: Store,
        private sanitizer: DomSanitizer
    ) { }

    banners = [
        {src: 'assets/img/banner-side/1.jpg'},
        {src: 'assets/img/banner-side/2.jpg'},
        {src: 'assets/img/banner-side/3.jpg'},
    ]

    ngOnInit(): void {
        this._initData();
    }

    ngOnChanges(changes: SimpleChanges) {
        if(this.content === null) this._router.navigate(['/news'])
        this.description = this.sanitizer.bypassSecurityTrustHtml(this.content?.description);
    }
    private _initData() {
        this._store.dispatch(new GetFeedbacks);
        // this._store.dispatch([
        //     new GetFeedbacks, new GetNews,
        //     new GetProductList({ first: 4, collectionName: 'best seller' }),
        //     new GetProductList({ first: 4, collectionName: 'newest', }),
        //     new GetProductList({ first: 4, collectionName: 'combo', }),
        //     new GetBannerList()
        // ]);
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }
}
