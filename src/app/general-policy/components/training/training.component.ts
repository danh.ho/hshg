import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { NewsNode } from 'src/app/shared/models/graphql.models';

@Component({
    selector: 'tdl-app-training-side',
    templateUrl: './training.component.html',
    styleUrls: ['./training.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrainningComponent implements OnInit, OnChanges {
    @Input() items: Array<NewsNode>

    constructor(private _router: Router) { }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges) {
        if(this.items === null) this._router.navigate(['/news'])
    }

    navigateTo(id: string) {
        this._router.navigate([`news/${id}`])
    }

}
