import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { GetNewsDetailByID, CmsNewsState } from 'src/app/shared/store/tip-article';
import { Observable, Subscription } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { IonContent } from '@ionic/angular';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
    templateUrl: './news-detail.component.html',
    styleUrls: ['./news-detail.component.scss']
})
export class NewsDetailComponent implements OnInit,OnDestroy {
    @Select(CmsNewsState.getNewsNode) newsNode$: Observable<NewsNode>;
    @ViewChild(IonContent) contentPage: IonContent ;
    isShowBackToTop = false;
    cartItemsNo: number;
    private _subscription = new Subscription();
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _store: Store,
    ) { }

    ngOnInit(): void {
        this._subscription.add(this._activatedRoute.paramMap.pipe(
            map(params => params.get('id')),
            switchMap(id => this._store.dispatch(new GetNewsDetailByID({id, amountNews:4})))
        ).subscribe());
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }
    scrollToTop() {    
        this.contentPage.scrollToTop(700);
    }
    logScrolling(event){
        if(event.detail.scrollTop > 100){
          this.isShowBackToTop = true;
        } else {
          this.isShowBackToTop = false;
        }
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }

}
