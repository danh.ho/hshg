import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { CmsNewsState, GetNews } from 'src/app/shared/store/tip-article';
import { SubSink } from 'subsink';

@Component({
  selector: 'tdl-app-beauty-tips-listing',
  templateUrl: './beauty-tips-listing.component.html',
  styleUrls: ['./beauty-tips-listing.component.scss']
})
export class BeautyTipsListingComponent implements OnInit {
  items: Array<NewsNode>;
  _subsink = new SubSink()
  constructor(
      private _store: Store,
      private _router: Router
  ) { }

  ngOnInit(): void {
      this.loadTips();
  }
  loadTips(){
    this._store.dispatch([new GetNews]);
    this._subsink.sink = this._store.select(CmsNewsState.getTips)
    .subscribe(res => this.items = res);
  }
  navigateTo(id: string) {
    this._router.navigate([`beauty-tips/${id}`])
  }
}
