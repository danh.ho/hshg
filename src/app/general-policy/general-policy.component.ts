import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'tdl-app-news',
    templateUrl: './general-policy.component.html',
    styleUrls: ['./general-policy.component.scss']
})
export class GeneralPolicyComponent implements OnInit {

    constructor(
        private _activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

}
