import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookPartyComponent } from './book-party.component';

const routes: Routes = [
  {
    path: '',
    component: BookPartyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookPartyRoutingModule {}
