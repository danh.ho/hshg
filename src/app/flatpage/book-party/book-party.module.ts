import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { SharedModule } from 'src/app/shared/shared.module';
import { BookPartyComponent } from './book-party.component';
import { BookPartyRoutingModule } from './book-party-routing.module';
import { NgxMaskModule } from 'ngx-mask';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookPartyRoutingModule, 
    SharedModule,
    CarouselModule,
    NgxGalleryModule,
    NgxMaskModule
  ],
  declarations: [BookPartyComponent]
})
export class BookPartyModule {}
