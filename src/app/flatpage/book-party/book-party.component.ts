import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IonContent } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { LoadDistrict, LoadProvince, LoadWard, ProfileState } from 'src/app/my-account/shared/states';
import { ContactUsInput, DistrictNodeConnection, PartyInforCreateInput, ProvinceNodeConnection, WardNodeConnection } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { BranchState } from 'src/app/shared/store/branch/branch.state';
import { CreateBookParty, CreateBookPartyFailed, CreateBookPartySuccessfully, CreateContactForm, CreateContactFormFailed, CreateContactFormSuccessfully } from 'src/app/shared/store/flatpage';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { SubSink } from 'subsink';

@Component({
    selector: 'app-book-party',
    templateUrl: './book-party.component.html',
    styleUrls: ['./book-party.component.scss'],
})
export class BookPartyComponent implements OnInit {
    @Select(ProfileState.getNodeConnectionProvince) provinces$: Observable<ProvinceNodeConnection>;
    @Select(ProfileState.getNodeConnectionDistrict) districts$: Observable<DistrictNodeConnection>;
    @Select(ProfileState.getNodeConnectionWard) wards$: Observable<WardNodeConnection>;
    @ViewChild(IonContent) content: IonContent;
    isShowBackToTop = false
    bookPartyForm: FormGroup;
    private _subSink = new SubSink();
    private _subscription = new Subscription();
    cartItemsNo: number;
    formatAddress;
    constructor(
        private _fb: FormBuilder,
        private _snackBar: MatSnackBar,
        private _store: Store,
        private _actions: Actions,
        private _translate: TranslateService,
        private spinner: NgxSpinnerService,
    ) {
        this.bookPartyForm = this._fb.group({
            customer: ['', [Validators.required]],
            phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern("[0-9 ]*")]],
            email: ['', [Validators.email, Validators.required]],
            street: ['', [Validators.required]],
            district: ['', [Validators.required]],
            province: ['', [Validators.required]],
            ward: [''],
            invitedNumber: [''],
            createdDate: [new Date(), Validators.required],
            confirm: [false]
        })

    }

    ngOnInit(): void {
        this._init();
        this._initDataProvince()
    }

    private _init() {
        this._subscription.add(
            this._actions.pipe(
                ofActionSuccessful(CreateBookPartyFailed),
                switchMap(({ payload }: CreateBookPartyFailed) => this._translate.get(payload.code)),
                tap(translation => {
                    this.spinner.hide();
                    this._snackBar.open(translation, null, {
                        duration: 7000
                    })
                })
            ).subscribe()
        )

        this._subscription.add(
            this._actions.pipe(
                ofActionSuccessful(CreateBookPartySuccessfully),
                tap(() => {
                    this.spinner.hide();
                    this._snackBar.open((this._translate.instant('SUCCESS.party_booking_created')), null, {
                        duration: 7000
                    })
                })
            ).subscribe()
        )

        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    onSubmitForm(formDirective?: FormGroupDirective) {
        if (this.bookPartyForm.valid) {
            const { customer, phone, email, street, ward, district, province, createdDate, invitedNumber }: PartyInforCreateInput = this.bookPartyForm.value;
            const data = { customer, phone, email, street, ward, district, province, createdDate, invitedNumber } as PartyInforCreateInput
            this._store.dispatch(new CreateBookParty(data));
            this.spinner.show();
            this.bookPartyForm.reset('');
            for (let name in this.bookPartyForm.controls) {
                this.bookPartyForm[name].setError(null)
            }
        }
    }
    _initDataProvince() {
        this._store.dispatch(new LoadProvince())
    }

    _initDataDistrict(province) {
        this._store.dispatch(new LoadDistrict({ province }))
    }

    _initDataWard(district) {
        this._store.dispatch(new LoadWard({ district }))
    }

    changeProvince(event, province) {
        if (event.source.selected) {
            this._initDataDistrict(province?.node?.id)
        }
    }

    changeDistrict(event, district) {
        if (event.source.selected) {
            this._initDataWard(district?.node?.id)
        }
    }
    scrollToTop() {
        this.content.scrollToTop(700);
    }
    logScrolling(event) {
        if (event.detail.scrollTop > 100) {
            this.isShowBackToTop = true;
        } else {
            this.isShowBackToTop = false;
        }
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
}
