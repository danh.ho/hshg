import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
  selector: 'app-my-bonus',
  templateUrl: './my-bonus.component.html',
  styleUrls: ['./my-bonus.component.scss'],
})
export class MyBonusComponent implements OnInit {
  cartItemsNo: number;
  private _subscription = new Subscription();
  constructor(
    private _store: Store
  ) { }

  ngOnInit() {
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
  }));
  }

}
