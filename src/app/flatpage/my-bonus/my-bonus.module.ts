import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { SharedModule } from 'src/app/shared/shared.module';
import { MyBonusRoutingModule } from './my-bonus-routing';
import { MyBonusComponent } from './my-bonus.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyBonusRoutingModule, 
    SharedModule,
  ],
  declarations: [MyBonusComponent]
})
export class MyBonusModule {}
