import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyBonusComponent } from './my-bonus.component';

const routes: Routes = [
  {
    path: '',
    component: MyBonusComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyBonusRoutingModule {}
