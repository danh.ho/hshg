import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { FlatpageState, GetFlatpageByID, GetFlatpageList } from 'src/app/shared/store/flatpage';
import { Observable, Subscription } from 'rxjs';
import { FlatpageNode } from 'src/app/shared/models/graphql.models';
import { map, switchMap, tap } from 'rxjs/operators';
import { NAVBAR_MENU } from 'src/app/header/menu';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
@Component({
    templateUrl: './flatpage-page.component.html',
    styleUrls: ['./flatpage-page.component.scss']
})
export class FlatpagePageComponent
    implements OnInit, OnDestroy {
    @Select(FlatpageState.getFlatpageByID) flatpageContent$: Observable<FlatpageNode>;
    navbar = NAVBAR_MENU
    private _subscription = new Subscription();
    cartItemsNo: number;
    flatpage;
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _store: Store,
    ) { }

    ngOnInit(): void {
        this._subscription.add(this._activatedRoute.paramMap.pipe(
            map(paramMap => paramMap.get('id')),
            tap(id => {
                if (id) {
                    // this._store.dispatch(new GetFlatpageByID(id));
                    this._store.dispatch(new GetFlatpageList({ itemCode: id }))
                    this._subscription.add(this._store.select(FlatpageState.getFlatpageList).pipe(
                    ).subscribe(res => {
                        if (res) {
                            this.flatpage = res[0]?.node;
                        }
                    }
                    ))
                }
            })
        ).subscribe())
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    ngOnDestroy() {
        this._subscription.unsubscribe();
    }

}
