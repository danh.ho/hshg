import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NAVBAR_MENU } from 'src/app/header/menu';
import { FlatpageNode } from 'src/app/shared/models/graphql.models';

@Component({
    selector: 'cos-frontend-flatpage-content',
    templateUrl: './flatpage-content.component.html',
    styleUrls: ['./flatpage-content.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlatpageContentComponent implements OnInit, OnChanges {
    @Input() content: FlatpageNode;
    navbar = NAVBAR_MENU
    constructor() { }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(this.content)
    }


}
