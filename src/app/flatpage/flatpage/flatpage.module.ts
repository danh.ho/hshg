import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlatpageRoutingModule } from './flatpage-routing.module';
import { FlatpageComponent } from './flatpage.component';
import { FlatpagePageComponent } from './pages/flatpage-page/flatpage-page.component';
import { FlatpageContentComponent } from './components/flatpage-content/flatpage-content.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [FlatpageComponent, FlatpagePageComponent, FlatpageContentComponent],
  imports: [
    CommonModule,
    FlatpageRoutingModule,
    SharedModule,
    IonicModule
  ]
})
export class FlatpageModule { }
