import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'cos-frontend-flatpage',
    template: `<router-outlet></router-outlet>`
})
export class FlatpageComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
    }

}
