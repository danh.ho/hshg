import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContactComponent } from './contact.component';
import { ContactRoutingModule } from './contact-routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactRoutingModule, 
    SharedModule,
    CarouselModule,
    NgxGalleryModule
  ],
  declarations: [ContactComponent]
})
export class ContactModule {}
