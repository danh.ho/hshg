import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IonContent } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { ProfileState } from 'src/app/my-account/shared/states';
import { ContactUsInput } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { BranchState } from 'src/app/shared/store/branch/branch.state';
import { CreateContactForm, CreateContactFormFailed, CreateContactFormSuccessfully } from 'src/app/shared/store/flatpage';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { SubSink } from 'subsink';
@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
    @ViewChild(IonContent) content: IonContent;
    isShowBackToTop = false
    contactForm: FormGroup;
    private _subSink = new SubSink();
    cartItemsNo: number;
    private _subscription = new Subscription();
    formatAddress;
    constructor(
        private _fb: FormBuilder,
        private _snackBar: MatSnackBar,
        private _store: Store,
        private _translate: TranslateService,
        private _actions: Actions,
        private spinner: NgxSpinnerService,
    ) {
        this.contactForm = this._fb.group({
            subject: ['', [Validators.required]],
            firstName: ['', [Validators.required]],
            phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
            email: ['', [Validators.email, Validators.required]],
            message: ['', [Validators.required]],
        })

    }

    ngOnInit(): void {
        this._init();
        this._subscription.add(
            this._store
                .select(BranchState.getSelectedNode)
                .subscribe((data) => {
                    if (data) {
                        this.formatAddress = data.formatAddress
                    }
                })
        );
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    private _init() {
        this._subscription.add(
            this._actions.pipe(
                ofActionSuccessful(CreateContactFormFailed),
                switchMap(({ payload }: CreateContactFormFailed) => this._translate.get(payload.code)),
                tap(translation => this._snackBar.open(translation, null, {
                    duration: 7000
                }))
            ).subscribe()
        )

        this._subscription.add(
            this._actions.pipe(
                ofActionSuccessful(CreateContactFormSuccessfully),
                tap(() => {
                    this.spinner.hide();
                    this._snackBar.open((this._translate.instant('SUCCESS.contact_us_created')), null, {
                        duration: 7000
                    })
                })
            ).subscribe()
        )

    }

    onSubmitForm(formDirective?: FormGroupDirective) {
        if (this.contactForm.valid) {
            const { firstName, message, subject, phone, email }: ContactUsInput = this.contactForm.value;
            const data = { firstName, message, subject, phone, email } as ContactUsInput;
            this._store.dispatch(new CreateContactForm(data));
            this.spinner.show();
            this.contactForm.reset('');
            for (let name in this.contactForm.controls) {
                this.contactForm[name].setError(null)
            }
        }
    }
    scrollToTop() {
        this.content.scrollToTop(700);
    }
    logScrolling(event) {
        if (event.detail.scrollTop > 100) {
            this.isShowBackToTop = true;
        } else {
            this.isShowBackToTop = false;
        }
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
}
