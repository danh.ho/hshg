import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { NAVBAR_MENU } from 'src/app/header/menu';
import { CmsNewsState, GetNewsDetailByID } from 'src/app/shared/store/tip-article';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss'],
})
export class BlogDetailComponent implements OnInit {
  navbar = NAVBAR_MENU;
  isShow = true;
  blog;
  private _subscription = new Subscription();
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _store: Store,
  ) {

  }

  ngOnInit() {
    this._subscription.add(this._activatedRoute.paramMap.pipe(
      map(params => params.get('id')),
      switchMap(id => this._store.dispatch(new GetNewsDetailByID({id, amountNews:4})))
    ).subscribe());
    this._subscription.add(this._store.select(CmsNewsState.getNewsNode).subscribe(res => {
      if (res) {
        this.blog = res
      }
    }
    ))
  }

  show() {
    this.isShow = !this.isShow;
  }
}
