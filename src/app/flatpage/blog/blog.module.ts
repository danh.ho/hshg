import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogRoutingModule } from './blog-routing.module';

import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { SharedModule } from 'src/app/shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { BlogComponent } from './blog.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
@NgModule({
    declarations: [BlogComponent, BlogDetailComponent],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        BlogRoutingModule,
        SharedModule,
        NgxGalleryModule,
    ]
})
export class BlogModule { }
