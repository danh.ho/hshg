import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { NAVBAR_MENU } from 'src/app/header/menu';
import { CmsNewsState, GetNews } from 'src/app/shared/store/tip-article';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
})
export class BlogComponent implements OnInit {
  navbar = NAVBAR_MENU;
  dataBlog;
  private _subscription = new Subscription();
  constructor(
    private _store: Store,
  ) { 

  }

  ngOnInit() {
    this._store.dispatch([new GetNews])
    this._subscription.add(this._store.select(CmsNewsState.getNews)
    .subscribe(res => {
      this.dataBlog = res}));
  }

}
