import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from 'src/app/shared/store/auth';
import { CmsState, GetGroupBanner } from 'src/app/shared/store/cms';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
})
export class AboutUsComponent implements OnInit {
  cartItemsNo: number;
  private _subscription = new Subscription();
  bannerList;
  constructor(
    private _store: Store
  ) { }

  ngOnInit() {
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
    }));
    // this._store.dispatch(new GetGroupBanner());
    this._subscription.add(this._store.select(CmsState.getBannerGroup)
    .subscribe(res => {
      if (res) {
        const main = res.filter(ele => ele.itemCode == 'about-us')
        this.bannerList = main[0]?.banner.edges;
      }
    }
    ));
  }

}
