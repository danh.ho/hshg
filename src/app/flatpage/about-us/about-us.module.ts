import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { AboutUsRoutingModule } from './about-us-routing.module';
import { AboutUsComponent } from './about-us.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AboutUsRoutingModule, 
    SharedModule,
  ],
  declarations: [AboutUsComponent]
})
export class AboutUsModule {}
