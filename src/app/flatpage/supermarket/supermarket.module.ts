import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { SupermarketComponent } from './supermarket.component';
import { SupermarketRoutingModule } from './supermarket-routing';
import { SharedModule } from 'src/app/shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SupermarketRoutingModule, 
    SharedModule,
    CarouselModule,
    NgxGalleryModule
  ],
  declarations: [SupermarketComponent]
})
export class SupermarketModule {}
