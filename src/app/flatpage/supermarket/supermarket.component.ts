import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from 'src/app/shared/store/auth';
import { LoadBranch, LoadProvinceBranch } from 'src/app/shared/store/branch/branch.action';
import { BranchState } from 'src/app/shared/store/branch/branch.state';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
  selector: 'app-supermarket',
  templateUrl: './supermarket.component.html',
  styleUrls: ['./supermarket.component.scss'],
})
export class SupermarketComponent implements OnInit {
  private _subscription = new Subscription();
  cartItemsNo: number;
  province = [];
  branch = [];
  mapLink ;
  constructor(
    private _store: Store,
  ) { }

  ngOnInit() {
    this.loadProvinceBranch();
    this.loadBranch();
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
  }));
  }
  loadProvinceBranch(){
    this._store.dispatch(new LoadProvinceBranch())
    this._subscription.add(this._store.select(BranchState.getProvinceNodeConnection).subscribe(
      (res) => {
        if (res)
          this.province = res?.edges
      }
    ))
  }
  loadBranch(){
    this._store.dispatch(new LoadBranch())
    this._subscription.add(this._store.select(BranchState.getNodeConnection).subscribe(
      (res) => {
        if (res) {
          this.branch = res?.edges;
          const provinceFirst = this.province[0]?.node?.province?.id
          for(let i of res?.edges){
            if(i?.node?.province?.id == provinceFirst){
              return this.mapLink = i?.node?.mapsLink
            }
          }
        }
          
      }
    ))
  }
  selectMap(branch){
    this.mapLink = branch?.mapsLink
  }
}
