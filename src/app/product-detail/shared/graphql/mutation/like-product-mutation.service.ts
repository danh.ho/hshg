import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationProductLikeActionArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation productLikeAction($input: ProductVariantLikeInput!) {
    productLikeAction(input: $input) {
        status
        errors {
            message
            field
        }
        product {
            likeCount
        }
        variant {
            likeCount
        }
    }
}
`

@Injectable({
    providedIn: 'root'
})
export class LikeProductMutation extends Mutation<ParentMutation, MutationProductLikeActionArgs> {
    document = MUTATION;
}
