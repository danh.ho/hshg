import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery, QueryProductToppingsArgs,
} from '../../../../shared/models/graphql.models';

const getProductByIDForTopping = gql`
  query GetProductByIDForTopping(    
    $before: String
    $after: String  
    $first: Int
    $last: Int
    $productVariant: ProductVariantInput
    ) {
    productToppings(
        before: $before
        after: $after
        first: $first
        last: $last 
        productVariant: $productVariant
    ) {
    totalCount
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
        node {
            id
            price
            isMandatory
            isDefault
            topping {
                id
                name 
                price
                unit {
                  unit {
                    name
                    id
                  }
                  id
                  isMain
                }
                category {
                  id 
                  name
                }
                parent {
                  id
                  name
                  
                }
            }

        }
      }
      toppingGroup {
        id
        name
        isToppingGroupMandatory
      }
    }

  }
`;
@Injectable({ providedIn: 'root' })
export class  GetByIdForToppingQuery extends Query<
  ParentQuery,
  QueryProductToppingsArgs
> {
  document = getProductByIDForTopping;
}
