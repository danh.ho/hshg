import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
  QueryProductForBuyDetailArgs,
} from '../../../../shared/models/graphql.models';
import { productForBuyFragment } from 'src/app/category/shared/graphql/fragments';


const getProductForBuy = gql`
  ${productForBuyFragment}
  query GetProductForBuy($id: ID!) {
    productForBuyDetail(id: $id) {
      ...ProductForBuyFragment
      relatedProduct{
        price
        advertisingPrice
        thumbnail
        name
        video
        created
        description
        id
      }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetByIdForBuyQuery extends Query<
  ParentQuery,
  QueryProductForBuyDetailArgs
> {
  document = getProductForBuy;
}
