import { ProductVariantLikeInput, QueryProductForBuyDetailArgs, QueryProductToppingsArgs } from 'src/app/shared/models/graphql.models';

const enum BuyActions {
    LOAD_PRODUCT_BY_ID = '[Product.BUY] Load Product by ID',
    LOAD_PRODUCT_BY_ID_FOR_TOPPING = '[Product.BUY] Load Product by ID For Topping',
    LIKE_PROUDCT = '[Product.BUY] Like Product',
    LIKE_PROUDCT_SUCCESSFULLY = '[Product.BUY] Like Product Successfully',
}

export class LoadProductByIdForBuy {
    static readonly type = BuyActions.LOAD_PRODUCT_BY_ID;
    constructor(public readonly payload: QueryProductForBuyDetailArgs) {}
}
export class LoadProductByIdForTopping {
    static readonly type = BuyActions.LOAD_PRODUCT_BY_ID_FOR_TOPPING;
    constructor(public readonly payload: QueryProductToppingsArgs) { }
}
export class LikeProduct {
    static readonly type = BuyActions.LIKE_PROUDCT;
    constructor(public readonly payload: ProductVariantLikeInput) { }
}
export class LikeProductSuccessfully {
    static readonly type = BuyActions.LIKE_PROUDCT_SUCCESSFULLY;
    constructor(public readonly payload: any) {}
}