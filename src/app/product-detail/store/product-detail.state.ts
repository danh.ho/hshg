import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { ProductForBuyStateModel, initialState } from './product-detail-state.model';
import {
  LikeProduct,
  LikeProductSuccessfully,
  LoadProductByIdForBuy, LoadProductByIdForTopping,
} from './product-detail.actions';

import { ProductDetailService } from '../product-detail.service';
import { ProductForBuyNode } from 'src/app/shared/models/graphql.models';
import { parseItem } from 'src/app/shared/utils';

@Injectable({ providedIn: 'root' })
@State<ProductForBuyStateModel>({
  name: 'productDetailBuy',
  defaults: initialState,
})
export class ProductDetailForBuyState {
  @Selector()
  static getNodeConnection({ nodeConnection }: ProductForBuyStateModel) {
    return nodeConnection;
  }
  @Selector()
  static getSelectedNode({ selectedNode }: ProductForBuyStateModel) {
    return selectedNode;
  }
  @Selector()
  static getProductTopping({ productToppingNode }: ProductForBuyStateModel) {
    return productToppingNode;
  }
  constructor(private _apiService: ProductDetailService) {}

  @Action(LoadProductByIdForBuy, { cancelUncompleted: true })
  loadProductById(
    { patchState }: StateContext<ProductForBuyStateModel>,
    { payload }: LoadProductByIdForBuy
  ) {
    return this._apiService.getByIdForBuy(payload).pipe(
      tap(({ data }) => {
        patchState({
          selectedNode: data.productForBuyDetail,
        });
      })
    );
  }
  @Action(LoadProductByIdForTopping,)
  loadProductByIdForTopping(
    { patchState }: StateContext<ProductForBuyStateModel>,
    { payload }: LoadProductByIdForTopping
  ) {
    return this._apiService.getByIdForTopping(payload).pipe(
      tap(({ data }) => {
        const product = data.productToppings;
        patchState({
          productToppingNode: product,
        });
      })
    );
  }
  @Action(LikeProduct)
  likeProduct(
    { patchState, dispatch }: StateContext<ProductForBuyStateModel>,
    { payload }: LikeProduct
  ) {
    return this._apiService.likeProduct({ input: payload}).pipe(
      tap(res => {
        const { status, product } = res;
        if (status) {
          dispatch(new LikeProductSuccessfully(res))
        } else {
        }
      })
    );
  }
}