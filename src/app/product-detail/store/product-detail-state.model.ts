import { ProductForBuyNode, ProductForBuyNodeConnection, ProductLikeAction, ProductToppingNodeConnection } from 'src/app/shared/models/graphql.models';

  export interface ProductForBuyStateModel {
    nodeConnection?: ProductForBuyNodeConnection;
    productToppingNode?: ProductToppingNodeConnection;
    selectedNode?: ProductForBuyNode;
    likeAction?: ProductLikeAction
  }
  
  export const initialState: ProductForBuyStateModel = {};