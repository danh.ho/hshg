import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MutationProductLikeActionArgs, QueryProductForBuyDetailArgs, QueryProductToppingsArgs } from '../shared/models/graphql.models';
import { LikeProductMutation } from './shared/graphql/mutation/like-product-mutation.service';
import { GetByIdForToppingQuery } from './shared/graphql/queries/get-by-id-for-topping.query';
import { GetByIdForBuyQuery } from './shared/graphql/queries/get-by-id.query';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailService {

  constructor(
    private _getForToppingById: GetByIdForToppingQuery,
    private _getForBuyById: GetByIdForBuyQuery,
    private _likeProduct: LikeProductMutation,
  ) { }

  getByIdForBuy(args: QueryProductForBuyDetailArgs) {
    return this._getForBuyById.watch(args).valueChanges;
  }
  getByIdForTopping(args: QueryProductToppingsArgs) {
    return this._getForToppingById.watch(args).valueChanges;
  }
  likeProduct(args: MutationProductLikeActionArgs) {
    return this._likeProduct.mutate(args).pipe(
        map(res => res.data.productLikeAction)
    )
}
}
