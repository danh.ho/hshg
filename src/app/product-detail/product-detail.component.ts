import { ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IonContent, ModalController } from '@ionic/angular';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryImageSize, NgxGalleryOptions } from '@kolkov/ngx-gallery';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ProductForBuyState } from '../category/store';
import { DialogLoginComponent } from '../shared/components/dialog-login/dialog-login.component';
import { ConfigService } from '../shared/config/config.service';
import { ProductForBuyNode } from '../shared/models/graphql.models';
import { NotificationService } from '../shared/services';
import { AuthState } from '../shared/store/auth';
import { AddItemToGuestBag, AddItemToMemberShoppingCart, AddItemToMemberShoppingCartSuccessfully, GetMemberShoppingCart, GetMemberShoppingCartId, ShoppingCartState, UpdateItemToGuestBag, UpdateItemToMemberShoppingCart } from '../shared/store/shopping-cart';
import { LikeProduct, LikeProductSuccessfully, LoadProductByIdForBuy, LoadProductByIdForTopping, ProductDetailForBuyState } from './store';
import { VideoProductDetailComponent } from '../shared/components/video-product-detail/video-product-detail.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  @Select(ProductForBuyState.getNodeConnection) productRelatedList$: Observable<Array<ProductForBuyNode>>;
  @ViewChild(IonContent) content: IonContent;
  galleryOptions: NgxGalleryOptions[];
  galleryOptionsSm: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  private _subscription = new Subscription();
  private _isLogin: boolean;
  menu;
  public dataProduct;
  productID;
  productRelated = [];
  topping = [];
  form: FormGroup;
  quantity = 1;
  isShowBackToTop = false;
  totalQuantityTopping = 0;
  readMore = false;
  cartItemsNo: number;
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 5000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 2
      },
      425: {
        items: 2
      },
      768: {
        items: 3
      },
      1024: {
        items: 4
      }
    },
    nav: false,
    autoWidth: true
  }
  cartGuest = []
  cartProduct;
  cartMember = [];
  cartId;
  likeCount;
  updateCartProduct = false;
  updateQuantity = false;
  constructor(
    private activatedRoute: ActivatedRoute,
    private _store: Store,
    private cdf: ChangeDetectorRef,
    private _fb: FormBuilder,
    public modalController: ModalController,
    private _notify: NotificationService,
    private router: Router,
    private config: ConfigService,
    private _actions: Actions,
    public dialog: MatDialog,
  ) {
    this.form = this._fb.group({
      topping: this._fb.array([]),
      note: [],
      quantity: [],
    });

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((queryParams:any) => {
      this.updateCartProduct = queryParams?.action;
      this.updateQuantity = false;
    });
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        tap(isLogin => this._isLogin = isLogin)
      ).subscribe(() => {

      })
    );
    this._subscription.add(this.activatedRoute.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      tap(id => {
        if (id) {
          this._store.dispatch(new LoadProductByIdForBuy({ id }));
          this._store.dispatch(new LoadProductByIdForTopping({ productVariant: { productID: id } }));
          if (!this._isLogin) {
            this._subscription.add(this._store.select(ShoppingCartState.getGuestCart).pipe()
              .subscribe(cart => {
                if (cart) {
                  this.cartGuest = cart;
                  if(this.updateCartProduct){
                    for (let item of cart) {
                      if (item?.product?.id == id) {
                        this.cartProduct = item;
                        this.form.patchValue({
                          note: item?.orderDetailNote,
                        })
                        if(!this.updateQuantity){
                          this.quantity = item?.quantity;
                        }
                        this.updateQuantity = true;
                        return;
                      }
                    }
                  } else {
                    this.cartProduct = null;
                    this.form.patchValue({
                      note: null
                    })
                    if(!this.updateQuantity){
                      this.quantity = 1;
                    }
                    this.updateQuantity = true;
                  }
                  // const arrRelatedProductFilter = []
                  // for (let i of this.productRelated.map(item => item.id)) {
                  //   if (!this.cartGuest.map(item => item?.product?.id).includes(i)) {
                  //     arrRelatedProductFilter.push(i)
                  //   }
                  // }
                  // this.productRelated = this.productRelated.filter(ele => arrRelatedProductFilter.includes(ele?.id))
                }
              }))
          } else {
            this._subscription.add(this._store.select(ShoppingCartState.getMemberCart).pipe()
              .subscribe(cart => {
                if (cart) {
                  this.cartMember = cart;
                  // const arrRelatedProductFilter = []
                  // for (let i of this.productRelated.map(item => item.id)) {
                  //   if (!this.cartMember.map(item => item?.product?.id).includes(i)) {
                  //     arrRelatedProductFilter.push(i)
                  //   }
                  // }
                  // this.productRelated = this.productRelated.filter(ele => arrRelatedProductFilter.includes(ele?.id))
                  if(this.updateCartProduct){
                    for (let item of cart) {
                      if (item?.product?.id == id) {
                        this.cartProduct = item;
                        this.form.patchValue({
                          note: item?.orderDetailNote,
                        })
                        if(!this.updateQuantity){
                          this.quantity = item?.quantity;
                        }
                        this.updateQuantity = true;
                        return;
                      }
                    }
                  } else {
                    this.cartProduct = null;
                    this.form.patchValue({
                      note: null
                    })
                    if(!this.updateQuantity){
                      this.quantity = 1;
                    }
                    this.updateQuantity = true;
                  }
                }
              }))
          }
        }
      })
    ).subscribe())
    this._subscription.add(this._actions
        .pipe(
          ofActionSuccessful(AddItemToMemberShoppingCartSuccessfully),
        )
        .subscribe((res) => {
          this._store.dispatch(new GetMemberShoppingCart)
          // this.likeCount = res?.payload?.product?.likeCount
        })
    );
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
  }));
    this.loadImageForSlider()
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this._initUpdateModeForBuy(this.productID)
    }, 300);
  }

  scrollToTop() {
    this.content.scrollToTop(700);
  }
  logScrolling1(event) {
    if (event.detail.scrollTop > 100) {
      this.isShowBackToTop = true;
    } else {
      this.isShowBackToTop = false;
    }
  }
  private loadImageForSlider() {
    this.galleryOptions = [
      {
        width: '500px',
        height: '580px',
        thumbnailsColumns: 6,
        arrowPrevIcon: 'fa fa-chevron-left',
        arrowNextIcon: 'fa fa-chevron-right',
        imageAnimation: NgxGalleryAnimation.Slide,
        previewCloseOnEsc: true,
        previewCloseOnClick: true,
        imagePercent: 100,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 5,
        closeIcon: ''
      },
      {
        breakpoint: 768,
        imageSize: NgxGalleryImageSize.Contain,

      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.galleryOptionsSm = [
      {
        width: '100%',
        height: '100%',
        thumbnailsColumns: 4,
        arrowPrevIcon: 'fa fa-chevron-left',
        arrowNextIcon: 'fa fa-chevron-right',
        previewCloseOnEsc: true,
        previewCloseOnClick: true,
        imageAnimation: NgxGalleryAnimation.Fade
      },
      {
        breakpoint: 768,
        height: '380px',
        imagePercent: 100,
        imageSize: NgxGalleryImageSize.Contain,
        thumbnailsPercent: 25,
        thumbnailsMargin: 15,
        thumbnailMargin: 5,
      },
      {
        breakpoint: 400,
        preview: false
      }
    ];
    this.galleryImages;
    this.dataProduct = []
    this.productID = +this.activatedRoute.snapshot.paramMap.get('id');
  }

  private _initUpdateModeForBuy(id: string) {
    this._subscription.add(this._store
      .select(ProductDetailForBuyState.getSelectedNode)
      .subscribe((product) => {
        if (product !== undefined) {
          const category = product.category.product.edges;
          this.loadProductRealed(category);
          this.dataProduct = product;
          this.likeCount = this.dataProduct?.likeCount;
          if (product?.descriptions) {
            this.getDataDescription(product.descriptions)
          }
          this.cdf.detectChanges()
        }
        if (product !== undefined && product.productImage.length > 0) {
          this.galleryImages = []
          for (var i = 0; i < product.productImage.length; i++) {
            let tempImage = {
              small: product.productImage[i].image,
              medium: product.productImage[i].image,
              big: product.productImage[i].image
            }
            this.galleryImages.push(tempImage);

          }
          this.cdf.detectChanges()
        }

        if (product?.relatedProduct && product.relatedProduct.length > 0) {
          setTimeout(() => {
            this.productRelated = product.relatedProduct
            // const arrRelatedProductFilter = []
            // for (let i of this.productRelated.map(item => item.id)) {
            //   if(this._isLogin){
            //     if (!this.cartMember.map(item => item?.product?.id).includes(i)) {
            //       arrRelatedProductFilter.push(i)
            //     }
            //   } else {
            //     if (!this.cartGuest.map(item => item?.product?.id).includes(i)) {
            //       arrRelatedProductFilter.push(i)
            //     }
            //   }
            // }
            // this.productRelated = this.productRelated.filter(ele => arrRelatedProductFilter.includes(ele?.id))
          }, 2000);
        }
      }));
    this._subscription.add(this._store
      .select(ProductDetailForBuyState.getProductTopping)
      .subscribe((topping) => {
        if (topping) {
          this.topping = topping.edges
          this.form.removeControl('topping');
          this.form.addControl('topping', this._fb.array([]));
          this.topping?.forEach((ele) => {
            if (this._isLogin) {
              if (this.cartProduct && this.cartProduct?.subCartDetail?.length > 0) {
                for (let item of this.cartProduct?.subCartDetail) {
                  if (item?.product?.id == ele.node?.topping?.id) {
                    this.formArray.push(
                      this._fb.group({
                        toppingID: ele.node?.topping?.id,
                        quantity: item?.quantity,
                      })
                    );
                    return;
                  }
                }
                this.formArray.push(
                  this._fb.group({
                    toppingID: ele.node?.topping?.id,
                    quantity: 0,
                  })
                );
              } else {
                this.formArray.push(
                  this._fb.group({
                    toppingID: ele.node?.topping?.id,
                    quantity: 0,
                  })
                );
              }
            } else {
              if (this.cartProduct && this.cartProduct?.subCartDetail?.length > 0) {
                for (let item of this.cartProduct?.subCartDetail) {
                  if (item?.toppingID == ele.node?.topping?.id) {
                    this.formArray.push(
                      this._fb.group({
                        toppingID: ele.node?.topping?.id,
                        quantity: item?.quantity,
                        product: ele.node
                      })
                    );
                    return;
                  }
                }
                this.formArray.push(
                  this._fb.group({
                    toppingID: ele.node?.topping?.id,
                    quantity: 0,
                    product: ele.node
                  })
                );
              } else {
                this.formArray.push(
                  this._fb.group({
                    toppingID: ele.node?.topping?.id,
                    quantity: 0,
                    product: ele.node
                  })
                );
              }
            }
          });
          this.totalQuantityTopping = 0;
          this.formArray.value.forEach(ele => {
            this.totalQuantityTopping += ele.quantity
          })
        }
      }));

  }

  loadProductRealed(category) {
    if (category) {
      const id = this.activatedRoute.snapshot.paramMap.get('id');
      const productRelated = category.filter(ele => ele.node.id != id)

      setTimeout(() => {
        // this.productRelated = productRelated
      }, 1000);
    }

  }

  getDataDescription(data) {
    if (data.length > 0) {
      this.menu = []
      for (let i = 0; i < data.length; i++) {
        const tempDescription = {
          name: data[i].name,
          fragment: 'description' + (i + 1),
          content: data[i].description
        }
        this.menu.push(tempDescription)
      }

    }
  }

  addToCart(item: ProductForBuyNode) {
    const topping = []
    const formValue = this.formArray.value
    for (let value of formValue) {
      if (value.quantity !== 0) {
        topping.push(value)
      }
    }
    const note = this.form.controls.note.value;
    if (this._isLogin) {
      this._store.dispatch(new AddItemToMemberShoppingCart({ product: item, quantity: this.quantity, topping, orderDetailNote: note }))
      this._store.dispatch(new GetMemberShoppingCart)
    } else {
      this.cartGuest?.forEach(ele => {
        ele?.subCartDetail?.forEach(e => {
          if(topping?.map(item => item?.toppingID).includes(e?.toppingID)){
            const quantityToppingAdd = topping.find(e1 => e1.toppingID == e?.toppingID)?.quantity
            const addTopping = {
              toppingID : e.toppingID,
              product: e.product,
              quantity: e.quantity + quantityToppingAdd
            }
            const indexToppingRemove = topping.findIndex(e2 => e2.toppingID == e?.toppingID)
            topping.splice(indexToppingRemove,1,addTopping)
          } else {
            const addTopping = {
              toppingID : e.toppingID,
              product: e.product,
              quantity: e.quantity
            }
            topping.push(addTopping)
          }
        });

      });
      this._store.dispatch(new AddItemToGuestBag({ product: item, quantity: this.quantity, topping, orderDetailNote: note }));
    }
  }
  updateToCart(item: ProductForBuyNode) {
    const topping = []
    const formValue = this.formArray.value
    for (let value of formValue) {
      if (value.quantity !== 0) {
        topping.push(value)
      }
    }
    const note = this.form.controls.note.value;
    if (this._isLogin) {
      this._store.dispatch(new UpdateItemToMemberShoppingCart({ cartDetailId: this.cartProduct?.id, quantity: this.quantity, topping, note }))
      this._store.dispatch(new GetMemberShoppingCart)
    } else {
      this._store.dispatch(new UpdateItemToGuestBag({ product: item, quantity: this.quantity, topping, orderDetailNote: note }));
    }
  }
  //   addToCart(product: ProductForBuyNode, quantity = this.quantity) {
  //     const isSellOutOfStock = this.selectedVariant ? this.selectedVariant?.isSellOutOfStock : product?.isSellOutOfStock
  //     if(isSellOutOfStock){
  //         if (this._isLogin) {
  //             this._store.dispatch(new AddItemToMemberShoppingCart({ product, variant:this.selectedVariant, quantity }))
  //         } else {
  //             this._store.dispatch(new AddItemToGuestBag({ product, variant:this.selectedVariant,quantity }))
  //         }
  //     }else {
  //         const quantityCart =this.selectedVariant ? this.cart?.find(ele => ele?.variant?.id == this.selectedVariant?.id)?.quantity : this.cart?.find(ele => ele?.product?.id == product.id)?.quantity 
  //         if(!quantityCart){
  //             if(this.quantity > this.totalQuantityVariant){
  //                 this._snackBar.open('Sản phẩm chỉ còn ' + this.totalQuantityVariant + ' sản phẩm', null, {
  //                     duration: 5000
  //                 });
  //             }else {
  //                 if (this._isLogin) {
  //                     this._store.dispatch(new AddItemToMemberShoppingCart({ product, variant:this.selectedVariant, quantity }))
  //                 } else {
  //                     this._store.dispatch(new AddItemToGuestBag({ product, variant:this.selectedVariant,quantity }))

  //                 }
  //             }
  //         } else {
  //             if((this.quantity + quantityCart) > this.totalQuantityVariant){
  //                 this._snackBar.open('Sản phẩm chỉ còn ' + this.totalQuantityVariant + ' sản phẩm. Vui lòng kiểm tra giỏ hàng', null, {
  //                     duration: 5000
  //                 });
  //             }else {
  //                 if (this._isLogin) {
  //                     this._store.dispatch(new AddItemToMemberShoppingCart({ product, variant:this.selectedVariant, quantity }))
  //                 } else {
  //                     this._store.dispatch(new AddItemToGuestBag({ product, variant:this.selectedVariant,quantity }))

  //                 }
  //             }
  //         }
  //     }
  // }
  get formArray() {
    return this.form.controls.topping as FormArray;
  }
  changeQuantityTopping(boolean, index) {
    // this.totalQuantityTopping = 0;
    // this.formArray.value.forEach(ele => {
    //   this.totalQuantityTopping += ele.quantity
    // })
    // if(boolean){
    //   if((this.totalQuantityTopping < (this.quantity))){
    //     this.formArray.at(index).patchValue({
    //       quantity: this.formArray.at(index).get('quantity').value+1
    //     })
    //   } else {
    //     this._notify.error('','Số lượng sản phẩm phải lớn hơn số lượng chế biến')
    //   }
    // } else {
    //   if(this.formArray.at(index).get('quantity').value - 1 >= 0){
    //     this.formArray.at(index).patchValue({
    //       quantity: this.formArray.at(index).get('quantity').value-1
    //     })
    //   }
    // }
    if (boolean) {
      this.formArray.at(index).patchValue({
        quantity: this.formArray.at(index).get('quantity').value + 1
      })
    } else {
      if (this.formArray.at(index).get('quantity').value - 1 >= 0) {
        this.formArray.at(index).patchValue({
          quantity: this.formArray.at(index).get('quantity').value - 1
        })
      }
    }
    this.totalQuantityTopping = 0;
    this.formArray.value.forEach(ele => {
      this.totalQuantityTopping += ele.quantity
    })
  }
  changeQuantityProduct(isIncrease) {
    this.totalQuantityTopping = 0;
    this.formArray.value.forEach(ele => {
      this.totalQuantityTopping += ele.quantity
    })
    if (isIncrease) {
      this.quantity += 1;
    } else {
      if (this.quantity == 1) {
        this.quantity == 1
      } else {
        if (this.totalQuantityTopping > (this.quantity - 1)) {
          this._notify.error('', 'Số lượng sản phẩm phải lớn hơn số lượng chế biến')
        } else {
          this.quantity -= 1
        }
      }
    }
  }

  addToCartRelated(item: ProductForBuyNode) {
    // let params = {
    //   product: item.id,
    //   quantity: 1
    // }
    const quantity = 1;
    if (this._isLogin) {
      //this.addToCartLogin(params);
      this._store.dispatch(new AddItemToMemberShoppingCart({ product: item, quantity }))
      // this._store.dispatch(new GetMemberShoppingCart)
    } else {
      this._store.dispatch(new AddItemToGuestBag({ product: item }));
    }
  }

  href(social) {
    if (social == 'facebook') {
      // const url = `https://www.facebook.com/sharer/sharer.php?u=${this.config.getConfig().URL}/product-detail/${this.productID}`
      const url = `https://www.facebook.com/sharer/sharer.php?u=https://hshg.nng.bz/product-detail/${this.productID}`
      window.open(url, "_blank")
    } else if (social == 'google') {
      const url = `https://plus.google.com/share?url=https://dev.hshg.nng.bz/product-detail/${this.productID}`
      window.open(url, "_blank")
    }
  }
  likeProduct() {
    if (!this._isLogin) {
      this.openDialog()
    } else {
      this._store.dispatch(new LikeProduct({ productId: this.productID }));
      this._subscription.add(
        this._actions
          .pipe(
            ofActionSuccessful(LikeProductSuccessfully),
          )
          .subscribe((res) => {
            this.likeCount = res?.payload?.product?.likeCount
          })
      );
    }

  }
  async openDialog() {
    const modal = await this.modalController.create({
      component: DialogLoginComponent,
      cssClass: 'dialog-login',
      componentProps: {
        title: 'sản phẩm'
      }
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.dismissed == true) {
      this.router.navigate(['/auth/login'])
    }
  }
  showMore() {
    this.readMore = !this.readMore
  }
  popupVideo(item){
    const dialogRef = this.dialog.open(VideoProductDetailComponent, {
      width: '900px',
      height: 'auto',
      panelClass: 'dialog-video',
      data: item
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result){

      }
    });
  }
  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
