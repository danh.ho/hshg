import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthState } from '../shared/store/auth';
import { ShoppingCartState } from '../shared/store/shopping-cart';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  private _subscription = new Subscription();
  cartItemsNo: number;
  constructor(
    private _store: Store,

  ) { }

  ngOnInit() {
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
      }));
  }

}
