import { NgModule } from '@angular/core';
import { environment } from 'src/environments/environment';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, DefaultOptions } from '@apollo/client/core';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from 'apollo-link-context';
import { Store } from '@ngxs/store';
import { AuthState } from 'src/app/shared/store/auth';


const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
  mutate: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

// const getJoinedLink = (store: Store) =>
//   ApolloLink.from([
//     // createUploadLink is obliged to upload file
//     createUploadLink({ uri: environment.API_URL }) as any,
//   ]);

  const getJoinedLink = (store: Store) =>
  ApolloLink.from([
    setContext(() => {
      const token = store.selectSnapshot(AuthState.getToken);
      const branchId = localStorage.getItem('branch');
      if(branchId){
        return {
          headers: {
            Authorization: `JWT ${token}`,
            branchId
          },
        };
      } else {
        return {
          headers: {
            Authorization: `JWT ${token}`,
          },
        };
      }
      // return {
      //   headers: {
      //     Authorization: `JWT ${token}`,
      //     // siteid: environment.SITE_ID,
      //   },
      // };
    }),
    // createUploadLink is obliged to upload file
    createUploadLink({ uri: environment.API_URL }) as any,
  ]);

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: (store) => ({
        cache: new InMemoryCache(),
        link: getJoinedLink(store),
        defaultOptions: defaultOptions,
      }),
      deps: [Store],
    },
  ],
})
export class GraphQLModule {}
