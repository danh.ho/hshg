import { ChangeDetectorRef, Component, HostListener, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IonSlides, ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { LoadCategoryList, LoadNotify, LoadProductsForBuy, ProductForBuyState } from '../category/store';
import { NAVBAR_MENU } from '../header/menu';
import { LoadProfile, ProfileState } from '../my-account/shared/states';
import { SelectBranchComponent } from '../shared/components/select-branch/select-branch.component';
import { AuthState, LogOut } from '../shared/store/auth';
import { LoadBranchById } from '../shared/store/branch/branch.action';
import { BranchState } from '../shared/store/branch/branch.state';
import { AddItemToMemberShoppingCart, GetMemberShoppingCart, RemoveItemGuestBag, ShoppingCartState } from '../shared/store/shopping-cart';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() scrollTop: any;
  @Input() showHeader: boolean = true;
  categories
  cartItemsNo: number;
  isLogin: boolean;
  public user;
  panelOpenState = false;
  hideKeyboard;
  private _subscription = new Subscription();
  navbar = NAVBAR_MENU
  private _subSink = new SubSink();
  searchFormGroup: FormGroup;
  selected
  selectedChild;
  selectedChild2;
  branchId;
  formatAddress;
  formatAddressUser;
  notify = true;
  subTotal = 0;
  totalAmountTopping = 0;
  
  @ViewChild('slider')  slides: IonSlides;
  swipeNext(){
    this.slides.slideNext();
  }
  swipePrev(){
    this.slides.slidePrev();
  }
  textPromotionArr;
  constructor(
    private _store: Store,
    private router: Router,
    private _cdRef: ChangeDetectorRef,
    private _spinner: NgxSpinnerService,
    private _formBuilder: FormBuilder,
    public modalController: ModalController,
  ) { 
  }
  slideNotifyOpts = {
    initialSlide: 1,
    speed: 2000,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 5000,
      // pauseOnMouseEnter: true,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  };
  notifyOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 5000,
    navText: ['', ''],
    nav: false,
    responsive: {
      0: {
        items: 1
      }
    },
    // dotsClass: 'owl-dots'
  }

  ngOnInit() {
    this.branchId = localStorage.getItem('branch')
    if(this.branchId){
      this._store.dispatch(new LoadBranchById({ id: this.branchId }));
    }
    this.searchFormGroup = this._formBuilder.group({
      search: [''],
    });
    this.loadCategories();
    let guestCart = [];
    this._store.select(ShoppingCartState.getGuestCart).subscribe(res => guestCart = res);
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        tap((isLogin) => {
          this.isLogin = isLogin;
        }),
        tap((isLogin) => {
          if (isLogin) {
            this.loadDataProfile();
            this._store.dispatch(new GetMemberShoppingCart())
            if (guestCart !== []) {
              guestCart.forEach(res => {
                this._store.dispatch(new AddItemToMemberShoppingCart(res, 'nav-bar'));
                this._store.dispatch(new RemoveItemGuestBag(res.product.id));
              })
            }
          }
        }),
        switchMap(isLogin => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            this.formatAddressUser = null;
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
        this.subTotal = 0;
        this.totalAmountTopping= 0;
        cart?.forEach(ele => {
            ele?.subCartDetail?.forEach(e => {
                this.totalAmountTopping += e.product.price * e.quantity
            })
            this.subTotal += ele.product.price * ele.quantity + this.totalAmountTopping
            this.totalAmountTopping = 0;
        });
      }));

    this._subscription.add(
      this._store
          .select(BranchState.getSelectedNode)
          .subscribe((data) => {
              if (data) {
                this.formatAddress = data.formatAddress
              }
          })
    );
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this._store.dispatch(new LoadNotify())
      this._subscription.add(this._store.select(ProductForBuyState.getNotifyNodeConnection).subscribe(
        (res) => {
          if (res)
            this.textPromotionArr = res?.edges
        }
      ))
      this._cdRef.markForCheck()
    }, 1000);
  }

  loadCategories() {
    this._store.dispatch(new LoadCategoryList({ level: '0' }))
    this._subscription.add(this._store.select(ProductForBuyState.getCategories).subscribe(
      (res) => {
        if (res)
          this.categories = res?.edges;
      }
    ))
  }
  loadSubcate() {
    this._store.dispatch(new LoadProductsForBuy())
  }
  loadDataProfile() {
    this._store.dispatch(new LoadProfile());
    this._subSink.sink = this._store
      .select(ProfileState.getProfileNode)
      .subscribe(this._fillDataSource.bind(this));
  }
  _fillDataSource(nodeConnection: any) {

    if (!nodeConnection) {
      return
    }
    this.user = nodeConnection;
    this.formatAddressUser = this.user?.address?.find(ele => ele.isDefault == true)?.formatAddress
    this._cdRef.detectChanges();
  }
  changeMyAccount() {
    if (this.isLogin) {
      this.router.navigate(['/my-account'])
    } else {
      this.router.navigate(['/auth/login'])
    }
  }

  changePassword() {
    if (this.isLogin) {
      this.router.navigate(['/my-account/change-password'])
    } else {
      this.router.navigate(['/auth/login'])
    }
  }

  orderHistory() {
    if (this.isLogin) {
      this.router.navigate(['/my-account/order-history'])
    } else {
      this.router.navigate(['/auth/login'])
    }
  }
  logOut() {
    if (this.isLogin) {
      this._store.dispatch(new LogOut);
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/auth/login'])
    }
  }
  public onSignUp(isLogin: boolean) {
    if (isLogin) {
      // this._store.dispatch(new LogOut);
      // this.router.navigate(['/']);
    } else {
      this.router.navigate(['/auth/login'])
    }
  }
  public searchProduct() {
    const value = this.searchFormGroup.controls.search.value;
    console.log(value)
    if (value) {
      this._spinner.show()
      this.searchFormGroup.reset();
      this.router.navigate(['/category'], { queryParams: { search: value } });
      setTimeout(() => {
        this._spinner.hide()
      }, 500)
    }
  }
  public searchProductSm() {
    const value = this.searchFormGroup.controls.search.value;
    if (value) {
        this._spinner.show()
        this.searchFormGroup.reset();
        this.router.navigate(['/category'], { queryParams: { search: value } });
        setTimeout(() => {
            this._spinner.hide()
        }, 500)
    }
    this.hideKeyboard = 'none';
    this.panelOpenState = !this.panelOpenState;
    this.searchFormGroup.reset();

}
selectBranch(){
  this.presentModal();
}
async presentModal() {
  const modal = await this.modalController.create({
    component: SelectBranchComponent,
    cssClass: 'my-custom-class',
    backdropDismiss:false
  });
  await modal.present();
}
showKeyboard() {
  this.hideKeyboard = 'text';
}
togglePanel() {
  this.panelOpenState = !this.panelOpenState;
  this.searchFormGroup.reset();

}
closeSearch() {
  this.panelOpenState = !this.panelOpenState;
  this.searchFormGroup.reset();
  this.hideKeyboard = 'none';
}

over(item) {
  this.selected = item; 
  this.selectedChild = null;
};
out() {
  this.selected = null; 
};
outChild() {
  if(!this.selectedChild2){
    this.selected = null;
  }
}
isActive(item) {
  return this.selected === item;
};
overChild(item,itemChild) {
  this.selected = item; 
  this.selectedChild = itemChild; 
};
overChild2(item,itemChild2) {
  this.selected = item; 
  this.selectedChild2 = itemChild2 
};
outChild2() {
  this.selected = null; 
  this.selectedChild2 = null;
};
isActiveChild(item) {
  return this.selectedChild === item;
};
hideNotify(){
  this.notify = false;
}
ngOnDestroy() {
  this._subscription.unsubscribe();
  this._subSink.unsubscribe();
}
}
