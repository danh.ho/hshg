import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsDetailComponent } from './pages/news-detail/news-detail.component';
import { NewsListingComponent } from './pages/news-listing/news-listing.component';

const routes: Routes = [
    { path: '', component: NewsListingComponent, },
    { path: ':id', component: NewsDetailComponent,  },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FoodCornerRoutingModule { }
