import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'tdl-app-news',
    templateUrl: './food-corner.component.html',
    styleUrls: ['./food-corner.component.scss']
})
export class FoodCornerComponent implements OnInit {

    constructor(
        private _activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

}
