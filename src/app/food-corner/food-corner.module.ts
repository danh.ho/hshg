import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsListingComponent } from './pages/news-listing/news-listing.component';
import { NewsDetailComponent } from './pages/news-detail/news-detail.component';
import { TrainningComponent } from './components/training/training.component';
import { ArtileListingComponent } from './components/artile-listing/artile-listing.component';
import { MatCarouselModule } from '@ngbmodule/material-carousel';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { SharedModule } from '../shared/shared.module';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { IonicModule } from '@ionic/angular';
import { FoodCornerComponent } from './food-corner.component';
import { FoodCornerRoutingModule } from './food-corner-routing.module';


@NgModule({
    declarations: [FoodCornerComponent, NewsListingComponent, NewsDetailComponent, TrainningComponent, ArtileListingComponent],
    imports: [
        CommonModule,
        FoodCornerRoutingModule,
        SharedModule,
        MatCarouselModule.forRoot(),
        NgxUsefulSwiperModule,
        NgxGalleryModule,
        CarouselModule, 
        IonicModule
    ]
})
export class FoodCornerModule { }
