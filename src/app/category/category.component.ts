import { ChangeDetectorRef, ViewChild } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { NAVBAR_MENU } from '../header/menu';
import { VideoProductDetailComponent } from '../shared/components/video-product-detail/video-product-detail.component';
import { ProductForBuyNode, ProductForBuyNodeConnection } from '../shared/models/graphql.models';
import { AuthState } from '../shared/store/auth';
import { LoadProductsWithVariantAll, ProductState } from '../shared/store/product';
import { AddItemToGuestBag, AddItemToMemberShoppingCart, GetMemberShoppingCart, ShoppingCartState } from '../shared/store/shopping-cart';
import { LoadCategoryByID, LoadCategoryList, LoadProductsForBuy, ProductForBuyState } from './store';

type NodeConnection = ProductForBuyNodeConnection;
interface Product {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit, OnDestroy {
  @ViewChild(IonContent) content: IonContent ;
  public selectedOrderBy = "-id"
  navbar = NAVBAR_MENU;
  config: any;
  product;
  category;
  dataCat;
  categories;
  subCategory;
  id;
  dataProduct;
  all = 'Tất cả';
  isShowBackToTop = false
  categoryAll = false;
  selectedProduct = null;
  public data;
  public search;
  public totalProduct = 0;
  totalItems = 0;
  private _subscription = new Subscription();
  cartItemsNo: number;
  private _isLogin: boolean;
  productOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    autoplayHoverPause: false,
    autoplaySpeed: 700,
    autoplayTimeout: 1000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
    },
    nav: false,
    // dotsClass: 'owl-dots'
  }
  itemsPerPage = 8;
  currentPage = 0;
  constructor(
    private _store: Store,
    private route: ActivatedRoute,
    private _cdRef: ChangeDetectorRef,
    public dialog: MatDialog,
  ) {
    let innerWidth = window.innerWidth;
    if(340 <= innerWidth && innerWidth <= 400){
      this.config = {
        itemsPerPage:8,
        currentPage: 1
      }
    } else {
      this.config = {
        itemsPerPage:8,
        currentPage: 1
      }
    }
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(data => {
        if (data.search) {
            this.search = data.search;
            this.loadDataSearchForBuy(this.search);

        } else if (data.search == "") {
            this.search = ""
            this.loadDataForBuy(this.selectedOrderBy);
        }
        // if (!data.search) {
        //     this.search = ""
        //     this.loadDataForBuy(this.selectedOrderBy);
        // }
    })
    this._subscription.add(this.route.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      tap(id => {
        if (id) {
          if(id == 'all'){
            this.categoryAll = true;
            this._store.dispatch(new LoadCategoryList({ level: '0' }))
            this.loadDataForBuy();

          } else {
            this.id = id;
            this._store.dispatch(new LoadCategoryByID({ id: id }));
            this.loadDataForBuy();
          }
        } 
      })
    ).subscribe())

    this._subscription.add(
      this._store.select(ProductForBuyState.getCategoryById).subscribe(
        (res) => {
          if (res) {
            this.subCategory= res?.subCategory.edges
            this.dataCat = res;
            this.categoryAll = false;
          }
        }
      )
    );
    this._subscription.add(
      this._store.select(ProductForBuyState.getCategories).subscribe(
        (res) => {
          if (res) {
            this.subCategory= res?.edges
            this.dataCat = null;
            this.categoryAll = true;
          }
        }
      )
    );

    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        switchMap((isLogin) => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
      }));
      this._subscription.add(this._store
        .select(ProductState.getProductWithVariantAll)
        .subscribe(this._fillDataSource.bind(this)));
    // if(this.subCategory?.length>0){
    //   this._store.dispatch(new LoadProductsWithVariantAll({ categoryList: this.subCategory[0]?.node?.id, hasTopping:false }))
    // }
    this._init();
  }
  loadCategories() {
    this._store.dispatch(new LoadCategoryList(({level: '0'})))
    this._subscription.add(this._store.select(ProductForBuyState.getCategories).subscribe(
      (res) => {
        if (res)
          this.categories = res.edges;
      }))
  }
  public loadDataForBuy(orderBy?): void {
    if(this.categoryAll){
      this._store.dispatch(new LoadProductsWithVariantAll({ hasTopping:false, 
        first: this.itemsPerPage,  after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}));
    } else {
      this._store.dispatch(new LoadProductsWithVariantAll({ hasTopping:false, categoryWithSub: {id: this.id, includeSubCategory: true},
        first: this.itemsPerPage,  after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(), }));
    }
  }
  private _init() {
    this._subscription.add(
        this._store.select(AuthState.isLogin).pipe(
            tap(isLogin => this._isLogin = isLogin)
        ).subscribe()
    );
  }

  private _fillDataSource(nodeConnection): void {
    if (!nodeConnection) {
      return;
    }
    this.dataProduct = nodeConnection.edges.map(ele => ele.node);
    this.totalProduct = nodeConnection.totalCount
    this.totalItems = nodeConnection.totalCount
    this._cdRef.detectChanges();
  }

  public loadDataSearchForBuy(value) {
    this.config.currentPage = 1;
    this.currentPage = 0
    this._store.dispatch(new LoadProductsWithVariantAll({ productVariantBarcode: value, hasTopping:false,
        first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(), }));
  }

  addToCart(item: ProductForBuyNode) {
    const quantity = 1;
    if (this._isLogin) {
      this._store.dispatch(new AddItemToMemberShoppingCart({ product: item, quantity }))
      this._store.dispatch(new GetMemberShoppingCart)
    } else {
      this._store.dispatch(new AddItemToGuestBag({ product: item, quantity }));
    }
  }

  onTabChanged(event: MatTabChangeEvent): void {
    const index = event.index
    this.dataProduct = []
    let innerWidth = window.innerWidth;
    this.config.currentPage = 1
    this.currentPage = 0
    if(340 <= innerWidth && innerWidth <= 400){
      this.config = {
        itemsPerPage:8,
        currentPage: 1
      }
    } else {
      this.config = {
        itemsPerPage:8,
        currentPage: 1
      }
    }
    if (event.index == 0) {
      if(this.categoryAll){
        this._store.dispatch(new LoadProductsWithVariantAll({hasTopping:false,
          first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}))
      } else {
        this._store.dispatch(new LoadProductsWithVariantAll({ categoryWithSub: {id: this.id, includeSubCategory: true},hasTopping:false, 
          first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}))
      }
    } else {
      this._store.dispatch(new LoadProductsWithVariantAll({categoryWithSub: {id: this.subCategory[index-1]?.node?.id, includeSubCategory: true}, hasTopping:false, 
        first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}))
    }
  }

  popupVideo(item){
    const dialogRef = this.dialog.open(VideoProductDetailComponent, {
      width: '900px',
      height: 'auto',
      panelClass: 'dialog-video',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){

      }
    });
  }
  scrollToTop() {    
    this.content.scrollToTop(700);
  }
  logScrolling(event){
    if(event.detail.scrollTop > 100){
      this.isShowBackToTop = true;
    } else {
      this.isShowBackToTop = false;
    }
  }
  pageChanged(event){
    this.currentPage = event - 1
    this.config.currentPage =event;
    if(this.categoryAll){
      this._store.dispatch(new LoadProductsWithVariantAll({ hasTopping:false,  productVariantBarcode: this.search,
        first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}));
    } else {
      this._store.dispatch(new LoadProductsWithVariantAll({ hasTopping:false, categoryWithSub: {id: this.id, includeSubCategory: true}, productVariantBarcode: this.search,
        first: this.itemsPerPage, after: this.itemsPerPage * this.currentPage > 0 ? (this.itemsPerPage * this.currentPage - 1).toString() : (this.itemsPerPage * this.currentPage - 1).toString(),}));
    }
  }
  over(item){
    this.selectedProduct = item
  }
  out(){
    this.selectedProduct = null;
  }
  showGIF(value){
    if(this.selectedProduct == value){
      return true;
    } 
    return false;
  }
  showProductList(value){
    if(this.selectedProduct == value){
      return true;
    } 
    return false;
  }
  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

}
