import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { map, take, tap } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services';
import { AuthState, LogOut } from 'src/app/shared/store/auth';
import { ProductService } from '../product.service';
import { ProductForBuyStateModel, initialState } from './product-state.model';
import { CartAdd, CartAddFailed, CartAddSuccessful, LoadCategoryByID, LoadCategoryList, LoadCategoryListForHeader, LoadListProductsForBuy, LoadNotify, LoadProductsForBuy } from './product.actions';

@Injectable({ providedIn: 'root' })
@State<ProductForBuyStateModel>({
    name: 'productBuy',
    defaults: initialState,
})
export class ProductForBuyState {
    @Selector()
    static getNodeConnection({ nodeConnection }: ProductForBuyStateModel) {
        return nodeConnection;
    }
    @Selector()
    static getListNodeConnection({ listNodeConnection }: ProductForBuyStateModel) {
        return listNodeConnection;
    }
    @Selector()
    static getSelectedNode({ selectedNode }: ProductForBuyStateModel) {
        return selectedNode;
    }
    @Selector()
    static getCategoryById({ categoryNode }: ProductForBuyStateModel) {
        return categoryNode;
    }
    @Selector()
    static getCategories({ categories }: ProductForBuyStateModel) {
        return categories;
    }
    @Selector()
    static getCategoriesForHeader({ categoriesforHeader }: ProductForBuyStateModel) {
        return categoriesforHeader;
    }
    @Selector()
    static getNotifyNodeConnection({ notifyNodeConnection }: ProductForBuyStateModel) {
        return notifyNodeConnection;
    }
    constructor(
        private _apiService: ProductService,
        private _router: Router,
        private _notification: NotificationService,
    ) { }

    @Action(LoadProductsForBuy, { cancelUncompleted: true })
    loadProducts(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadProductsForBuy
    ) {
        return this._apiService
            .getAllForBuy(payload)
            .pipe(
                tap(({ data }) => {
                    if(data){
                        patchState({
                            nodeConnection: data.productsForBuy,
                        });
                    }

                })
            );
    }
    @Action(LoadListProductsForBuy, { cancelUncompleted: true })
    loadListProductsForBuy(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadListProductsForBuy
    ) {
        return this._apiService
            .getListProductForBuy(payload)
            .pipe(
                tap(({ data }) => {
                    console.log(data)
                    if(data){
                        patchState({
                            listNodeConnection: data.productsForBuy,
                        });
                    }

                })
            );
    }
    @Action(LoadCategoryByID)
    loadCategoryByID(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadCategoryByID
    ) {
        return this._apiService
            .getCategoryById({ ...payload })
            .pipe(
                tap(({ data }) => {
                    patchState({
                        categoryNode: data.categoryById,
                    });
                })
            );
    }
    @Action(LoadCategoryList)
    loadCategoryList(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadCategoryList
    ) {
        return this._apiService
            .getCategoryList({ ...payload })
            .pipe(
                tap(({ data }) => {
                    patchState({
                        categories: data.categories,
                    });
                })
            );
    }
    @Action(LoadCategoryListForHeader)
    loadCategoryListForHeader(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadCategoryListForHeader
    ) {
        return this._apiService
            .getCategoryList({ ...payload })
            .pipe(
                tap(({ data }) => {
                    patchState({
                        categoriesforHeader: data.categories,
                    });
                })
            );
    }
    @Action(CartAdd, { cancelUncompleted: true })
    cartAdd(
        { dispatch }: StateContext<ProductForBuyStateModel>,
        { payload }: CartAdd
    ) {
        return this._apiService
            .cartAdd({ input: payload })
            .pipe(
                tap(res => {
                    if (!res) {
                        return dispatch(new CartAddFailed)
                    } else {
                        const { errors, status } = res;
                        if (status) {
                            return dispatch(new CartAddSuccessful);
                        }
                        return dispatch(new CartAddFailed(errors[0]));
                    }
                }));
    }

    @Action(CartAddFailed)
    addCartFailed(
        { dispatch }: StateContext<AuthState>,
        { payload }: CartAddFailed
    ) {
        if (!payload || payload.code === 'ORDER_01') {
            dispatch(new LogOut)
            this._router.navigate(['/auth/login'])
        } else {
            // this._translate.get(['FAILED.failed', 'FAILED.add_to_cart_failed']).pipe(
            //     take(1),
            //     tap(translation => {
            //         this._notification.error(translation['FAILED.failed'], translation['FAILED.add_to_cart_failed'])
            //     })
            // ).subscribe();
        }
    }

    @Action(CartAddSuccessful)
    addCartSuccessfully() {
        // this._translate.get(['SUCCESS.success', 'SUCCESS.add_to_cart_successfully',]).pipe(
        //     take(1),
        //     tap(translation => {
        //         this._notification.success(translation['SUCCESS.success'], translation['SUCCESS.add_to_cart_successfully'])
        //     })
        // ).subscribe();
    }
    
    @Action(LoadNotify, )
    loadNews(
        { patchState }: StateContext<ProductForBuyStateModel>,
        { payload }: LoadNotify,
    ) {
        return this._apiService
            .getNotify({ ...payload })
            .pipe(
                map(res => res.data.headerNews),
                tap(notifyNodeConnection => patchState({ notifyNodeConnection }))
            )
    }
}

