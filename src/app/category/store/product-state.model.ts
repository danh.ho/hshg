import { CategoryNode, CategoryNodeConnection, HeaderNewsNodeConnection, ProductForBuyNode, ProductForBuyNodeConnection } from 'src/app/shared/models/graphql.models';

  export interface ProductForBuyStateModel {
    nodeConnection?: ProductForBuyNodeConnection;
    listNodeConnection?: ProductForBuyNodeConnection;
    selectedNode?: ProductForBuyNode;
    categoryNode?: CategoryNode;
    categories?: CategoryNodeConnection
    categoriesforHeader?: CategoryNodeConnection
    notifyNodeConnection?: HeaderNewsNodeConnection
  }
  
  export const initialState: ProductForBuyStateModel = {};

