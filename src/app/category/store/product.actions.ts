import { AddToCartInput, OrderError, QueryCategoriesArgs, QueryCategoryByIdArgs, QueryHeaderNewsArgs, QueryProductsForBuyArgs } from 'src/app/shared/models/graphql.models';

  const enum BuyActions {
    LOAD_NOTIFY = '[Product.BUY] Load Notify',
    LOAD_PRODUCTS = '[Product.BUY] Load Products',
    LOAD_LIST_PRODUCTS = '[Product.BUY] Load List Products',
    LOAD_CATEGORY_BY_ID = '[Product.BUY] Load Category By Id',
    LOAD_CATEGORY_LIST = '[Product.BUY] Load Category List',
    LOAD_CATEGORY_LIST_FOR_HEADER = '[Product.BUY] Load Category List For Header',
    CART_ADD = '[Product.Cart] Cart Add',
    CART_ADD_SUCCESSFUL = '[Product.Cart] Cart Add Successfully',
    CART_ADD_FAILED = '[Product.BUY] Cart Add Failed',
  }

  export class LoadProductsForBuy {
    static readonly type = BuyActions.LOAD_PRODUCTS;
    constructor(public readonly payload?: QueryProductsForBuyArgs) { }
  }
  export class LoadListProductsForBuy {
    static readonly type = BuyActions.LOAD_LIST_PRODUCTS;
    constructor(public readonly payload?: QueryProductsForBuyArgs) { }
  }
  export class LoadCategoryByID {
    static readonly type = BuyActions.LOAD_CATEGORY_BY_ID;
    constructor(public readonly payload?: QueryCategoryByIdArgs) {
    }
  }
  export class LoadCategoryList {
    static readonly type = BuyActions.LOAD_CATEGORY_LIST;
    constructor(public readonly payload?: QueryCategoriesArgs) {
    }
  }
  export class LoadCategoryListForHeader {
    static readonly type = BuyActions.LOAD_CATEGORY_LIST_FOR_HEADER;
    constructor(public readonly payload?: QueryCategoriesArgs) {
    }
  }
  export class CartAdd {
    static readonly type = BuyActions.CART_ADD;
    constructor(public readonly payload: AddToCartInput) {}
  }
  export class CartAddSuccessful {
    static readonly type = BuyActions.CART_ADD_SUCCESSFUL;
  }
  export class CartAddFailed {
    static readonly type = BuyActions.CART_ADD_FAILED;
    constructor(public readonly payload?: OrderError) {}
  }
  export class LoadNotify {
    static readonly type = BuyActions.LOAD_NOTIFY;
    constructor(public readonly payload?: QueryHeaderNewsArgs) {
    }
}