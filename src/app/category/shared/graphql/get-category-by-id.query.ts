import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery, QueryCategoryByIdArgs,
} from '../../../shared/models/graphql.models';


const getCategoryById = gql`
  query getCategoryById($id: ID!) {
    categoryById(id: $id) {
      name
      description
      id
      icon
      level
      parent {
        id
        name
      }
      product {
        edges {
          node {
            id
            thumbnail
            price
            flashSaleProduct {
              edges {
                node {
                  id
                  price
                }
              }
            }
          }
        }
      }
      subCategory {
        edges {
          node {
            id
            name
            description
            product {
              edges {
                node {
                  id
                  thumbnail
                  price
                  flashSaleProduct {
                    edges {
                      node {
                        id
                        price
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetCategoryById extends Query<
  ParentQuery,
  QueryCategoryByIdArgs
> {
  document = getCategoryById;
}
