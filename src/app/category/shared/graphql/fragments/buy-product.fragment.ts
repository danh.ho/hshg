import gql from 'graphql-tag';

export const productForBuyFragment = gql`
  fragment ProductForBuyFragment on ProductForBuyNode {
    id
    name
    thumbnail
    description
    description2
    description3
    descriptions {
      description
      name
    }
    price
    advertisingPrice
    productType
    visibility
    visibilityDate
    video
    likeCount
    productImage {
      id
      image
    }
    flashSaleProduct{
      edges {
        node {
          id
          price
          flashSale {
            id
            startTime
            endTime
            isActive
          }
        }
      }
    }
    category {
      id
      name
      parent {
        id
        name
      }
      subCategory {
        edges {
          node {
            id
            name
          }
        }
      }
      product {
        edges {
          node {
            id
            name
            thumbnail
            price
            flashSaleProduct{
              edges {
                node {
                  id
                  price
                }
              }
            }
            productImage {
              id
              image
              }
            }
          }
      }
      
    }
    variant {
      totalCount
      pageInfo {
          hasNextPage
          hasPreviousPage
          startCursor
          endCursor
      }
      edges {
          node {
              attributes {
                  edges {
                      node {
                          description
                          id
                          name
                          specification {
                              id
                              name
                          }
                      }
                  }
              }
              price
              id
              thumbnail
              images {
                  id
                  image
              }
              variantName
              totalQuantityInventory
              totalOrderCustomer
              isSellOutOfStock
              inventory {
                  customerOrder
                  id
                  name
                  quantity
              }
          }
      }
  }
  }
`;

export const productForBuyFragmentBrief = gql`
  fragment ProductForBuyFragmentBrief on ProductForBuyNode {
    id
    name
    description
    thumbnail
  }
`;
