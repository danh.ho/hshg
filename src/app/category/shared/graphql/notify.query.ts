import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
  QueryHeaderNewsArgs,
} from '../../../shared/models/graphql.models';

const getAllNotify = gql`
query HeaderNews($before: String, $after: String, $first: Int, $last: Int, $orderBy: String, $title: String, $beginTime: DateTime, $endTime: DateTime) {
  headerNews(before: $before, after: $after, first: $first, last: $last, orderBy: $orderBy, title: $title, beginTime: $beginTime, endTime: $endTime) {
    totalCount
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        beginTime
        endTime
        id
        title
      }
    }
  }
}
`;
@Injectable({ providedIn: 'root' })
export class NotifyQuery extends Query<
  ParentQuery,
  QueryHeaderNewsArgs
> {
  document = getAllNotify;
}
