import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
  QueryProductsForBuyArgs,
} from '../../../shared/models/graphql.models';
import { productForBuyFragment } from './fragments';

const getAllProductsForBuy = gql`
  ${productForBuyFragment}
  query AllProductsForBuy(
    $categoryWithSub : SubCategoryInput
    $before: String
    $after: String
    $first: Int
    $last: Int
    $category: String
    $name: String
    $price: String
    $orderBy: String
    $collection: String
    $hasFlashSale : Boolean
    $collectionName: String
    $collectionItemCode: String
  ) {
    productsForBuy(
      collectionItemCode : $collectionItemCode
      collectionName: $collectionName
      categoryWithSub: $categoryWithSub
      before: $before
      after: $after
      first: $first
      last: $last
      category: $category
      name: $name
      price: $price
      orderBy: $orderBy
      collection: $collection
      hasFlashSale: $hasFlashSale
    ) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          ...ProductForBuyFragment
          relatedProduct{
            price
            advertisingPrice
            thumbnail
            name
            video
            created
            description
            id
          }
        }
      }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetAllForBuyQuery extends Query<
  ParentQuery,
  QueryProductsForBuyArgs
> {
  document = getAllProductsForBuy;
}
