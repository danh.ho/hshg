import { Injectable } from '@angular/core';
import { Mutation as ApolloMutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation, MutationCartAddArgs } from 'src/app/shared/models/graphql.models';

@Injectable({ providedIn: 'root' })
export class CartAddMutation extends ApolloMutation<
  Mutation,
  MutationCartAddArgs
> {
  document = gql`
    mutation CartAddMutation($input: AddToCartInput!){
        cartAdd(input: $input) {
          status
          errors {
            code
            field
            message
          }
        }
      }
  `;
}
