import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Categories ($level: String, $first: Int) {
    categories (level: $level, first: $first) {
        edges {
          node {
            id
            name
            subCategory{
              edges {
                node {
                  id
                  name
                }
              }
            }
            product {
              edges {
                node {
                  id
                }
              }
            }
            parent {
              id
              itemCode
            }
            icon
            description
          }
        }
      }
}
`

@Injectable({
    providedIn: 'root'
})
export class CategoriesQuery extends Query<ParentQuery> {
    document = QUERY;
}
