import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
  QueryProductsForBuyArgs,
} from '../../../shared/models/graphql.models';

const getListProductsForBuy = gql`
  query ListProductsForBuy(
    $productIdList: [String]
  ) {
    productsForBuy(
      productIdList : $productIdList
    ) {
      edges {
        node {
          id
          thumbnail
          price
          flashSaleProduct{
            edges {
              node {
                id
                price
              }
            }
          }
          advertisingPrice
        }
      }
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class GetListForBuyQuery extends Query<
  ParentQuery,
  QueryProductsForBuyArgs
> {
  document = getListProductsForBuy;
}
