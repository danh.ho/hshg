export interface DisplayedColumns {
    actions: boolean | string;
    id: boolean | string;
    name: boolean | string;
    description: boolean | string;
    icon: boolean | string;
}
  