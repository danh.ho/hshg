import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { MutationCartAddArgs, QueryCategoriesArgs, QueryCategoryByIdArgs, QueryHeaderNewsArgs, QueryProductsForBuyArgs } from '../shared/models/graphql.models';
import { GetAllForBuyQuery } from './shared/graphql/get-all.query';
import { CategoriesQuery } from './shared/graphql/get-category-all.query';
import { GetCategoryById } from './shared/graphql/get-category-by-id.query';
import { GetListForBuyQuery } from './shared/graphql/get-list-product.query';
import { CartAddMutation } from './shared/graphql/mutations';
import { NotifyQuery } from './shared/graphql/notify.query';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private _getAllForBuy: GetAllForBuyQuery,
    private _getListForBuy: GetListForBuyQuery,
    private _getCategoryById: GetCategoryById,
    private _getCategoryList: CategoriesQuery,
    private _cartAdd: CartAddMutation,
    private _notifyQuery: NotifyQuery,

  ) { }

  getAllForBuy(args: QueryProductsForBuyArgs) {
    return this._getAllForBuy.watch(args).valueChanges;
  }
  getListProductForBuy(args: QueryProductsForBuyArgs) {
    return this._getListForBuy.watch(args).valueChanges;
  }
  getCategoryById(args: QueryCategoryByIdArgs) {
    return this._getCategoryById.watch(args).valueChanges;
  }
  getCategoryList(args: QueryCategoriesArgs) {
    return this._getCategoryList.watch(args).valueChanges;
  }
  cartAdd(args: MutationCartAddArgs) {
    return this._cartAdd.mutate(args).pipe(
        map(res => res.data.cartAdd)
    );
  }
  getNotify(args?: QueryHeaderNewsArgs) {
    return this._notifyQuery.watch(args).valueChanges;
}
}
