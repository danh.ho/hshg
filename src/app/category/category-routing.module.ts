import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './category.component';

const routes: Routes = [
    { path: '', component: CategoryComponent },
    { path: ':id', component: CategoryComponent },
    { path: ':id/:sub', component: CategoryComponent },
    { path: 'flashsale', component: CategoryComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CategoryRoutingModule { }
