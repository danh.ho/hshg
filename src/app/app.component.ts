import { ChangeDetectorRef, Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { IonContent, MenuController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Actions, ofActionCompleted, Store } from '@ngxs/store';
import { StateReset } from 'ngxs-reset-plugin';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { LoadCategoryList, ProductForBuyState } from './category/store';
import { OrderCartState } from './checkout/store';
import { NAVBAR_MENU } from './header/menu';
import { LoadProfile, ProfileState } from './my-account/shared/states';
import { PopupPromotionComponent } from './shared/components/popup-promotion/popup-promotion.component';
import { SelectBranchComponent } from './shared/components/select-branch/select-branch.component';
import { AuthState, LogOut } from './shared/store/auth';
import { CmsState, GetGroupBanner } from './shared/store/cms';
import { GetMemberShoppingCart, ShoppingCartState } from './shared/store/shopping-cart';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild(IonContent) content: IonContent ;
  navbar = NAVBAR_MENU;
  show = '';
  showSub = '';
  formatAddressUser;
  cartItemsNo: number;
  isLogin: boolean;
  subTotal = 0;
  totalAmountTopping = 0;
  showHeader = true;
  public user;
  private _subSink = new SubSink();
  private _subscription = new Subscription();
  categories
  scrollY = 0;
  lang: 'vi' | 'en' = 'vi';
  bannerList = [];
  menuState = false;
  isLoadedPopup = false;
  constructor(
    private translate: TranslateService,
    public menuCtrl: MenuController,
    private _action: Actions,
    private _store: Store,
    public modalController: ModalController,
    private _cdRef: ChangeDetectorRef,
    private router: Router,
    public dialog: MatDialog,

  ) {
    _action.pipe(ofActionCompleted(LogOut)).subscribe(() => {
      // localStorage.clear();
      _store.dispatch(new StateReset(OrderCartState, AuthState, ShoppingCartState))
    })
    router.events.subscribe((val) => {
      if(val){
        this.showHeader = true;
      }
    })
    const storeLanguage = localStorage.getItem('language');
    if (storeLanguage === 'vi' || storeLanguage === 'en') {
        (this.lang as any) = storeLanguage;
        this.translate.setDefaultLang(localStorage.getItem('language'))
        localStorage.setItem("language", this.lang)
    } else {
        this.translate.setDefaultLang(this.lang)
        localStorage.setItem("language", this.lang)
    }
  }
  async presentModal() {
    const modal = await this.modalController.create({
      component: SelectBranchComponent,
      cssClass: 'my-custom-class',
      backdropDismiss:false
    });
    await modal.present();
  }
  async promotionModal() {
    const modal = await this.modalController.create({
      component: PopupPromotionComponent,
      cssClass: 'my-custom-promotion',
    });
    await modal.present();
  }
  ngOnInit() {
    this.loadCategories();
    const branchId = localStorage.getItem('branch');
    if(!branchId){
      this.presentModal();
    } else {
      this._store.dispatch(new GetGroupBanner());
      this._subscription.add(this._store.select(CmsState.getBannerGroup)
        .subscribe(res => {
          if (res) {
            const main = res.find(ele => ele.itemCode == 'popup-promotion')
            if(main?.visibility && !this.isLoadedPopup){
              this.promotionModal();
              this.isLoadedPopup = true    
            }
            this._cdRef.detectChanges();
          }
        }
        ));
    }
    this._subscription.add(
      this._store.select(AuthState.isLogin).pipe(
        tap((isLogin) => {
          this.isLogin = isLogin;
        }),
        tap((isLogin) => {
          if (isLogin) {
            this.loadDataProfile();
          }
        }),
        switchMap(isLogin => {
          if (isLogin) {
            return this._store.select(ShoppingCartState.getMemberCart)
          } else {
            return this._store.select(ShoppingCartState.getGuestCart)
          }
        })
      ).subscribe(cart => {
        this.cartItemsNo = cart.length;
        this.subTotal = 0;
        this.totalAmountTopping= 0;
        cart?.forEach(ele => {
            ele?.subCartDetail?.forEach(e => {
                this.totalAmountTopping += e.product.price * e.quantity
            })
            this.subTotal += ele.product.price * ele.quantity + this.totalAmountTopping
            this.totalAmountTopping = 0;
        });
      }));
  }
  loadCategories(){
    this._store.dispatch(new LoadCategoryList({ level: '0' }))
    this._subscription.add(this._store.select(ProductForBuyState.getCategories).subscribe(
        (res) => {
          if(res)
          this.categories = res.edges;
        }
      ))
  }
  showMenuSub(name){
    if (this.show == name) {
      this.show = null;
      return;
  } else
      this.show = name;
  }
  showMenuSub2(name){
    if (this.showSub == name) {
      this.showSub = null;
      return;
  } else
      this.showSub = name;
  }
  close(){
    this.menuCtrl.close()
  }
  open(){
    this.menuCtrl.enable;
  }
  loadDataProfile() {
    this._store.dispatch(new LoadProfile());
    this._subSink.sink = this._store
      .select(ProfileState.getProfileNode)
      .subscribe(this._fillDataSource.bind(this));
  }
  _fillDataSource(nodeConnection: any) {

    if (!nodeConnection) {
      return
    }
    this.user = nodeConnection;
    this.formatAddressUser = this.user?.address?.find(ele => ele.isDefault == true)?.formatAddress
    this._cdRef.detectChanges();
  }
  logScrolling(event){
    const scrollTop = event.detail.scrollTop;
    const velocityY = event.detail.velocityY;
    const deltaY = event.detail.deltaY;
    // if(velocityY < 0) {
    //   if(this.scrollY >= 0){
    //     this.showHeader = true;
    //     this.scrollY = velocityY;
    //   }
    // } else if(velocityY > 0 && scrollTop>135) {
    //   if(this.scrollY < 0){
    //     this.scrollY = velocityY;
    //     this.showHeader = false;
    //   }
    // }
    if(scrollTop > 135 && scrollTop > this.scrollY && deltaY>0){
      this.showHeader = false;
      this.scrollY = scrollTop
    } else if(scrollTop < this.scrollY && deltaY<0) {
      this.showHeader = true;
      this.scrollY = scrollTop
    }
  }
  scrollToTop() {    
    this.content.scrollToTop(700);
  }
  updateMenuState(value){
    this.menuState = value
  }
}
