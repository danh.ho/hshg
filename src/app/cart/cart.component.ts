import { ThrowStmt } from '@angular/compiler';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { IonContent, ModalController, ViewWillEnter } from '@ionic/angular';
import { Select, Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription, Observable } from 'rxjs';
import { switchMap, tap, } from 'rxjs/operators';
import { ByProductAndVariant, CartDetailNode, ProductForBuyNode, PromotionCodeNodeConnection, PromotionCodeUnitDiscount, RelatedProductNode, UnitDiscountInput, } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { AddItemToGuestBag, AddItemToMemberShoppingCart, EditQuantityGuestCart, EditQuantityItemMemberShoppingCart, GetMemberShoppingCart, GUEST_ITEM_CARD, RemoveItemGuestBag, RemoveItemMemberShoppingCart, ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { LoadListProductsForBuy, LoadProductsForBuy, ProductForBuyState } from '../category/store';
import { LoadPromotionCode, LoadPromotionCodeByCode, OrderCartState } from '../checkout/store';
import { NAVBAR_MENU } from '../header/menu';
import { DeleteProductModalComponent } from '../shared/components/delete-product-modal/delete-product-modal.component';
import { SubcribleModalComponent } from '../shared/components/subcrible-modal/subcrible-modal.component';

@Component({
    selector: 'cos-frontend-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, ViewWillEnter {
    @Select(ShoppingCartState.getGuestCart) guestCart$: Observable<Array<GUEST_ITEM_CARD>>;
    @Select(ShoppingCartState.getMemberCart) memberCart$: Observable<Array<CartDetailNode>>;
    @Select(OrderCartState.getNodeConnectionPromotionCode) promotionCode$: Observable<PromotionCodeNodeConnection>;
    @ViewChild(IonContent) content: IonContent ;
    isShowBackToTop = false;
    isLogin: boolean;
    public subTotal = 0
    public subTotalMember = 0
    navbar = NAVBAR_MENU
    private _subscription = new Subscription();
    public memberCart;
    contentTpl;
    product = [];
    cartGuest;
    activePromotion = null;
    totalDiscount = 0
    code;
    totalAmountTopping = 0
    totalAmountToppingGuest = 0
    relatedProduct = []
    form: FormGroup;
    maxDiscount = 0;
    indexMax = 0;
    codeMax;
    needToDiscount = 0;
    limitAmount = 0;
    cartItemsNo: number;
    listIdProduct = [];
    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        navSpeed: 700,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        navText: ['', ''],
        responsive: {
          0: {
            items: 1
          },
          425: {
            items: 2
          },
          768: {
            items: 3
          },
          1024: {
            items: 4
          }
        },
        nav: false,
        autoWidth: true
    }

    changeQuantity2(quantity: number, id: string) {
        if (!this.isLogin) {
            if (quantity === 0) {
                this._store.dispatch(new RemoveItemGuestBag(id));
            } else if (quantity > 0) {
                this._store.dispatch(new EditQuantityGuestCart({ id, quantity }))
            }
        } else {
            this._store.dispatch(new EditQuantityItemMemberShoppingCart({ productId: id, quantity }))
        }

    }


    constructor(
        private _store: Store,
        public modalController: ModalController,
        private _cdRef: ChangeDetectorRef,
        private _router: Router,
        private _formBuilder: FormBuilder
    ) {

    }

    ionViewWillEnter(): void {
        // const dataGuest = this._store.selectSnapshot(ShoppingCartState.getGuestCart)
        // if(!this.isLogin){
        //     this.subTotal = 0;
        //     dataGuest?.forEach(ele => {
        //         this.subTotal += ele.product.price * ele.quantity + this.totalAmountToppingGuest
        //     });
        //     setTimeout(() => {
        //         this._initPromotionCode(this.subTotal)
        //     }, 500);
        // }else{
        //     this._store.dispatch(new GetMemberShoppingCart).subscribe();
        // }
        
        // this.addProductRelatedGuest(dataGuest)
    }
    scrollToTop() {    
        this.content.scrollToTop(700);
      }
      logScrolling(event){
        if(event.detail.scrollTop > 100){
          this.isShowBackToTop = true;
        } else {
          this.isShowBackToTop = false;
        }
      }
    ngOnInit(): void {
        this.form = this._formBuilder.group({
            code: ['']
        })

        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
                tap(isLogin => { this.isLogin = isLogin; })
            ).subscribe()
        );
        this._subscription.add(this._store.select(ShoppingCartState.getGuestCart).pipe(
            tap(res => {
                if (res) {
                    this.listIdProduct = [];
                    this.subTotal = 0;
                    this.cartGuest = res;
                    this.totalAmountToppingGuest = 0;
                    this.cartGuest?.forEach(ele => {
                        console.log(ele)
                        this.listIdProduct.push(ele?.product?.id)
                        ele?.subCartDetail?.forEach(e => {
                            this.totalAmountToppingGuest += e.product.price * e.quantity
                        })
                        this.subTotal += ele.product.price * ele.quantity + this.totalAmountToppingGuest
                        this.totalAmountToppingGuest = 0;
                    });
                    console.log(this.listIdProduct)
                    setTimeout(() => {
                        this._initPromotionCode(this.subTotal)
                    }, 500);
                    this.addProductRelatedGuest(res)
                }
                this._store.dispatch(new LoadListProductsForBuy({productIdList: this.listIdProduct}));
                this._subscription.add(this._store
                    .select(ProductForBuyState.getListNodeConnection).pipe(tap(res => {
                        if (res) {
                            this.product = res.edges;
                            if (this.product.length > 0) {
                                this.product = []
                                this.product = res.edges;
                            }
                        }
                    }))
                    .subscribe((res) => {
                        this._cdRef.detectChanges();

                    }
                    )
                )

            })
        ).subscribe())
        setTimeout(() => {
            if (this.isLogin) {
                if (this.subTotalMember !== 0) {

                } else {
                    this._store.dispatch(new GetMemberShoppingCart).subscribe();
                    this._subscription.add(this._store
                        .select(ShoppingCartState.getMemberCart)
                        .subscribe(res => {
                            if (res) {
                                this.memberCart = res;
                                this.subTotalMember = 0;
                                this.totalAmountTopping = 0;
                                res.forEach(ele => {
                                    if (ele.product.flashSaleProduct?.edges.length > 0) {
                                        this.subTotalMember += ele.product.flashSaleProduct?.edges[0].node.price * ele.quantity
                                    } else {
                                        ele?.subCartDetail.forEach(e => {
                                            this.totalAmountTopping += e.product.price * e.quantity
                                        })
                                        this.subTotalMember += ele.product.price * ele.quantity + this.totalAmountTopping
                                        this.totalAmountTopping = 0;

                                    }

                                })

                                setTimeout(() => {
                                    this._initPromotionCode(this.subTotalMember)
                                }, 500);
                            }


                        }));

                        this._subscription.add(this._store
                            .select(ShoppingCartState.getRelatedProduct)
                            .subscribe(res => {
                                if (res) {
                                    this.relatedProduct = res
                                }
                            })
                        );
                    }
                }
        // this._translate.get('FAILED.no_data').subscribe((result: string) => {
        //     this.contentTpl = result
        // });
        }, 2000);
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
        // this._initPromotionCode()
    }

    addProductRelatedGuest(data){
        if(data && data.length > 0){
            setTimeout(() => {
                this.relatedProduct = []
                for(let i = 0; i < data.length; i++){
                    if(data[i].product?.relatedProduct?.length > 0){
                        for(let j = 0; j < data[i].product.relatedProduct.length; j++){
                            if(this.checkLoopRelatedProduct(data[i].product.relatedProduct[j])){
                                this.relatedProduct.push(data[i].product.relatedProduct[j])
                            }
                        }
                    }
                }
            }, 2000);
        }
    }

    checkLoopRelatedProduct(data){
        for(let i = 0; i < this.relatedProduct.length; i++){
            if(this.relatedProduct[i].id == data.id){
                return false
            }
        }
        return true
    }

    addToCart(item: ProductForBuyNode) {
        // let params = {
        //   product: item.id,
        //   quantity: 1
        // }
        const quantity = 1;
        if (this.isLogin) {
          //this.addToCartLogin(params);
          this._store.dispatch(new AddItemToMemberShoppingCart({ product: item, quantity }))
          this._store.dispatch(new GetMemberShoppingCart)
        } else {
          this._store.dispatch(new AddItemToGuestBag({ product: item }));
        }
    }

    _initPromotionCode(limitAmount){
        this._store.dispatch(new LoadPromotionCode({isPublic: true}))
        this._subscription.add(
            this._store.select(OrderCartState.getNodeConnectionPromotionCode).subscribe(data => {
                if(data){
                    const {edges} = data
                    this.maxDiscount = 0;
                    this.indexMax = 0;
                    this.code = null;
                    for(let item of edges){
                        if(item?.node?.limitAmount <= limitAmount){
                            if(item?.node?.unitDiscount == PromotionCodeUnitDiscount.Vnd){
                                if(item?.node?.discount > this.maxDiscount){
                                    this.limitAmount = item?.node?.limitAmount
                                    this.maxDiscount = item?.node?.discount
                                    this.code = item?.node?.code
                                    this.activePromotion = item?.node?.code
                                }
                            } else {
                                const discount = (limitAmount * item?.node?.discount)/100
                                if(discount > item?.node?.limitDiscount){
                                    if(discount > this.maxDiscount){
                                        this.limitAmount = item?.node?.limitAmount
                                        this.maxDiscount = discount
                                        this.code = item?.node?.code;
                                        this.activePromotion = item?.node?.code
                                    } 
                                } else {
                                    if(item?.node?.limitDiscount > this.maxDiscount){
                                        this.limitAmount = item?.node?.limitAmount
                                        this.maxDiscount = item?.node?.limitDiscount
                                        this.code = item?.node?.code;
                                        this.activePromotion = item?.node?.code
                                    } 
                                }
                            }
                        }
                    }
                    this.totalDiscount = this.maxDiscount
                    if(this.isLogin){
                        this.needToDiscount = this.limitAmount - this.subTotalMember
                    } else {
                        this.needToDiscount = this.limitAmount - this.subTotal

                    }
                }
            })
        )
    }

    async openDialog(id) {
        const modal = await this.modalController.create({
            component: DeleteProductModalComponent,
            cssClass: 'delete-product'
        });
        await modal.present();
        const { data } = await modal.onWillDismiss();
        if (data.dismissed == true) {
            if (this.isLogin) {
                // this._store.dispatch(new DeleteCart(param))
                this._store.dispatch(new RemoveItemMemberShoppingCart(id))
            } else {
                this._store.dispatch(new RemoveItemGuestBag(id));
            }
        }
    }

    changeOptionPromotion(promotion){
        if(promotion?.node?.id){
            if(this.activePromotion == promotion?.node?.code){
                this.activePromotion = null
                this.code = null
                this.limitAmount =  0;
                this.totalDiscount = 0;
                this.needToDiscount = 0;
            } else {
                this.activePromotion = promotion?.node?.code
                this.code = promotion?.node?.code
                this.limitAmount =  promotion?.node?.limitAmount
                if(this.isLogin){
                    if(this.subTotalMember >= promotion?.node?.limitAmount){                    
                        if(promotion?.node?.unitDiscount == UnitDiscountInput.Vnd){
                            this.totalDiscount = promotion?.node?.discount
                        }else{
                            if(this.subTotalMember && promotion?.node?.limitDiscount && (this.subTotalMember * promotion?.node?.discount / 100) > promotion?.node?.limitDiscount){
                                this.totalDiscount = promotion?.node?.limitDiscount
                            }else{
                                this.totalDiscount = (this.subTotalMember * promotion?.node?.discount) / 100
                            }
                        }
                        this.needToDiscount = 0;
                    } else {
                        this.totalDiscount = 0;
                        this.code = null;
                        this.needToDiscount = promotion?.node?.limitAmount - this.subTotalMember
                    }
                }else{
                    if(this.subTotal >= promotion?.node?.limitAmount){ 
                        if(promotion?.node?.unitDiscount == UnitDiscountInput.Vnd){
                            this.totalDiscount = promotion?.node?.discount
                        }else{
                            if(this.subTotal && promotion?.node?.limitDiscount && (this.subTotal * promotion?.node?.discount / 100) > promotion?.node?.limitDiscount){
                                this.totalDiscount = promotion?.node?.limitDiscount
                            }else{
                                this.totalDiscount = (this.subTotal * promotion?.node?.discount) / 100
                            }
                        }
                        this.needToDiscount = 0
                    } else {
                        this.totalDiscount = 0;
                        this.code = null;
                        this.needToDiscount = promotion?.node?.limitAmount - this.subTotal
                        console.log(this.needToDiscount)
                    }
                }
            }
        }
    }

    checkout(){
        const params = {
            total: this.totalDiscount,
            code: this.code
        }
        localStorage.setItem("discount", JSON.stringify(params))
        this.totalDiscount = 0
        this.activePromotion = null
        this._router.navigate(['/checkout'])
    }

    applyPromotionCode(value){
        if(value){
            this._store.dispatch(new LoadPromotionCodeByCode(value))
            this._subscription.add(
                this._store.select(OrderCartState.getNodeConnectionPromotionCodeByCode).subscribe(data => {
                    if(data){
                        const {edges} = data
                        if(edges.length > 0){
                            this.changePromotionApply(edges[0])
                        }
                    }
                })
            )
        }
    }

    changePromotionApply(promotion){
        if(this.isLogin){
            if(promotion?.node?.unitDiscount == UnitDiscountInput.Vnd){
                this.totalDiscount = promotion?.node?.discount
            }else{
                if(this.subTotalMember && promotion?.node?.limitDiscount && (this.subTotalMember * promotion?.node?.discount / 100) > promotion?.node?.limitDiscount){
                    this.totalDiscount = promotion?.node?.limitDiscount
                }else{
                    this.totalDiscount = (this.subTotalMember * promotion?.node?.discount) / 100
                }
            }
        }else{
            if(promotion?.node?.unitDiscount == UnitDiscountInput.Vnd){
                this.totalDiscount = promotion?.node?.discount
            }else{
                if(this.subTotal && promotion?.node?.limitDiscount && (this.subTotal * promotion?.node?.discount / 100) > promotion?.node?.limitDiscount){
                    this.totalDiscount = promotion?.node?.limitDiscount
                }else{
                    this.totalDiscount = (this.subTotal * promotion?.node?.discount) / 100
                }
            }
        }

        this.activePromotion = null
        this.code = promotion?.node?.code
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }

}

export interface quantity {
    quantity: number;
}