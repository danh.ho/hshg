import { Injectable } from '@angular/core';
import { CartDeleteMutation, CartUpdateMutation } from '../shared/graphql/mutation';
import { CartNode, MutationCartDeleteArgs, MutationCartUpdateArgs } from '../shared/models/graphql.models';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(
    // private _getAllCart: GetAllCart,
    private _cartDelete: CartDeleteMutation,
    private _cartUpdate: CartUpdateMutation
  ) { }

  // getAllCart(args: CartNode) {
  //   return this._getAllCart.watch(args).valueChanges;
  // }

  cartDelete(args: MutationCartDeleteArgs) {
    return this._cartDelete.mutate(args);
  }

  cartUpdate(args: MutationCartUpdateArgs) {
    return this._cartUpdate.mutate(args);
  }
}
