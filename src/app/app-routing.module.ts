import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomePage } from './home/home.page';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'category',
    loadChildren: () => import('./category/category.module').then( m => m.CategoryModule)
  },
  {
    path: 'product-detail',
    loadChildren: () => import('./product-detail/product-detail.module').then( m => m.ProductDetailModule)
  },
  {
    path: 'blog',
    loadChildren: () => import('./flatpage/blog/blog.module').then( m => m.BlogModule)
  },
  {
    path: 'supermarket',
    loadChildren: () => import('./flatpage/supermarket/supermarket.module').then( m => m.SupermarketModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./flatpage/contact/contact.module').then( m => m.ContactModule)
  },
  { path: 'flatpage', 
    loadChildren: () => import('./flatpage/flatpage/flatpage.module').then(m => m.FlatpageModule) 
  },
  { path: 'book-party', 
  loadChildren: () => import('./flatpage/book-party/book-party.module').then(m => m.BookPartyModule) 
  },
  { path: 'my-bonus', loadChildren: () => import('./flatpage/my-bonus/my-bonus.module').then(m => m.MyBonusModule)},
  { path: 'about-us', loadChildren: () => import('./flatpage/about-us/about-us.module').then(m => m.AboutUsModule)},
  { path: 'cart', loadChildren: () => import('./cart/cart.module').then(m => m.CartModule) },
  { path: 'checkout', loadChildren: () => import('./checkout/checkout.module').then(m => m.CheckoutModule) },
  { path: 'success/:id', loadChildren: () => import('./shared/success/success.module').then(m => m.SuccessModule) },
  { path: 'my-account', loadChildren: () => import('./my-account/my-account.module').then(m => m.MyAccountModule) },
  { path: 'promotion', loadChildren: () => import('./promotion/promotion.module').then(m => m.PromotionModule), data: { q: 'news' } },
  { path: 'news', loadChildren: () => import('./news/news.module').then(m => m.NewsModule), data: { q: 'news' } },
  { path: 'recruit', loadChildren: () => import('./recruit/recruit.module').then(m => m.RecruitModule), data: { q: 'news' } },
  { path: 'general-policy', loadChildren: () => import('./general-policy/general-policy.module').then(m => m.GeneralPolicyModule), data: { q: 'news' } },
  { path: 'food-corner', loadChildren: () => import('./food-corner/food-corner.module').then(m => m.FoodCornerModule), data: { q: 'news' } },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { 
      // preloadingStrategy: PreloadAllModules,
      relativeLinkResolution: 'legacy',
      anchorScrolling: 'enabled',
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
