import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
    selector: 'tdl-app-artile-listing',
    templateUrl: './artile-listing.component.html',
    styleUrls: ['./artile-listing.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArtileListingComponent {
    @Input() contentItems: Array<NewsNode>;
    paginatorLength: number = 0;
    cartItemsNo: number;
    private _subscription = new Subscription();
    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        navSpeed: 700,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        navText: ['', ''],
        responsive: {
          0: {
            items: 1
          },
          425: {
            items: 2
          },
          768: {
            items: 3
          },
          1024: {
            items: 4
          }
        },
        nav: false,
        autoWidth: true
    }
    constructor(
      private _router: Router,
      private _store: Store
    ) { }
    replaceBlank = /\s/g;

    ngOnInit(): void {
      this._subscription.add(
        this._store.select(AuthState.isLogin).pipe(
          switchMap((isLogin) => {
            if (isLogin) {
              return this._store.select(ShoppingCartState.getMemberCart)
            } else {
              return this._store.select(ShoppingCartState.getGuestCart)
            }
          })
        ).subscribe(cart => {
          this.cartItemsNo = cart.length;
    }));
    }
    goToPromotionDetail(data){
      if(data?.url){
        window.location.href = data?.url
      }
  }
}
