import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'tdl-app-recruit',
    templateUrl: './recruit.component.html',
    styleUrls: ['./recruit.component.scss']
})
export class RecruitComponent implements OnInit {

    constructor(
        private _activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
    }

}
