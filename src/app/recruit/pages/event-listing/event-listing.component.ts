import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { NewsNode } from 'src/app/shared/models/graphql.models';
import { CmsNewsState, GetNews } from 'src/app/shared/store/tip-article';
import { SubSink } from 'subsink';

@Component({
    selector: 'tdl-app-event-listing',
    templateUrl: './event-listing.component.html',
    styleUrls: ['./event-listing.component.scss']
})
export class EventListingComponent implements OnInit {
    contentItems: Array<NewsNode>;
    _subsink = new SubSink()
    constructor(
        private _store: Store,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.loadEvent();
    }
    loadEvent() {
        // this._store.dispatch([new GetNews])
        // this._subsink.sink = this._store.select(CmsNewsState.getNews).subscribe(res => this.contentItems = res)
    }
    routerNews(id) {
        this.router.navigate([`/news/${id}`])
    }
}
