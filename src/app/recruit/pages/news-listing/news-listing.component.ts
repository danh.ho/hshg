import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { FlatpageNode, NewsNode } from 'src/app/shared/models/graphql.models';
import { GetNews, GetRecruit } from 'src/app/shared/store/tip-article';
import { CmsNewsState } from 'src/app/shared/store/tip-article'
import { Observable, Subscription } from 'rxjs';
import { FlatpageState, GetFlatpageByID } from 'src/app/shared/store/flatpage';
import { CmsState, GetGroupBanner } from 'src/app/shared/store/cms';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { IonContent } from '@ionic/angular';
import { AuthState } from 'src/app/shared/store/auth';
import { switchMap } from 'rxjs/operators';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
@Component({
    templateUrl: './news-listing.component.html',
    styleUrls: ['./news-listing.component.scss']
})
export class NewsListingComponent implements OnInit {
    @Select(CmsNewsState.getRecruit) news$: Observable<Array<NewsNode>>
    @Select(CmsNewsState.getTips) beautyTips$: Observable<Array<NewsNode>>
    @Select(CmsNewsState.getTraining) training$: Observable<Array<NewsNode>>
    @Select(FlatpageState.getFlatpageByID) flatpageContent$: Observable<FlatpageNode>;
    @ViewChild(IonContent) content: IonContent ;
    isShowBackToTop = false
    _subscription = new Subscription()
    private fragment: string;
    bannerList;
    cartItemsNo: number;
    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: true,
        navSpeed: 700,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 2000,
        autoplayTimeout: 5000,
        navText: ['', ''],
        responsive: {
          0: {
            items: 1
          },
        },
        nav: false,
        // dotsClass: 'owl-dots'
    }

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _store: Store,
        private route: ActivatedRoute,
    ) {
    }

    ngOnInit(): void {
        this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
        const { q } = this._activatedRoute.snapshot.data;
        switch (q) {
            case 'news':
                this._store.dispatch([new GetRecruit({categoryName: 'Tuyển dụng', orderBy:'-created'})])
                break;
        }
        this._subscription.add(
          this._store.select(AuthState.isLogin).pipe(
            switchMap((isLogin) => {
              if (isLogin) {
                return this._store.select(ShoppingCartState.getMemberCart)
              } else {
                return this._store.select(ShoppingCartState.getGuestCart)
              }
            })
          ).subscribe(cart => {
            this.cartItemsNo = cart.length;
      }));
        // this._store.dispatch(new GetFlatpageByID('9'));
    }
    ngAfterViewInit(): void {
        if (this.fragment) {
            const element = document.querySelector(`#${this.fragment}`);
            setTimeout(() => {
                element.scrollIntoView({ behavior: 'smooth' });
            }, 10);
        }

        setTimeout(() => {
            // this._store.dispatch(new GetGroupBanner());
            
            this._subscription.add(this._store.select(CmsState.getBannerGroup)
              .subscribe(res => {
                if (res) {
                  const main = res.filter(ele => ele.itemCode == 'promotion')
                  this.bannerList = main[0]?.banner.edges;
                }
              }
              ));
          }, 600);
    }
    scrollToTop() {    
        this.content.scrollToTop(700);
    }
    logScrolling(event){
        if(event.detail.scrollTop > 100){
          this.isShowBackToTop = true;
        } else {
          this.isShowBackToTop = false;
        }
    }
}
