import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { NewsComponent } from './promotion.component';
// import { BeautyTipsListingComponent } from './pages/beauty-tips-listing/beauty-tips-listing.component';
// import { EventListingComponent } from './pages/event-listing/event-listing.component';
import { NewsDetailComponent } from './pages/news-detail/news-detail.component';
import { NewsListingComponent } from './pages/news-listing/news-listing.component';

const routes: Routes = [
    { path: '', component: NewsListingComponent, },
    // { path: 'events', component: EventListingComponent, },
    // { path: 'beauty-tips', component: BeautyTipsListingComponent, },
    { path: ':id', component: NewsDetailComponent,  },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class NewsRoutingModule { }
