
import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { QuerySiteDeliveriesMethodArgs, Query as ParentQuery, } from 'src/app/shared/models/graphql.models';


const getAllDeliveryMethodSite = gql`
query AllDeliveryMethodSite(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int,
  $isActive: String
  ) {
    siteDeliveriesMethod(
    before: $before, after: $after, 
    first: $first, last: $last, 
    isActive: $isActive
) {
    edges {
        node {
          deliveryMethod {
            description
            id
            itemCode
            name
          }
          id
          isActive
          site {
            id
            email
            name
            isActive
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
      totalCount
  }
}
`;

@Injectable({ providedIn: 'root' })
export class GetAllDeliveryMethodSite extends Query<ParentQuery, QuerySiteDeliveriesMethodArgs> {
  document = getAllDeliveryMethodSite
  ;
}
