
import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QuerySitePaymentMethodArgs } from 'src/app/shared/models/graphql.models';
// import {
//   Query as ParentQuery,
//     QuerySitePaymentMethodArgs,
// } from '@shared/models/graphql.models';

const getAllPaymentMethodSite = gql`
query AllPaymentMethodSite(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int, 
  $orderBy: String, 
  $site: String,
  $paymentMethod: String
  ) {
    sitePaymentMethod(
    before: $before, after: $after, 
    first: $first, last: $last,
    orderBy: $orderBy, 
    site: $site,
    paymentMethod: $paymentMethod
) {
    edges {
        node {
          id
          isActive
          paymentMethod {
            description
            id
            name
            itemCode
          }
          site {
            id
            name
            email
          }
        }
    }
    pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
    }
    totalCount
  }
}
`;

@Injectable({ providedIn: 'root' })
export class GetAllPaymentMethodSite extends Query<ParentQuery, QuerySitePaymentMethodArgs> {
  document = getAllPaymentMethodSite
  ;
}
