import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QueryPromotionCodesArgs } from 'src/app/shared/models/graphql.models';

const getAllPromotionCode = gql`
query Promotion(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int,
  $beforeStartTime: DateTime,
  $afterEndTime: DateTime,
  $limitAmount: Float,
  $isPublic: Boolean
)  {
  promotionCodes(
    before: $before, 
    after: $after, 
    first: $first, 
    last: $last,
    beforeStartTime: $beforeStartTime,
    afterEndTime: $afterEndTime,
    limitAmount: $limitAmount,
    isPublic: $isPublic
  ) {
    edges {
      node {
        code
        description
        discount
        endTime
        image
        id
        isActive
        limitDiscount
        quantity
        startTime
        title
        unitDiscount
        limitAmount
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
    totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllPromotionCodeQuery extends Query<ParentQuery, QueryPromotionCodesArgs> {
  document = getAllPromotionCode;
}