import { Injectable } from '@angular/core';
import { Mutation as ApolloMutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation, MutationOrderUserCreateArgs,  } from 'src/app/shared/models/graphql.models';

@Injectable({ providedIn: 'root' })
export class OrderUserCreate extends ApolloMutation<
  Mutation,
  MutationOrderUserCreateArgs
> {
  document = gql`
    mutation OrderUserCreate($input: UserOrderCreateInput!){
        orderUserCreate(input: $input) {
            errors {
              code
              field
              message
            }
            status
            order {
                created
                id
                totalAmount
                customer {
                  firstName
                  lastName
                  id
                  phone
                }
            }
        }
    }
  `;
}