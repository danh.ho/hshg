import { Injectable } from '@angular/core';
import { Mutation as ApolloMutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation, MutationOrderGuestCreateArgs,  } from 'src/app/shared/models/graphql.models';

@Injectable({ providedIn: 'root' })
export class OrderGuestCreate extends ApolloMutation<
  Mutation,
  MutationOrderGuestCreateArgs
> {
  document = gql`
    mutation OrderGuestCreateMutation($input: UserGuestInput!){
        orderGuestCreate(input: $input) {
            errors {
              code
              field
              message
            }
            status
        }
    }
  `;
}
