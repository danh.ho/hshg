import { OrderNode, QueryPromotionCodesArgs, QuerySiteDeliveriesMethodArgs, QuerySitePaymentMethodArgs } from 'src/app/shared/models/graphql.models';

  const enum Actions {
    ORDER_CARTS = '[Cart] Order Cart',
    ORDER_CART_SUCCESSFUL = '[Cart] Order Cart Successfully',
    ORDER_CART_FAILED = '[Cart] Order Cart Failed',
    ORDER_CARTS_GUEST = '[Cart] Order Cart Guest',
    ORDER_CART_GUEST_SUCCESSFUL = '[Cart] Order Cart Guest Successfully',
    ORDER_CART_GUEST_FAILED = '[Cart] Order Cart Guest Failed',
    LOAD_SITE_PAYMENT_METHOD = "[Load All Site Payment Method] Load All Site Payment Method",
    LOAD_SITE_DELIVERY_METHOD = "[Load All Site Delivery Method] Load All Site Delivery Method",
    LOAD_ALL_PROMOTION_CODE = "[Load All Promotion Code] Load All Promotion Code",
    LOAD_ALL_PROMOTION_CODE_BY_CODE = "[Load All Promotion Code By Code] Load All Promotion Code By Code"
  }

  export class OrderCart {
    static readonly type = Actions.ORDER_CARTS;
    constructor(public readonly payload: any) {}
  }
  
  export class OrderCartSuccessful {
    static readonly type = Actions.ORDER_CART_SUCCESSFUL;
    constructor(public readonly payload: OrderNode) {}
  }

  export class OrderCartFailed {
    static readonly type = Actions.ORDER_CART_FAILED;
    constructor(public readonly payload: any) {}
  }

  export class OrderCartGuest {
    static readonly type = Actions.ORDER_CARTS_GUEST;
    constructor(public readonly payload: any) {}
  }
  
  export class OrderCartGuestSuccessful {
    static readonly type = Actions.ORDER_CART_GUEST_SUCCESSFUL;
    constructor(public readonly payload: any) {}
  }

  export class OrderCartGuestFailed {
    static readonly type = Actions.ORDER_CART_GUEST_FAILED;
    constructor(public readonly payload: any) {}
  }
  export class LoadSitePaymentMethod {
    static readonly type = Actions.LOAD_SITE_PAYMENT_METHOD;
    constructor(public readonly payload?: QuerySitePaymentMethodArgs) {
    }
  }


export class LoadSiteDeliveryMethod {
  static readonly type = Actions.LOAD_SITE_DELIVERY_METHOD;
  constructor(public readonly payload?: QuerySiteDeliveriesMethodArgs) {}
}

export class LoadPromotionCode {
  static readonly type = Actions.LOAD_ALL_PROMOTION_CODE;
  constructor(public readonly payload?: QueryPromotionCodesArgs) {}
}

export class LoadPromotionCodeByCode {
  static readonly type = Actions.LOAD_ALL_PROMOTION_CODE_BY_CODE;
  constructor(public readonly payload?: QueryPromotionCodesArgs) {}
}