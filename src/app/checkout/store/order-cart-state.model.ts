import { MutationOrderUserCreateArgs, PromotionCodeNodeConnection, PromotionNode, SiteDeliveryMethodNode, SiteDeliveryMethodNodeConnection, SitePaymentMethodNodeConnection, SitePaymentNode } from 'src/app/shared/models/graphql.models';
  
  export interface OrderCartStateModel {
    selectedNode?: MutationOrderUserCreateArgs;
    nodeConnectionPayment?: SitePaymentMethodNodeConnection;
    selectedNodePayment?: SitePaymentNode;
    nodeConnectionDelivery?: SiteDeliveryMethodNodeConnection;
    selectedNodeDelivery?: SiteDeliveryMethodNode;
    nodeConnectionPromotion?: PromotionCodeNodeConnection;
    selectedNodePromotion?: PromotionNode;
    nodeConnectionPromotionByCode?: PromotionCodeNodeConnection;
  }
  
  export const initialState: OrderCartStateModel = {};