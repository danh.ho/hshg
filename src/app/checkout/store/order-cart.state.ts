import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { LoadPromotionCode, LoadPromotionCodeByCode } from './order-cart.actions';
import { CheckoutService } from '../checkout.service';
import { OrderCartStateModel, initialState } from './order-cart-state.model';
import { LoadSiteDeliveryMethod, LoadSitePaymentMethod, OrderCart, OrderCartFailed, OrderCartGuest, 
    OrderCartGuestFailed, OrderCartGuestSuccessful, OrderCartSuccessful } from './order-cart.actions';

@Injectable({ providedIn: 'root' })
@State<OrderCartStateModel>({
    name: 'checkout',
    defaults: initialState,
})
export class OrderCartState {
    @Selector()
    static getCartNode({ selectedNode }: OrderCartStateModel) {
        return selectedNode;
    }
    @Selector()
    static getNodeConnectionPayment({ nodeConnectionPayment }: OrderCartStateModel) {
        return nodeConnectionPayment;
    }
    @Selector()
    static getSelectedNodePayment({ selectedNodePayment }: OrderCartStateModel) {
        return selectedNodePayment;
    }

    @Selector()
    static getNodeConnectionDelivery({ nodeConnectionDelivery }: OrderCartStateModel) {
      return nodeConnectionDelivery;
    }
    @Selector()
    static getSelectedNodeDelivery({ selectedNodeDelivery }: OrderCartStateModel) {
      return selectedNodeDelivery;
    }

    @Selector()
    static getNodeConnectionPromotionCode({ nodeConnectionPromotion }: OrderCartStateModel) {
      return nodeConnectionPromotion;
    }
    @Selector()
    static getSelectedNodePromotionCode({ selectedNodePromotion }: OrderCartStateModel) {
      return selectedNodePromotion;
    }

    @Selector()
    static getNodeConnectionPromotionCodeByCode({ nodeConnectionPromotionByCode }: OrderCartStateModel) {
      return nodeConnectionPromotionByCode;
    }

    constructor(private _apiService: CheckoutService) { }

    @Action(OrderCart, { cancelUncompleted: true })
    orderCart(
        { dispatch }: StateContext<OrderCartStateModel>,
        { payload }: OrderCart
    ) {
        return this._apiService.orderCart({ input: payload }).pipe(
            tap(({ data }) => {
                const { errors, status, order } = data.orderUserCreate;
                if (status) {
                    return dispatch(new OrderCartSuccessful(order));
                }
                return dispatch(new OrderCartFailed(errors));
            })
        );
    }

    @Action(OrderCartGuest, { cancelUncompleted: true })
    orderCartGuest(
        { dispatch }: StateContext<OrderCartStateModel>,
        { payload }: OrderCartGuest
    ) {
        return this._apiService.orderCartGuest({ input: payload }).pipe(
            tap(({ data }) => {
                const { errors, status } = data.orderGuestCreate;
                if (status) {
                    return dispatch(new OrderCartGuestSuccessful(status));
                }
                return dispatch(new OrderCartGuestFailed(errors));
            })
        );
    }
    @Action(LoadSitePaymentMethod, { cancelUncompleted: true })
    loadAllSitePaymentMethod(
        { patchState }: StateContext<OrderCartStateModel>,
        { payload }: LoadSitePaymentMethod
    ) {
    return this._apiService.getAllSitePaymentMethod({ ...payload })
        .pipe(
            tap(({ data }) => {
                patchState({
                    nodeConnectionPayment: data.sitePaymentMethod,
                });
            })
        );
    }

    @Action(LoadSiteDeliveryMethod, { cancelUncompleted: true })
    loadAllSiteDeliveryMethod(
        { patchState }: StateContext<OrderCartStateModel>,
        { payload }: LoadSiteDeliveryMethod
    ) {
    return this._apiService.getAllSiteDeliveryMethod({ ...payload })
        .pipe(
            tap(({ data }) => {
                patchState({
                    nodeConnectionDelivery: data.siteDeliveriesMethod,
                });
            })
        );
    }

    @Action(LoadPromotionCode, { cancelUncompleted: true })
    loadAllPromotionCode(
        { patchState }: StateContext<OrderCartStateModel>,
        { payload }: LoadPromotionCode
    ) {
    return this._apiService.getAllPromotionCode({ ...payload })
        .pipe(
            tap(({ data }) => {
                patchState({
                    nodeConnectionPromotion: data.promotionCodes,
                });
            })
        );
    }

    @Action(LoadPromotionCodeByCode, { cancelUncompleted: true })
    loadAllPromotionCodeByCode(
        { patchState }: StateContext<OrderCartStateModel>,
        { payload }: LoadPromotionCodeByCode
    ) {
    return this._apiService.getAllPromotionCode({ ...payload })
        .pipe(
            tap(({ data }) => {
                patchState({
                    nodeConnectionPromotionByCode: data.promotionCodes,
                });
            })
        );
    }
}
