import { Injectable } from '@angular/core';
import { MutationOrderGuestCreateArgs, MutationOrderUserCreateArgs, ProductForBuyNodeEdge, ProductNode, QueryPromotionCodesArgs, QuerySiteDeliveriesMethodArgs, QuerySitePaymentMethodArgs } from '../shared/models/graphql.models';
import { OrderGuestCreate, OrderUserCreate } from './shared/graphql/mutations';
import { map } from 'rxjs/operators';
import {environment} from '../../environments/environment'
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { GetAllPaymentMethodSite } from './shared/query/get-payment-method.query';
import { GetAllDeliveryMethodSite } from './shared/query/get-all-delivery.query';
import { GetAllPromotionCodeQuery } from './shared/query/get-all-promotion-code.query';
var Buffer = require('buffer/').Buffer

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
    constructor(
        private _orderCart: OrderUserCreate,
        private _orderCartGuest: OrderGuestCreate,
        private _store: Store,
        private _actions: Actions,
        private _spinner: NgxSpinnerService,
        private _getAllPaymentMothod: GetAllPaymentMethodSite,
        private _getAllDeliveryMothodSite: GetAllDeliveryMethodSite,
        private _getAllPromotionCode: GetAllPromotionCodeQuery
    ) { }

    _subscription = new Subscription()
    
    orderCart(args: MutationOrderUserCreateArgs) {
        return this._orderCart.mutate(args);
    }

    orderCartGuest(args: MutationOrderGuestCreateArgs) {
        return this._orderCartGuest.mutate(args);
    }
    getAllSitePaymentMethod(args?: QuerySitePaymentMethodArgs) {
        return this._getAllPaymentMothod.watch(args).valueChanges;
    }

    getAllSiteDeliveryMethod(args?: QuerySiteDeliveriesMethodArgs) {
        return this._getAllDeliveryMothodSite.watch(args).valueChanges;
    }

    getAllPromotionCode(args?: QueryPromotionCodesArgs) {
        return this._getAllPromotionCode.watch(args).valueChanges;
    }
    // public onePayRedirect(amount: number, orderNo: string, id: string, vpc_TicketNo: string, createdDate: string) {
    //     let SECURE_SECRET = Buffer.from(this.configService.onePayConfig.SECURE_SECRET, 'hex'),
    //     // vpcUnordered = {
    //     //     ... this.configService.onePayConfig.vpcs, vpc_Amount: amount * 100, vpc_MerchTxnRef: orderNo + '-' + moment.utc(createdDate).format('YYYYMMDDHHmmss'),
    //     //     vpc_OrderInfo: orderNo + ' - YSD', vpc_ReturnURL: `${window.location.origin}/success/${id}`,
    //     //     vpc_TicketNo,
    //     // };
        
    //     vpcUnordered = {
    //         ... this.configService.onePayConfig.vpcs, 
    //         vpc_Amount: amount * 100, vpc_MerchTxnRef: orderNo + '-' + moment.utc(createdDate).format('YYYYMMDDHHmmss'),
    //         vpc_OrderInfo: orderNo + ' - YSD', vpc_ReturnURL: `${environment.urlRedirect}order/success/${id}`,
    //         vpc_TicketNo,
    //     };
    //     const vpcOrdered = {};
    //     Object.keys(vpcUnordered).sort().forEach((key) => vpcOrdered[key] = vpcUnordered[key]);

    //     localStorage.setItem("payment", id)

    //     const vpcUnorderedSave = {
    //         ... this.configService.onePayConfig.vpcs, 
    //         vpc_Amount: amount * 100, 
    //         vpc_MerchTxnRef: orderNo + '-' + moment.utc(createdDate).format('YYYYMMDDHHmmss'),
    //         vpc_OrderInfo: orderNo + ' - YSD', vpc_TicketNo,
    //     };
    //     const vpcOrderedSaveBE = {};
    //     Object.keys(vpcUnorderedSave).sort().forEach((key) => vpcOrderedSaveBE[key] = vpcUnorderedSave[key]);
    //     const tempData = {
    //         hashKey: this.hashKeyGenerate(this.vpcProcess(vpcOrderedSaveBE), SECURE_SECRET),
    //         data: vpcOrderedSaveBE
    //     }

    //     this._store.dispatch(new OrderCustomerUpdateStatus({id: id, paymentReturnData: JSON.stringify(tempData)}))
    //     this._subscription.add(this._actions
    //     .pipe(
    //         ofActionSuccessful(OrderCustomerUpdateStatusSuccessfully),
    //     )
    //     .subscribe((translation) => {
    //         if(translation){
    //             window.location.href = this.paymentUrlGenerate(this.vpcProcess(vpcOrdered), this.hashKeyGenerate(this.vpcProcess(vpcOrdered), SECURE_SECRET)).replace(/\s/gi, '%20');
    //         }
    //     }));
    // }

    // private paymentUrlGenerate(vpcString: string, hashKey: string) {
    //     let redirectUrl = '';
    //     const paymentUrl = this.configService.onePayConfig.urls.paymentUrl;
    //     redirectUrl += paymentUrl + 'AgainLink=AgainLink&Title=Title&' + vpcString + '&vpc_SecureHash=' + hashKey;
    //     return redirectUrl;
    // }

    // private vpcProcess(vpc: Object) {
    //     let string = '';
    //     for (let key in vpc) {
    //         string += key + '=' + vpc[key] + '&';
    //     }
    //     const regex = /\\s/;
    //     return string.substring(0, string.length - 1);
    // }

    // private hashKeyGenerate(vpcsString: string, SECURE_SECRET: any) {
    //     return sha256.hmac(SECURE_SECRET, vpcsString).toUpperCase();
    // }

    // public getIPAdress() {
    //     return this._httpClient.get('https://api.ipify.org/?format=json').pipe(
    //         map((res: { ip: string }) => res.ip)
    //     )
    // }

}
