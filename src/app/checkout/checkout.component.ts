import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { IonContent, ViewWillEnter } from '@ionic/angular';
import { Actions, ofActionCompleted, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription, Observable } from 'rxjs';
import { filter, switchMap, take, tap } from 'rxjs/operators';
import { AddAddressInput, CartDetailNode, DistrictNodeConnection, OrderStatusInput, PaymentNode, PaymentNodeConnection, PaymentStatusInput, ProductInput, ProvinceNodeConnection, SiteDeliveryMethodNodeConnection, SitePaymentMethodNodeConnection, UserAddress, UserAddressNode, UserGuestInput, UserOrderCreateInput, WardNodeConnection } from 'src/app/shared/models/graphql.models';
import { AuthState } from 'src/app/shared/store/auth';
import { GUEST_ITEM_CARD } from 'src/app/shared/store/shopping-cart';
import {
    AddUserAddress,
    CreateGuestOrder, CreateGuestOrderFailed,
    CreateGuestOrderSuccessfully, CreateUserOrder,  EditUserAddress, GetMemberShoppingCartId, GetPaymentMethodList
} from 'src/app/shared/store/shopping-cart/shopping-card.action';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart/shopping-card.state';
import { environment } from 'src/environments/environment';
import { SubSink } from 'subsink';
import { LoadListProductsForBuy, LoadProductsForBuy, ProductForBuyState } from '../category/store';
import { AddressState, LoadAddresses } from '../my-account/components/profile-address/store';
import { LoadDistrict, LoadProfile, LoadProvince, LoadWard, ProfileState } from '../my-account/shared/states';
import { NotificationService } from '../shared/services';
import { BranchState } from '../shared/store/branch/branch.state';

import { CheckoutService } from './checkout.service';
import { LoadSiteDeliveryMethod, LoadSitePaymentMethod, OrderCart, OrderCartFailed, OrderCartGuest, OrderCartGuestFailed, OrderCartGuestSuccessful, OrderCartState, OrderCartSuccessful } from './store';
import { VnPay, VnPayConnect } from './vnpay/models';

@Component({
    selector: 'cos-frontend-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent
    implements OnInit, OnDestroy, ViewWillEnter {
    @Select(ProfileState.getNodeConnectionProvince) provinces$: Observable<ProvinceNodeConnection>;
    @Select(ProfileState.getNodeConnectionDistrict) districts$: Observable<DistrictNodeConnection>;
    @Select(ProfileState.getNodeConnectionWard) wards$: Observable<WardNodeConnection>;
    @Select(ShoppingCartState.getPaymentMethodList) paymentMethodList$: Observable<Array<PaymentNode>>;
    @Select(ShoppingCartState.getGuestCart) guestCart$: Observable<Array<GUEST_ITEM_CARD>>
    @Select(ShoppingCartState.getMemberCart) memberCart$: Observable<Array<CartDetailNode>>;
    @Select(OrderCartState.getNodeConnectionPayment) paymentMethod$: Observable<SitePaymentMethodNodeConnection>;
    @Select(OrderCartState.getNodeConnectionDelivery) siteDelivery$: Observable<SiteDeliveryMethodNodeConnection>;
    @ViewChild(IonContent) content: IonContent ;
    isShowBackToTop = false

    isLinear = false;
    public subTotal = 0
    public checkCart = false
    private _subscription = new Subscription();
    isLogin: boolean;
    subsink = new SubSink();
    formGroup: FormGroup;
    formGroupUser: FormGroup;
    product: Array<ProductInput>;
    addressId: string = '';
    selectedId: string;
    idExpand;
    data;
    cartId;
    id;
    userAddress;
    addressFilter;
    addressUserNew;
    addressUserLast;
    nodeConnection: any;
    _subSink = new SubSink();
    selectedCar;
    showPayment = false;
    productGuest;
    cartGuest;
    checkIsDeliveryMethod = false
    checkDisabledDelivery = false
    checkRequiedAddress = true
    deliveryBefore = '';
    selectedPaymentMethod = '';
    totalDiscount = 0
    totalAmountTopping = 0;
    totalAmountToppingGuest = 0;
    code = "0"
    checkBeforePayment = "";
    formatAddress;
    cartItemsNo: number;
    constructor(
        private _notify: NotificationService,
        private _formBuilder: FormBuilder,
        private router: Router,
        private _store: Store,
        private _actions: Actions,
        private _action: Actions,
        private _cdRef: ChangeDetectorRef,
        private route: ActivatedRoute,
        private _spinner: NgxSpinnerService,
        private _checkoutService: CheckoutService,
        domSanitizer: DomSanitizer,
        matIconRegistry: MatIconRegistry,
    ) {
        if(JSON.parse(localStorage.getItem("payment"))){
            this.router.navigate(
                ["/success/" + JSON.parse(localStorage.getItem("payment"))], 
                { queryParams: {payment_success: false}}
            )
        }
        matIconRegistry.addSvgIcon("ship-60-logo",
            domSanitizer.bypassSecurityTrustResourceUrl('assets/icon/ship_60.svg')
        )
        matIconRegistry.addSvgIcon("in-house-logo",
            domSanitizer.bypassSecurityTrustResourceUrl('assets/icon/in_house.svg')
        )
    }

    ionViewWillEnter(){
        const discount = localStorage.getItem("discount")
        if(discount){
            this.totalDiscount = JSON.parse(discount).total
            this.code = JSON.parse(discount).code
        }
    }

    ngOnInit() {
        this.formGroup = this._formBuilder.group({
            fullName: ['', [Validators.required]],
            phone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
            email: ['', [Validators.required, Validators.email]],
            paymentMethodId: ['', [Validators.required]],
            orderNote: [''],
            city: ['', [Validators.required]],
            street: ['', Validators.required],
            ward: ['', [Validators.required]],
            addressNote: [''],
            district: ['', [Validators.required]],
            deliveryMethod: ['']
        });

        this.formGroupUser = this._formBuilder.group({
            receiver: ['', [Validators.required]],
            phone: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
            paymentMethodId: ['', [Validators.required]],
            orderNote: [''],
            city: ['', [Validators.required]],
            street: ['', Validators.required],
            //addressNote: [''],
            district: ['', [Validators.required]],
            ward: ['', [Validators.required]],
            title: [''],
            deliveryMethod: ['']
        })

        this._subSink.sink = this._actions
            .pipe<LoadAddresses>(ofActionCompleted(LoadAddresses))
            .pipe(switchMap(() => this._store.select(AddressState.getAddressNode)))
            .subscribe(addresses => {
                
                this.userAddress = addresses;
                if (this.userAddress.length > 0) {
                    
                    this.addressFilter = this.userAddress.find(ele => ele.isDefault == true);
                    this.addressId = this.addressFilter.id;
                }
                else {
                    this.addressFilter = this.userAddress.pop();
                    this.addressId = this.addressFilter.id;
                }

                this.formGroupUser.patchValue({
                    title: this.addressFilter.id,
                    receiver: this.addressFilter.receiver,
                    phone: this.addressFilter.phone,
                    street: this.addressFilter.street,
                    district: this.addressFilter.district?.id,
                    city: this.addressFilter.province?.id,
                    ward: this.addressFilter.ward?.id,
                })
            })
        this._store.select(AuthState.isLogin).pipe(
            take(2),
            tap(res => {
                this.isLogin = res;
            })
        ).subscribe();
        if (this.isLogin) {
            this._subscription.add(this._store
                .select(ShoppingCartState.getMemberCart)
                .subscribe(res => {
                    this.totalAmountTopping = 0;
                    this.subTotal = 0;
                    res.forEach(ele => {
                        if(ele.product.flashSaleProduct?.edges.length > 0){
                            this.subTotal += ele.product.flashSaleProduct?.edges[0].node.price * ele.quantity
                        } else {
                            ele?.subCartDetail.forEach(e => {
                                this.totalAmountTopping += e.product.price * e.quantity
                            })
                            this.subTotal += ele.product.price * ele.quantity + this.totalAmountTopping
                            this.totalAmountTopping = 0;
                        }
    
                    })
                }));
            this._store.dispatch(new GetMemberShoppingCartId);
            this._subscription.add(this._store
                .select(ShoppingCartState.getMemberCartID)
                .subscribe(res => {
                    this.cartId = res;

                }));
            this.loadAddressUser();
            this.loadDataProfile();
        } else {
            this._subscription.add(this._store.select(ShoppingCartState.getGuestCart).pipe(
                tap(res => {

                    this.subTotal = 0;
                    this.cartGuest = res;
                    this.totalAmountToppingGuest = 0;

                    this.cartGuest?.forEach(ele => {
                        ele?.subCartDetail?.forEach(e => {
                            this.totalAmountToppingGuest += e.product.price * e.quantity
                        })
                        this.subTotal += ele.product.price * ele.quantity + this.totalAmountToppingGuest
                        this.totalAmountToppingGuest = 0;
                    });
                    // res.forEach(ele => {
                    //     if(ele.product.flashSaleProduct?.edges.length > 0){
                    //         this.subTotal += ele.product.flashSaleProduct?.edges[0].node.price * ele.quantity
                    //     } else {
                    //         this.subTotal += ele.product.price * ele.quantity
                    //     }
    
                    // })
                })
            ).subscribe(() => {
                // this._store.dispatch(new LoadProductsForBuy());
                this._subscription.add(this._store
                    .select(ProductForBuyState.getListNodeConnection)
                    .subscribe((res) => {
                        if(res){
                            const product = res.edges;
                            this.productGuest = res.edges;
                            if(this.productGuest.length > 0){
                                this.productGuest = [];
                                this.productGuest = res.edges;
                            }

                        }
    
                    }
                    )
                )
            }));
        }
        this.paymentMethodList$.pipe(
            take(2),
            tap(res => {
                if (res.length > 0) {
                    this.formGroup.controls.paymentMethodId.setValue(res[0].id)
                }
            })
        ).subscribe();
        this._store.dispatch([new GetPaymentMethodList])

        if (this.isLogin) {
            this.memberCart$.subscribe(data => {
                this.id = data[0]?.id
            })
        } else {
            this.guestCart$.subscribe(data => {
                this.id = data[0]?.product.id
            })
        }
        if (this.isLogin) {
            this.checkCart = true
        } else {
            this.checkCart = false
        }
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
        this._initData();

        this._initDataProvince();
        this._subscription.add(
            this._store
                .select(BranchState.getSelectedNode)
                .subscribe((data) => {
                    if (data) {
                        this.formatAddress = data.formatAddress
                    }
                })
        );
    }
    
    scrollToTop() {    
        this.content.scrollToTop(700);
    }

    logScrolling(event){
        if(event.detail.scrollTop > 100){
          this.isShowBackToTop = true;
        } else {
          this.isShowBackToTop = false;
        }
    }
    private _initData() {
        if (!this.isLogin) {
            this._subscription.add(this._actions.pipe(
                ofActionSuccessful(CreateGuestOrderSuccessfully),
            ).subscribe());

            this._subscription.add(this._actions.pipe(
                ofActionSuccessful(CreateGuestOrderFailed),
            ).subscribe())
        }
        this._store.dispatch([new LoadSitePaymentMethod()])
        this._store.dispatch(new LoadSiteDeliveryMethod({isActive: "true"}))
        
        this._subscription.add(
            this._store.select(OrderCartState.getNodeConnectionDelivery)
            .subscribe(data => {
                if(data?.edges && data?.edges.length > 0){
                    this.formGroup.patchValue({
                        deliveryMethod: data?.edges[0]?.node?.deliveryMethod?.id
                    })
                    this.formGroupUser.patchValue({
                        deliveryMethod: data?.edges[0]?.node?.deliveryMethod?.id
                    })
                    this.checkIsDeliveryMethod = true
                }else{
                    this.checkIsDeliveryMethod = false
                }
            })
        )

        this._subscription.add(
            this._store.select(OrderCartState.getNodeConnectionPayment)
            .subscribe(data => {
                if(data?.edges && data?.edges.length > 0){
                    this.formGroup.patchValue({
                        paymentMethodId: data?.edges[0]?.node?.paymentMethod?.id
                    })
                    this.formGroupUser.patchValue({
                        paymentMethodId: data?.edges[0]?.node?.paymentMethod?.id
                    })
                    if(data?.edges[0]?.node?.paymentMethod?.itemCode == "DIRECT"){
                        this.checkDisabledDelivery = true
                    }else{
                        this.checkDisabledDelivery = false
                    }
                }
            })
        )
    }

    loadDataProfile() {
        this._store.dispatch(new LoadProfile());
        this._subscription.add(this._store
            .select(ProfileState.getProfileNode)
            .subscribe(this._fillDataSource.bind(this)));
    }
    _fillDataSource(nodeConnection: any) {
        if (!nodeConnection) {
            return
        }
        if (nodeConnection.__typename === "Query") {
            this.data = nodeConnection.userProfile;
        } else if (nodeConnection.__typename === "UserProfile") {
            this.data = nodeConnection
        }
        this._cdRef.detectChanges();

    }
    loadAddressUser() {
        this._store.dispatch(new LoadAddresses());
    }

    _initDataProvince(){
        this._store.dispatch(new LoadProvince())
    }

    _initDataDistrict(province){
        this._store.dispatch(new LoadDistrict({province}))
    }

    _initDataWard(district){
        this._store.dispatch(new LoadWard({district}))
    }

    changeProvince(event, province){
        if (event.source.selected) {
            this._initDataDistrict(province?.node?.id)
        }
    }

    changeDistrict(event, district){
        if (event.source.selected) {
            this._initDataWard(district?.node?.id)
        }
    }

    checkoutWithUser(params) {
        this._store.dispatch(new OrderCart(params))
        this._subscription.add(this._actions
            .pipe(
                ofActionSuccessful(OrderCartFailed))
            .subscribe((translation) => {
            }));
        this._subscription.add(this._actions
        .pipe(
            ofActionSuccessful(OrderCartSuccessful))
        .subscribe((translation) => {
            this.router.navigate(['/success']);
        }));
    }

    checkoutWithGuest(params) {
        this._store.dispatch(new OrderCartGuest(params))
        this._subscription.add(this._actions
            .pipe(
                ofActionSuccessful(OrderCartGuestFailed))
            .subscribe((translation) => {

            }));
        this._subscription.add(this._actions
            .pipe(
                ofActionSuccessful(OrderCartGuestSuccessful))
            .subscribe((translation) => {
                this.router.navigate(['/success'])
            }));
    }
    changeExpand(id, data){
        this.idExpand = id
        if(data?.itemCode == "DIRECT"){
            this.formGroup.patchValue({
                deliveryMethod: undefined
            })
            this.formGroupUser.patchValue({
                deliveryMethod: undefined
            })
            this.checkDisabledDelivery = true
            this.deliveryBefore = "SUPERMARKET"
            this.checkRequiedAddress = false
            this.checkRequiedAddress = false
            this.formGroup.controls["city"].setValidators([]);
            this.formGroup.controls['city'].updateValueAndValidity()

            this.formGroup.controls["street"].setValidators([]);
            this.formGroup.controls['street'].updateValueAndValidity()

            this.formGroup.controls["district"].setValidators([]);
            this.formGroup.controls['district'].updateValueAndValidity()

            this.formGroup.controls["ward"].setValidators([]);
            this.formGroup.controls['ward'].updateValueAndValidity()

            this.formGroupUser.controls["city"].setValidators([]);
            this.formGroupUser.controls['city'].updateValueAndValidity()

            this.formGroupUser.controls["street"].setValidators([]);
            this.formGroupUser.controls['street'].updateValueAndValidity()

            this.formGroupUser.controls["district"].setValidators([]);
            this.formGroupUser.controls['district'].updateValueAndValidity()

            this.formGroupUser.controls["ward"].setValidators([]);
            this.formGroupUser.controls['ward'].updateValueAndValidity()
        }else{
            this.checkDisabledDelivery = false
            this.checkRequiedAddress = true

            if(this.checkBeforePayment == 'DIRECT'){
                this.formGroupUser.patchValue({
                    deliveryMethod: "1"
                })
                this.formGroup.patchValue({
                    deliveryMethod: "1"
                })
    
                this.formGroup.controls["city"].setValidators([Validators.required]);
                this.formGroup.controls['city'].updateValueAndValidity()
    
                this.formGroup.controls["street"].setValidators([Validators.required]);
                this.formGroup.controls['city'].updateValueAndValidity()
    
                this.formGroup.controls["district"].setValidators([Validators.required]);
                this.formGroup.controls['district'].updateValueAndValidity()
    
                this.formGroup.controls["ward"].setValidators([Validators.required]);
                this.formGroup.controls['ward'].updateValueAndValidity()
    
                this.formGroupUser.controls["city"].setValidators([Validators.required]);
                this.formGroupUser.controls['city'].updateValueAndValidity()
    
                this.formGroupUser.controls["street"].setValidators([Validators.required]);
                this.formGroupUser.controls['city'].updateValueAndValidity()
    
                this.formGroupUser.controls["district"].setValidators([Validators.required]);
                this.formGroupUser.controls['district'].updateValueAndValidity()
    
                this.formGroupUser.controls["ward"].setValidators([Validators.required]);
                this.formGroupUser.controls['ward'].updateValueAndValidity()
            }
        }

        this.checkBeforePayment = data?.itemCode
    }

    public orderCreate(formValue: UserGuestInput = this.formGroup.value, items: Array<GUEST_ITEM_CARD>) {
        if (this.formGroup.valid) {
            if (!this.isLogin) {
                const  { 
                    fullName,
                    phone,
                    email,
                    paymentMethodId,
                    orderNote,
                    city: province,
                    street,
                    ward,
                    addressNote,
                    district,
                    deliveryMethod: deliveryMethodId
                } 
                = this.formGroup.value
                const dataAddress = {
                    fullName,
                    phone,
                    email,
                    paymentMethodId,
                    orderNote,
                    province: this.checkRequiedAddress ? province : undefined,
                    street: this.checkRequiedAddress ? street : undefined,
                    ward: this.checkRequiedAddress ? ward : undefined,
                    addressNote,
                    district: this.checkRequiedAddress ? district : undefined,
                    deliveryMethodId,
                    promotionCodeCode: (this.code && this.code != "0") ? this.code : undefined
                }
                this._spinner.show()
                const paymentStatus = PaymentStatusInput.WaitForPay
                let product: Array<ProductInput> = [];
                
                items.forEach((ele, idx) => {
                    const topping = [];
                    ele?.subCartDetail?.forEach(e => {
                        const itemTopping = {
                            toppingID: e?.toppingID,
                            quantity: e?.quantity,
                        }
                        topping.push(itemTopping)
                    });
                    product.push({ productID: ele.product.id, quantity: ele.quantity, topping, orderDetailNote: ele?.orderDetailNote })
                })
                const a = { ...formValue, product, paymentStatus };
                
                // this._store.dispatch(new CreateGuestOrder({ ...formValue, product, paymentStatus }));
                this._store.dispatch(new CreateGuestOrder({ ...dataAddress, product, paymentStatus }));
                this._subscription.add(this._actions
                    .pipe(ofActionSuccessful(CreateGuestOrderFailed))
                    .subscribe());
                    // this.router.navigate(['/success']);
            }
            else {
            }
        }

    }
    userOrderCreate() {
        //Address
        if (this.formGroupUser.valid) {
            const receiver = this.formGroupUser.get('receiver').value;
            const phone = this.formGroupUser.get('phone').value;
            const street = this.formGroupUser.get('street').value;
            const district = this.formGroupUser.get('district').value;
            const province = this.formGroupUser.get('city').value;
            const ward = this.formGroupUser.get('ward').value;
            const dataAdd = { receiver, phone, street, district, province, ward } as AddAddressInput
            if (this.addressId == 'new' || this.addressId == '') {
                this._store.dispatch(new AddUserAddress(dataAdd))
                this._subscription.add(this._store
                    .select(ShoppingCartState.getUserAddressNew)
                    .subscribe(res => {
                        this.addressUserNew = res;
                        this.addressId = this.addressUserNew.id;
                        this.creteOrder();
                    }))

            } else {
                const id = this.addressId;
                const addressIdInput = { id } as UserAddress
                const dataAddress = Object.assign({}, addressIdInput, dataAdd);
                this._store.dispatch(new EditUserAddress(dataAddress))
                this.creteOrder();
            }
        } else {
        }

    }
    creteOrder() {
        //Payment
        const cartId = this.cartId;
        const addressId = this.addressId;
        const orderStatus = OrderStatusInput.Pending;
        const paymentStatus = PaymentStatusInput.WaitForPay;
        const paymentMethodId = this.formGroupUser.get('paymentMethodId').value;
        const deliveryMethodId = this.formGroupUser.get('deliveryMethod').value;
        const orderNote = this.formGroupUser.get('orderNote').value;
        const data = { 
            cartId, 
            addressId: this.checkRequiedAddress ? addressId : undefined, 
            orderStatus, 
            paymentMethodId, 
            orderNote, 
            paymentStatus,
            deliveryMethodId,
            promotionCodeCode: (this.code && this.code != "0") ? this.code : undefined
         } as UserOrderCreateInput;
        this._spinner.show()
        this._store.dispatch(new CreateUserOrder(data));
        this.loadAddressUser();
    }
    changeFilterProduct(value) {
        if (value == 'new') {
            this.addressId = value;
            this.addressFilter =
            {
                receiver: null,
                phone: null,
                street: null,
                district: null,
                city: null,
            }
        }
        else {
            this.addressId = value;
            this.addressFilter = this.userAddress.find(ele => ele.id == value);

        }
        this.formGroupUser.patchValue({
            receiver: this.addressFilter.receiver,
            phone: this.addressFilter.phone,
            street: this.addressFilter.street,
            district: this.addressFilter.district,
            city: this.addressFilter.city
        })

    }
    routerLogin(){
        this.router.navigate(['/auth/login'], {queryParams: {returnUrl: this.router.url}})
    }
    selectedRadio() {

    }
    changeDeliveryMethod(value){
        this.deliveryBefore = value
        if(value == "SUPERMARKET"){
            this.checkRequiedAddress = false
            this.formGroup.controls["city"].setValidators([]);
            this.formGroup.controls['city'].updateValueAndValidity()

            this.formGroup.controls["street"].setValidators([]);
            this.formGroup.controls['street'].updateValueAndValidity()

            this.formGroup.controls["district"].setValidators([]);
            this.formGroup.controls['district'].updateValueAndValidity()

            this.formGroup.controls["ward"].setValidators([]);
            this.formGroup.controls['ward'].updateValueAndValidity()

            this.formGroupUser.controls["city"].setValidators([]);
            this.formGroupUser.controls['city'].updateValueAndValidity()

            this.formGroupUser.controls["street"].setValidators([]);
            this.formGroupUser.controls['street'].updateValueAndValidity()

            this.formGroupUser.controls["district"].setValidators([]);
            this.formGroupUser.controls['district'].updateValueAndValidity()

            this.formGroupUser.controls["ward"].setValidators([]);
            this.formGroupUser.controls['ward'].updateValueAndValidity()
        }else{
            this.checkRequiedAddress = true
            if(this.deliveryBefore == "SUPERMARKET"){
                this.formGroup.controls["city"].setValidators([Validators.required]);
                this.formGroup.controls['city'].updateValueAndValidity()

                this.formGroup.controls["street"].setValidators([Validators.required]);
                this.formGroup.controls['city'].updateValueAndValidity()

                this.formGroup.controls["district"].setValidators([Validators.required]);
                this.formGroup.controls['district'].updateValueAndValidity()

                this.formGroup.controls["ward"].setValidators([Validators.required]);
                this.formGroup.controls['ward'].updateValueAndValidity()

                this.formGroupUser.controls["city"].setValidators([Validators.required]);
                this.formGroupUser.controls['city'].updateValueAndValidity()

                this.formGroupUser.controls["street"].setValidators([Validators.required]);
                this.formGroupUser.controls['city'].updateValueAndValidity()

                this.formGroupUser.controls["district"].setValidators([Validators.required]);
                this.formGroupUser.controls['district'].updateValueAndValidity()

                this.formGroupUser.controls["ward"].setValidators([Validators.required]);
                this.formGroupUser.controls['ward'].updateValueAndValidity()
            }
        }
        this.deliveryBefore = value
    }
    changePaymentMethod(value){
        this.selectedPaymentMethod = value
    }
    ngOnDestroy(): void {
        localStorage.removeItem("discount")
        this._subscription.unsubscribe();
        this._subSink.unsubscribe();
    }

}
