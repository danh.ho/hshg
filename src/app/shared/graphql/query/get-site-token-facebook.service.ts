import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
} from '../../../shared/models/graphql.models';

const getSiteTokenFacebook = gql`
  query GetSiteTokenFacebook {
    siteTokenFacebookId
  }
`;
@Injectable({ providedIn: 'root' })
export class QueryGetSiteTokenFacebook extends Query<
  ParentQuery
> {
  document = getSiteTokenFacebook;
}
