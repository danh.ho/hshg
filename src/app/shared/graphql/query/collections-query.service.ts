import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryCollectionsArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Collections($before: String, $after: String, $first: Int, $last: Int, $name: String, $parentId: String, $level: Int, $site: String,$itemCode: String, $orderBy: String) {
    collections(before: $before, after: $after, first: $first, last: $last, name: $name, parentId: $parentId, level: $level, site: $site,itemCode: $itemCode, orderBy: $orderBy) {
        totalCount
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
        edges {
            node {
                id
                name
                thumbnail
                description
                level
                itemCode
              	child {
                  edges {
                    node {
                      id
                      name
                      thumbnail
                      description
                      level
                    }
                  }
                }
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class CollectionsQuery extends Query<ParentQuery, QueryCollectionsArgs> {
    document = QUERY;
}
