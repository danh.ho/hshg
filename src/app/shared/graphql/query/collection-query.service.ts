import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryCollectionArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Collection($id: String!) {
    collection(id: $id) {
        id
        name
        thumbnail
        description
        level
        child {
            pageInfo {
                hasNextPage
                hasPreviousPage
                startCursor
                endCursor
            }
            edges {
                node {
                    id
                    name
                    thumbnail
                    description
                    level
                }
            }
            totalCount
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class CollectionQuery extends Query<ParentQuery,QueryCollectionArgs> {
    document = QUERY;
}
