import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryPaymentMethodsArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query PaymentMethods($before: String, $after: String, $first: Int, $last: Int, $name: String, $orderBy: String) {
    paymentMethods(before: $before, after: $after, first: $first, last: $last, name: $name, orderBy: $orderBy) {
        edges {
            node {
                id
                name
                description
            }
        }
    }
}
`



@Injectable({
    providedIn: 'root'
})
export class PaymentMethodsQuery extends Query<ParentQuery, QueryPaymentMethodsArgs>  {
    document = QUERY;
}
