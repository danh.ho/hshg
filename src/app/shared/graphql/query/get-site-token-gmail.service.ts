import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
} from '../../../shared/models/graphql.models';

const getSiteTokenGmail = gql`
  query GetSiteTokenGmail {
    gmailClientId
  }
`;
@Injectable({ providedIn: 'root' })
export class QueryGetSiteTokenGmail extends Query<
  ParentQuery
> {
  document = getSiteTokenGmail;
}
