import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBranchArgs, QueryProvinceBranchesArgs } from 'src/app/shared/models/graphql.models';

const getAllProvinceBranch = gql`
query GetProvinceBranch(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int,
)  {
  provinceBranches(
    before: $before, 
    after: $after, 
    first: $first, 
    last: $last,
  ){
    edges {
      node {
        id
        province {
          code
          id
          nameProvince
          sortId
        }
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
    totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllProvinceBranchQuery extends Query<ParentQuery, QueryProvinceBranchesArgs> {
  document = getAllProvinceBranch;
}
