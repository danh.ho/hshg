import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryNewsArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query News($before: String, $after: String, $first: Int, $last: Int, $title: String,  $site: String, $orderBy: String, $category: String, $categoryName: String) {
    news(first: $first, last: $last, before: $before, after: $after, title: $title, site: $site, orderBy: $orderBy, category: $category, categoryName: $categoryName) {
      totalCount
      pageInfo {
        hasNextPage
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          title
          thumbnail
          description
          visibility
          visibilityDate
          created
          url
          createdBy{
            firstName
            lastName
          }
          category {
              id 
              name
          }
          likeCount
        }
      }
    }
  }
`

@Injectable({
  providedIn: 'root'
})
export class NewsQuery extends Query<ParentQuery, QueryNewsArgs> {
  document = QUERY;
}