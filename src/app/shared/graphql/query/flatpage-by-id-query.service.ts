import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryFlatpageByIdArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query FlatpageByID($id: ID!) {
    flatpageById(id: $id) {
        id
        title
        description
        publishDate
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class FlatpageByIDQuery extends Query<ParentQuery, QueryFlatpageByIdArgs> {
    document = QUERY;
}
