import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBranchArgs, QueryDistrictBranchesArgs, QueryProvinceBranchesArgs } from 'src/app/shared/models/graphql.models';

const getAllDistrictBranch = gql`
query GetDistrictBranch(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int,
  $provinceId: String
)  {
  districtBranches(
    before: $before, 
    after: $after, 
    first: $first, 
    last: $last,
    provinceId: $provinceId
    orderBy: "district_sort"
  ){
    edges {
      node {
        district {
            code
            id
            nameDistrict
            province {
              code
              id
              nameProvince
              sortId
            }
            sortId
          }
        id
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
    totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllDistrictBranchQuery extends Query<ParentQuery, QueryDistrictBranchesArgs> {
  document = getAllDistrictBranch;
}
