import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryOurPartnersArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query OurPartners($before: String, $after: String, $first: Int, $last: Int, $name: String, $sortOrder: Int, $site: String, $orderBy: String) {
    ourPartners(first: $first, last: $last, before: $before, after: $after, name: $name, sortOrder: $sortOrder, site: $site, orderBy: $orderBy) {
    totalCount
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        name
        image
        sortOrder
        description
        link
        position
        site {
            id
            name
            domain
        }
      }
    }
  }
}
`

@Injectable({
  providedIn: 'root'
})
export class OurPartnersQuery extends Query<ParentQuery, QueryOurPartnersArgs> {
  document = QUERY;
}
