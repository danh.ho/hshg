import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryProductsForBuyArgs } from 'src/app/shared/models/graphql.models';


const QUERY = gql`
query ProductsForBuy($before: String, $after: String, $first: Int, $last: Int, $visibility: Boolean, $isStock: Boolean, $category: String, $name: String, $price: String, $site: String, $collection: String, $collectionName: String, $collectionItemCode: String, $orderBy: String) {
    productsForBuy(before: $before, after: $after, first: $first, last: $last, visibility: $visibility, isStock: $isStock, category: $category, name: $name, price: $price, site: $site, collection: $collection, collectionName: $collectionName, collectionItemCode:$collectionItemCode, orderBy: $orderBy) {
        totalCount
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
        edges {
            node {
                id
                created
                name
                description
                thumbnail
                price
                totalRate
                category {
                    id
                    name
                }
                flashSaleProduct{
                    edges {
                      node {
                        id
                        price
                        flashSale {
                          id
                          startTime
                          endTime
                          isActive
                        }
                      }
                    }
                  }
                productCombo {
                    product {
                        id
                        name
                        thumbnail
                    }
                    quantity
                }
                collections {
                    id
                    name
                    thumbnail
                }
                productImage{
                    id
                    image
                }
                video
            }
        }
    }
}
`



@Injectable({
    providedIn: 'root'
})
export class ProductsForBuyQuery extends Query<ParentQuery, QueryProductsForBuyArgs> {
    document = QUERY;
}
