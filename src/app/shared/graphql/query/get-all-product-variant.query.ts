import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import { Query as ParentQuery, QueryProductForBuyWithVariantArgs } from 'src/app/shared/models/graphql.models';


const getAllProductsForBuyWithVariant = gql`
    query AllProductsForBuyWithVariant(
        $before: String
        $after: String
        $first: Int
        $last: Int
        $categoryList: [String]
        $attributeList: [String]
        $productVariantBarcode: String
        $branchId: String
        $hasToppingGroup: Boolean
        $hasVisibility: Boolean
        $isManageInventory: Boolean
        $isSellOutOfStock: Boolean
        $warehouseId: String
        $categoryName: String
        $price: String
        $collectionId: String
        $categoryWithSub: SubCategoryInput
    ) {
        productForBuyWithVariant(
            before: $before
            after: $after
            first: $first
            last: $last
            categoryList: $categoryList
            attributeList: $attributeList
            productVariantBarcode: $productVariantBarcode
            hasToppingGroup: $hasToppingGroup
            hasVisibility: $hasVisibility
            branchId: $branchId
            isManageInventory: $isManageInventory
            isSellOutOfStock: $isSellOutOfStock
            warehouseId: $warehouseId
            categoryName: $categoryName
            price: $price
            collectionId: $collectionId
            categoryWithSub: $categoryWithSub
        ) {
            pageInfo {
                hasNextPage
                hasPreviousPage
                startCursor
                endCursor
            }
            totalCount
            edges {
                node {
                    product {
                        id
                        name
                        thumbnail
                        price
                        advertisingPrice
                        productType
                        visibility
                        visibilityDate
                        isTopping
                        descriptions {
                            description
                            name
                          }
                        productImage {
                            id
                            image
                        }
                        flashSaleProduct {
                            edges {
                                node {
                                    id
                                    price
                                }
                            }
                        }
                        video
                        gif
                        gifFile
                        tagImage
                        
                    }
                }
            }
        }
    }
`;
@Injectable({ providedIn: 'root' })
export class GetAllForBuyWithVariantQuery extends Query<
    ParentQuery,
    QueryProductForBuyWithVariantArgs
> {
    document = getAllProductsForBuyWithVariant;
}
