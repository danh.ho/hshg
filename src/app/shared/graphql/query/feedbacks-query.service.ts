import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryFeedbacksArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Feedbacks($before: String, $after: String, $first: Int, $last: Int, $name: String, $sortOrder: Int, $site: String, $orderBy: String) {
  feedbacks(first: $first, last: $last, before: $before, after: $after, name: $name, sortOrder: $sortOrder, site: $site, orderBy: $orderBy) {
    totalCount
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        name
        image
        sortOrder
        description
        link
        site {
            id
            name
            domain
        }
      }
    }
  }
}
`

@Injectable({
  providedIn: 'root'
})
export class FeedbacksQuery extends Query<ParentQuery, QueryFeedbacksArgs> {
  document = QUERY;
}
