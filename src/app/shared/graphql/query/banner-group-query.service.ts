import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBannerGroupsArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query BannerGroups ($before: String, $after: String, $first: Int, $last: Int, $name: String, $itemCode: String,  $orderBy: String) {
    bannerGroups (before: $before, after: $after, first: $first, last: $last, name: $name, itemCode: $itemCode,  orderBy: $orderBy) {
        edges {
            node {
                id
                description
                itemCode
                name
                visibility
                banner (orderBy: "sort_order"){
                    edges{
                        node{
                            id
                            file
                            fileMobile
                            name
                            sortOrder
                            description
                            created
                        }
                    }
                }
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class BannerGroupsQuery extends Query<ParentQuery, QueryBannerGroupsArgs> {
    document = QUERY;
}
