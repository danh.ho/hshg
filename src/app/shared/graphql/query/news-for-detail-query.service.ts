import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryNewsForDetailArgs } from 'src/app/shared/models/graphql.models';


const QUERY = gql`
query NewsForDetail($id: ID!, $amountNews: Int) {
    newsForDetail(id: $id) {
        id
        category {
            id
            name
        }
        title
        description
        thumbnail
        created
        url
        likeCount
        newsRelated(amountNews: $amountNews) {
            id
            title
            thumbnail
            visibilityDate
            description
            category {
                id
                name
            }
            url
            created
            likeCount
        }
        visibilityDate
        createdBy {
            firstName
            lastName
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class NewsForDetailQuery extends Query<ParentQuery, QueryNewsForDetailArgs> {
    document = QUERY;
}
