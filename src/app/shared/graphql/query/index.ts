export * from './feedbacks-query.service';
export * from './product-for-buy-best-seller-query.service';
export * from './news-query.service';
export * from './news-for-detail-query.service';

export * from './flatpages-query.service';
export * from './flatpage-by-id-query.service';

export * from './collection-query.service';
export * from './collections-query.service';

export * from './products-for-buy-query.service';

export * from './cart-view-query.service';

export * from './payment-methods-query.service';

export * from './banners-query.service';
