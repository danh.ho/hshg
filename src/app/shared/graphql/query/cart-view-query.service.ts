import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query CartView {
    cartView {
        id
        cartDetail {
            id
            product {
                id
                name
                thumbnail
                price
                advertisingPrice
                sku
                flashSaleProduct {
                    edges {
                        node {
                            id
                            price
                        }
                    }
                }
            }
            note
            quantity
            subCartDetail {
                id
                product {
                  id
                  name
                  price
                }
                quantity
              }
        }
        relatedProduct{
            id
            name
            thumbnail
            descriptions {
                name
                description
            }
            description
            description2
            description3
            price
            advertisingPrice
            productType
            visibility
            visibilityDate
            sortOrder
            tag
            productImage {
                id
                image
            }
            category {
                id
                name
            }
            collections {
                id
                name
            }
            isTopping
            isToppingGroup
            isToppingGroupMandatory
            parent {
                id
                name
            }
            unit {
                id
                isMain
                exchangeValue
                price
                productUnitType
                unit {
                    id
                    name
                }
            }
            totalOrderCustomer
            video
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class CartViewQuery extends Query<ParentQuery> {
    document = QUERY;
}
