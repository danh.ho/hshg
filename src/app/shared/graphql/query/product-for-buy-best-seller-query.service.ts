import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery } from 'src/app/shared/models/graphql.models';
const QUERY = gql`
query ProductForBuyBestSeller {
  productForBuyBestSeller {
    description
    id
    name
    price
    thumbnail
  }
}
`

@Injectable({
    providedIn: 'root'
})
export class ProductForBuyBestSellerQuery extends Query<ParentQuery> {
    document = QUERY;
}
