import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBranchDetailArgs } from 'src/app/shared/models/graphql.models';


const getBranchDetail = gql`
    query GetBranchById($id: String!) {
        branchDetail(id: $id) {
            formatAddress
            id
            isActive
            latitude
            longtitude
            name
            street
            site {
                email
                id
                isActive
                name
            }
            district {
                code
                id
                nameDistrict
            }
            province {
                code
                id
                nameProvince
            }
            ward {
                code
                id
                nameWard
            }
            branchWarehouse {
                edges {
                    node {
                        id
                        sortOrder
                        warehouse {
                            id
                            name
                        }
                    }
                }
            }
        }
    }
`;
@Injectable({ providedIn: 'root' })
export class GetBranchByIdQuery extends Query<
    ParentQuery,
    QueryBranchDetailArgs
> {
    document = getBranchDetail;
}
