import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Flatpages ($itemCode: String) {
    flatpages (itemCode: $itemCode) {
        edges{
            node{
                id
                title
                description
                publishDate
                itemCode
            }
        }

    }
}
`

@Injectable({
    providedIn: 'root'
})
export class FlatpagesQuery extends Query<ParentQuery> {
    document = QUERY;
}
