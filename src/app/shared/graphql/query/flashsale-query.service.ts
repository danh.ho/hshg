import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { productForBuyFragment } from 'src/app/category/shared/graphql/fragments';
import { Query as ParentQuery, QueryFlashSaleArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query FlashSale  {
    flashSale {
        edges{
            node {
                id
                image
                image2
                image3
                image4
                description
                startTime
                endTime
                isActive
                title
                site {
                    id
                  }
                productFlashSale {
                    edges {
                        node {
                            id
                            price
                            product {
                                id
                                thumbnail
                            }
                        }
                    }
                }
            }
        }

    }
}
`

@Injectable({
    providedIn: 'root'
})
export class FlashSaleQuery extends Query<ParentQuery, QueryFlashSaleArgs> {
    document = QUERY;
}
