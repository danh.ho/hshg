import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import {
  Query as ParentQuery,
} from '../../../shared/models/graphql.models';

const getSiteTokenVnPay = gql`
  query GetSiteTokenVnPay {
    siteTokenVnpayDetail {
      vnpaySecret
      vnpayTmnKey
      paymentGateway
      id
    }
  }
`;
@Injectable({ providedIn: 'root' })
export class QueryGetSiteTokenVnPay extends Query<
  ParentQuery
> {
  document = getSiteTokenVnPay;
}
