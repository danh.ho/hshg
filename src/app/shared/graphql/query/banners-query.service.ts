import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBannersArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query Banners($before: String, $after: String, $first: Int, $last: Int, $name: String, $sortOrder: Int, $groupId: String, $siteId: String, $orderBy: String) {
    banners(before: $before, after: $after, first: $first, last: $last, name: $name, sortOrder: $sortOrder, groupId: $groupId, siteId: $siteId, orderBy: $orderBy) {
        edges {
            node {
                id
                name
                sortOrder
                description
                file
                fileMobile
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class BannersQuery extends Query<ParentQuery, QueryBannersArgs> {
    document = QUERY;
}
