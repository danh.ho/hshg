import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryCustomerReviewsArgs } from 'src/app/shared/models/graphql.models';

const QUERY = gql`
query CustomerReviews($before: String, $after: String, $first: Int, $last: Int, $name: String, $sortOrder: Int, $site: String, $orderBy: String) {
  customerReviews(first: $first, last: $last, before: $before, after: $after, name: $name, sortOrder: $sortOrder, site: $site, orderBy: $orderBy) {
    totalCount
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        name
        image
        sortOrder
        description
        link
        position
        site {
            id
            name
            domain
        }
      }
    }
  }
}
`

@Injectable({
  providedIn: 'root'
})
export class CustomerReviewsQuery extends Query<ParentQuery, QueryCustomerReviewsArgs> {
  document = QUERY;
}
