import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { Query as ParentQuery, QueryBranchArgs } from 'src/app/shared/models/graphql.models';

const getAllBranch = gql`
query GetAllBranch(
  $before: String, 
  $after: String, 
  $first: Int, 
  $last: Int,
  $name: String,
  $districtId: String,
)  {
  branch(
    before: $before, 
    after: $after, 
    first: $first, 
    last: $last,
    name: $name,
    districtId: $districtId
  ){
    edges {
      node {
        id
        isActive
        latitude
        longtitude
        name
        street
        formatAddress
        mapsLink
        site {
          id
          isActive
          name
          email
        }
        province {
          id 
          nameProvince
        }
        district {
          id
          nameDistrict
        }
        ward {
          id
          nameWard
        }
      }
    }
    pageInfo {
      endCursor
      hasNextPage
      hasPreviousPage
      startCursor
    }
    totalCount
  }
}
`;


@Injectable({ providedIn: 'root' })
export class GetAllBranchQuery extends Query<ParentQuery, QueryBranchArgs> {
  document = getAllBranch;
}
