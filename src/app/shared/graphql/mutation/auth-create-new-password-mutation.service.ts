import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthCreateNewPasswordArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation CreateNewPassword($input: CreateNewPasswordInput!) {
  authCreateNewPassword(input: $input) {
    status
    errors {
      field
      message
      code
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthCreateNewPasswordMutation extends Mutation<ParentMutation, MutationAuthCreateNewPasswordArgs>{
    document = MUTATION;
}
