import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationCartDeleteArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation CartDelete($input: DeteteCartInput!) {
    cartDelete(input: $input) {
        status
        errors {
            message
            field
            code
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class CartDeleteMutation extends Mutation<ParentMutation, MutationCartDeleteArgs> {
    document = MUTATION;
}
