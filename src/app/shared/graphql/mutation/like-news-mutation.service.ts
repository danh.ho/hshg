import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationNewsLikeActionArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation newsLikeAction($input: NewsLikeInput!) {
    newsLikeAction(input: $input) {
        status
        errors {
            message
            field
        }
        news {
            likeCount
            id
        }
    }
}
`

@Injectable({
    providedIn: 'root'
})
export class LikeNewsMutation extends Mutation<ParentMutation, MutationNewsLikeActionArgs> {
    document = MUTATION;
}
