import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationContactUsCreateArgs } from 'src/app/shared/models/graphql.models';


const MUTATION = gql`
mutation ContactUsCreate($input: ContactUsInput!) {
  contactUsCreate(input: $input) {
    status
    errors {
      message
      code
      field
    }
  }
}
`

@Injectable({
    providedIn: 'root'
})
export class ContactUsCreateMutation extends Mutation<ParentMutation, MutationContactUsCreateArgs> {
    document = MUTATION;
}
