import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationOrderCustomerUpdateArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation OrderUpdateStatus ($input: OrderCustomerUpdateInput!) {
    orderCustomerUpdate(input: $input) {
        status
        errors {
            message
            field
            code
        }
        order {
            created
            id
            totalAmount
            customer {
              firstName
              lastName
              id
              phone
            }
            paymentMethod {
                description
                id
                name
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class OrderCustomerUpdateMutation extends Mutation<ParentMutation, MutationOrderCustomerUpdateArgs> {
    document = MUTATION;
}
