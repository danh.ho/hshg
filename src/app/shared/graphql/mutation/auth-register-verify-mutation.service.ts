import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthRegisterVerifyArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation AuthRegisterVerify($input: TokenVerifyInput!) {
  authRegisterVerify(input: $input) {
    status
    errors {
      field
      message
      code
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthRegisterVerifyMutation extends Mutation<ParentMutation, MutationAuthRegisterVerifyArgs>{
    document = MUTATION;
}
