import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationCartUpdateArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation CartUpdate($input: UpdateCartInput!) {
    cartUpdate(input: $input) {
        status
        errors {
            message
            field
            code
        }
        cart {
            id
            cartDetail {
                id
                product {
                    id
                    name
                    price
                    thumbnail
                }
                quantity
            }
        }
    }
}
`



@Injectable({
    providedIn: 'root'
})
export class CartUpdateMutation extends Mutation<ParentMutation, MutationCartUpdateArgs> {
    document = MUTATION;
}
