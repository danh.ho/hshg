import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthRegisterTokenArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation AuthRegisterToken($input: RegisterAccountTokenInput!) {
  authRegisterToken(input: $input) {
    status
    errors {
      field
      message
      code
    }
    user {
      id
      firstName
      avatar
      email
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthRegisterMutation extends Mutation<ParentMutation, MutationAuthRegisterTokenArgs>{
    document = MUTATION;
}
