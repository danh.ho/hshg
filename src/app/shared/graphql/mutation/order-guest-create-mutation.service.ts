import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationOrderGuestCreateArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation OrderGuestCreate($input: UserGuestInput!) {
    orderGuestCreate(input: $input) {
        status
        errors {
            message
            field
            code
        }
        order {
            created
            id
            totalAmount
            customer {
              firstName
              lastName
              id
              phone
            }
            paymentMethod {
                description
                id
                name
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class OrderGuestCreateMutation extends Mutation<ParentMutation, MutationOrderGuestCreateArgs> {
    document = MUTATION;
}
