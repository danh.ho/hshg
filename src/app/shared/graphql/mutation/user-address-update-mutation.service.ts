import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationUserAddressUpdateArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation UserAddressUpdate($input: UpdateAddressInput!) {
    userAddressUpdate(input: $input) {
        status
        errors {
            message
            field
            code
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class UserAddressUpdateMutation extends Mutation<ParentMutation, MutationUserAddressUpdateArgs> {
    document = MUTATION;
}
