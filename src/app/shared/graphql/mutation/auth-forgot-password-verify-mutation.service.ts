import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthForgotPasswordVerifyArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation ForgotPasswordVerify($input: TokenVerifyInput!) {
  authForgotPasswordVerify(input: $input) {
    status
    errors {
      field
      message
      code
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthForgotPasswordVerifyMutation extends Mutation<ParentMutation, MutationAuthForgotPasswordVerifyArgs>{
    document = MUTATION;
}
