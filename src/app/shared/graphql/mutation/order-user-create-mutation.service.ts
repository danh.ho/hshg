import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationOrderUserCreateArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation    ($input: UserOrderCreateInput!) {
    orderUserCreate(input: $input) {
        status
        errors {
            message
            field
            code
        }
        order {
            created
            id
            totalAmount
            customer {
              firstName
              lastName
              id
              phone
            }
            paymentMethod {
                description
                id
                name
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class OrderUserCreateMutation extends Mutation<ParentMutation, MutationOrderUserCreateArgs> {
    document = MUTATION;
}
