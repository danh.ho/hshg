import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthLoginArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation AuthLogin($input: AuthencationLoginInput!) {
  authLogin(input: $input) {
    status
    errors {
      field
      message
      code
    }
    token
    user {
      id
      firstName
      avatar
      userType
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthLoginMutation extends Mutation<ParentMutation, MutationAuthLoginArgs>{
    document = MUTATION;
}
