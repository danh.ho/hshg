import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationCartAddArgs, } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation CartAdd($input: AddToCartInput!) {
    cartAdd(input: $input) {
        status
        errors {
            message
            field
        }
        cart {
            cartDetail {
              id
              note
              product {
                advertisingPrice
                id
                name
                price
                sku
                thumbnail
              }
              quantity
              subCartDetail {
                id
                product {
                  id
                  name
                  price
                }
              }
            }
          }
    }
}
`

@Injectable({
    providedIn: 'root'
})
export class CartAddMutation extends Mutation<ParentMutation, MutationCartAddArgs> {
    document = MUTATION;
}
