import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationUserAddressAddArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation UserAddressAdd($input: AddAddressInput!) {
    userAddressAdd(input: $input) {
        status
        errors {
            message
            field
            code
        }
        userAddress {
            formatAddress
            id
            isDefault
            phone
            receiver
            street
            ward {
              code
              id
              nameWard
            }
            district {
              code
              id
              nameDistrict
            }
            province {
              code
              id
              nameProvince
            }
        }
    }
}
`


@Injectable({
    providedIn: 'root'
})
export class UserAddressAddMutation extends Mutation<ParentMutation, MutationUserAddressAddArgs> {
    document = MUTATION;
}
