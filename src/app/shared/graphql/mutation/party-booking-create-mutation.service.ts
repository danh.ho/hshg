import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationPartyInfoCreateArgs } from 'src/app/shared/models/graphql.models';


const MUTATION = gql`
mutation PartyBookingCreate($input: PartyInforCreateInput!) {
  partyInfoCreate(input: $input) {
    status
    errors {
      message
      code
      field
    }
  }
}
`

@Injectable({
    providedIn: 'root'
})
export class PartyBookingCreateMutation extends Mutation<ParentMutation, MutationPartyInfoCreateArgs> {
    document = MUTATION;
}
