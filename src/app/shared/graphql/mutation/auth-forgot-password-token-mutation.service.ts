import { Injectable } from '@angular/core';
import { Mutation } from 'apollo-angular';
import gql from 'graphql-tag';
import { Mutation as ParentMutation, MutationAuthForgotPasswordTokenArgs } from 'src/app/shared/models/graphql.models';

const MUTATION = gql`
mutation ForgotPasswordToken($input: ForgotPasswordInput!) {
  authForgotPasswordToken(input: $input) {
    status
    errors {
      field
      message
      code
    }
  }
}
`
@Injectable({
    providedIn: 'root'
})
export class AuthForgotPasswordTokenMutation extends Mutation<ParentMutation, MutationAuthForgotPasswordTokenArgs>{
    document = MUTATION;
}
