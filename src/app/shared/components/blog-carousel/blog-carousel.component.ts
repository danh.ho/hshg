import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { CmsNewsState, GetNews } from '../../store/tip-article';

@Component({
  selector: 'app-blog-carousel',
  templateUrl: './blog-carousel.component.html',
  styleUrls: ['./blog-carousel.component.scss'],
})
export class BlogCarouselComponent implements OnInit ,OnDestroy, OnChanges{
  // @Input() items: Array<NewsNode> = [];
  items;
  private _subscription = new Subscription();
  blog = [
    {
      id: "1",
      thumbnail: 'assets/image/banner1.jpg',
      visibilityDate: '28/01/2016 Đăng bởi: Admin',
      title: 'Mẹo bảo quản và vệ sinh các đồ nội thất',
      description: 'Đối với đồ nội thất bằng kim loại-Khi xử lý các vết bẩn thông thường, bạn chỉ cần..'
    },
    {
      id: "2",
      thumbnail: 'assets/image/banner1.jpg',
      visibilityDate: '28/01/2016 Đăng bởi: Admin',
      title: 'Mẹo bảo quản và vệ sinh các đồ nội thất',
      description: 'Đối với đồ nội thất bằng kim loại-Khi xử lý các vết bẩn thông thường, bạn chỉ cần..'
    },
    {
      id: "3",
      thumbnail: 'assets/image/banner1.jpg',
      visibilityDate: '28/01/2016 Đăng bởi: Admin',
      title: 'Mẹo bảo quản và vệ sinh các đồ nội thất',
      description: 'Đối với đồ nội thất bằng kim loại-Khi xử lý các vết bẩn thông thường, bạn chỉ cần..'
    }
  ]
  constructor(
    private _store: Store,
  ) { }

  ngOnInit() {
    this.loadBlog();
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
  ngOnChanges(){

  }
  public loadBlog(){
    this._store.dispatch(new GetNews);
    this._subscription.add(this._store.select(CmsNewsState.getNews)
    .subscribe(res => {
      const data = res;
      this.items = data.slice(0,3)}));
    
  }
}
