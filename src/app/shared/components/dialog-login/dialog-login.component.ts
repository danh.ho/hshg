import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-dialog-login',
  templateUrl: './dialog-login.component.html',
  styleUrls: ['./dialog-login.component.scss'],
})
export class DialogLoginComponent implements OnInit {
  @Input() title: string;;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
  ngOnChanges(){

  }
  async approve(){
    await this.modalCtrl.dismiss({
      'dismissed': true
    })
  }
  async close(){
    await this.modalCtrl.dismiss({
      'dismissed': false
    })
  }
}
