import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-delete-product-modal',
  templateUrl: './delete-product-modal.component.html',
  styleUrls: ['./delete-product-modal.component.scss'],
})
export class DeleteProductModalComponent implements OnInit {

  constructor(private modalCtrl: ModalController,) { }

  ngOnInit() {}
  async approve(){
    await this.modalCtrl.dismiss({
      'dismissed': true
    })
  }
  async close(){
    await this.modalCtrl.dismiss({
      'dismissed': false
    })
  }
}
