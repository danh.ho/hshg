import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { LoadBranch, LoadBranchById, LoadDistrictBranch, LoadProvinceBranch } from '../../store/branch/branch.action';
import { BranchState } from '../../store/branch/branch.state';

@Component({
  selector: 'app-select-branch',
  templateUrl: './select-branch.component.html',
  styleUrls: ['./select-branch.component.scss'],
})
export class SelectBranchComponent implements OnInit {
  form: FormGroup;
  private _subscription = new Subscription();
  popupBanner;
  province = [];
  district = [];
  branch = [];
  validBranch = true;
  subscription = new Subscription();
  constructor(private modalCtrl: ModalController,
              private _fb: FormBuilder,
              private _store: Store,) { 

  }
  onSubmitForm(formDirective?: FormGroupDirective) {
    this.form.markAllAsTouched();
    if (this.form.valid) {
        const { province, district, branch } = this.form.value;
        localStorage.setItem('branch', branch)
        this._store.dispatch(new LoadBranchById({ id: branch }));
        this.modalCtrl.dismiss()
        setTimeout(() => {
          window.location.reload();            
        }, 300);
    }
}
  ngOnInit() {
    this.form = this._fb.group({
      province: ['', [Validators.required]],
      district: ['', [Validators.required]],
      branch: ['', [Validators.required]],
  })

    this.subscription.add(this.form.valueChanges.pipe(distinctUntilChanged())
      .subscribe((res) => {
        if (this.form.valid) {
          this.validBranch = false;
        } else {
          this.validBranch = true;
        }
    }))
    this.loadProvinceBranch();

    const branchId = localStorage.getItem('branch')
    if(branchId){
      this._store.dispatch(new LoadBranchById({ id: branchId}));
    }
    this._subscription.add(
      this._store
          .select(BranchState.getSelectedNode)
          .subscribe((data) => {
              if (data) {
                this.form.patchValue({
                  province: data?.province?.id,
                  district: data?.district?.id,
                  branch: data?.id,
                })
              }
          })
    );
  }
  loadProvinceBranch(){
    this._store.dispatch(new LoadProvinceBranch())
    this._subscription.add(this._store.select(BranchState.getProvinceNodeConnection).subscribe(
      (res) => {
        if (res)
          this.province = res?.edges
      }
    ))
  }
  _initDataBranch(districtId){
    this._store.dispatch(new LoadBranch({districtId}))
    this._subscription.add(this._store.select(BranchState.getNodeConnection).subscribe(
      (res) => {
        if (res)
          this.branch = res?.edges
          const { province, district, branch } = this.form.value;
          this.branch?.forEach(ele => {
            if(province == ele?.node?.province?.id && district == ele?.node?.district?.id && branch == ele?.node?.id){
              return this.validBranch = false;
            } else {
              this.validBranch = true;
            }
          })
      }
    ))
  }
  changeProvince(event, province){
    if (event.source.selected) {
      this.form.patchValue({
        district: null
      })
      console.log(this.form.value)
      this._initDataDistrict(province?.node?.province?.id)
    }
  }
  changeDistrict(event, district){
    if (event.source.selected) {
      this._initDataBranch(district?.node?.district?.id)
    }
  }
  changeBranch(event, branch){
    if(event.source.selected){
      this.validBranch = false;
    } else {
      this.validBranch = true;
    }
  }
  _initDataDistrict(provinceId){
    this._store.dispatch(new LoadDistrictBranch({provinceId}))
    this._subscription.add(this._store.select(BranchState.getDistrictNodeConnection).subscribe(
      (res) => {
        if (res)
          this.district = res?.edges
      }
    ))
  }
  async close(){
    await this.modalCtrl.dismiss()
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
