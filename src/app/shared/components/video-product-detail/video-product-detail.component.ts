import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-product-detail',
  templateUrl: './video-product-detail.component.html',
  styleUrls: ['./video-product-detail.component.scss'],
})
export class VideoProductDetailComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<VideoProductDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private sanitizer : DomSanitizer
  ) { }

  ngOnInit() {
    console.log(this.data)
  }
  close(): void {
    this.dialogRef.close();
  }
  videoURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.data?.video);
  }
}
