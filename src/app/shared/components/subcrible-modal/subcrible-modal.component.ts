import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { ContactUsInput } from '../../models/graphql.models';
import { CmsState, GetGroupBanner } from '../../store/cms';
import { CreateContactForm } from '../../store/flatpage';

@Component({
  selector: 'app-subcrible-modal',
  templateUrl: './subcrible-modal.component.html',
  styleUrls: ['./subcrible-modal.component.scss'],
})
export class SubcribleModalComponent implements OnInit, OnDestroy {
  contactForm: FormGroup;
  private _subscription = new Subscription();
  popupBanner;
  constructor(private modalCtrl: ModalController,
              private _fb: FormBuilder,
              private _store: Store,) { 
    this.contactForm = this._fb.group({
      firstName: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(15), Validators.pattern("[0-9 ]*")]],
      email: ['', [Validators.email, Validators.required]],
      message: [''],
      subject: [''],
  })
  }
  onSubmitForm(formDirective?: FormGroupDirective) {
    if (this.contactForm.valid) {
        const { firstName, message, subject, phone, email }: ContactUsInput = this.contactForm.value;
        const data = { firstName, message, subject, phone, email } as ContactUsInput;        
        this._store.dispatch(new CreateContactForm(data));
        this.contactForm.reset('');

        this.modalCtrl.dismiss()
    }
}
  ngOnInit() {
    // this._store.dispatch(new GetGroupBanner());
    this._subscription.add(this._store.select(CmsState.getBannerGroup)
        .subscribe(res => {
          if (res) {
            const popup = res.filter(ele => ele.itemCode == 'popup');
            this.popupBanner = popup[0]?.banner.edges[0];
          }
        }
        ));
  }
  async close(){
    await this.modalCtrl.dismiss()
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
