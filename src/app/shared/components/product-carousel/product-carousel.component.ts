import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { ProductForBuyNode } from '../../models/graphql.models';

@Component({
  selector: 'app-product-carousel',
  templateUrl: './product-carousel.component.html',
  styleUrls: ['./product-carousel.component.scss'],
})
export class ProductCarouselComponent implements OnInit, OnDestroy {
  @ViewChild('slider')  slides: IonSlides;
  @ViewChild('sliderSm')  slidesSm: IonSlides;
  private _subscription = new Subscription();
  swipeNext(){
    this.slides.slideNext();
  }
  swipePrev(){
    this.slides.slidePrev();
  }
  swipeNextSm(){
    this.slidesSm.slideNext();
  }
  swipePrevSm(){
    this.slidesSm.slidePrev();
  }
  product;
  @Input() productRelated: Array<ProductForBuyNode> = [];
  @Input() items: Array<ProductForBuyNode> = [];
  constructor() { 
  }

  ngOnInit() {
  }

  customOurPartnerOptions: OwlOptions = {
    loop: true,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 1000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      900: {
        items: 3
      }
    },
    nav: false,
    // dotsClass: 'owl-dots'
  }
  slideCustomerReviewsOpts = {
    slidesPerView: 1,
    initialSlide: 0,
    speed: 700,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
      // pauseOnMouseEnter: true,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  }
  
  slideCustomerReviewsOpts1 = {
    slidesPerView: 1,
    initialSlide: 0,
    speed: 700,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  }
  slideCustomerReviewsOpts2 = {
    slidesPerView: 2,
    initialSlide: 0,
    speed: 700,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  }
  slideCustomerReviewsOpts3 = {
    slidesPerView: 3,
    initialSlide: 0,
    speed: 700,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  }
  slideCustomerReviewsOpts4 = {
    slidesPerView: 4,
    initialSlide: 0,
    speed: 700,
    pagination: false,
    loop: true,
    autoplay: {
      disableOnInteraction: true,
      delay: 2500,
    },
    allowSlideNext: true,
    allowSlidePrev: true,
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
