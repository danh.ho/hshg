import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NotificationService } from '../../services';

@Component({
    selector: 'cos-frontend-item-quantity',
    templateUrl: './item-quantity.component.html',
    styleUrls: ['./item-quantity.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemQuantityComponent
    implements OnInit, OnChanges {
    @Input() quantity;
    @Input() item;
    @Output() outputQuantity = new EventEmitter<number>();
    totalQuantityTopping = 0;
    constructor(
        private _notify: NotificationService,

    ) { }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges) {
        return;
    }

    onChangeQuantity(value:string) {
        if(Number(value) > -1) {
            this.outputQuantity.emit(Number(value))
        } else {
            this.outputQuantity.emit(this.quantity)
        }
    }

    onClickBtn(isIncrease: boolean) {
        this.totalQuantityTopping = 0;
        this.item?.subCartDetail?.forEach(ele => {
          this.totalQuantityTopping += ele.quantity
        })
        if(isIncrease) {
            this.outputQuantity.emit(this.quantity + 1); 
        } else {
            if(this.totalQuantityTopping <= (this.quantity-1)){
                this.outputQuantity.emit(this.quantity - 1);
            } else {
                this._notify.error('','Số lượng sản phẩm phải lớn hơn số lượng chế biến')
            }
        }
    }


}
