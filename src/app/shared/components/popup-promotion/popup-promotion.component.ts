import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Subscription } from 'rxjs';
import { CmsState, GetGroupBanner } from '../../store/cms';

@Component({
  selector: 'app-popup-promotion',
  templateUrl: './popup-promotion.component.html',
  styleUrls: ['./popup-promotion.component.scss'],
})
export class PopupPromotionComponent implements OnInit {
  @Input() banner: any
  bannerList = [];
  _subscription = new Subscription()
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 2000,
    autoplayTimeout: 5000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
    },
    nav: false,
    // dotsClass: 'owl-dots'
  }
  constructor(
    private _store: Store,
    private modalCtrl: ModalController,
  ) {

   }

  ngOnInit() {
  }
  ngAfterViewInit() {
    setTimeout(() => {
      // this._store.dispatch(new GetGroupBanner());
      this._subscription.add(this._store.select(CmsState.getBannerGroup)
        .subscribe(res => {
          if (res) {
            const main = res.filter(ele => ele.itemCode == 'popup-promotion')
            this.bannerList = main[0]?.banner.edges;
          }
        }
        ));
    }, 600);
  }
  async close(){
    await this.modalCtrl.dismiss()
  }
  ngOnDestroy() {
    this._subscription.unsubscribe();
  }
}
