import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";


import { BlogCarouselComponent } from "./components/blog-carousel/blog-carousel.component";
import { FooterComponent } from "../footer/footer.component";
import { ProductCarouselComponent } from "./components/product-carousel/product-carousel.component";
import { CarouselModule } from "ngx-owl-carousel-o";
import { SafePipe } from "./pipes/pipe-safe-html";
import { SubcribleModalComponent } from "./components/subcrible-modal/subcrible-modal.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ItemQuantityComponent } from "./components/item-quantity/item-quantity.component";
import { NavbarComponent } from "../navbar/navbar.component";

//Angular Material Components
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import {NgxPaginationModule} from 'ngx-pagination';
import { VideoProductDetailComponent } from "./components/video-product-detail/video-product-detail.component";
import { SelectBranchComponent } from "./components/select-branch/select-branch.component";
import { MatNativeDateModule, MAT_DATE_LOCALE } from "@angular/material/core";
import { NgxMaskModule } from "ngx-mask";
import { TranslateModule } from "@ngx-translate/core";
import { PopupPromotionComponent } from "./components/popup-promotion/popup-promotion.component";


const COMPONENTS = [BlogCarouselComponent, FooterComponent, ProductCarouselComponent, SafePipe,
    SubcribleModalComponent, ItemQuantityComponent, NavbarComponent, VideoProductDetailComponent,SelectBranchComponent, PopupPromotionComponent];
const MATERIAL_MODULES = [
    FormsModule,
    ReactiveFormsModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatNativeDateModule
]
@NgModule({
    declarations: COMPONENTS,
    imports: [
        CommonModule,
        RouterModule,
        IonicModule,
        TranslateModule,
        CarouselModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        NgxMaskModule,
        ...MATERIAL_MODULES
    ],
    providers: [
        {
          provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
          useValue: { appearance: 'outline' },
        },
        { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }
    ],
    exports: [
        COMPONENTS,
        ...MATERIAL_MODULES,
        TranslateModule
    ]
})
export class SharedModule { }
