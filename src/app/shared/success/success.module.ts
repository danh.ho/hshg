import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessComponent } from './success.component';
import { SuccessRoutingModule } from './success-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../shared.module';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [SuccessComponent],
  imports: [
    CommonModule,
    SuccessRoutingModule,
    FormsModule,
    MatButtonModule,
    ReactiveFormsModule,
    SharedModule,
    IonicModule

  ]
})
export class SuccessModule { }
