import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';

import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { VnPay } from 'src/app/checkout/vnpay/models';
import { environment } from 'src/environments/environment';
import { PaymentStatusInput } from '../models/graphql.models';
import { AuthState } from '../store/auth';
import { FlatpageState, GetFlatpageList } from '../store/flatpage';
import { OrderCustomerUpdateStatus, ShoppingCartState } from '../store/shopping-cart';



@Component({
    selector: 'cos-frontend-success',
    templateUrl: './success.component.html',
    styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
    id: string;
    checkMethodpayment = 1;
    idUpdate;
    _subscription = new Subscription()
    errorCode = "0"
    checkVnPay = false
    checkPaymentVnPaySuccess = false
    flatpageSuccess;
    cartItemsNo: number;
    contentError = [
        {code: "07", content: "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường)"},
        {code: "09", content: "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng."},
        {code: "10", content: "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần"},
        {code: "11", content: "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch."},
        {code: "12", content: "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa."},
        {code: "13", content: "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch."},
        {code: "24", content: "Giao dịch không thành công do: Khách hàng hủy giao dịch"},
        {code: "51", content: "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch."},
        {code: "65", content: "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày."},
        {code: "75", content: "Ngân hàng thanh toán đang bảo trì."},
        {code: "79", content: "Giao dịch không thành công do: KH nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch"},
        {code: "99", content: "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)"},
    ]
    showErrorCode = ''
    constructor(
        private _route: ActivatedRoute,
        private _store: Store,
        private _actions: Actions,
        private _snackBar: MatSnackBar,
        private _http: HttpClient
    ) {
        this.idUpdate = this._route.snapshot.paramMap.get('id');
    }

    ngOnInit(): void {
        this.checkVnPay = false
        const params = this._route.snapshot.queryParams;
        this.loadFlatPage();
        if(params['vnp_TransactionStatus']){
            this.checkVnPay = true
            this.errorCode = params['vnp_TransactionStatus']
            this.checkErrorCode(this.contentError, this.errorCode)
        }
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
        const parsedResponse: VnPay.CheckoutResponse = {
            amount: params['vnp_Amount'],
            bankCode: params['vnp_BankCode'],
            bankTransactionNo: params['vnp_BankTranNo'],
            cardType: params['vnp_CardType'],
            payDate: params['vnp_PayDate'],
            orderId: params['vnp_TxnRef'],
            orderInfo: params['vnp_OrderInfo'],
            transactionNo: params['vnp_TransactionNo'],
            responseCode: params['vnp_ResponseCode']
        };

        if (params?.vnp_TransactionStatus == '00') {
            this.checkMethodpayment = 3
            // this.updateOrderAfterPayment()
            this.checkPaymentVnPaySuccess = false
            this.checkOrderPayment(params['vnp_TxnRef'], params['vnp_PayDate']).subscribe(data => {
                if(data?.vnp_TransactionStatus == "00"){
                    this.updateOrderAfterPayment()
                    this.checkMethodpayment = 3
                }else{
                    this.checkMethodpayment = 4
                }
            })
        }else{
            this.checkMethodpayment = 4
        }

        this._route.queryParams.subscribe(param => {
            
            localStorage.removeItem("payment")
            if(param?.payment_method){
                this.checkMethodpayment = param?.payment_method
                if(param?.payment_method != 1 && param?.payment_method != 2){
                    this.checkMethodpayment = 1
                }
            }else{
                this.checkMethodpayment = 1
                if(param?.payment_success == "true"){
                    this.checkMethodpayment = 3
                    this.paymentSuccess(this.idUpdate)
                }else if(param?.payment_success == "false"){
                    this.checkMethodpayment = 4
                    this.paymentFail(this.idUpdate)
                }
                if(param?.vnp_TransactionStatus == '00') {
                    this.checkMethodpayment = 3
                }else if(param?.vnp_TransactionStatus != '00'){
                    this.checkMethodpayment = 4
                }
            }

            if(param?.error_code){
                this.errorCode = param?.error_code
            }
        })

        this._route.params.subscribe(param => {
            if (param['id'] != 0)
                this.id = param['id']
        })
        // this._subscription.add(this._actions
        // .pipe(
        //     ofActionSuccessful(OrderCustomerUpdateStatusSuccessfully),
        // )
        // .subscribe((translation) => {
        //     if(translation){
        //         this.translate.get('ORDER.NOTIFY.success'
        //         , { name: this.idUpdate }).subscribe((result: string) => {
        //             this._snackBar.open(result)
        //         });
        //     }
        // }));

        // this._subscription.add(
        //     this._actions.pipe(
        //         ofActionSuccessful(OrderCustomerUpdateStatusFailed)
        //     )
        //     .subscribe((translation) => {
        //         if(translation){
        //             this.translate.get('ORDER.NOTIFY.fail'
        //             , { name: this.idUpdate }).subscribe((result: string) => {
        //                 this._snackBar.open(result)
        //             });
        //         }
        //     })
        // )
    }


    checkErrorCode(data, errorCode){
        if(data.length > 0){
            for(let i = 0; i < data.length; i++){
                if(errorCode == data[i].code){
                    this.showErrorCode = data[i].content
                    return true
                }
            }
        }
        return false
    }

    checkOrderPayment(id, date): Observable<any>{
        const params = {
            orderId: id,
            transDate: date
        }
        return this._http.get(environment.API_VNPAY, {params: params})
    }

    paymentSuccess(name){
        // this.translate.get('ORDER.NOTIFY.success'
        // , { name: name }).subscribe((result: string) => {
        //     this._snackBar.open(result)
        // });
    }

    paymentFail(name){
        // this.translate.get('ORDER.NOTIFY.fail'
        // , { name: name }).subscribe((result: string) => {
        //     this._snackBar.open(result)
        // });
    }

    updateOrderAfterPayment(){
        this._store.dispatch(new OrderCustomerUpdateStatus({id: this.idUpdate, paymentStatus: PaymentStatusInput.Paid}))
    }
    loadFlatPage() {
        this._store.dispatch(new GetFlatpageList());
            this._subscription.add(
            this._store.select(FlatpageState.getFlatpageList).pipe(
            ).subscribe(res =>{
              if(res){
                this.flatpageSuccess = res.find(ele => (ele.node.itemCode == 'created-order-successfully' ));
              }
            }
            )
        );
      }
}
