import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ContactUsCreateMutation } from 'src/app/shared/graphql/mutation';
import { FlatpageByIDQuery, FlatpagesQuery } from 'src/app/shared/graphql/query';
import { MutationContactUsCreateArgs, MutationPartyInfoCreateArgs, QueryFlatpageByIdArgs, QueryFlatpagesArgs } from 'src/app/shared/models/graphql.models';
import { PartyBookingCreateMutation } from '../graphql/mutation/party-booking-create-mutation.service';

@Injectable({
    providedIn: 'root'
})
export class FlatpageService {

    constructor(
        private _flatpagesQuery: FlatpagesQuery,
        private _flatpageByIDQuery: FlatpageByIDQuery,
        private _contactUsCreateMutation: ContactUsCreateMutation,
        private _partyBookingCreateMutation: PartyBookingCreateMutation
    ) { }

    getFlatpageList(args: QueryFlatpagesArgs) {
        return this._flatpagesQuery.watch(args).valueChanges.pipe(
            map(res => res.data.flatpages)
        )
    }

    getFlatpageByID(args: QueryFlatpageByIdArgs) {
        return this._flatpageByIDQuery.watch(args).valueChanges.pipe(
            map(res => res.data.flatpageById)
        )
    }

    createContactForm(args: MutationContactUsCreateArgs) {
        return this._contactUsCreateMutation.mutate(args).pipe(
            map(res => res.data.contactUsCreate)
        )
    }

    createPartyBooking(args: MutationPartyInfoCreateArgs) {
        return this._partyBookingCreateMutation.mutate(args).pipe(
            map(res => res.data.partyInfoCreate)
        )
    }
}
