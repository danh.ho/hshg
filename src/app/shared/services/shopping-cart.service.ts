import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { CartAddMutation, CartDeleteMutation, CartUpdateMutation, OrderGuestCreateMutation } from 'src/app/shared/graphql/mutation';
import { CartViewQuery, PaymentMethodsQuery } from 'src/app/shared/graphql/query';
import { MutationCartAddArgs, MutationCartDeleteArgs, MutationCartUpdateArgs, MutationOrderCustomerUpdateArgs, MutationOrderGuestCreateArgs, MutationOrderUserCreateArgs, MutationUserAddressAddArgs, MutationUserAddressUpdateArgs, QueryPaymentMethodsArgs } from 'src/app/shared/models/graphql.models';
import { OrderCustomerUpdateMutation } from '../graphql/mutation/customer-update-order.mutation';
import { OrderUserCreateMutation } from '../graphql/mutation/order-user-create-mutation.service';
import { UserAddressAddMutation } from '../graphql/mutation/user-address-add-mutation.service';
import { UserAddressUpdateMutation } from '../graphql/mutation/user-address-update-mutation.service';

@Injectable({
    providedIn: 'root'
})
export class ShoppingCartService {

    constructor(
        private _cartViewQuery: CartViewQuery,
        private _cartAddMutation: CartAddMutation,
        private _cartUpdateMutation: CartUpdateMutation,
        private _cartDeleteMutation: CartDeleteMutation,
        private _paymentMethodsQuery: PaymentMethodsQuery,
        private _orderGuestCreateMutation: OrderGuestCreateMutation,
        private _orderGuestUserMutation: OrderUserCreateMutation,
        private _userAddressUpdateMutation: UserAddressUpdateMutation,
        private _userAddressAddMutation: UserAddressAddMutation,
        private _orderCustomerUpdate: OrderCustomerUpdateMutation,
    ) { }

    getMemberCart() {
        return this._cartViewQuery.watch().valueChanges.pipe(
            map(res => res.data.cartView[0])
        )
    }
    getMemberCartId() {
        return this._cartViewQuery.watch().valueChanges.pipe(
            map(res => res.data.cartView[0]?.id)
        )
    }

    addCart(args: MutationCartAddArgs) {
        return this._cartAddMutation.mutate(args).pipe(
            map(res => res.data.cartAdd)
        )
    }

    updateCart(args: MutationCartUpdateArgs) {
        return this._cartUpdateMutation.mutate(args).pipe(
            map(res => res.data.cartUpdate)
        )
    }

    deleteCart(args: MutationCartDeleteArgs) {
        return this._cartDeleteMutation.mutate(args).pipe(
            map(res => res.data.cartDelete)
        )
    }

    getPaymentMethodList(args?: QueryPaymentMethodsArgs) {
        return this._paymentMethodsQuery.watch(args).valueChanges.pipe(
            map(res => res.data.paymentMethods)
        )
    }

    createGuestOrder(args: MutationOrderGuestCreateArgs) {
        return this._orderGuestCreateMutation.mutate(args).pipe(
            map(res => res.data.orderGuestCreate)
        )
    }
    createUserOrder(args: MutationOrderUserCreateArgs) {
        return this._orderGuestUserMutation.mutate(args).pipe(
            map(res => res.data.orderUserCreate)
        )
    }
    EditUserAddress(args: MutationUserAddressUpdateArgs) {
        return this._userAddressUpdateMutation.mutate(args).pipe(
            map(res => res.data.userAddressUpdate)
        )
    }
    AddUserAddress(args: MutationUserAddressAddArgs) {
        return this._userAddressAddMutation.mutate(args).pipe(
            map(res => res.data.userAddressAdd)
        )
    }

    orderCustomerUpdateStatus(args: MutationOrderCustomerUpdateArgs) {
        return this._orderCustomerUpdate.mutate(args)
    }
}
