import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AuthLoginMutation } from 'src/app/shared/graphql/mutation';
import { MutationAuthCreateNewPasswordArgs, MutationAuthForgotPasswordTokenArgs, MutationAuthForgotPasswordVerifyArgs, MutationAuthLoginArgs, MutationAuthRegisterTokenArgs, MutationAuthRegisterVerifyArgs } from 'src/app/shared/models/graphql.models';
import { AuthCreateNewPasswordMutation } from '../graphql/mutation/auth-create-new-password-mutation.service';
import { AuthForgotPasswordTokenMutation } from '../graphql/mutation/auth-forgot-password-token-mutation.service';
import { AuthForgotPasswordVerifyMutation } from '../graphql/mutation/auth-forgot-password-verify-mutation.service';
import { AuthRegisterMutation } from '../graphql/mutation/auth-register-mutation.service';
import { AuthRegisterVerifyMutation } from '../graphql/mutation/auth-register-verify-mutation.service';
import { QueryGetSiteTokenFacebook } from '../graphql/query/get-site-token-facebook.service';
import { QueryGetSiteTokenGmail } from '../graphql/query/get-site-token-gmail.service';
import { QueryGetSiteTokenVnPay } from '../graphql/query/get-site-token-vnpay.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
      private _authLoginMutation: AuthLoginMutation,
      private _authRegisterMutation: AuthRegisterMutation,
      private _authRegisterVerifyMutation: AuthRegisterVerifyMutation,
      private _authForgotPasswordToken: AuthForgotPasswordTokenMutation,
      private _authForgotPasswordVerify: AuthForgotPasswordVerifyMutation,
      private _authCreateNewPassword: AuthCreateNewPasswordMutation,
      private _getSiteTokenFacebook: QueryGetSiteTokenFacebook,
      private _getSiteTokenGmail: QueryGetSiteTokenGmail,
      private _getSiteTokenVnPay: QueryGetSiteTokenVnPay,
  ) { }

  authLogin(args: MutationAuthLoginArgs) {
      return this._authLoginMutation.mutate(args).pipe(
          map(res => res.data.authLogin)
      )
  }
  authRegister(args: MutationAuthRegisterTokenArgs) {
    return this._authRegisterMutation.mutate(args).pipe(
        map(res => res.data.authRegisterToken)
    )
  }
  authRegisterVerifyCode(args: MutationAuthRegisterVerifyArgs) {
    return this._authRegisterVerifyMutation.mutate(args).pipe(
        map(res => res.data.authRegisterVerify)
    )
  }
  authForgotPasswordToken(args: MutationAuthForgotPasswordTokenArgs){
    return this._authForgotPasswordToken.mutate(args).pipe(
        map(res => res.data.authForgotPasswordToken)
    )
  }
  authForgotPasswordVerify(args: MutationAuthForgotPasswordVerifyArgs){
    return this._authForgotPasswordVerify.mutate(args).pipe(
        map(res => res.data.authForgotPasswordVerify)
    )
  }
  authCreateNewPassword(args: MutationAuthCreateNewPasswordArgs){
    return this._authCreateNewPassword.mutate(args).pipe(
        map(res => res.data.authCreateNewPassword)
    )
  }
  getSiteTokenFacebook(){
    return this._getSiteTokenFacebook.watch().valueChanges;
  }
  getSiteTokenGmail(){
    return this._getSiteTokenGmail.watch().valueChanges;
  }
  getSiteTokenVnPay(){
    return this._getSiteTokenVnPay.watch().valueChanges;
  }
}
