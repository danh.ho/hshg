import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BannersQuery, FeedbacksQuery, NewsForDetailQuery, NewsQuery } from 'src/app/shared/graphql/query';
import { MutationNewsLikeActionArgs, QueryBannerGroupsArgs, QueryBannersArgs, QueryCustomerReviewsArgs, QueryFeedbacksArgs, QueryNewsArgs, QueryNewsForDetailArgs, QueryOurPartnersArgs } from 'src/app/shared/models/graphql.models';
import { LikeNewsMutation } from '../graphql/mutation/like-news-mutation.service';
import { BannerGroupsQuery } from '../graphql/query/banner-group-query.service';
import { CustomerReviewsQuery } from '../graphql/query/customer-reviews-query.service';
import { OurPartnersQuery } from '../graphql/query/our-partner-query.service';

@Injectable({
    providedIn: 'root'
})
export class CmsService {

    constructor(
        private _feedbacksQuery: FeedbacksQuery,
        private _customerReviewsQuery: CustomerReviewsQuery,
        private _ourPartnersQuery: OurPartnersQuery,
        private _newsQuery: NewsQuery,
        private _newsForDetailQuery: NewsForDetailQuery,
        private _bannersQuery: BannersQuery,
        private _bannerGroupQuery: BannerGroupsQuery,
        private _likeNews: LikeNewsMutation,
    ) { }

    getFeedbacks(args?: QueryFeedbacksArgs) {
        return this._feedbacksQuery.watch(args).valueChanges.pipe(
            map(res => res.data.feedbacks)
        )
    }
    getCustomerReviews(args?: QueryCustomerReviewsArgs) {
        return this._customerReviewsQuery.watch(args).valueChanges.pipe(
            map(res => res.data.customerReviews)
        )
    }
    getOurPartners(args?: QueryOurPartnersArgs) {
        return this._ourPartnersQuery.watch(args).valueChanges.pipe(
            map(res => res.data.ourPartners)
        )
    }
    getNews(args?: QueryNewsArgs) {
        return this._newsQuery.watch(args).valueChanges.pipe(
            map(res => res.data.news)
        )
    }

    getNewsDetailByID(args: QueryNewsForDetailArgs) {
        return this._newsForDetailQuery.watch(args).valueChanges.pipe(
            map(res => res.data.newsForDetail)
        )
    }

    getBannerList(args?: QueryBannersArgs) {
        return this._bannersQuery.watch(args).valueChanges.pipe(
            map(res => res.data.banners)
        )
    }
    getGroupBanner(args?: QueryBannerGroupsArgs) {
        return this._bannerGroupQuery.watch(args).valueChanges.pipe(
            map(res => res.data.bannerGroups)
        )
    }
    likeNews(args: MutationNewsLikeActionArgs) {
        return this._likeNews.mutate(args).pipe(
            map(res => res.data.newsLikeAction)
        )
    }
}
