import { Injectable } from '@angular/core';
import { GetAllBranchQuery } from '../graphql/query/get-all-branch.query';
import { GetAllDistrictBranchQuery } from '../graphql/query/get-all-district-branch.query';
import { GetAllProvinceBranchQuery } from '../graphql/query/get-all-province-branch-query';
import { GetBranchByIdQuery } from '../graphql/query/get-detail-branch-id.query';
import { QueryBranchArgs, QueryBranchDetailArgs, QueryDistrictBranchesArgs, QueryProvinceBranchesArgs } from '../models/graphql.models';

@Injectable({
  providedIn: 'root'
})
export class ManageBranchService {

  constructor(
    private _getAllBranch: GetAllBranchQuery,
    private _getAllProvinceBranch: GetAllProvinceBranchQuery,
    private _getAllDistrictBranch: GetAllDistrictBranchQuery,
    private _getDetailBranch: GetBranchByIdQuery,
  ) { }

  getAllBranch(args: QueryBranchArgs) {
    return this._getAllBranch.watch(args).valueChanges;
  }
  getBranchById(args: QueryBranchDetailArgs) {
    return this._getDetailBranch.watch(args).valueChanges;
  }
  getAllProvinceBranch(args: QueryProvinceBranchesArgs) {
    return this._getAllProvinceBranch.watch(args).valueChanges;
  }
  getAllDistrictBranch(args: QueryDistrictBranchesArgs) {
    return this._getAllDistrictBranch.watch(args).valueChanges;
  }
}
