import { Injectable } from '@angular/core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(
        private _toast: ToastrService,
    ) { }

    success(title: string, message: string, options?: IndividualConfig) {
        this._toast.success(message,title,{
            timeOut: options?.timeOut ? options.timeOut : 2000,
            progressBar: options?.progressBar ? options?.progressBar : true,
            progressAnimation:  options?.progressAnimation ? options?.progressAnimation : 'decreasing',
            tapToDismiss: options?.tapToDismiss ? options?.tapToDismiss : true,
            toastClass: 'ysd-success-toast'
        })
    }

    error(title: string, message: string, options?: IndividualConfig) {
        this._toast.error(message,title,{
            timeOut: options?.timeOut ? options.timeOut : 3000,
            progressBar: options?.progressBar ? options?.progressBar : true,
            progressAnimation:  options?.progressAnimation ? options?.progressAnimation : 'decreasing',
            toastClass: 'ysd-error-toast'
        })
    }


}
