import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { CollectionQuery, CollectionsQuery, ProductForBuyBestSellerQuery, ProductsForBuyQuery } from 'src/app/shared/graphql/query';
import { QueryCollectionArgs, QueryCollectionsArgs, QueryFlashSaleArgs, QueryProductForBuyWithVariantArgs, QueryProductsForBuyArgs } from 'src/app/shared/models/graphql.models';
import { FlashSaleQuery } from '../graphql/query/flashsale-query.service';
import { GetAllForBuyWithVariantQuery } from '../graphql/query/get-all-product-variant.query';

@Injectable({
  providedIn: 'root'
})
export class SharedProductService {

  constructor(
      private _productForBuyBestSellerQuery :ProductForBuyBestSellerQuery,
      private _collectionQuery: CollectionQuery,
      private _collectionsQuery: CollectionsQuery,
      private _productsForBuyQuery: ProductsForBuyQuery,
      private _flashSaleQuery: FlashSaleQuery,
      private _getAllForBuyWithVariant: GetAllForBuyWithVariantQuery,

  ) { }

  getBestSellerProductList() {
      return this._productForBuyBestSellerQuery.watch().valueChanges.pipe(
          map(res => res.data.productForBuyBestSeller)
      )
  }

  getCollectionByID(args: QueryCollectionArgs) {
      return this._collectionQuery.watch(args).valueChanges.pipe(
          map(res => res.data.collection)
      )
  }

  getCollectionList(args?: QueryCollectionsArgs) {
      return this._collectionsQuery.watch(args).valueChanges.pipe(
          map(res => res.data.collections)
      )
  }

  getProductList(args?: QueryProductsForBuyArgs) {
      return this._productsForBuyQuery.watch(args).valueChanges.pipe(
          map(res => res.data.productsForBuy)
      )
  }

  getFlashSale(args?: QueryFlashSaleArgs) {
    return this._flashSaleQuery.watch(args).valueChanges.pipe(
        map(res => res.data.flashSale)
    )
}
  getAllForBuyWithVariant(args: QueryProductForBuyWithVariantArgs) {
    return this._getAllForBuyWithVariant.watch(args).valueChanges;
  }
}
