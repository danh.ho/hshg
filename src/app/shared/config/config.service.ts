import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private configUrl: any = {
    "URL": window.location.protocol + '//' + window.location.hostname
  }
  
  getConfig() {
    return this.configUrl;
  }
}
