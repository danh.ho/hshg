import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { CmsService } from 'src/app/shared/services/cms.service';
import { CMSStateModel, GetBannerList, GetFeedbacks, GetGroupBanner } from 'src/app/shared/store/cms';
import { CMSInitialState } from 'src/app/shared/store/cms/cms-state.model';
import { GetCustomerReviews, GetOurPartners } from './cms.action';


@Injectable({ providedIn: 'root' })
@State<CMSStateModel>({
    name: 'cms',
    defaults: CMSInitialState,
})
export class CmsState {
    @Selector()
    static getFeedBacks({ feedbacks }: CMSStateModel) {
        return feedbacks;
    }
    @Selector()
    static getCustomerReviews({ customerRevies }: CMSStateModel) {
        return customerRevies;
    }
    @Selector()
    static getOurPartners({ ourPartners }: CMSStateModel) {
        return ourPartners;
    }
    @Selector()
    static getBannerList({ bannerList }: CMSStateModel) {
        return bannerList ? bannerList.edges.map(ele => ele.node) : [];
    }
    @Selector()
    static getBannerGroup({ bannerGroup }: CMSStateModel) {
        return bannerGroup ? bannerGroup.edges.map(ele => ele.node) : [];
    }

    constructor(private _cmsService: CmsService) { }

    @Action(GetFeedbacks)
    getFeedbacks(
        { patchState }: StateContext<CMSStateModel>,
        { payload }: GetFeedbacks
    ) {

        return this._cmsService.getFeedbacks({ ...payload, orderBy: 'sort_order' }).pipe(
            tap(res => {
                patchState({ feedbacks: res.edges.map(ele => ele.node) });
            })
        )
    }
    @Action(GetCustomerReviews)
    getCustomerReviews(
        { patchState }: StateContext<CMSStateModel>,
        { payload }: GetCustomerReviews
    ) {

        return this._cmsService.getCustomerReviews({ ...payload, orderBy: 'sort_order' }).pipe(
            tap(res => {
                patchState({ customerRevies: res.edges.map(ele => ele.node) });
            })
        )
    }
    @Action(GetOurPartners)
    getOurPartners(
        { patchState }: StateContext<CMSStateModel>,
        { payload }: GetOurPartners
    ) {

        return this._cmsService.getOurPartners({ ...payload, orderBy: 'sort_order' }).pipe(
            tap(res => {
                patchState({ ourPartners: res.edges.map(ele => ele.node) });
            })
        )
    }
    @Action(GetBannerList)
    getBannerList(
        { patchState }: StateContext<CMSStateModel>,
        { payload }: GetBannerList
    ) {
        return this._cmsService.getBannerList({ ...payload, orderBy: 'sort_order' }).pipe(
            tap(res => {
                patchState({ bannerList: res })
            })
        )
    }
    @Action(GetGroupBanner)
    getGroupBanner(
        { patchState }: StateContext<CMSStateModel>,
    ) {
        return this._cmsService.getGroupBanner().pipe(
            tap(res => {
                patchState({ bannerGroup: res })
            })
        )
    }
}
