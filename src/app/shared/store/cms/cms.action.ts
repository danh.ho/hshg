import { QueryBannersArgs, QueryCustomerReviewsArgs, QueryFeedbacksArgs, QueryOurPartnersArgs } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    GET_FEEDBACKS = '[CMS_FEEDBACK] Get Feedbacks',
    GET_CUSTOMER_REVIEW = '[CMS_FEEDBACK] Get Customer Reivew',
    GET_OUR_PARTNER = '[CMS_FEEDBACK] Get Our Partner',

    GET_BANNER_LIST = '[CMS.BANNER] Get Banner List',
    GET_GROUP_BANNER = '[CMS.BANNER] Get Group Banner',
}

export class GetFeedbacks {
    static readonly type = ACTIONS.GET_FEEDBACKS;
    constructor(public readonly payload?: QueryFeedbacksArgs) { }
}
export class GetCustomerReviews {
    static readonly type = ACTIONS.GET_CUSTOMER_REVIEW;
    constructor(public readonly payload?: QueryCustomerReviewsArgs) { }
}
export class GetOurPartners{
    static readonly type = ACTIONS.GET_OUR_PARTNER;
    constructor(public readonly payload?: QueryOurPartnersArgs) { }
}
export class GetBannerList {
    static readonly type = ACTIONS.GET_BANNER_LIST;
    constructor(public readonly payload?: QueryBannersArgs) { }
}
export class GetGroupBanner {
    static readonly type = ACTIONS.GET_GROUP_BANNER;
    constructor() { }
}