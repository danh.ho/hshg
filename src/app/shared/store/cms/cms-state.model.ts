import {  BannerGroupNodeConnection, BannerNodeConnection, CustomerReviewNode, FeedbackNode, OurPartnerNode, } from "src/app/shared/models/graphql.models";

export interface CMSStateModel {
    feedbacks?: Array<FeedbackNode>
    customerRevies?: Array<CustomerReviewNode>
    ourPartners?: Array<OurPartnerNode>
    bannerList?: BannerNodeConnection
    bannerGroup?: BannerGroupNodeConnection;
}

export const CMSInitialState: CMSStateModel = {
    feedbacks: [],
};
