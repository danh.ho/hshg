import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { NotificationService } from "src/app/shared/services";
import { AuthService } from "src/app/shared/services/auth.service";
import { authInitialState, AuthStateModel, GetSiteTokenGmail, GetSiteTokenVnPay, Login, LoginFailed, LoginSuccessfully, LogOut } from "src/app/shared/store/auth";
import { RegisterAccountVerify, UserError, UserUserType } from "../../models/graphql.models";
import { CreateNewPassword, CreateNewPasswordFailed, CreateNewPasswordSuccessfully, ForgotPasswordToken, ForgotPasswordTokenFailed, ForgotPasswordVerify, ForgotPasswordVerifyFailed, ForgotPasswordVerifySuccessfully, GetSiteTokenFacebook, Register, RegisterFailed, RegisterSuccessfully, SendCode, SendCodeSuccessfully, SetToken, VerifyCode, VerifyCodeFailed, VerifyCodeSuccessfully } from "./auth.action";

import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({ providedIn: 'root' })
@State<AuthStateModel>({
    name: 'auth',
    defaults: authInitialState,
})

export class AuthState {
    @Selector()
    static getToken({ token }: AuthStateModel) {
        return token
    }

    @Selector()
    static isLogin({ token }: AuthStateModel) {
        return token ? true : false;
    }
    @Selector()
    static getFacebookId({ facebookId }: AuthStateModel) {
        return facebookId;
    }
    @Selector()
    static getGmailId({ gmailId }: AuthStateModel) {
        return gmailId;
    }
    @Selector()
    static getVnPay({ vnPay }: AuthStateModel) {
        return vnPay;
    }
    constructor(
        private _auth: AuthService,
        private _notify: NotificationService,
        private _router: Router,
        private _spinner: NgxSpinnerService
    ) { }
    @Action(SetToken)
    SetToken(
        { dispatch }: StateContext<AuthStateModel>, { payload }: SetToken) {
        const { token } = payload;
        const profile = null;
        dispatch(new LoginSuccessfully({ token, profile }))
    }
    @Action(Login)
    Login(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: Login,
    ) {
        return this._auth.authLogin({ input: payload }).pipe(
            tap(res => {
                const { status, errors, user, token } = res;
                if (status && (user.userType === UserUserType.Customer || user.userType === UserUserType.Guest)) {
                    dispatch(new LoginSuccessfully({ token, profile: user }))
                } else if (!errors) {
                    const error: UserError[] = [{ field: "email", message: "Email or Password does not match", code: "AUTH_15", __typename: "UserError" }]
                    dispatch(new LoginFailed(error[0]))
                } else {
                    dispatch(new LoginFailed(errors[0]))
                }
            })
        )
    }

    @Action(LoginSuccessfully)
    loginSuccessfully(
        { patchState }: StateContext<AuthStateModel>,
        { payload }: LoginSuccessfully,
    ) {
        const { token, profile } = payload;
        patchState({ token, profile });
        //this._router.navigate(['/'])
    }

    @Action(LoginFailed)
    loginFailed(
        { error }: LoginFailed,
    ) {
        const userError = error;
        return userError;
        // return this._translate.get(`FAILED.USER_ERRORS.${error.code}`).pipe(
        //     tap(translation => this._notify.error('',translation))
        // )
    }

    @Action(LogOut)
    LogOut(
        { setState }: StateContext<AuthStateModel>
    ) {
        setState(authInitialState)
    }
    @Action(Register)
    Register(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: Register,
    ) {
        return this._auth.authRegister({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    return dispatch(new RegisterSuccessfully())
                } else {
                    return dispatch(new RegisterFailed(errors[0]))
                }
            })
        )
    }
    // @Action(RegisterSuccessfully)
    // registerSuccessfully(
    //     { patchState }: StateContext<EmailRegisterStateModel>,
    //     { payload }: RegisterSuccessfully,
    // ) {
    //     const email = payload;
    //     return email;
    //     patchState({ email});
    // }
    @Action(RegisterFailed)
    RegisterFailed(
        { error }: RegisterFailed,
    ) {
        const messError = error;
        return messError;
    }
    @Action(VerifyCode)
    VerifyCode(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: VerifyCode,
    ) {
        return this._auth.authRegisterVerifyCode({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    dispatch(new VerifyCodeSuccessfully())
                } else {
                    dispatch(new VerifyCodeFailed(errors[0]));
                }
            })
        )
    }
    @Action(VerifyCodeFailed)
    VerifyCodeFailed(
        { error }: VerifyCodeFailed,
    ) {
        const userError = error;
        return userError;
    }
    @Action(SendCode)
    SendCode(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: Register,
    ) {
        return this._auth.authRegister({ input: payload }).pipe(
            tap(res => {
                this._spinner.show()
                const { status } = res;
                if (status) {
                    return dispatch(new SendCodeSuccessfully())
                }
            })
        )
    }
    @Action(ForgotPasswordToken)
    ForgotPasswordToken(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: ForgotPasswordToken,
    ) {
        return this._auth.authForgotPasswordToken({ input: payload }).pipe(
            tap(res => {
                this._spinner.show()
                const { status, errors } = res;
                if (status) {
                    dispatch(new SendCodeSuccessfully()).subscribe(() => this._spinner.hide())
                }
                else {
                    dispatch(new ForgotPasswordTokenFailed(errors[0]))
                }
            })
        )
    }
    @Action(ForgotPasswordTokenFailed)
    ForgotPasswordTokenFailed(
        { error }: ForgotPasswordTokenFailed,
    ) {
        const userError = error;
        return userError;
    }
    @Action(ForgotPasswordVerify)
    ForgotPasswordVerify(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: ForgotPasswordVerify,
    ) {
        return this._auth.authForgotPasswordVerify({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    dispatch(new ForgotPasswordVerifySuccessfully())
                }
                else {
                    dispatch(new ForgotPasswordVerifyFailed(errors[0]))
                }
            })
        )
    }
    @Action(CreateNewPassword)
    CreateNewPassword(
        { dispatch }: StateContext<AuthStateModel>,
        { payload }: CreateNewPassword,
    ) {
        return this._auth.authCreateNewPassword({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    dispatch(new CreateNewPasswordSuccessfully())
                }
                else {
                    dispatch(new CreateNewPasswordFailed(errors[0]))
                }
            })
        )
    }
    @Action(GetSiteTokenFacebook)
    getSiteTokenFacebook(
        { patchState }: StateContext<AuthStateModel>,
    ) {
        return this._auth.getSiteTokenFacebook().pipe(
            tap(({data}) => {
                if(data){
                    patchState({
                        facebookId: data.siteTokenFacebookId,
                    });
                }
            })
        )
    }
    @Action(GetSiteTokenGmail)
    getSiteTokenGmail(
        { patchState }: StateContext<AuthStateModel>,
    ) {
        return this._auth.getSiteTokenGmail().pipe(
            tap(({data}) => {
                if(data){
                    patchState({
                        gmailId: data.gmailClientId,
                    });
                }
            })
        )
    }
    @Action(GetSiteTokenVnPay)
    getSiteTokenVnPay(
        { patchState }: StateContext<AuthStateModel>,
    ) {
        return this._auth.getSiteTokenVnPay().pipe(
            tap(({data}) => {
                if(data){
                    patchState({
                        vnPay: data.siteTokenVnpayDetail,
                    });
                }
            })
        )
    }
}
