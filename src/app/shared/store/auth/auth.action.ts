import { AuthencationLoginInput, CreateNewPasswordInput, ForgotPasswordInput, QuerySiteTokenFacebooksArgs, RegisterAccountTokenInput, RegisterAccountVerify, TokenVerifyInput, UserError, UserProfile } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    LOGIN = '[AUTH] Login',
    LOGIN_FAILED = '[AUTH] Login Failed',
    LOGIN_SUCCESSFULLY = '[AUTH] Login Successfully',
    SETTOKEN = '[AUTH] Set Token',
    LOG_OUT = '[AUTH] Log Out',
    REGISTER = '[AUTH] Register',
    SENDCODE = '[AUTH] Send Code',
    SENDCODE_SUCCESSFULLY = '[AUTH] Send Code Successfully',
    VERIFY_CODE = '[AUTH] Verify code',
    VERIFY_CODE_FAILED = '[AUTH] Verify code Failed',
    REGISTER_SUCCESSFULLY = '[AUTH] Register Successfully',
    REGISTER_FAILED = '[AUTH] Register Failed',
    FORGOT_PASSWORD = '[AUTH] Forgot Password',
    FORGOT_PASSWORD_FAILED = '[AUTH] Forgot Password Failed',
    FORGOT_PASSWORD_VERIFY = '[AUTH] Forgot Password Verify',
    FORGOT_PASSWORD_VERIFY_SUCCESSFULLY = '[AUTH] Forgot Password Verify Successfully',
    FORGOT_PASSWORD_VERIFY_FAILED = '[AUTH] Forgot Password Verify Failed',
    CREATE_NEW_PASSWORD = '[AUTH] Create New Password',
    CREATE_NEW_PASSWORD_SUCCESSFULLY = '[AUTH] Create New Password Successfully',
    CREATE_NEW_PASSWORD_FAILED = '[AUTH] Create New Password Failed',

    GET_SITE_TOKEN_FACEBOOK = '[AUTH] Get Site Token Facebook',
    GET_SITE_TOKEN_GMAIL = '[AUTH] Get Site Token Gmail',
    GET_SITE_TOKEN_VNPAY = '[AUTH] Get Site Token VnPay',
}

export class Login {
    static readonly type = ACTIONS.LOGIN;
    constructor(public readonly payload: AuthencationLoginInput) { }
}

export class LoginSuccessfully {
    static readonly type = ACTIONS.LOGIN_SUCCESSFULLY;
    constructor(public readonly payload: { token: string, profile: UserProfile }) { }
}
export class SetToken {
    static readonly type = ACTIONS.SETTOKEN;
    constructor(public readonly payload: { token: string }) { }
}
export class LoginFailed {
    static readonly type = ACTIONS.LOGIN_FAILED;
    constructor(public readonly error: UserError) { }
}

export class LogOut {
    static readonly type = ACTIONS.LOG_OUT;
}
export class Register {
    static readonly type = ACTIONS.REGISTER;
    constructor(public readonly payload: RegisterAccountTokenInput) { }
}
export class SendCode {
    static readonly type = ACTIONS.SENDCODE;
    constructor(public readonly payload: RegisterAccountTokenInput) { }
}
export class SendCodeSuccessfully {
    static readonly type = ACTIONS.SENDCODE_SUCCESSFULLY;
    constructor() { }
}
export class RegisterSuccessfully {
    static readonly type = ACTIONS.REGISTER_SUCCESSFULLY;
}
export class RegisterFailed {
    static readonly type = ACTIONS.REGISTER_FAILED;
    constructor(public readonly error: UserError) { }
}
export class VerifyCode {
    static readonly type = ACTIONS.VERIFY_CODE;
    constructor(public readonly payload: TokenVerifyInput) { }
}
export class VerifyCodeSuccessfully {
    static readonly type = ACTIONS.REGISTER_SUCCESSFULLY;
    constructor() { }
}
export class VerifyCodeFailed {
    static readonly type = ACTIONS.VERIFY_CODE_FAILED;
    constructor(public readonly error: UserError) { }
}
export class ForgotPasswordToken {
    static readonly type = ACTIONS.FORGOT_PASSWORD;
    constructor(public readonly payload: ForgotPasswordInput) { }
}
export class ForgotPasswordTokenFailed {
    static readonly type = ACTIONS.FORGOT_PASSWORD_FAILED;
    constructor(public readonly error: UserError) { }
}
export class ForgotPasswordVerify {
    static readonly type = ACTIONS.FORGOT_PASSWORD_VERIFY;
    constructor(public readonly payload: TokenVerifyInput) { }
}
export class ForgotPasswordVerifySuccessfully {
    static readonly type = ACTIONS.FORGOT_PASSWORD_VERIFY_SUCCESSFULLY;
    constructor() { }
}
export class ForgotPasswordVerifyFailed {
    static readonly type = ACTIONS.FORGOT_PASSWORD_FAILED;
    constructor(public readonly error: UserError) { }
}
export class CreateNewPassword {
    static readonly type = ACTIONS.CREATE_NEW_PASSWORD;
    constructor(public readonly payload: CreateNewPasswordInput) { }
}
export class CreateNewPasswordSuccessfully {
    static readonly type = ACTIONS.CREATE_NEW_PASSWORD_SUCCESSFULLY;
    constructor() { }
}
export class CreateNewPasswordFailed {
    static readonly type = ACTIONS.CREATE_NEW_PASSWORD_FAILED;
    constructor(public readonly error: UserError) { }
}
export class GetSiteTokenFacebook {
    static readonly type = ACTIONS.GET_SITE_TOKEN_FACEBOOK;
    constructor() { }
}
export class GetSiteTokenGmail {
    static readonly type = ACTIONS.GET_SITE_TOKEN_GMAIL;
    constructor() { }
}
export class GetSiteTokenVnPay {
    static readonly type = ACTIONS.GET_SITE_TOKEN_VNPAY;
    constructor() { }
}