import { UserProfile, UserAddressNode } from "src/app/shared/models/graphql.models";

export interface AuthStateModel {
    token?: string,
    profile?: UserProfile,
    profileAddress?: UserAddressNode,
    facebookId?: string;
    gmailId?: string;
    vnPay?: any;
}
export const authInitialState: AuthStateModel = {
    token: '',
    profile: null,
    profileAddress: null,
    facebookId: '',
    gmailId: '',
    vnPay: null
};
