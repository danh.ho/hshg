import { QueryCollectionsArgs, QueryFlashSaleArgs, QueryProductForBuyWithVariantArgs, QueryProductsForBuyArgs } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    GET_BEST_SELLER_PRODUCT_LIST = '[Product] Get Best Seller Product List',

    GET_COLLECTION_BY_ID = '[Product.Collection] Get Collection By ID',
    GET_COLLECTION_LIST = '[Product.Collection] Get Collection List',

    GET_PRODUCT_LIST = '[PRODUCT] Get Product List',
    GET_PRODUCT_LIST_BY_COLLECTION = '[PRODUCT] Get Product List By Collection',
    GET_BEST_SELLER_PRODUCTS = '[PRODUCT] Get Best Seller Products',

    GET_FLASHSALE = '[PRODUCT] Get FlashSale',

    LOAD_PRODUCT_WITH_VARIANT_ALL = '[Product] Load Product With Variant All',
    LOAD_PRODUCT_WITH_VARIANT_HOME_PAGE = '[Product] Load Product With Variant Home Page',
}

export class GetBestSellerProductList {
    static readonly type = ACTIONS.GET_BEST_SELLER_PRODUCT_LIST;
}

export class GetCollectionByID {
    static readonly type = ACTIONS.GET_COLLECTION_BY_ID;
    constructor(public readonly payload: string) { }
}

export class GetCollectionList {
    static readonly type = ACTIONS.GET_COLLECTION_LIST;
    constructor(public readonly payload?: QueryCollectionsArgs) { }
}

export class GetProductList {
    static readonly type = ACTIONS.GET_PRODUCT_LIST;
    constructor(public readonly payload?: QueryProductsForBuyArgs) { }
}
export class GetProductListByCollection {
    static readonly type = ACTIONS.GET_PRODUCT_LIST_BY_COLLECTION;
    constructor(public readonly payload?: QueryProductsForBuyArgs) { }
}
export class GetBestSellerProducts {
    static readonly type = ACTIONS.GET_BEST_SELLER_PRODUCTS;
    constructor(public readonly payload?: QueryProductsForBuyArgs) { }
}

export class GetFlashSale {
    static readonly type = ACTIONS.GET_FLASHSALE;
    constructor(public readonly payload?: QueryFlashSaleArgs) { }
}
export class LoadProductsWithVariantAll {
    static readonly type = ACTIONS.LOAD_PRODUCT_WITH_VARIANT_ALL;
    constructor(public readonly payload?: QueryProductForBuyWithVariantArgs) {}
}
export class LoadProductsWithVariantHomePage {
    static readonly type = ACTIONS.LOAD_PRODUCT_WITH_VARIANT_HOME_PAGE;
    constructor(public readonly payload?: QueryProductForBuyWithVariantArgs) {}
}