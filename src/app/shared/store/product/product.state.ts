import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { ProductService } from "src/app/category/product.service";
import { SharedProductService } from "src/app/shared/services/shared-product.service";
import { productInitialState, ProductStateModel } from "src/app/shared/store/product";
import { GetBestSellerProductList, GetBestSellerProducts, GetCollectionByID, GetCollectionList, GetFlashSale, GetProductList, GetProductListByCollection, LoadProductsWithVariantAll, LoadProductsWithVariantHomePage } from "src/app/shared/store/product/product.action";


@Injectable({ providedIn: 'root' })
@State<ProductStateModel>({
    name: 'product',
    defaults: productInitialState,
})
export class ProductState {
    @Selector()
    static getBestSellerProductList({ bestSellerProductList }: ProductStateModel) {
        return bestSellerProductList;
    }

    @Selector()
    static getCollectionNode({ collectionNode }: ProductStateModel) {
        return collectionNode;
    }

    @Selector()
    static getCollectionNodeConnection({ collectionNodeConnection }: ProductStateModel) {
        return collectionNodeConnection ? collectionNodeConnection.edges.map(ele => ele.node) : [];;
    }

    @Selector()
    static getProductsNodeConnection({ productsNodeConnection }: ProductStateModel) {
        return productsNodeConnection ? productsNodeConnection.edges.map(ele => ele.node) : [];;
    }

    @Selector()
    static getBestSellerProducts({ bestSellerProducts }: ProductStateModel) {
        return bestSellerProducts ? bestSellerProducts.edges.map(ele => ele.node) : [];
    }

    @Selector()
    static getNewestProducts({ newestProductsNodeConnection }: ProductStateModel) {
        return newestProductsNodeConnection ? newestProductsNodeConnection.edges.map(ele => ele.node) : [];
    }
    @Selector()
    static getFlashsaleProducts({ flashsaleNodeConnection }: ProductStateModel) {
        return flashsaleNodeConnection ? flashsaleNodeConnection.edges.map(ele => ele.node) : [];
    }
    @Selector()
    static getCombo({ comboNodeConnection }: ProductStateModel) {
        return comboNodeConnection ? comboNodeConnection.edges.map(ele => ele.node) : [];
    }

    @Selector()
    static getFlashsale({ flashSale }: ProductStateModel) {
        return flashSale ? flashSale.edges.map(ele => ele.node) : [];
    }

    @Selector()
    static getProductWithVariantAll({ productWithVariantAll }: ProductStateModel) {
        return productWithVariantAll;
    }
    @Selector()
    static getProductWithVariantHomePage({ productWithVariantHomePage }: ProductStateModel) {
        return productWithVariantHomePage ? productWithVariantHomePage.edges.map(ele => ele.node) : [];
    }
    constructor(private _productService: SharedProductService) { }

    @Action(GetBestSellerProductList)
    getBestSellerProductList(
        { patchState }: StateContext<ProductStateModel>,
    ) {
        return this._productService.getBestSellerProductList().pipe(
            tap(res => {
                return patchState({ bestSellerProductList: res })
            })
        )
    }

    @Action(GetCollectionByID)
    getCollectionByID(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetCollectionByID
    ) {
        return this._productService.getCollectionByID({ id: payload }).pipe(
            tap(res => {
                return patchState({ collectionNode: res })
            })
        )
    }

    @Action(GetCollectionList)
    getCollectionList(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetCollectionList
    ) {
        return this._productService.getCollectionList(payload).pipe(
            tap(res => {
                return patchState({ collectionNodeConnection: res })
            })
        )
    }

    @Action(GetProductList)
    getProductList(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetProductList
    ) {
        return this._productService.getProductList(payload).pipe(
            tap(res => {
                switch (payload.collectionItemCode) {
                    case 'featured-products':
                        return patchState({ bestSellerProducts: res });
                    case 'combo':
                        return patchState({ comboNodeConnection: res })
                    case 'newest':
                        return patchState({ newestProductsNodeConnection: res })
                    case 'flashsale':
                        return patchState({ flashsaleNodeConnection: res })
                    default:
                        return patchState({ productsNodeConnection: res })
                }

            })
        )
    }

    @Action(GetProductListByCollection)
    getProductListByCollection(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetProductList
    ) {
        return this._productService.getProductList(payload).pipe(
            tap(res => {
                return patchState({ productsNodeConnection: res })
            })
        )
    }
    @Action(GetBestSellerProducts)
    getBestSellerProducts(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetBestSellerProducts
    ) {
        return this._productService.getProductList({ ...payload, collectionName: 'best seller' }).pipe(
            tap(res => {
                return patchState({ bestSellerProducts: res })
            })
        )
    }

    @Action(GetFlashSale)
    getFlashSale(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: GetFlashSale
    ) {
        return this._productService.getFlashSale(payload).pipe(
            tap(res => {
                return patchState({ flashSale: res })
            })
        )
    }

    @Action(LoadProductsWithVariantAll)
    loadProductsWithVariantAll(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: LoadProductsWithVariantAll
    ) {
        return this._productService.getAllForBuyWithVariant({ ...payload }).pipe(
            tap(({ data }) => {
                patchState({
                    productWithVariantAll: data.productForBuyWithVariant,
                });
            })
        );
    }
    @Action(LoadProductsWithVariantHomePage)
    loadProductsWithVariantHomePage(
        { patchState }: StateContext<ProductStateModel>,
        { payload }: LoadProductsWithVariantHomePage
    ) {
        return this._productService.getAllForBuyWithVariant({ ...payload }).pipe(
            tap(({ data }) => {
                patchState({
                    productWithVariantHomePage: data.productForBuyWithVariant,
                });
            })
        );
    }
}
