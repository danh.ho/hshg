import {
    CollectionNode, CollectionNodeConnection,
    FlashSaleNode,
    FlashSaleNodeConnection,
    FlashSaleNodeEdge,
    ProductAndVariantConnection,
    ProductForBuyNode, ProductForBuyNodeConnection
} from "src/app/shared/models/graphql.models";

export interface ProductStateModel {
    bestSellerProductList?: Array<ProductForBuyNode>;
    collectionNode?: CollectionNode,
    collectionNodeConnection?: CollectionNodeConnection,
    productsNodeConnection?: ProductForBuyNodeConnection,
    bestSellerProducts?: ProductForBuyNodeConnection,
    comboNodeConnection?: ProductForBuyNodeConnection,
    newestProductsNodeConnection?: ProductForBuyNodeConnection,
    flashsaleNodeConnection?: ProductForBuyNodeConnection,
    flashSale?: FlashSaleNodeConnection,
    productWithVariantAll?: ProductAndVariantConnection;
    productWithVariantHomePage?: ProductAndVariantConnection;
}

export const productInitialState: ProductStateModel = {
    bestSellerProductList: [],
};
