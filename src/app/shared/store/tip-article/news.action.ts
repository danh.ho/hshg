import { NewsLikeInput, QueryNewsArgs } from "src/app/shared/models/graphql.models"

const enum ACTIONS {
    GET_NEWS = '[NEWS] Get News',
    GET_PROMOTION = '[NEWS] Get Promotions',
    GET_FOOD_CORNER = '[NEWS] Get Food Corners',
    GET_RECRUIT= '[NEWS] Get Recruit',
    GET_GENERAL_POLICY = '[NEWS] Get General Policy',
    GET_NEWS_DETAIL_BY_ID = '[NEWS] Get News Detail By ID',
    GET_NEWS_ALL = '[NEWS] Get News All',
    LIKE_NEWS = '[NEWS] Like News',
    LIKE_NEWS_SUCCESSFULLY = '[NEWS] Like News Successfully',
}

export class GetNews {
    static readonly type = ACTIONS.GET_NEWS;
    constructor(public readonly payload?: QueryNewsArgs) { }
}
export class GetPromotion {
    static readonly type = ACTIONS.GET_PROMOTION;
    constructor(public readonly payload?: QueryNewsArgs) { }
}
export class GetFoodCorner {
    static readonly type = ACTIONS.GET_FOOD_CORNER;
    constructor(public readonly payload?: QueryNewsArgs) { }
}
export class GetRecruit {
    static readonly type = ACTIONS.GET_RECRUIT;
    constructor(public readonly payload?: QueryNewsArgs) { }
}
export class GetGeneralPolicy {
    static readonly type = ACTIONS.GET_GENERAL_POLICY;
    constructor(public readonly payload?: QueryNewsArgs) { }
}
export class GetNewsDetailByID {
    static readonly type = ACTIONS.GET_NEWS_DETAIL_BY_ID;
    constructor(public readonly payload: any) {}
}

export class GetAllNews {
    static readonly type = ACTIONS.GET_NEWS_ALL;
    constructor(public readonly payload?: QueryNewsArgs) { }
}

export class LikeNews {
    static readonly type = ACTIONS.LIKE_NEWS;
    constructor(public readonly payload: NewsLikeInput) { }
}
export class LikeNewsSuccessfully {
    static readonly type = ACTIONS.LIKE_NEWS_SUCCESSFULLY;
    constructor(public readonly payload: any) {}
}