import { NewsNode, NewsNodeConnection, } from "src/app/shared/models/graphql.models";

export interface NewsStateModel {
    newsNodeConnection?: NewsNodeConnection
    newsAllNodeConnection?: NewsNodeConnection
    promotionNodeConnection?: NewsNodeConnection
    foodCornerNodeConnection?: NewsNodeConnection
    recruitNodeConnection?: NewsNodeConnection
    policyNodeConnection?: NewsNodeConnection
    newsNode?: NewsNode,
    newsAllNode?: NewsNode,
}

export const newsInitialState: NewsStateModel = { }
