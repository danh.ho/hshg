import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { CmsService } from 'src/app/shared/services/cms.service';
import { NewsStateModel, GetNews, GetNewsDetailByID } from 'src/app/shared/store/tip-article';
import { newsInitialState } from 'src/app/shared/store/tip-article/news-state.model';
import { GetFoodCorner, GetGeneralPolicy, GetPromotion, GetRecruit, LikeNews, LikeNewsSuccessfully } from './news.action';

@Injectable({ providedIn: 'root' })
@State<NewsStateModel>({
    name: 'cms_news',
    defaults: newsInitialState,
})
export class CmsNewsState {
    @Selector()
    static getNewsTip({ newsNodeConnection }: NewsStateModel) {
        let newsArr = [];
        if (newsNodeConnection) newsArr = [...newsNodeConnection.edges.map(ele => ele.node)]
        return newsArr;
    }

    @Selector()
    static getTips({ newsNodeConnection }: NewsStateModel) {
        if (!newsNodeConnection) return [];
        return newsNodeConnection.edges.map(ele => ele.node).filter(ele => ele.category.name === 'Tip làm đẹp')
    }

    @Selector()
    static getTraining({ newsNodeConnection }: NewsStateModel) {
        if (!newsNodeConnection) return [];
        return newsNodeConnection.edges.map(ele => ele.node).filter(ele => ele.category.name === 'Đào tạo')
    }

    @Selector()
    static getNews({ newsNodeConnection }: NewsStateModel) {
        if (!newsNodeConnection) return [];
        return newsNodeConnection.edges.map(ele => ele.node);
    }
    @Selector()
    static getNewsNodeConnection({ newsAllNodeConnection }: NewsStateModel) {
        return newsAllNodeConnection
    }
    @Selector()
    static getNewsAll({ newsNodeConnection }: NewsStateModel) {
        if (!newsNodeConnection) return [];
        return newsNodeConnection.edges.map(ele => ele.node).filter(ele => ele.category.name === 'Tin tức');
    }

    @Selector()
    static getRecruit({ recruitNodeConnection }: NewsStateModel) {
        return recruitNodeConnection.edges.map(ele => ele.node);
    }

    @Selector()
    static getGeneralPolicy({ policyNodeConnection }: NewsStateModel) {
        return policyNodeConnection.edges.map(ele => ele.node);
    }

    @Selector()
    static getFoodCorner({ foodCornerNodeConnection }: NewsStateModel) {
        return foodCornerNodeConnection;
    }

    @Selector()
    static getNewsNode({newsNode}: NewsStateModel) {
        return newsNode;
    }
    @Selector()
    static getPromotion({promotionNodeConnection}: NewsStateModel) {
        return promotionNodeConnection;
    }
    
    constructor(private _cmsService: CmsService) {
    }

    @Action(GetNews)
    getNews(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetNews
    ) {
        return this._cmsService.getNews({ ...payload,  }).pipe(
            tap(res => {
                return patchState({ newsNodeConnection: res, newsAllNodeConnection: res });
            })
        )
    }
    @Action(GetPromotion)
    getPromotion(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetPromotion
    ) {
        return this._cmsService.getNews({ ...payload,  }).pipe(
            tap(res => {
                return patchState({ promotionNodeConnection: res });
            })
        )
    }
    @Action(GetFoodCorner)
    getFoodCorner(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetFoodCorner
    ) {
        return this._cmsService.getNews({ ...payload,  }).pipe(
            tap(res => {
                return patchState({ foodCornerNodeConnection: res });
            })
        )
    }
    @Action(GetRecruit)
    getRecruit(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetRecruit
    ) {
        return this._cmsService.getNews({ ...payload,  }).pipe(
            tap(res => {
                return patchState({ recruitNodeConnection: res });
            })
        )
    }
    @Action(GetGeneralPolicy)
    getGeneralPolicy(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetGeneralPolicy
    ) {
        return this._cmsService.getNews({ ...payload,  }).pipe(
            tap(res => {
                return patchState({ policyNodeConnection: res });
            })
        )
    }
    @Action(GetNewsDetailByID)
    getNewsDetailByID(
        { patchState }: StateContext<NewsStateModel>,
        { payload }: GetNewsDetailByID
    ) {
        return this._cmsService.getNewsDetailByID(payload).pipe(
            tap(res => {
                if (res) return patchState({ newsNode: res })
                else return patchState({ newsNode: null })
            })
        )
    }
    @Action(LikeNews)
    likeNews(
      { dispatch }: StateContext<NewsStateModel>,
      { payload }: LikeNews
    ) {
      return this._cmsService.likeNews({ input: payload}).pipe(
        tap(res => {
          const { status } = res;
          if (status) {
            dispatch(new LikeNewsSuccessfully(res))
          } else {
          }
        })
      );
    }
}
