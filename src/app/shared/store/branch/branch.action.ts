import { QueryBranchArgs, QueryBranchDetailArgs, QueryDistrictBranchesArgs, QueryProvinceBranchesArgs } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    LOAD_BRANCH = '[BRANCH] Load Branch',
    LOAD_BRANCH_BY_ID = '[BRANCH] Load Branch By Id',
    LOAD_PROVINCE_BRANCH = '[BRANCH] Load Province Branch',
    LOAD_DISTRICT_BRANCH = '[BRANCH] Load District Branch',
}

export class LoadBranch {
    static readonly type = ACTIONS.LOAD_BRANCH;
    constructor(public readonly payload?: QueryBranchArgs) { }
}
export class LoadBranchById {
    static readonly type = ACTIONS.LOAD_BRANCH_BY_ID;
    constructor(public readonly payload: QueryBranchDetailArgs) { }
}
export class LoadProvinceBranch {
    static readonly type = ACTIONS.LOAD_PROVINCE_BRANCH;
    constructor(public readonly payload?: QueryProvinceBranchesArgs) { }
}
export class LoadDistrictBranch {
    static readonly type = ACTIONS.LOAD_DISTRICT_BRANCH;
    constructor(public readonly payload?: QueryDistrictBranchesArgs) { }
}