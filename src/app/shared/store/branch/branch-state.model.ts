import { BranchNode, BranchNodeConnection, DistrictBranchNodeConnection, FlatpageNode, FlatpageNodeConnection, FlatpageNodeEdge, ProvinceBranchNodeConnection } from "src/app/shared/models/graphql.models";

export interface BranchStateModel {
    nodeConnection?: BranchNodeConnection;
    selectedNode?: BranchNode;
    nodeConnectionProvince?: ProvinceBranchNodeConnection;
    nodeConnectionDistrict?: DistrictBranchNodeConnection;
}

export const branchInitialState: BranchStateModel = {
 }
