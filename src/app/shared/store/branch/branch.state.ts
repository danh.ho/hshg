import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { FlatpageService } from "src/app/shared/services";
import { CreateContactForm, CreateContactFormFailed, CreateContactFormSuccessfully, GetFlatpageByID, GetFlatpageList } from "src/app/shared/store/flatpage";
import { ManageBranchService } from "../../services/branch.service";
import { BranchStateModel, branchInitialState } from "./branch-state.model";
import { LoadBranch, LoadBranchById, LoadDistrictBranch, LoadProvinceBranch } from "./branch.action";

@Injectable({ providedIn: 'root' })
@State<BranchStateModel>({
    name: 'branch',
    defaults: branchInitialState,
})
export class BranchState {
    @Selector()
    static getNodeConnection({ nodeConnection }: BranchStateModel) {
        return nodeConnection;
    }

    @Selector()
    static getSelectedNode({ selectedNode }: BranchStateModel) {
        return selectedNode;
    }
    @Selector()
    static getProvinceNodeConnection({ nodeConnectionProvince }: BranchStateModel) {
        return nodeConnectionProvince;
    }
    @Selector()
    static getDistrictNodeConnection({ nodeConnectionDistrict }: BranchStateModel) {
        return nodeConnectionDistrict;
    }
    constructor(private _branchService: ManageBranchService) { }

    @Action(LoadBranch)
    loadBranch(
        { patchState }: StateContext<BranchStateModel>,
        { payload }: LoadBranch
    ) {
        return this._branchService.getAllBranch(payload).pipe(
            tap(res => {
                if (res) return patchState({ nodeConnection: res.data.branch })
            })
        )
    }
    @Action(LoadBranchById)
    loadBranchById(
        { patchState }: StateContext<BranchStateModel>,
        { payload }: LoadBranchById
    ) {
        return this._branchService.getBranchById(payload).pipe(
        tap(({ data }) => {
            patchState({
                selectedNode: data.branchDetail
            });
        })
        );
    }
    @Action(LoadProvinceBranch)
    loadProvinceBranch(
        { patchState }: StateContext<BranchStateModel>,
        { payload }: LoadProvinceBranch
    ) {
        return this._branchService.getAllProvinceBranch(payload).pipe(
            tap(res => {
                if (res) return patchState({ nodeConnectionProvince: res.data.provinceBranches })
            })
        )
    }
    @Action(LoadDistrictBranch)
    loadDistrictBranch(
        { patchState }: StateContext<BranchStateModel>,
        { payload }: LoadDistrictBranch
    ) {
        return this._branchService.getAllDistrictBranch(payload).pipe(
            tap(res => {
                if (res) return patchState({ nodeConnectionDistrict: res.data.districtBranches })
            })
        )
    }
}
