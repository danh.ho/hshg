import { ContactUsInput, CustomizeMutationErrorType, ErrorType, PartyInforCreateInput, QueryFlatpagesArgs } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    GET_FLATPAGE_LIST = '[CMS.FLATPAGE] Get Flatpage List',
    GET_FLATPAGE_BY_ID = '[CMS.FLATPAGE] Get Flatpage By ID',
    GET_FLATPAGE_BY_ITEMCODE = '[CMS.FLATPAGE] Get Flatpage By Itemcode',

    CREATE_CONTACT_FORM = '[CMS.CONTACT_US] Create Contact Form',
    CREATE_CONTACT_FORM_FAILED = '[CMS.CONTACT_US] Create Contact Form Failed',
    CREATE_CONTACT_FORM_SUCCESSFULLY = '[CMS.CONTACT_US] Create Contact Form Successfully',

    CREATE_PARTY_BOOKING = '[CMS.CONTACT_US] Create Party Booking',
    CREATE_PARTY_BOOKING_FAILED = '[CMS.CONTACT_US] Create Party Booking Failed',
    CREATE_PARTY_BOOKING_SUCCESSFULLY = '[CMS.CONTACT_US] Create Party Booking Successfully',
}

export class GetFlatpageList {
    static readonly type = ACTIONS.GET_FLATPAGE_LIST;
    constructor(public readonly payload?: QueryFlatpagesArgs) { }
}

export class GetFlatpageByID {
    static readonly type = ACTIONS.GET_FLATPAGE_BY_ID;
    constructor(public readonly payload: string) { }
}

export class CreateContactForm {
    static readonly type = ACTIONS.CREATE_CONTACT_FORM;
    constructor(public readonly payload: ContactUsInput) { }
}

export class CreateContactFormFailed {
    static readonly type = ACTIONS.CREATE_CONTACT_FORM_FAILED;
    constructor(public readonly payload: ErrorType) { }
}

export class CreateContactFormSuccessfully {
    static readonly type = ACTIONS.CREATE_CONTACT_FORM_SUCCESSFULLY;
}

export class CreateBookParty {
    static readonly type = ACTIONS.CREATE_PARTY_BOOKING;
    constructor(public readonly payload: PartyInforCreateInput) { }
}

export class CreateBookPartyFailed {
    static readonly type = ACTIONS.CREATE_PARTY_BOOKING_FAILED;
    constructor(public readonly payload: CustomizeMutationErrorType) { }
}

export class CreateBookPartySuccessfully {
    static readonly type = ACTIONS.CREATE_PARTY_BOOKING_SUCCESSFULLY;
}
