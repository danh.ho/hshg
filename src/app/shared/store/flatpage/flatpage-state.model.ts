import { FlatpageNode, FlatpageNodeConnection, FlatpageNodeEdge } from "src/app/shared/models/graphql.models";

export interface FlatpageStateModel {
    flatpageList?: FlatpageNodeEdge[],
    flatpageNode?: FlatpageNode,
}

export const flatpageInitialState: FlatpageStateModel = {
 }
