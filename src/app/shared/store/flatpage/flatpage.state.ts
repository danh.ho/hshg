import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { tap } from "rxjs/operators";
import { FlatpageService, NotificationService } from "src/app/shared/services";
import { CreateBookParty, CreateBookPartyFailed, CreateBookPartySuccessfully, CreateContactForm, CreateContactFormFailed, CreateContactFormSuccessfully, flatpageInitialState, FlatpageStateModel, GetFlatpageByID, GetFlatpageList } from "src/app/shared/store/flatpage";

@Injectable({ providedIn: 'root' })
@State<FlatpageStateModel>({
    name: 'cms_flatpage',
    defaults: flatpageInitialState,
})
export class FlatpageState {
    @Selector()
    static getFlatpageList({ flatpageList }: FlatpageStateModel) {
        return flatpageList;
    }

    @Selector()
    static getFlatpageByID({ flatpageNode }: FlatpageStateModel) {
        return flatpageNode;
    }

    constructor(private _flatpageService: FlatpageService,
        private _notify: NotificationService,
        ) { }

    @Action(GetFlatpageList)
    getFlatpageList(
        { patchState }: StateContext<FlatpageStateModel>,
        { payload }: GetFlatpageList
    ) {
        return this._flatpageService.getFlatpageList(payload).pipe(
            tap(res => {
                if (res) return patchState({ flatpageList: res.edges })
            })
        )
    }

    @Action(GetFlatpageByID)
    getFlatpageByID(
        { patchState }: StateContext<FlatpageStateModel>,
        { payload }: GetFlatpageByID
    ) {
        return this._flatpageService.getFlatpageByID({ id: payload }).pipe(
            tap(res => {
                if (res) return patchState({ flatpageNode: res })
            })
        )
    }

    @Action(CreateContactForm)
    createContactForm(
        { dispatch }: StateContext<FlatpageStateModel>,
        { payload }: CreateContactForm
    ) {
        return this._flatpageService.createContactForm({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    return dispatch(new CreateContactFormSuccessfully)
                } else {
                    this._notify.error(null,errors[0].message);
                    return dispatch(new CreateContactFormFailed(errors[0]))
                }
            })
        )
    }
    @Action(CreateBookParty)
    createBookParty(
        { dispatch }: StateContext<FlatpageStateModel>,
        { payload }: CreateBookParty
    ) {
        return this._flatpageService.createPartyBooking({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    return dispatch(new CreateBookPartySuccessfully)
                } else {
                    return dispatch(new CreateBookPartyFailed(errors[0]))
                }
            })
        )
    }


}
