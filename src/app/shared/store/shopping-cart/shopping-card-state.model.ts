import { CartDetailNode, PaymentNodeConnection, ProductForBuyNode, UserAddressNode } from "src/app/shared/models/graphql.models";

export interface GUEST_ITEM_CARD {
    product?: ProductForBuyNode,
    quantity?: number,
    subCartDetail?: any;
    orderDetailNote?: any;
}

export interface ShoppingCartStateModel {
    memberShoppingCart?: Array<any>,
    guestShoppingCart?: Array<GUEST_ITEM_CARD>,
    paymentNodeConnection?: PaymentNodeConnection,
    relatedProduct?: Array<any>,
}

export const shoppingCartInitialState: ShoppingCartStateModel = {
    guestShoppingCart: [],
    memberShoppingCart: [],
}
export interface ShoppingCartIdStateModel {
    id: string;
}
export interface UserAddressNewStateModel {
    addressNew?: UserAddressNode;
}