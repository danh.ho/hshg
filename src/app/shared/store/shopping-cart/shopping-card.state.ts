import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { Action, Actions, ofActionSuccessful, Selector, State, StateContext, Store, } from "@ngxs/store";

import { patch, append, updateItem, removeItem, } from "@ngxs/store/operators";
import { NgxSpinnerService } from "ngx-spinner";
import { Subscription } from "rxjs";
import { dispatch } from "rxjs/internal/observable/pairs";
import { elementAt, take, tap } from "rxjs/operators";
import { VnPay, VnPayConnect } from "src/app/checkout/vnpay/models";
import { CartDetailNode, UpdateAddressInput } from "src/app/shared/models/graphql.models";
import { NotificationService, ShoppingCartService } from "src/app/shared/services";
import { GUEST_ITEM_CARD, ShoppingCartIdStateModel, shoppingCartInitialState, ShoppingCartStateModel, UserAddressNewStateModel } from "src/app/shared/store/shopping-cart/shopping-card-state.model";
import {
    AddItemToGuestBag, AddItemToGuestBagFailed, AddItemToGuestBagSuccessfully, AddItemToMemberShoppingCart, AddItemToMemberShoppingCartFailed, AddItemToMemberShoppingCartSuccessfully, AddUserAddress, CreateGuestOrder,
    CreateGuestOrderFailed, CreateGuestOrderSuccessfully,
    CreateUserOrder,
    CreateUserOrderFailed,
    CreateUserOrderSuccessfully,
    EditQuantityGuestCart,
    EditQuantityItemMemberShoppingCart,
    EditQuantityItemMemberShoppingCartFailed,
    EditQuantityItemMemberShoppingCartSuccessfully,
    EditUserAddress,
    GetGuestShoppingCart,
    GetMemberShoppingCart, GetMemberShoppingCartId, GetPaymentMethodList, OrderCustomerUpdateStatus, 
    OrderCustomerUpdateStatusFailed, OrderCustomerUpdateStatusSuccessfully, RemoveItemGuestBag, 
    RemoveItemMemberShoppingCart, RemoveItemMemberShoppingCartFailed, RemoveItemMemberShoppingCartSuccessfully, UpdateItemToGuestBag, UpdateItemToMemberShoppingCart
} from "src/app/shared/store/shopping-cart/shopping-card.action";
import { environment } from "src/environments/environment";
import { AuthState, GetSiteTokenVnPay } from "../auth";

@Injectable({ providedIn: 'root' })
@State<ShoppingCartStateModel>({
    name: 'shopping_cart',
    defaults: shoppingCartInitialState,
})
export class ShoppingCartState {
    _subscription = new Subscription()
    static getMemberShoppingCart(getMemberShoppingCart: any) {
        throw new Error('Method not implemented.');
    }
    @Selector()
    static getMemberCart({ memberShoppingCart }: ShoppingCartStateModel) {
        return memberShoppingCart ? memberShoppingCart : []
    }

    @Selector()
    static getGuestCart({ guestShoppingCart }: ShoppingCartStateModel) {
        return guestShoppingCart ? guestShoppingCart : [];
    }

    @Selector()
    static getRelatedProduct({ relatedProduct }: ShoppingCartStateModel) {
        return relatedProduct ? relatedProduct : []
    }

    @Selector()
    static getPaymentMethodList({ paymentNodeConnection }: ShoppingCartStateModel) {
        if (!paymentNodeConnection) return [];
        else {
            return paymentNodeConnection.edges.map(ele => ele.node);
        }
    }
    @Selector()
    static getUserAddressNew({ addressNew }: UserAddressNewStateModel) {
        return addressNew ? addressNew : [];
    }
    // @Selector()
    // static getMemberCartID({ id }: ShoppingCartStateModel) {
    //     return id ? id: []
    // }
    @Selector()
    static getMemberCartID({ id }: ShoppingCartIdStateModel) {
        return id ? id : []
    }
    
    constructor(
        private _shoppingCartService: ShoppingCartService,
        private _router: Router,
        private _spinner: NgxSpinnerService,
        private _notify: NotificationService,
        private _snackBar: MatSnackBar,
        private _actions: Actions,
        private _store: Store,        
    ) { }


    @Action(GetMemberShoppingCart)
    getMemberShoppingCart(
        { patchState }: StateContext<ShoppingCartStateModel>
    ) {
        return this._shoppingCartService.getMemberCart().pipe(
            tap(res => {
                if (res?.cartDetail.length > 0) {
                    patchState({ memberShoppingCart: res.cartDetail, relatedProduct: res.relatedProduct })
                    const data = res.cartDetail;
                    return data;
                }

            })
        )
    }    
    @Action(GetMemberShoppingCartId)
    getMemberShoppingCartId(
        { patchState }: StateContext<ShoppingCartIdStateModel>
    ) {
        return this._shoppingCartService.getMemberCart().pipe(
            tap(res => {
                patchState({ id: res.id })
            })
        )
    }
    @Action(CreateUserOrder)
    createUserOrder(
        { dispatch }: StateContext<ShoppingCartStateModel>,
        { payload }: CreateUserOrder
    ) {
        this._spinner.show()

        return this._shoppingCartService.createUserOrder({ input: payload }).pipe(
            tap(res => {
                const { status, errors, order } = res
                if (status) {
                    if (payload.paymentMethodId == "2") {

                        return dispatch(new CreateUserOrderSuccessfully(order))
                    } else {
                        return dispatch(new CreateUserOrderSuccessfully(order))
                    }

                } else if (errors) {
                    return dispatch(new CreateUserOrderFailed(errors[0]))
                }
            })
        )
    }
    @Action(CreateUserOrderSuccessfully)
    createUserOrderSuccessfully(
        { setState }: StateContext<ShoppingCartStateModel>,
        { payload }: CreateUserOrderSuccessfully
    ) {
        // setState({
        //     memberShoppingCart: [],
        // })
        setState(
            patch<ShoppingCartStateModel>({
                memberShoppingCart: []
            })
        )
        // this._spinner.hide()
        // this._notify.success('','Đơn hàng được tạo thành công')
        // this._router.navigate(['/success'])
        if(payload.paymentMethod.id == "3"){
            console.log("OnePay")
        }if(payload.paymentMethod.id == "4"){
            this._store.dispatch(new GetSiteTokenVnPay())
            this._subscription.add(this._store.select(AuthState.getVnPay).subscribe(
                res => {
                    if(res){
                        const checkoutPayload: VnPay.CheckoutPayload = {
                            amount: payload?.totalAmount,
                            orderId: payload?.id,
                            orderInfo: "Checkoutorder" + payload?.id,
                            orderType: '250000',
                            clientIp: '127.0.21.1',
                            returnUrl: environment.vnpayRedirectURL + payload?.id,
                            locale: 'vn',
                        };
                        const vnPay = new VnPayConnect({
                            // paymentGateway: "https://pay.vnpay.vn/vpcpay.html", // "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                            // merchant: 'LIFEON01' , // "TLIFEON1" | "11V8QAQ9"
                            // secureSecret: 'WYAROPPLLAFUYZHBTXCZAROBIPJZXUSL', // "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH" | "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
                            // paymentGateway: "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                            paymentGateway: res?.paymentGateway,
                            merchant: res?.vnpayTmnKey,
                            secureSecret: res?.vnpaySecret,
                            // paymentGateway: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                            // merchant: "TLIFEON1", //| "11V8QAQ9",
                            // secureSecret: "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH", //| "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
                        });
            
                        const checkoutUrl = vnPay.buildCheckoutUrl(checkoutPayload);
                        window.location.href = checkoutUrl.toString()

                    }
                }
            ))
            // const checkoutPayload: VnPay.CheckoutPayload = {
            //     amount: payload?.totalAmount,
            //     orderId: payload?.id,
            //     orderInfo: "Checkoutorder" + payload?.id,
            //     orderType: '250000',
            //     clientIp: '127.0.21.1',
            //     returnUrl: environment.vnpayRedirectURL + payload?.id,
            //     locale: 'vn',
            // };
            // const vnPay = new VnPayConnect({
            //     // paymentGateway: "https://pay.vnpay.vn/vpcpay.html", // "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
            //     // merchant: 'LIFEON01' , // "TLIFEON1" | "11V8QAQ9"
            //     // secureSecret: 'WYAROPPLLAFUYZHBTXCZAROBIPJZXUSL', // "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH" | "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
            //     // paymentGateway: "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
            //     paymentGateway: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
            //     merchant: "HSHGTEST",
            //     secureSecret: "XGMBYHITYQOJNZXPVHOYEPLLNSAIWTGK",
            //     // paymentGateway: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
            //     // merchant: "TLIFEON1", //| "11V8QAQ9",
            //     // secureSecret: "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH", //| "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
            // });

            // const checkoutUrl = vnPay.buildCheckoutUrl(checkoutPayload);
        
            // this._store.dispatch(new OrderCustomerUpdateStatus({id: payload?.id, paymentReturnData: JSON.stringify(checkoutUrl)}))
            
            // this._subscription.add(this._actions
            // .pipe(
            //     ofActionSuccessful(OrderCustomerUpdateStatusSuccessfully),
            // )
            // .subscribe((translation) => {
            //     if(translation){
            //         window.location.href = checkoutUrl.toString()
            //     }
            // }));
            // window.location.href = checkoutUrl.toString()
        }else{
            this._spinner.hide()
            this._notify.success('','Đơn hàng được tạo thành công')
            this._router.navigate(['/success/', payload.id], {queryParams: {payment_method: payload.paymentMethod.id}})
        }
    }
    @Action(CreateUserOrderFailed)
    createUserOrderFailed(
        { payload }: CreateUserOrderFailed
    ) {
        this._spinner.hide()
    }
    @Action(AddItemToMemberShoppingCart)
    addItemToMemberShoppingCart(
        { getState, setState, dispatch, }: StateContext<ShoppingCartStateModel>,
        { payload, form }: AddItemToMemberShoppingCart
    ) {
        const currentCart = getState().memberShoppingCart;
        const indexAddedItem = currentCart.findIndex(ele => ele.product.id === payload.product.id);
        const cartDetailId = currentCart.find(ele => ele.product.id === payload.product.id);
        return this._shoppingCartService.addCart({ input: { product: payload.product.id, quantity: payload.quantity, topping: payload.topping, note: payload.orderDetailNote } }).pipe(
            tap(res => {
                const { status, errors, cart } = res;
                if (status) {
                    dispatch(new AddItemToMemberShoppingCartSuccessfully({ product: payload.product, quantity: payload.quantity, topping: payload.topping }, form));
                } else {
                    dispatch(new AddItemToMemberShoppingCartFailed(errors[0]))
                }
            })
        )
    }

    @Action(AddItemToMemberShoppingCartSuccessfully)
    addItemToMemberShoppingCartSuccessfully(
        { setState, getState }: StateContext<ShoppingCartStateModel>,
        { payload, form }: AddItemToMemberShoppingCartSuccessfully
    ) {   
        // const currentCart = getState().memberShoppingCart;
        // const indexAddedItem = currentCart.findIndex(ele => ele.product.id === payload.product.id);
        // const cartDetailId = currentCart.find(ele => ele.product.id === payload.product.id);
        // const { product, quantity, topping } = payload;
        // console.log(payload)
        // if (indexAddedItem > -1) {
        //     setState(
        //         patch<ShoppingCartStateModel>({
        //             memberShoppingCart: updateItem<any>(ele =>
        //                 ele.product.id === payload.product.id,
        //                 { id: cartDetailId.id, product: cartDetailId.product, quantity: payload.quantity, topping: topping })
        //         })
        //     )
        // } else {
        //     setState(
        //         patch<ShoppingCartStateModel>({
        //             memberShoppingCart: append([{ id: product.id, product: product, quantity: quantity, topping: topping }])
        //         })
        //     )
        // }
        if (form !== 'nav-bar') {
            this._notify.success(payload?.product?.name,'Thêm giỏ hàng thành công')
        }
    }
    @Action(AddItemToMemberShoppingCartFailed)
    addItemToMemberShoppingCartFailed(
        { error }: AddItemToMemberShoppingCartFailed
    ) {
        // this._translate.get(['', `FAILED.ORDER_ERRORS.${error}`]).pipe(
        //     take(1),
        //     tap(translation => {
        //         this._notify.success(translation['SUCCESS.success'], translation['SUCCESS.add_to_cart_successfully'])
        //     })
        // ).subscribe();
    }
    @Action(UpdateItemToMemberShoppingCart)
    updateItemToMemberShoppingCart(
        { getState, setState, dispatch, }: StateContext<ShoppingCartStateModel>,
        { payload }: UpdateItemToMemberShoppingCart
    ) {
        return this._shoppingCartService.updateCart({ input: payload}).pipe(
            tap(res => {
                const { status } = res;
                const nameItem = res?.cart?.cartDetail?.find(e => e?.id == payload?.cartDetailId)?.product?.name
                if (status) {
                    this._notify.success(nameItem,'Cập nhật giỏ hàng thành công')
                } else {
                    this._notify.error(nameItem,'Cập nhật giỏ hàng thất bại')
                }
            })
        )
    }
    @Action(RemoveItemMemberShoppingCart)
    removeItemMemberShoppingCart(
        { dispatch }: StateContext<ShoppingCartStateModel>,
        { cartDetailId }: RemoveItemMemberShoppingCart
    ) {
        return this._shoppingCartService.deleteCart({ input: { cartDetailId } }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                    dispatch(new RemoveItemMemberShoppingCartSuccessfully(cartDetailId))
                } else {
                    dispatch(new RemoveItemMemberShoppingCartFailed(errors[0]))
                }
            })
        )

    }

    @Action(RemoveItemMemberShoppingCartSuccessfully)
    removeItemMemberShoppingCartSuccessfully(
        { setState }: StateContext<ShoppingCartStateModel>,
        { cartDetailId }: RemoveItemMemberShoppingCart,
    ) {
        setState(
            patch<ShoppingCartStateModel>({
                memberShoppingCart: removeItem<CartDetailNode>(ele =>
                    ele.id === cartDetailId)
            })
        )
    }

    @Action(RemoveItemMemberShoppingCartFailed)
    removeItemMemberShoppingCartFailed(
        { error }: RemoveItemMemberShoppingCartFailed,
    ) {
        // return this._translate.get(['', `ERRORS.ORDER_ERRORS.${error.code}`]).pipe(
        //     tap(translation => {
        //         this._snackBar.open(translation);
        //     })
        // )
    }

    @Action(EditQuantityItemMemberShoppingCart)
    editQuantityItemMemberShoppingCart(
        { getState, dispatch, setState }: StateContext<ShoppingCartStateModel>,
        { payload }: EditQuantityItemMemberShoppingCart
    ) {
        const currentCart = getState().memberShoppingCart;
        const cartDetailId = currentCart.find(ele => ele.product.id === payload.productId).id;
        const { productId, quantity } = payload;
        if (payload.quantity === 0) {
            return dispatch(new RemoveItemMemberShoppingCart(cartDetailId))
        } else {
            return this._shoppingCartService.updateCart({
                input: {
                    cartDetailId,
                    quantity: payload.quantity
                }
            }).pipe(
                tap(res => {
                    const { status, errors, } = res;
                    if (status) {
                        // setState(
                        //     patch<ShoppingCartStateModel>({
                        //         memberShoppingCart: updateItem<CartDetailNode>(ele =>
                        //             ele.product.id === payload.productId,
                        //             { id: cartDetailId, product: currentCart.find(ele => ele.id === cartDetailId).product, quantity: payload.quantity })
                        //     })
                        // )
                        dispatch(new EditQuantityItemMemberShoppingCartSuccessfully({ productId, quantity }))
                    } else {
                        return dispatch(new EditQuantityItemMemberShoppingCartFailed(errors[0]))
                    }
                })
            )
        }
    }

    @Action(EditQuantityItemMemberShoppingCartSuccessfully)
    editQuantityItemMemberShoppingCartSuccessfully(
        { setState, getState }: StateContext<ShoppingCartStateModel>,
        { payload }: EditQuantityItemMemberShoppingCartSuccessfully
    ) {
        const currentCart = getState().memberShoppingCart;
        const item = currentCart.find(ele => ele.product.id === payload.productId);
        setState(
            patch<ShoppingCartStateModel>({
                memberShoppingCart: updateItem<any>(ele =>
                    ele.product.id === payload.productId,
                    { id: item.id, product: item.product, quantity: payload.quantity, subCartDetail: item.subCartDetail })
            })
        )
    }



    @Action(GetPaymentMethodList)
    getPaymentMethodList(
        { patchState }: StateContext<ShoppingCartStateModel>,
        { payload }: GetPaymentMethodList
    ) {
        return this._shoppingCartService.getPaymentMethodList(payload).pipe(
            tap(res => {
                return patchState({ paymentNodeConnection: res })
            })
        )
    }

    @Action(CreateGuestOrder)
    createGuestOrder(
        { dispatch }: StateContext<ShoppingCartStateModel>,
        { payload }: CreateGuestOrder
    ) {
        this._spinner.show();
        return this._shoppingCartService.createGuestOrder({ input: payload }).pipe(
            tap(res => {
                const { status, errors, order } = res;
                if (status) {
                    if (payload.paymentMethodId == "2") {

                        return dispatch(new CreateGuestOrderSuccessfully(order))
                    } else {
                        return dispatch(new CreateGuestOrderSuccessfully(order))

                    }
                } else if (errors) {
                    return dispatch(new CreateGuestOrderFailed(errors[0]))
                }
            })
        )
    }

    @Action(CreateGuestOrderSuccessfully)
    createGuestOrderSuccessfully(
        { setState }: StateContext<ShoppingCartStateModel>,
        { payload }: CreateGuestOrderSuccessfully
    ) {
        setState(
            patch<ShoppingCartStateModel>({
                guestShoppingCart: []
            })
        )
        // this._spinner.hide()
        if(payload.paymentMethod.id == "3"){
            console.log("OnePay")
        }if(payload.paymentMethod.id == "4"){
            console.log("VNPAY")
            const checkoutPayload: VnPay.CheckoutPayload = {
                amount: payload?.totalAmount,
                orderId: payload?.id,
                orderInfo: "Checkoutorder" + payload?.id,
                orderType: '250000',
                clientIp: '127.0.21.1',
                returnUrl: environment.vnpayRedirectURL + payload?.id,
                locale: 'vn',
            };
            const vnPay = new VnPayConnect({
                // paymentGateway: "https://pay.vnpay.vn/vpcpay.html", // "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                // merchant: 'LIFEON01' , // "TLIFEON1" | "11V8QAQ9"
                // secureSecret: 'WYAROPPLLAFUYZHBTXCZAROBIPJZXUSL', // "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH" | "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
                // paymentGateway: "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                paymentGateway: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                merchant: "HSHGTEST",
                secureSecret: "XGMBYHITYQOJNZXPVHOYEPLLNSAIWTGK",
                // paymentGateway: "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html",
                // merchant: "TLIFEON1", //| "11V8QAQ9",
                // secureSecret: "LQXYJHAAZYWBUZWAKAQQVDQYAZZKNNAH", //| "8EAPXWTJUEL6M9METI4Z1O9PBTF2C96I",
            });
            const checkoutUrl = vnPay.buildCheckoutUrl(checkoutPayload);
            // this._store.dispatch(new OrderCustomerUpdateStatus({id: payload?.id, paymentReturnData: JSON.stringify(checkoutUrl)}))
            
            // this._subscription.add(this._actions
            // .pipe(
            //     ofActionSuccessful(OrderCustomerUpdateStatusSuccessfully),
            // )
            // .subscribe((translation) => {
            //     if(translation){
            //         window.location.href = checkoutUrl.toString()
            //     }
            // }));
            window.location.href = checkoutUrl.toString()
        }else{
            this._spinner.hide()
            this._router.navigate(['/success/', payload.id], {queryParams: {payment_method: payload.paymentMethod.id}})
        }
        // return this._translate.get('SUCCESS.order_created').pipe(
        //     tap(translation => {
        //         this._snackBar.open(translation)
        //         this._router.navigate(['/success', id])
        //     })
        // )
    }

    @Action(CreateGuestOrderFailed)
    createGuestOrderFailed(
        { payload }: CreateGuestOrderFailed
    ) {
        this._spinner.hide()
        return payload;
    }

    @Action(AddItemToGuestBag)
    addItemToGuestBag(
        { setState, getState, dispatch }: StateContext<ShoppingCartStateModel>,
        { payload }: AddItemToGuestBag,
    ) {
        const currentCart = getState().guestShoppingCart;
        if (payload) {
            console.log(payload)
            dispatch(new AddItemToGuestBagSuccessfully(payload))
            this._notify.success(payload?.product?.name,'Thêm giỏ hàng thành công');
        }
        if (payload.quantity) {
            const indexAddedItem = currentCart.findIndex(ele => ele.product.id === payload.product.id);
            if (indexAddedItem > -1) {
                setState(
                    patch<ShoppingCartStateModel>({
                        guestShoppingCart: updateItem<GUEST_ITEM_CARD>(ele =>
                            ele.product.id === payload.product.id,
                            { product: payload.product, 
                              quantity: currentCart[indexAddedItem].quantity + payload.quantity,
                              subCartDetail: payload.topping ? payload.topping : currentCart[indexAddedItem].subCartDetail,
                              orderDetailNote: currentCart[indexAddedItem].orderDetailNote   })
                    })
                )
            } else {
                setState(
                    patch<ShoppingCartStateModel>({
                        guestShoppingCart: append([{ product: payload.product, quantity: payload.quantity, subCartDetail: payload.topping, orderDetailNote: payload.orderDetailNote }])
                    })
                )
            }
        } else {
            const indexAddedItem = currentCart.findIndex(ele => ele.product.id === payload.product.id);
            if (indexAddedItem > -1) {
                setState(
                    patch<ShoppingCartStateModel>({
                        guestShoppingCart: updateItem<GUEST_ITEM_CARD>(ele =>
                            ele.product.id === payload.product.id,
                            { product: payload.product, quantity: currentCart[indexAddedItem].quantity + 1, subCartDetail: payload.topping })
                    })
                )
            } else {
                setState(
                    patch<ShoppingCartStateModel>({
                        guestShoppingCart: append([{ product: payload.product, quantity: 1,subCartDetail: payload.topping }])
                    })
                )
            }
        }
    }

    @Action(RemoveItemGuestBag)
    removeItemGuestBag(
        { setState }: StateContext<ShoppingCartStateModel>,
        { payload }: RemoveItemGuestBag
    ) {
        setState(
            patch<ShoppingCartStateModel>({
                guestShoppingCart: removeItem<GUEST_ITEM_CARD>(ele =>
                    ele.product.id === payload)
            })
        )
    }

    @Action(EditQuantityGuestCart)
    editQuantityGuestCart(
        { setState, getState }: StateContext<ShoppingCartStateModel>,
        { payload }: EditQuantityGuestCart
    ) {
        const currentCart = getState().guestShoppingCart;
        setState(
            patch<ShoppingCartStateModel>({
                guestShoppingCart: updateItem<GUEST_ITEM_CARD>(ele =>
                    ele.product.id === payload.id, {
                    product: currentCart.find(ele => ele.product.id === payload.id).product,
                    quantity: payload.quantity, subCartDetail: currentCart.find(ele => ele.product.id === payload.id).subCartDetail,
                    orderDetailNote: currentCart.find(ele => ele.product.id === payload.id).orderDetailNote,
                })
            })
        )
    }
    @Action(UpdateItemToGuestBag)
    updateItemToGuestBag(
        { setState, getState }: StateContext<ShoppingCartStateModel>,
        { payload }: UpdateItemToGuestBag
    ) {
        const currentCart = getState().guestShoppingCart;
        if(payload){
            this._notify.success(payload?.product?.name,'Cập nhật giỏ hàng thành công');
        }
        setState(
            patch<ShoppingCartStateModel>({
                guestShoppingCart: updateItem<GUEST_ITEM_CARD>(ele =>
                    ele.product.id === payload.product.id, {
                    product: currentCart.find(ele => ele.product.id === payload.product.id).product,
                    quantity: payload.quantity, subCartDetail: payload.topping, orderDetailNote: payload.orderDetailNote
                })
            })
        )
    }
    @Action(AddItemToGuestBagSuccessfully)
    addItemToGuestBagSuccessfully(
        { payload }: AddItemToGuestBagSuccessfully
    ) {
        // console.log(payload)
        // this._notify.success(payload?.product?.name,'Thêm giỏ hàng thành công');
        // this._translate.get(['SUCCESS.success', 'SUCCESS.add_to_cart_successfully',]).pipe(
        //     take(1),
        //     tap(translation => {
        //         this._notify.success(null, translation['SUCCESS.add_to_cart_successfully'])
        //     })
        // ).subscribe();
    }

    @Action(AddItemToGuestBagFailed)
    addItemToGuestBagFailed() {
    }
    @Action(EditUserAddress)
    editUserAddress(
        { getState, dispatch, setState }: StateContext<ShoppingCartStateModel>,
        { payload }: EditUserAddress
    ) {
        return this._shoppingCartService.EditUserAddress({ input: payload }).pipe(
            tap(res => {
                const { status, errors } = res;
                if (status) {
                } else {
                }
            })
        )

    }
    @Action(AddUserAddress)
    addUserAddress(
        { getState, patchState, setState }: StateContext<UserAddressNewStateModel>,
        { payload }: AddUserAddress
    ) {
        return this._shoppingCartService.AddUserAddress({ input: payload }).pipe(
            tap(res => {
                const { status, errors, userAddress } = res;
                patchState({
                    addressNew: userAddress
                });
            })
        )

    }

    @Action(OrderCustomerUpdateStatus)
    orderUpdatePaymentStatus(
        { dispatch }: StateContext<ShoppingCartStateModel>,
        { payload }: OrderCustomerUpdateStatus
    ) {
        return this._shoppingCartService.orderCustomerUpdateStatus({ input: payload }).pipe(
            tap(res => {
                const {  errors, status } = res.data.orderCustomerUpdate
                if (status) {
                    return dispatch(new OrderCustomerUpdateStatusSuccessfully(status))
                } else if (errors) {
                    return dispatch(new OrderCustomerUpdateStatusFailed(errors[0]))
                }
            })
        )
    }

}
