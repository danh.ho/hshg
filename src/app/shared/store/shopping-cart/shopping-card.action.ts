import { AddAddressInput, AddToCartInput, CartDetailNode, DeteteCartInput, OrderCustomerUpdateInput, OrderError, ProductForBuyNode, ProductToppingNode, QueryPaymentMethodsArgs, UpdateAddressInput, UpdateCartInput, UserGuestInput, UserOrderCreateInput } from "src/app/shared/models/graphql.models";

const enum ACTIONS {
    GET_MEMBER_SHOPPING_CART = '[SHOPPING_CART] Get Member Shopping Cart',
    GET_MEMBER_SHOPPING_CART_ID = '[SHOPPING_CART] Get Member Shopping Cart Id',
    GET_PAYMENT_METHOD_LIST = '[SHOPPING_CART.DETAIL] Get Payment Method List',

    CREATE_USER_ORDER = '[SHOPPING_CART.ORDER] Create Order User',
    CREATE_USER_ORDER_FAILED = '[SHOPPING_CART.ORDER] Create Order User Failed',
    CREATE_USER_ORDER_SUCCESSFULLY = '[SHOPPING_CART.ORDER] Create Order User Successfully',

    ADD_ITEM_TO_MEMBER_SHOPPING_CART = '[SHOPPING_CART] Add Item To Member Shopping Cart',
    ADD_ITEM_TO_MEMBER_SHOPPING_CART_FAILED = '[SHOPPING_CART] Add Item To Member Shopping Cart Failed',
    ADD_ITEM_TO_MEMBER_SHOPPING_CART_SUCCESSFULLY = '[SHOPPING_CART] Add Item To Member Shopping Cart Successfully',

    UPDATE_ITEM_TO_MEMBER_SHOPPING_CART = '[SHOPPING_CART] Update Item To Member Shopping Cart',

    REMOVE_ITEM_MEMBER_SHOPPING_CART = '[SHOPPING_CART] Remove Item Member Shopping Cart',
    REMOVE_ITEM_MEMBER_SHOPPING_CART_FAILED = '[SHOPPING_CART] Remove Item Member Shopping Cart Failed',
    REMOVE_ITEM_MEMBER_SHOPPING_CART_SUCCESSFULLY = '[SHOPPING_CART] Remove Item Member Shopping Cart Successfully',

    EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART = '[SHOPPING_CART] Edit Quantity Item Member Shopping Cart',
    EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART_SUCESSFULLY = '[SHOPPING_CART] Edit Quantity Item Member Shopping Cart Successfully',
    EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART_FAILED = '[SHOPPING_CART] Edit Quantity Item Member Shopping Cart Failed',

    GET_GUEST_SHOPPING_CART = '[SHOPPING_CART] Get Guest Shopping Cart',

    CREATE_GUEST_ORDER = '[SHOPPING_CART.ORDER] Create Order Guest',
    CREATE_GUEST_ORDER_FAILED = '[SHOPPING_CART.ORDER] Create Order Guest Failed',
    CREATE_GUEST_ORDER_SUCCESSFULLY = '[SHOPPING_CART.ORDER] Create Order Guest Successfully',

    ADD_ITEM_TO_GUEST_BAG = '[SHOPPING_CART] Add Item To Bag',
    ADD_ITEM_TO_GUEST_BAG_SUCCESSFULLY = '[SHOPPING_CART] Add Item To Bag Successfully',
    ADD_ITEM_TO_GUEST_BAG_FAILED = '[SHOPPING_CART] Add Item To Bag Failed',

    UPDATE_ITEM_TO_GUEST_BAG = '[SHOPPING_CART] Update Item To Bag',

    REMOVE_ITEM_GUEST_BAG = '[SHOPPING_CART] Remove Item Guest Bag',
    REMOVE_ITEM_GUEST_BAG_SUCCESSFULLY = '[SHOPPING_CART] Remove Item Guest Bag Successfully',
    REMOVE_ITEM_GUEST_BAG_FAILED = '[SHOPPING_CART] Remove Item Guest Bag Failed',

    EDIT_QUANTITY_ITEM_GUEST_CART = '[SHOPPING_CART] Edit Quantity Item Guest Cart',

    UPDATE_MEMBER_CART = '[SHOPPING_CART] Update member cart',

    EDIT_USER_ADDRESS = '[SHOPPING_CART] Edit User Address',

    ADD_USER_ADDRESS = '[SHOPPING_CART] Add User Address',

    ORDER_CUSTOMER_UPDATE_STATUS = '[ORDER UPDATE STATUS] Order Update Status',
    ORDER_CUSTOMER_UPDATE_STATUS_FAILED = '[ORDER UPDATE STATUS] Order Update Status Failed',
    ORDER_CUSTOMER_UPDATE_STATUS_SUCCESSFULLY = '[ORDER UPDATE STATUS] Order Update Status Successfully',
}

export class GetMemberShoppingCart {
    static readonly type = ACTIONS.GET_MEMBER_SHOPPING_CART;
}
export class GetMemberShoppingCartId {
    static readonly type = ACTIONS.GET_MEMBER_SHOPPING_CART_ID;
}
export class CreateUserOrder {
    static readonly type = ACTIONS.CREATE_USER_ORDER;
    constructor(public readonly payload: any) { }
}
export class CreateUserOrderFailed {
    static readonly type = ACTIONS.CREATE_USER_ORDER_FAILED;
    constructor(public readonly payload: OrderError) { }
}

export class CreateUserOrderSuccessfully {
    static readonly type = ACTIONS.CREATE_USER_ORDER_SUCCESSFULLY;
    constructor(public readonly payload: any) { }
}
export class AddItemToMemberShoppingCart {
    static readonly type = ACTIONS.ADD_ITEM_TO_MEMBER_SHOPPING_CART;
    constructor(public readonly payload: { product: ProductForBuyNode, quantity?: number, topping?: any;  orderDetailNote?: string; }, public readonly form?: string) { 
    }
}

export class AddItemToMemberShoppingCartFailed {
    static readonly type = ACTIONS.ADD_ITEM_TO_MEMBER_SHOPPING_CART_FAILED;
    constructor(public readonly error: OrderError) { }
}

export class AddItemToMemberShoppingCartSuccessfully {
    static readonly type = ACTIONS.ADD_ITEM_TO_MEMBER_SHOPPING_CART_SUCCESSFULLY;
    constructor(public readonly payload: any, public readonly form?: string) { }
}
export class UpdateItemToMemberShoppingCart {
    static readonly type = ACTIONS.UPDATE_ITEM_TO_MEMBER_SHOPPING_CART;
    constructor(public readonly payload: UpdateCartInput) { 
    }
}
export class RemoveItemMemberShoppingCart {
    static readonly type = ACTIONS.REMOVE_ITEM_MEMBER_SHOPPING_CART;
    constructor(public readonly cartDetailId: string) { }
}

export class RemoveItemMemberShoppingCartFailed {
    static readonly type = ACTIONS.REMOVE_ITEM_MEMBER_SHOPPING_CART_FAILED;
    constructor(public readonly error: OrderError) { }
}

export class RemoveItemMemberShoppingCartSuccessfully {
    static readonly type = ACTIONS.REMOVE_ITEM_MEMBER_SHOPPING_CART_SUCCESSFULLY;
    constructor(public readonly cartDetailId: string) { }
}

export class EditQuantityItemMemberShoppingCart {
    static readonly type = ACTIONS.EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART;
    constructor(public readonly payload: { productId: string, quantity: number }) { }
}

export class EditQuantityItemMemberShoppingCartSuccessfully {
    static readonly type = ACTIONS.EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART_SUCESSFULLY;
    constructor(public readonly payload: { productId: string, quantity: number }) { }
}

export class EditQuantityItemMemberShoppingCartFailed {
    static readonly type = ACTIONS.EDIT_QUANTITY_ITEM_MEMBER_SHOPPING_CART_FAILED;
    constructor(public readonly error: OrderError) { }
}
export class GetGuestShoppingCart {
    static readonly type = ACTIONS.GET_GUEST_SHOPPING_CART;
}
export class GetPaymentMethodList {
    static readonly type = ACTIONS.GET_PAYMENT_METHOD_LIST;
    constructor(public readonly payload?: QueryPaymentMethodsArgs) { }
}

export class CreateGuestOrder {
    static readonly type = ACTIONS.CREATE_GUEST_ORDER;
    constructor(public readonly payload: UserGuestInput) {
        
     }
}

export class CreateGuestOrderFailed {
    static readonly type = ACTIONS.CREATE_GUEST_ORDER_FAILED;
    constructor(public readonly payload: OrderError) { }
}

export class CreateGuestOrderSuccessfully {
    static readonly type = ACTIONS.CREATE_GUEST_ORDER_SUCCESSFULLY;
    constructor(public readonly payload?: any) { }
}

export class AddItemToGuestBag {
    static readonly type = ACTIONS.ADD_ITEM_TO_GUEST_BAG;
    constructor(public readonly payload?: { product: ProductForBuyNode, quantity?: number, topping?: any, orderDetailNote?: string; }) { }
}
export class AddItemToGuestBagSuccessfully {
    static readonly type = ACTIONS.ADD_ITEM_TO_GUEST_BAG_SUCCESSFULLY;
    constructor(public readonly payload?: { product: ProductForBuyNode, quantity?: number, topping?: any, orderDetailNote?: string; }) { }
}

export class AddItemToGuestBagFailed {
    static readonly type = ACTIONS.ADD_ITEM_TO_GUEST_BAG_FAILED;
}

export class UpdateItemToGuestBag {
    static readonly type = ACTIONS.UPDATE_ITEM_TO_GUEST_BAG;
    constructor(public readonly payload?: { product: ProductForBuyNode, quantity?: number, topping?: any, orderDetailNote?: string; }) { }
}

export class RemoveItemGuestBag {
    static readonly type = ACTIONS.REMOVE_ITEM_GUEST_BAG;
    constructor(public readonly payload: string) { }
}

export class RemoveItemGuestBagSuccessfully {
    static readonly type = ACTIONS.REMOVE_ITEM_GUEST_BAG_SUCCESSFULLY;
}

export class RemoveItemGuestBagFailed {
    static readonly type = ACTIONS.REMOVE_ITEM_GUEST_BAG_FAILED;
}

export class EditQuantityGuestCart {
    static readonly type = ACTIONS.EDIT_QUANTITY_ITEM_GUEST_CART;
    constructor(public readonly payload: { id: string, quantity: number }) { }
}
export class EditUserAddress {
    static readonly type = ACTIONS.EDIT_USER_ADDRESS;
    constructor(public readonly payload: UpdateAddressInput) { }
}
export class AddUserAddress {
    static readonly type = ACTIONS.ADD_USER_ADDRESS;
    constructor(public readonly payload: AddAddressInput) { }
}

export class OrderCustomerUpdateStatus {
    static readonly type = ACTIONS.ORDER_CUSTOMER_UPDATE_STATUS;
    constructor(public readonly payload: OrderCustomerUpdateInput) { }
}

export class OrderCustomerUpdateStatusFailed {
    static readonly type = ACTIONS.ORDER_CUSTOMER_UPDATE_STATUS_FAILED;
    constructor(public readonly payload: OrderError) { }
}

export class OrderCustomerUpdateStatusSuccessfully {
    static readonly type = ACTIONS.ORDER_CUSTOMER_UPDATE_STATUS_SUCCESSFULLY;
    constructor(public readonly payload: boolean) { }
}