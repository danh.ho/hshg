export function stringifyItem<T extends { description?: string }>(item: T): T {
    const { description = '' } = item;
    const jsonDescription = JSON.stringify(description);
    return {
      ...item,
      description: jsonDescription.substring(1, jsonDescription.length - 2),
    };
  }
  export function parseItem<T extends { description?: string }>(item: T): T {
    const { description = '' } = item;
    try {
      const transformDescription = JSON.parse(`"${description}"`);
      return {
        ...item,
        description: transformDescription,
      };
    } catch (error) {
      console.warn(error);
  
      return {
        ...item,
        description,
      };
    }
  }