export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /**
   * The `Date` scalar type represents a Date
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  Date: any;
  /**
   * The `DateTime` scalar type represents a DateTime
   * value as specified by
   * [iso8601](https://en.wikipedia.org/wiki/ISO_8601).
   */
  DateTime: any;
  /**
   *
   *     Errors messages and codes mapped to
   *     fields or non fields errors.
   *     Example:
   *     {
   *         field_name: [
   *             {
   *                 "message": "error message",
   *                 "code": "error_code"
   *             }
   *         ],
   *         other_field: [
   *             {
   *                 "message": "error message",
   *                 "code": "error_code"
   *             }
   *         ],
   *         nonFieldErrors: [
   *             {
   *                 "message": "error message",
   *                 "code": "error_code"
   *             }
   *         ]
   *     }
   *
   */
  ExpectedErrorType: any;
  /**
   * The `GenericScalar` scalar type represents a generic
   * GraphQL scalar value that could be:
   * String, Boolean, Int, Float, List or Object.
   */
  GenericScalar: any;
  /**
   * Create scalar that ignores normal serialization/deserialization, since
   * that will be handled by the multipart request spec
   */
  Upload: any;
  /**
   * The upload of a file.
   *
   * Variables of this type must be set to null in mutations. They will be
   * replaced with a filename from a following multipart part containing a
   * binary file.
   *
   * See: https://github.com/jaydenseric/graphql-multipart-request-spec
   */
  UploadType: any;
};

export type AddAddressInput = {
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AddAddressPayload = {
  __typename?: 'AddAddressPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
};

export type AddToCart = {
  __typename?: 'AddToCart';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  cart?: Maybe<CartNode>;
};

export type AddToCartInput = {
  product: Scalars['String'];
  variant?: Maybe<Scalars['String']>;
  quantity: Scalars['Int'];
  topping?: Maybe<Array<Maybe<ToppingInput>>>;
  note?: Maybe<Scalars['String']>;
};

export type AdminChangePassword = {
  __typename?: 'AdminChangePassword';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type AdminChangePasswordInput = {
  userId: Scalars['String'];
  newPassword: Scalars['String'];
};

export type AdminCreateUserAddressInput = {
  user: Scalars['String'];
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AdminCreateUserAddressPayload = {
  __typename?: 'AdminCreateUserAddressPayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AdminDeleteUserAddressInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AdminDeleteUserAddressPayload = {
  __typename?: 'AdminDeleteUserAddressPayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
};

export type AdminDeliveryBatchCreate = {
  __typename?: 'AdminDeliveryBatchCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
  delivery?: Maybe<DeliveryNode>;
};

export type AdminDeliveryCreate = {
  __typename?: 'AdminDeliveryCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
  delivery?: Maybe<DeliveryNode>;
};

export type AdminDeliveryCreateInput = {
  orderId: Scalars['String'];
  shipper?: Maybe<Scalars['String']>;
  deliveryType?: Maybe<DeliveryTypeInput>;
  address: DeliveryAddressInput;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  distance?: Maybe<Scalars['Float']>;
  deliveryNote?: Maybe<Scalars['String']>;
  branch?: Maybe<Scalars['String']>;
  deliveryMethodId: Scalars['String'];
  durationTime?: Maybe<Scalars['Int']>;
  shippingCost?: Maybe<Scalars['Float']>;
  sortOrder?: Maybe<Scalars['Int']>;
  lalabag?: Maybe<Scalars['Boolean']>;
};

export type AdminDeliveryDelete = {
  __typename?: 'AdminDeliveryDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
};

export type AdminDeliveryDeleteInput = {
  id: Scalars['String'];
};

export type AdminDeliveryProductDelete = {
  __typename?: 'AdminDeliveryProductDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
  delivery?: Maybe<DeliveryNode>;
};

export type AdminDeliveryProductDeleteInput = {
  id: Scalars['String'];
  detailDeliveryId: Scalars['String'];
};

export type AdminDeliveryUpdate = {
  __typename?: 'AdminDeliveryUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
  delivery?: Maybe<DeliveryNode>;
};

export type AdminDeliveryUpdateInput = {
  id: Scalars['String'];
  deliveryStatus?: Maybe<DeliveryStatusInput>;
  paymentStatus?: Maybe<DeliveryPaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  deliveryNote?: Maybe<Scalars['String']>;
  product?: Maybe<Array<Maybe<ProductDeliveryInput>>>;
  shipper?: Maybe<Scalars['String']>;
  deliveryType?: Maybe<DeliveryTypeInput>;
  shippingCost?: Maybe<Scalars['Float']>;
  address?: Maybe<DeliveryAddressInput>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  distance?: Maybe<Scalars['Float']>;
  discount?: Maybe<Scalars['Float']>;
  branch?: Maybe<Scalars['String']>;
  cancelNote?: Maybe<Scalars['String']>;
  durationTime?: Maybe<Scalars['Int']>;
  isStart?: Maybe<Scalars['Boolean']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
};

export type AdminIncomingPaymentCreate = {
  __typename?: 'AdminIncomingPaymentCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<IncomingPaymentError>>>;
};

export type AdminIncomingPaymentCreateInput = {
  invoiceId: Scalars['Int'];
  incomingPaymentProduct?: Maybe<Array<Maybe<IncomingPaymentProductInput>>>;
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  addressId: Scalars['String'];
  incomingPaymentStatus: IncomingStatusInput;
  paymentStatus?: Maybe<IncomingPaymentPaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  incomingPaymentNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type AdminIncomingPaymentDelete = {
  __typename?: 'AdminIncomingPaymentDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<IncomingPaymentError>>>;
};

export type AdminIncomingPaymentDeleteInput = {
  id: Scalars['String'];
};

export type AdminIncomingPaymentProductDelete = {
  __typename?: 'AdminIncomingPaymentProductDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<IncomingPaymentError>>>;
  incomingPayment?: Maybe<IncomingPaymentNode>;
};

export type AdminIncomingPaymentProductDeleteInput = {
  id: Scalars['String'];
  detailIncomingPaymentId: Scalars['String'];
};

export type AdminIncomingPaymentUpdate = {
  __typename?: 'AdminIncomingPaymentUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<IncomingPaymentError>>>;
  incomingPayment?: Maybe<IncomingPaymentNode>;
};

export type AdminIncomingPaymentUpdateInput = {
  id: Scalars['String'];
  addressId?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  incomingPaymentStatus?: Maybe<IncomingStatusInput>;
  paymentStatus?: Maybe<IncomingPaymentPaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  incomingPaymentNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  product?: Maybe<Array<Maybe<IncomingPaymentProductInput>>>;
};

export type AdminInvoiceCreate = {
  __typename?: 'AdminInvoiceCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<InvoiceError>>>;
};

export type AdminInvoiceCreateInput = {
  deliveryId: Scalars['Int'];
  invoiceProduct?: Maybe<Array<Maybe<InvoiceProductInput>>>;
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  addressId: Scalars['String'];
  invoiceStatus: InvoiceStatusInput;
  paymentStatus?: Maybe<InvoicePaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  invoiceNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type AdminInvoiceDelete = {
  __typename?: 'AdminInvoiceDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<InvoiceError>>>;
};

export type AdminInvoiceDeleteInput = {
  id: Scalars['String'];
};

export type AdminInvoiceProductDelete = {
  __typename?: 'AdminInvoiceProductDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<InvoiceError>>>;
  invoice?: Maybe<InvoiceNode>;
};

export type AdminInvoiceProductDeleteInput = {
  id: Scalars['String'];
  detailInvoiceId: Scalars['String'];
};

export type AdminInvoiceUpdate = {
  __typename?: 'AdminInvoiceUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<InvoiceError>>>;
  invoice?: Maybe<InvoiceNode>;
};

export type AdminInvoiceUpdateInput = {
  id: Scalars['String'];
  addressId?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  invoiceStatus?: Maybe<InvoiceStatusInput>;
  paymentStatus?: Maybe<InvoicePaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  invoiceNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  product?: Maybe<Array<Maybe<InvoiceProductInput>>>;
};

export type AdminOrderCreate = {
  __typename?: 'AdminOrderCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  order?: Maybe<OrderNode>;
};

export type AdminOrderCreateInput = {
  product: Array<Maybe<ProductInput>>;
  promotionCode?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  orderStatus: OrderStatusInput;
  paymentStatus?: Maybe<PaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  orderNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  addressId?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  comment?: Maybe<Scalars['String']>;
  method?: Maybe<LoginStatusInput>;
  facebookId?: Maybe<Scalars['String']>;
  facebookName?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<SalesChannelInput>;
  otherFee?: Maybe<Scalars['Float']>;
  shippingCost?: Maybe<Scalars['Float']>;
  paid?: Maybe<Scalars['Float']>;
  shippingTime?: Maybe<Scalars['DateTime']>;
  isDelivery?: Maybe<Scalars['Boolean']>;
  isDeliveryNow?: Maybe<Scalars['Boolean']>;
  deliveryNote?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  productPricesListId?: Maybe<Scalars['String']>;
  customerGroupId?: Maybe<Scalars['String']>;
  unitDiscount?: Maybe<UnitDiscountInput>;
  promotionId?: Maybe<Scalars['String']>;
  discounts?: Maybe<Array<Maybe<DiscountInput>>>;
  created?: Maybe<Scalars['DateTime']>;
  orderOtherFee?: Maybe<Array<Maybe<OrderOtherFeeInput>>>;
};

export type AdminOrderWarehouseUpdate = {
  __typename?: 'AdminOrderWarehouseUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
};

export type AdminOrderWarehouseUpdateInput = {
  orderDetailId: Scalars['String'];
  warehouseId: Scalars['String'];
  quantity: Scalars['Float'];
};

export type AdminOutgoingPaymentCreate = {
  __typename?: 'AdminOutgoingPaymentCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OutgoingPaymentError>>>;
};

export type AdminOutgoingPaymentCreateInput = {
  invoiceId: Scalars['Int'];
  outgoingPaymentProduct?: Maybe<Array<Maybe<OutgoingPaymentProductInput>>>;
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  addressId: Scalars['String'];
  outgoingPaymentStatus: OutgoingPaymentStatusInput;
  paymentStatus?: Maybe<OutgoingPaymentPaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  outgoingPaymentNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type AdminOutgoingPaymentDelete = {
  __typename?: 'AdminOutgoingPaymentDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OutgoingPaymentError>>>;
};

export type AdminOutgoingPaymentDeleteInput = {
  id: Scalars['String'];
};

export type AdminOutgoingPaymentProductDelete = {
  __typename?: 'AdminOutgoingPaymentProductDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OutgoingPaymentError>>>;
  outgoingPayment?: Maybe<OutgoingPaymentNode>;
};

export type AdminOutgoingPaymentProductDeleteInput = {
  id: Scalars['String'];
  detailOutgoingPaymentId: Scalars['String'];
};

export type AdminOutgoingPaymentUpdate = {
  __typename?: 'AdminOutgoingPaymentUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OutgoingPaymentError>>>;
  outgoingPayment?: Maybe<OutgoingPaymentNode>;
};

export type AdminOutgoingPaymentUpdateInput = {
  id: Scalars['String'];
  addressId?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  outgoingPaymentStatus?: Maybe<OutgoingPaymentStatusInput>;
  paymentStatus?: Maybe<OutgoingPaymentPaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  outgoingPaymentNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  product?: Maybe<Array<Maybe<OutgoingPaymentProductInput>>>;
};

export type AdminSiteDetailNode = CustomizeInterface & {
  __typename?: 'AdminSiteDetailNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  address?: Maybe<UserAddressNode>;
  site?: Maybe<SiteNode>;
  tags: Array<UserTagRelatedNode>;
  staffGroupUser: StaffGroupStaffNodeConnection;
  role?: Maybe<Scalars['String']>;
  branch?: Maybe<Array<Maybe<BranchNode>>>;
  permission?: Maybe<Array<Maybe<BranchPermissionFieldNode>>>;
  bonusPoint?: Maybe<Scalars['Float']>;
};


export type AdminSiteDetailNodeStaffGroupUserArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

export enum AdminSiteRoleInput {
  Owner = 'OWNER',
  Manager = 'MANAGER',
  Staff = 'STAFF',
  Shipper = 'SHIPPER',
  HeadChef = 'HEAD_CHEF',
  Chef = 'CHEF'
}

export type AdminUpdateUserAddressInput = {
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isDefault?: Maybe<Scalars['Boolean']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AdminUpdateUserAddressPayload = {
  __typename?: 'AdminUpdateUserAddressPayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AttributeCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  specification?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AttributeCreatePayload = {
  __typename?: 'AttributeCreatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  attribute?: Maybe<AttributeNode>;
};

export type AttributeDeleteInput = {
  site?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AttributeDeletePayload = {
  __typename?: 'AttributeDeletePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  attribute?: Maybe<AttributeNode>;
};

export type AttributeInterface = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type AttributeNode = AttributeInterface & {
  __typename?: 'AttributeNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  specification?: Maybe<SpecificationNode>;
  description?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
};

export type AttributeNodeConnection = {
  __typename?: 'AttributeNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<AttributeNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `AttributeNode` and its cursor. */
export type AttributeNodeEdge = {
  __typename?: 'AttributeNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<AttributeNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type AttributeUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  specification?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type AttributeUpdatePayload = {
  __typename?: 'AttributeUpdatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  attribute?: Maybe<AttributeNode>;
};

export type AuthencationLoginInput = {
  clientMutationId?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  password: Scalars['String'];
};

export type AuthencationLoginPayload = {
  __typename?: 'AuthencationLoginPayload';
  token?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  user?: Maybe<UserProfile>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerCreateInput = {
  file: Scalars['Upload'];
  fileMobile?: Maybe<Scalars['Upload']>;
  bannerGroup?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  animation?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerCreatePayload = {
  __typename?: 'BannerCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  banner?: Maybe<BannerNode>;
};

export type BannerDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerDeletePayload = {
  __typename?: 'BannerDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  banner?: Maybe<BannerNode>;
};

export type BannerGroupBannerCreateInput = {
  banners?: Maybe<Array<Maybe<BannerGroupBannerInput>>>;
  autoplayTimeout?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  itemCode: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupBannerCreatePayload = {
  __typename?: 'BannerGroupBannerCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerGroupBannerDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupBannerDeletePayload = {
  __typename?: 'BannerGroupBannerDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerGroupBannerInput = {
  name: Scalars['String'];
  file?: Maybe<Scalars['Upload']>;
  fileMobile?: Maybe<Scalars['Upload']>;
  sortOrder?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  animation?: Maybe<Scalars['String']>;
};

export type BannerGroupBannerUpInput = {
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  file?: Maybe<Scalars['Upload']>;
  fileMobile?: Maybe<Scalars['Upload']>;
  sortOrder?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  animation?: Maybe<Scalars['String']>;
};

export type BannerGroupBannerUpdateInput = {
  banners?: Maybe<Array<Maybe<BannerGroupBannerUpInput>>>;
  bannersRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  bannersAdd?: Maybe<Array<Maybe<BannerGroupBannerInput>>>;
  itemCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  autoplayTimeout?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupBannerUpdatePayload = {
  __typename?: 'BannerGroupBannerUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerGroupCreateInput = {
  autoplayTimeout?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  itemCode: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupCreatePayload = {
  __typename?: 'BannerGroupCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerGroupDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupDeletePayload = {
  __typename?: 'BannerGroupDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerGroupNode = CustomizeInterface & {
  __typename?: 'BannerGroupNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  itemCode: Scalars['String'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  autoplayTimeout: Scalars['Int'];
  visibility: Scalars['Boolean'];
  banner: BannerNodeConnection;
};


export type BannerGroupNodeBannerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  groupId?: Maybe<Scalars['String']>;
  siteId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type BannerGroupNodeConnection = {
  __typename?: 'BannerGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BannerGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BannerGroupNode` and its cursor. */
export type BannerGroupNodeEdge = {
  __typename?: 'BannerGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<BannerGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BannerGroupUpdateInput = {
  banners?: Maybe<Array<Maybe<BannerInput>>>;
  bannersRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  itemCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  autoplayTimeout?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerGroupUpdatePayload = {
  __typename?: 'BannerGroupUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  bannerGroup?: Maybe<BannerGroupNode>;
};

export type BannerInput = {
  id: Scalars['String'];
  name: Scalars['String'];
  file?: Maybe<Scalars['Upload']>;
  fileMobile?: Maybe<Scalars['Upload']>;
  sortOrder?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
};

export type BannerNode = CustomizeInterface & {
  __typename?: 'BannerNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  group?: Maybe<BannerGroupNode>;
  name: Scalars['String'];
  file: Scalars['String'];
  fileMobile?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  sortOrder: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
  animation?: Maybe<Scalars['String']>;
};

export type BannerNodeConnection = {
  __typename?: 'BannerNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BannerNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BannerNode` and its cursor. */
export type BannerNodeEdge = {
  __typename?: 'BannerNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<BannerNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BannerSort = {
  __typename?: 'BannerSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type BannerSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type BannerUpdateInput = {
  file?: Maybe<Scalars['Upload']>;
  fileMobile?: Maybe<Scalars['Upload']>;
  bannerGroup?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  animation?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BannerUpdatePayload = {
  __typename?: 'BannerUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  banner?: Maybe<BannerNode>;
};

export type BetHistory = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type BetHistoryConnection = {
  __typename?: 'BetHistoryConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BetHistoryEdge>>;
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BetHistory` and its cursor. */
export type BetHistoryEdge = {
  __typename?: 'BetHistoryEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductForBetHistoryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BetTransactionInput = {
  product: Scalars['String'];
};

export type BranchCreateInput = {
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  key: Scalars['String'];
  street?: Maybe<Scalars['String']>;
  mapsLink?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BranchCreatePayload = {
  __typename?: 'BranchCreatePayload';
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  branch?: Maybe<BranchNode>;
};

export type BranchDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BranchDeletePayload = {
  __typename?: 'BranchDeletePayload';
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  branch?: Maybe<BranchNode>;
};

export type BranchNode = CustomizeInterface & {
  __typename?: 'BranchNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  isActive?: Maybe<Scalars['Boolean']>;
  site?: Maybe<SiteNode>;
  key: Scalars['String'];
  province?: Maybe<ProvinceNode>;
  district?: Maybe<DistrictNode>;
  ward?: Maybe<WardNode>;
  street?: Maybe<Scalars['String']>;
  mapsLink?: Maybe<Scalars['String']>;
  branchWarehouse: BranchWarehouseNodeConnection;
  productList?: Maybe<Array<Maybe<ProductBranchProductNode>>>;
};


export type BranchNodeBranchWarehouseArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type BranchNodeConnection = {
  __typename?: 'BranchNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BranchNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BranchNode` and its cursor. */
export type BranchNodeEdge = {
  __typename?: 'BranchNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<BranchNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BranchPermissionFieldNode = {
  __typename?: 'BranchPermissionFieldNode';
  branchId?: Maybe<Scalars['String']>;
  permissionField?: Maybe<Array<Maybe<PermissionFieldNode>>>;
};

export type BranchUpdateInput = {
  productVariantPrice?: Maybe<Array<Maybe<ProductVariantPriceInput>>>;
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  street?: Maybe<Scalars['String']>;
  mapsLink?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BranchUpdatePayload = {
  __typename?: 'BranchUpdatePayload';
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  status?: Maybe<Scalars['Boolean']>;
  /** The mutated object. */
  branch?: Maybe<BranchNode>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BranchWarehouseNode = CustomizeInterface & {
  __typename?: 'BranchWarehouseNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  warehouse: WarehouseNode;
  sortOrder: Scalars['Int'];
};

export type BranchWarehouseNodeConnection = {
  __typename?: 'BranchWarehouseNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BranchWarehouseNodeEdge>>;
};

/** A Relay edge containing a `BranchWarehouseNode` and its cursor. */
export type BranchWarehouseNodeEdge = {
  __typename?: 'BranchWarehouseNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<BranchWarehouseNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BranchWarehouseSort = {
  __typename?: 'BranchWarehouseSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type BranchWarehouseSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type BranchWarehouseUpdate = {
  __typename?: 'BranchWarehouseUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type BranchWarehouseUpdateInput = {
  branchId: Scalars['String'];
  warehouseAdd?: Maybe<Array<Maybe<Scalars['String']>>>;
  warehouseRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type BrandCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BrandCreatePayload = {
  __typename?: 'BrandCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  brand?: Maybe<BrandNode>;
};

export type BrandDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BrandDeletePayload = {
  __typename?: 'BrandDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  brand?: Maybe<BrandNode>;
};

export type BrandNode = CustomizeInterface & {
  __typename?: 'BrandNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
};

export type BrandNodeConnection = {
  __typename?: 'BrandNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<BrandNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `BrandNode` and its cursor. */
export type BrandNodeEdge = {
  __typename?: 'BrandNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<BrandNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type BrandUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type BrandUpdatePayload = {
  __typename?: 'BrandUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  brand?: Maybe<BrandNode>;
};

export type ByCustomer = {
  __typename?: 'ByCustomer';
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type ByEmployee = {
  __typename?: 'ByEmployee';
  employeeId?: Maybe<Scalars['String']>;
  employeeName?: Maybe<Scalars['String']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type ByProduct = {
  __typename?: 'ByProduct';
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Float']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type ByProductAndVariant = {
  __typename?: 'ByProductAndVariant';
  product?: Maybe<ProductForBuyNode>;
  variant?: Maybe<VariantNode>;
  pricesList?: Maybe<Array<Maybe<ProductPricesNode>>>;
  toppingList?: Maybe<Array<Maybe<ToppingNode>>>;
  price?: Maybe<Scalars['Float']>;
  inventory?: Maybe<Array<Maybe<ProductBranchWarehouse>>>;
  warehouse?: Maybe<Array<Maybe<WarehouseUnitInventory>>>;
  unit?: Maybe<Array<Maybe<ProductUnitNode>>>;
  unitExchange?: Maybe<Array<Maybe<ProductUnitNode>>>;
  unitParallel?: Maybe<Array<Maybe<ProductUnitNode>>>;
  totalQuantityInventory?: Maybe<Scalars['Float']>;
  totalOrderCustomer?: Maybe<Scalars['Float']>;
};

export type BySalesChannel = {
  __typename?: 'BySalesChannel';
  salesChannel?: Maybe<Scalars['String']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type BySalesChannelByProduct = {
  __typename?: 'BySalesChannelByProduct';
  salesChannel?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Float']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type ByUserByProduct = {
  __typename?: 'ByUserByProduct';
  userId?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
  saleAmount?: Maybe<Scalars['Float']>;
};

export type CartDetailNode = {
  __typename?: 'CartDetailNode';
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  variant?: Maybe<VariantNode>;
  quantity: Scalars['Int'];
  parent?: Maybe<CartDetailNode>;
  note?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  level: Scalars['Int'];
  subCartDetail: Array<CartDetailNode>;
};

export type CartNode = {
  __typename?: 'CartNode';
  id: Scalars['ID'];
  note?: Maybe<Scalars['String']>;
  cartDetail: Array<CartDetailNode>;
  relatedProduct?: Maybe<Array<Maybe<ProductForBuyNode>>>;
};

export type Category = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type CategoryCreateInput = {
  icon?: Maybe<Scalars['Upload']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  itemCode: Scalars['String'];
  parent?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CategoryCreatePayload = {
  __typename?: 'CategoryCreatePayload';
  errors?: Maybe<Array<Maybe<CategoryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  category?: Maybe<CategoryNode>;
};

export type CategoryDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CategoryDeletePayload = {
  __typename?: 'CategoryDeletePayload';
  errors?: Maybe<Array<Maybe<CategoryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  category?: Maybe<CategoryNode>;
};

export type CategoryError = {
  __typename?: 'CategoryError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type CategoryNode = Category & {
  __typename?: 'CategoryNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  parent?: Maybe<CategoryNode>;
  site?: Maybe<SiteNode>;
  itemCode?: Maybe<Scalars['String']>;
  level: Scalars['Int'];
  subCategory: CategoryNodeConnection;
  icon?: Maybe<Scalars['String']>;
  product?: Maybe<ProductForBuyNodeConnection>;
};


export type CategoryNodeSubCategoryArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['String']>;
  parentName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type CategoryNodeProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  isStock?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  collection?: Maybe<Scalars['String']>;
  collectionName?: Maybe<Scalars['String']>;
  hasFlashSale?: Maybe<Scalars['Boolean']>;
  collectionItemCode?: Maybe<Scalars['String']>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type CategoryNodeConnection = {
  __typename?: 'CategoryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CategoryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CategoryNode` and its cursor. */
export type CategoryNodeEdge = {
  __typename?: 'CategoryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CategoryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CategoryUpdateInput = {
  id: Scalars['String'];
  icon?: Maybe<Scalars['Upload']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CategoryUpdatePayload = {
  __typename?: 'CategoryUpdatePayload';
  errors?: Maybe<Array<Maybe<CategoryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  category?: Maybe<CategoryNode>;
};

export type ChangePassword = {
  __typename?: 'ChangePassword';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type ChangePasswordInput = {
  password: Scalars['String'];
  newPassword: Scalars['String'];
  confirmPassword: Scalars['String'];
};

export type ChangeProfile = {
  __typename?: 'ChangeProfile';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  user?: Maybe<UserProfile>;
};

export type ChangeProfileInput = {
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['Upload']>;
  email?: Maybe<Scalars['String']>;
};

export type CheckLalamove = {
  __typename?: 'CheckLalamove';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type CollectionCreateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  itemCode: Scalars['String'];
  parent?: Maybe<Scalars['ID']>;
  thumbnail?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CollectionCreatePayload = {
  __typename?: 'CollectionCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  collection?: Maybe<CollectionNode>;
};

export type CollectionDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CollectionDeletePayload = {
  __typename?: 'CollectionDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  collection?: Maybe<CollectionNode>;
};

export type CollectionNode = CustomizeInterface & {
  __typename?: 'CollectionNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  thumbnail?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  itemCode?: Maybe<Scalars['String']>;
  sortOrder: Scalars['Int'];
  level: Scalars['Int'];
  child: CollectionNodeConnection;
};


export type CollectionNodeChildArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type CollectionNodeConnection = {
  __typename?: 'CollectionNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CollectionNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CollectionNode` and its cursor. */
export type CollectionNodeEdge = {
  __typename?: 'CollectionNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CollectionNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CollectionSort = {
  __typename?: 'CollectionSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type CollectionSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type CollectionUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  parent?: Maybe<Scalars['ID']>;
  thumbnail?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CollectionUpdatePayload = {
  __typename?: 'CollectionUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  collection?: Maybe<CollectionNode>;
};

export type ContactUsCreate = {
  __typename?: 'ContactUsCreate';
  status?: Maybe<Scalars['Boolean']>;
  contactUs?: Maybe<ContactUsNode>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type ContactUsDelete = {
  __typename?: 'ContactUsDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type ContactUsDeleteInput = {
  id: Scalars['String'];
};

export type ContactUsInput = {
  firstName: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  phone: Scalars['String'];
  email: Scalars['String'];
  message: Scalars['String'];
  subject: Scalars['String'];
};

export type ContactUsNode = CustomizeInterface & {
  __typename?: 'ContactUsNode';
  firstName: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  phone: Scalars['String'];
  email: Scalars['String'];
  message: Scalars['String'];
  subject: Scalars['String'];
  site: SiteNode;
  isReplied: Scalars['Boolean'];
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ContactUsNodeConnection = {
  __typename?: 'ContactUsNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ContactUsNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ContactUsNode` and its cursor. */
export type ContactUsNodeEdge = {
  __typename?: 'ContactUsNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ContactUsNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ContactUsUpdate = {
  __typename?: 'ContactUsUpdate';
  status?: Maybe<Scalars['Boolean']>;
  contactUs?: Maybe<ContactUsNode>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type ContactUsUpdateInput = {
  id: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
  subject?: Maybe<Scalars['String']>;
  isReplied?: Maybe<Scalars['Boolean']>;
};

export type ContentTypeNode = {
  __typename?: 'ContentTypeNode';
  id: Scalars['ID'];
  appLabel: Scalars['String'];
  model: Scalars['String'];
  permission?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type Country = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type CountryCreateInput = {
  itemCode: Scalars['String'];
  name: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CountryCreatePayload = {
  __typename?: 'CountryCreatePayload';
  errors?: Maybe<Array<Maybe<CountryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  country?: Maybe<CountryNode>;
};

export type CountryDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CountryDeletePayload = {
  __typename?: 'CountryDeletePayload';
  errors?: Maybe<Array<Maybe<CountryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  country?: Maybe<CountryNode>;
};

export type CountryError = {
  __typename?: 'CountryError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type CountryNode = Country & {
  __typename?: 'CountryNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  itemCode: Scalars['String'];
  name: Scalars['String'];
};

export type CountryNodeConnection = {
  __typename?: 'CountryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CountryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CountryNode` and its cursor. */
export type CountryNodeEdge = {
  __typename?: 'CountryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CountryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CountryUpdateInput = {
  id: Scalars['String'];
  itemCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CountryUpdatePayload = {
  __typename?: 'CountryUpdatePayload';
  errors?: Maybe<Array<Maybe<CountryError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  country?: Maybe<CountryNode>;
};

export type CreateAdminSiteInput = {
  avatar?: Maybe<Scalars['UploadType']>;
  site?: Maybe<Scalars['String']>;
  role: AdminSiteRoleInput;
  staffGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  branch?: Maybe<Array<Maybe<Scalars['String']>>>;
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CreateAdminSitePayload = {
  __typename?: 'CreateAdminSitePayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type CreateCustomerInput = {
  avatar?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['String']>;
  tag?: Maybe<Array<Maybe<Scalars['String']>>>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CreateCustomerPayload = {
  __typename?: 'CreateCustomerPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type CreateFlatpageInput = {
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  title: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CreateFlatpagePayload = {
  __typename?: 'CreateFlatpagePayload';
  errors?: Maybe<Array<Maybe<FlatpageError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flatpage?: Maybe<FlatpageNode>;
};

export type CreateNewPassword = {
  __typename?: 'CreateNewPassword';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type CreateNewPasswordInput = {
  code: Scalars['String'];
  password: Scalars['String'];
  confirmPassword: Scalars['String'];
};

export type CreatePosCustomer = {
  __typename?: 'CreatePosCustomer';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  customer?: Maybe<CustomerDetailNode>;
};

export type CreatePosCustomerInput = {
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  note?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  branch?: Maybe<Scalars['String']>;
  tax?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['Upload']>;
  receiver?: Maybe<Scalars['String']>;
};

export type CreateSuperAdminInput = {
  avatar?: Maybe<Scalars['Upload']>;
  site: Scalars['String'];
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod: UserLoginMethod;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  isSpam: Scalars['Boolean'];
  /** Set list of orders */
  adminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  delivery?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  deliveryAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery groups */
  deliveryGroup?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery groups */
  deliveryGroups?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  deliveryShipper?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery historys */
  deliveryUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of goods receipt transfers */
  goodReceiptTransferReceiver?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payments */
  incomingPayment?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payments */
  incomingPaymentAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payment historys */
  incomingPaymentUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of inventory counts */
  inventoryCountUserCompleted?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  invoice?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  invoiceAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoice historys */
  invoiceUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of orders */
  order?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of kitchen order details */
  orderKitchenChef?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  outgoingPayment?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  outgoingPaymentAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payment historys */
  outgoingPaymentUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of order historys */
  userHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of newss */
  userNews?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CreateSuperAdminPayload = {
  __typename?: 'CreateSuperAdminPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type CustomerConnection = {
  __typename?: 'CustomerConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CustomerEdge>>;
  totalAmount?: Maybe<Scalars['Float']>;
};

export type CustomerDetailNode = CustomizeInterface & {
  __typename?: 'CustomerDetailNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive: Scalars['Boolean'];
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  loginMethod: UserLoginMethod;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  isSpam: Scalars['Boolean'];
  customerGroupUser: CustomerGroupCustomerNodeConnection;
  tags: Array<UserTagRelatedNode>;
  bet?: Maybe<Array<Maybe<ProductForBetTransactionNode>>>;
  referral?: Maybe<Array<Maybe<UserCustomerNode>>>;
  invited?: Maybe<Array<Maybe<UserCustomerNode>>>;
  order?: Maybe<Array<Maybe<OrderNode>>>;
  cart?: Maybe<Array<Maybe<CartNode>>>;
  customerGroup?: Maybe<Array<Maybe<CustomerGroupCustomerNode>>>;
  branch?: Maybe<Array<Maybe<BranchNode>>>;
  tax?: Maybe<Scalars['String']>;
  customerType?: Maybe<Scalars['String']>;
  debt?: Maybe<Scalars['Float']>;
  address?: Maybe<Array<Maybe<UserAddressNode>>>;
  bonusPoint?: Maybe<Scalars['Float']>;
  rank?: Maybe<Scalars['String']>;
};


export type CustomerDetailNodeCustomerGroupUserArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

/** A Relay edge containing a `Customer` and its cursor. */
export type CustomerEdge = {
  __typename?: 'CustomerEdge';
  /** The item at the end of the edge */
  node?: Maybe<ByCustomer>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CustomerGroupCreateInput = {
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  unitDiscount?: Maybe<CustomerGroupUnitDiscount>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerGroupCreatePayload = {
  __typename?: 'CustomerGroupCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerGroup?: Maybe<CustomerGroupNode>;
};

export type CustomerGroupCustomerNode = CustomizeInterface & {
  __typename?: 'CustomerGroupCustomerNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  customerGroup: CustomerGroupNode;
  customer?: Maybe<UserCustomerNode>;
};

export type CustomerGroupCustomerNodeConnection = {
  __typename?: 'CustomerGroupCustomerNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CustomerGroupCustomerNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CustomerGroupCustomerNode` and its cursor. */
export type CustomerGroupCustomerNodeEdge = {
  __typename?: 'CustomerGroupCustomerNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CustomerGroupCustomerNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CustomerGroupDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerGroupDeletePayload = {
  __typename?: 'CustomerGroupDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerGroup?: Maybe<CustomerGroupNode>;
};

export type CustomerGroupNode = CustomizeInterface & {
  __typename?: 'CustomerGroupNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  discount: Scalars['Float'];
  unitDiscount: CustomerGroupUnitDiscount;
  customerGroupUser: CustomerGroupCustomerNodeConnection;
  productPricesListCustomerGroup: ProductPricesListCustomerGroupNodeConnection;
  order: OrderNodeConnection;
  customerList?: Maybe<Array<Maybe<UserCustomerNode>>>;
};


export type CustomerGroupNodeCustomerGroupUserArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type CustomerGroupNodeProductPricesListCustomerGroupArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type CustomerGroupNodeOrderArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  isDeliveryNow?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  isDelivery?: Maybe<Scalars['Boolean']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderInfomation?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  isCancelled?: Maybe<Scalars['Boolean']>;
  isPending?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type CustomerGroupNodeConnection = {
  __typename?: 'CustomerGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CustomerGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CustomerGroupNode` and its cursor. */
export type CustomerGroupNodeEdge = {
  __typename?: 'CustomerGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CustomerGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum CustomerGroupUnitDiscount {
  /** Percent */
  Percent = 'PERCENT',
  /** vnd */
  Vnd = 'VND'
}

export type CustomerGroupUpdateInput = {
  name?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  unitDiscount?: Maybe<CustomerGroupUnitDiscount>;
  /** The ID of the object. */
  id: Scalars['ID'];
  /** Set list of orders */
  order?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerGroupUpdatePayload = {
  __typename?: 'CustomerGroupUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerGroup?: Maybe<CustomerGroupNode>;
};

export type CustomerReviewCreateInput = {
  site?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['Upload']>;
  name: Scalars['String'];
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerReviewCreatePayload = {
  __typename?: 'CustomerReviewCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerReview?: Maybe<CustomerReviewNode>;
};

export type CustomerReviewDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerReviewDeletePayload = {
  __typename?: 'CustomerReviewDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerReview?: Maybe<CustomerReviewNode>;
};

export type CustomerReviewNode = CustomizeInterface & {
  __typename?: 'CustomerReviewNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  sortOrder: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
  position?: Maybe<Scalars['String']>;
};

export type CustomerReviewNodeConnection = {
  __typename?: 'CustomerReviewNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<CustomerReviewNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `CustomerReviewNode` and its cursor. */
export type CustomerReviewNodeEdge = {
  __typename?: 'CustomerReviewNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<CustomerReviewNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type CustomerReviewSort = {
  __typename?: 'CustomerReviewSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type CustomerReviewSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type CustomerReviewUpdateInput = {
  image?: Maybe<Scalars['Upload']>;
  name?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type CustomerReviewUpdatePayload = {
  __typename?: 'CustomerReviewUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  customerReview?: Maybe<CustomerReviewNode>;
};

export type CustomizeInterface = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

/** An error that happened in a mutation. */
export type CustomizeMutationErrorType = {
  __typename?: 'CustomizeMutationErrorType';
  /** The field that caused the error, or `null` if it isn't associated with any particular field. */
  field?: Maybe<Scalars['String']>;
  /** The error message. */
  message?: Maybe<Scalars['String']>;
  /** The error code */
  code?: Maybe<Scalars['String']>;
};

export enum DashBoardGroupBy {
  Day = 'DAY',
  Hour = 'HOUR'
}

export type DashBoardNode = {
  __typename?: 'DashBoardNode';
  time?: Maybe<Scalars['DateTime']>;
  orderQuantity?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Float']>;
};



export type DeleteAddressInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeleteAddressPayload = {
  __typename?: 'DeleteAddressPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
};

export type DeleteAdminSiteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeleteAdminSitePayload = {
  __typename?: 'DeleteAdminSitePayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type DeleteCart = {
  __typename?: 'DeleteCart';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
};

export type DeleteFlatpageInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeleteFlatpagePayload = {
  __typename?: 'DeleteFlatpagePayload';
  errors?: Maybe<Array<Maybe<FlatpageError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flatpage?: Maybe<FlatpageNode>;
};

export type DeleteReviewInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeleteReviewPayload = {
  __typename?: 'DeleteReviewPayload';
  errors?: Maybe<Array<Maybe<ReviewError>>>;
  status?: Maybe<Scalars['Boolean']>;
  /** The mutated object. */
  userReview?: Maybe<UserReviewNode>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type Delivery = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type DeliveryAddressInput = {
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
};

export type DeliveryAddressNode = {
  __typename?: 'DeliveryAddressNode';
  id: Scalars['ID'];
  address?: Maybe<UserAddressNode>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  province?: Maybe<ProvinceNode>;
  district?: Maybe<DistrictNode>;
  ward?: Maybe<WardNode>;
  street?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
};

export type DeliveryBranchCreateInput = {
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  key: Scalars['String'];
  isActive?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeliveryBranchCreatePayload = {
  __typename?: 'DeliveryBranchCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  deliveryBranch?: Maybe<DeliveryBranchNode>;
};

export type DeliveryBranchDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeliveryBranchDeletePayload = {
  __typename?: 'DeliveryBranchDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  deliveryBranch?: Maybe<DeliveryBranchNode>;
};

export type DeliveryBranchNode = {
  __typename?: 'DeliveryBranchNode';
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  isActive?: Maybe<Scalars['Boolean']>;
  site?: Maybe<SiteNode>;
  key: Scalars['String'];
};

export type DeliveryBranchUpdateInput = {
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeliveryBranchUpdatePayload = {
  __typename?: 'DeliveryBranchUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  deliveryBranch?: Maybe<DeliveryBranchNode>;
};

/** An enumeration. */
export enum DeliveryDeliveryStatus {
  /** Pending */
  Pending = 'PENDING',
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Delivered */
  Delivered = 'DELIVERED',
  /** Received */
  Received = 'RECEIVED',
  /** Delivering */
  Delivering = 'DELIVERING',
  /** Customer_cancelled */
  CustomerCancelled = 'CUSTOMER_CANCELLED',
  /** Picked_up */
  PickedUp = 'PICKED_UP',
  /** Pickup_failed */
  PickupFailed = 'PICKUP_FAILED',
  /** Delivery_failed */
  DeliveryFailed = 'DELIVERY_FAILED',
  /** First_pickup_attempt_failed */
  FirstPickupAttemptFailed = 'FIRST_PICKUP_ATTEMPT_FAILED',
  /** Second_pickup_attempt_failed */
  SecondPickupAttemptFailed = 'SECOND_PICKUP_ATTEMPT_FAILED',
  /** Third_pickup_attempt_failed */
  ThirdPickupAttemptFailed = 'THIRD_PICKUP_ATTEMPT_FAILED',
  /** First_send_attempt_failed */
  FirstSendAttemptFailed = 'FIRST_SEND_ATTEMPT_FAILED',
  /** Second_send_attempt_failed */
  SecondSendAttemptFailed = 'SECOND_SEND_ATTEMPT_FAILED',
  /** Third_send_attempt_failed */
  ThirdSendAttemptFailed = 'THIRD_SEND_ATTEMPT_FAILED',
  /** Being_return */
  BeingReturn = 'BEING_RETURN',
  /** First_return_attempt_failed */
  FirstReturnAttemptFailed = 'FIRST_RETURN_ATTEMPT_FAILED',
  /** Second_return_attempt_failed */
  SecondReturnAttemptFailed = 'SECOND_RETURN_ATTEMPT_FAILED',
  /** Third_return_attempt_failed */
  ThirdReturnAttemptFailed = 'THIRD_RETURN_ATTEMPT_FAILED',
  /** Package_returned */
  PackageReturned = 'PACKAGE_RETURNED',
  /** Return_failed */
  ReturnFailed = 'RETURN_FAILED',
  /** Rider_assigned */
  RiderAssigned = 'RIDER_ASSIGNED',
  /** package_lost */
  PackageLost = 'PACKAGE_LOST'
}

/** An enumeration. */
export enum DeliveryDeliveryType {
  /** Automatic */
  Automatic = 'AUTOMATIC',
  /** Assign */
  Assign = 'ASSIGN'
}

export type DeliveryDetailNode = {
  __typename?: 'DeliveryDetailNode';
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  productName: Scalars['String'];
  quantity: Scalars['Float'];
  unitPrice: Scalars['Float'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  isReviewed: Scalars['Boolean'];
  subTotal: Scalars['Float'];
};

export type DeliveryError = {
  __typename?: 'DeliveryError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

/** An enumeration. */
export enum DeliveryGroupDeliveryGroupStatus {
  /** Delivering */
  Delivering = 'DELIVERING',
  /** Delivered */
  Delivered = 'DELIVERED'
}

export type DeliveryGroupNode = CustomizeInterface & {
  __typename?: 'DeliveryGroupNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  createdBy?: Maybe<UserProfile>;
  deliveryGroupStatus: DeliveryGroupDeliveryGroupStatus;
  deliveries: DeliveryNodeConnection;
};


export type DeliveryGroupNodeDeliveriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  shipper?: Maybe<Scalars['String']>;
  deliveryStatus?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  deliveryAddress?: Maybe<Scalars['String']>;
  receiveAddress?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  deliveryPhone?: Maybe<Scalars['String']>;
  receivePhone?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type DeliveryGroupNodeConnection = {
  __typename?: 'DeliveryGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DeliveryGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DeliveryGroupNode` and its cursor. */
export type DeliveryGroupNodeEdge = {
  __typename?: 'DeliveryGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DeliveryGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type DeliveryHistoryDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DeliveryHistoryDeletePayload = {
  __typename?: 'DeliveryHistoryDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  deliveryHistory?: Maybe<DeliveryHistoryNode>;
};

/** An enumeration. */
export enum DeliveryHistoryDeliveryStatus {
  /** Pending */
  Pending = 'PENDING',
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Delivered */
  Delivered = 'DELIVERED',
  /** Received */
  Received = 'RECEIVED',
  /** Delivering */
  Delivering = 'DELIVERING',
  /** Customer_cancelled */
  CustomerCancelled = 'CUSTOMER_CANCELLED',
  /** Picked_up */
  PickedUp = 'PICKED_UP',
  /** Pickup_failed */
  PickupFailed = 'PICKUP_FAILED',
  /** Delivery_failed */
  DeliveryFailed = 'DELIVERY_FAILED',
  /** First_pickup_attempt_failed */
  FirstPickupAttemptFailed = 'FIRST_PICKUP_ATTEMPT_FAILED',
  /** Second_pickup_attempt_failed */
  SecondPickupAttemptFailed = 'SECOND_PICKUP_ATTEMPT_FAILED',
  /** Third_pickup_attempt_failed */
  ThirdPickupAttemptFailed = 'THIRD_PICKUP_ATTEMPT_FAILED',
  /** First_send_attempt_failed */
  FirstSendAttemptFailed = 'FIRST_SEND_ATTEMPT_FAILED',
  /** Second_send_attempt_failed */
  SecondSendAttemptFailed = 'SECOND_SEND_ATTEMPT_FAILED',
  /** Third_send_attempt_failed */
  ThirdSendAttemptFailed = 'THIRD_SEND_ATTEMPT_FAILED',
  /** Being_return */
  BeingReturn = 'BEING_RETURN',
  /** First_return_attempt_failed */
  FirstReturnAttemptFailed = 'FIRST_RETURN_ATTEMPT_FAILED',
  /** Second_return_attempt_failed */
  SecondReturnAttemptFailed = 'SECOND_RETURN_ATTEMPT_FAILED',
  /** Third_return_attempt_failed */
  ThirdReturnAttemptFailed = 'THIRD_RETURN_ATTEMPT_FAILED',
  /** Package_returned */
  PackageReturned = 'PACKAGE_RETURNED',
  /** Return_failed */
  ReturnFailed = 'RETURN_FAILED',
  /** Rider_assigned */
  RiderAssigned = 'RIDER_ASSIGNED',
  /** package_lost */
  PackageLost = 'PACKAGE_LOST'
}

export type DeliveryHistoryNode = {
  __typename?: 'DeliveryHistoryNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  status?: Maybe<DeliveryHistoryStatus>;
  description?: Maybe<Scalars['String']>;
  user?: Maybe<UserProfile>;
  deliveryStatus?: Maybe<DeliveryHistoryDeliveryStatus>;
  modifiedName?: Maybe<Scalars['String']>;
};

/** An enumeration. */
export enum DeliveryHistoryStatus {
  /** Comment */
  Comment = 'COMMENT',
  /** Action */
  Action = 'ACTION'
}

export type DeliveryMethodCreate = {
  __typename?: 'DeliveryMethodCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryMethodError>>>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
};

export type DeliveryMethodCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};

export type DeliveryMethodDelete = {
  __typename?: 'DeliveryMethodDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryMethodError>>>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
};

export type DeliveryMethodDeleteInput = {
  id: Scalars['String'];
};

export type DeliveryMethodError = {
  __typename?: 'DeliveryMethodError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type DeliveryMethodInput = {
  id?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
};

export type DeliveryMethodNode = CustomizeInterface & {
  __typename?: 'DeliveryMethodNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};

export type DeliveryMethodNodeConnection = {
  __typename?: 'DeliveryMethodNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DeliveryMethodNodeEdge>>;
};

/** A Relay edge containing a `DeliveryMethodNode` and its cursor. */
export type DeliveryMethodNodeEdge = {
  __typename?: 'DeliveryMethodNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DeliveryMethodNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type DeliveryMethodUpdate = {
  __typename?: 'DeliveryMethodUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryMethodError>>>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
};

export type DeliveryMethodUpdateInput = {
  id: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};

export type DeliveryNode = Delivery & {
  __typename?: 'DeliveryNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  deliveryGroup?: Maybe<DeliveryGroupNode>;
  createdBy?: Maybe<UserProfile>;
  paymentMethod: PaymentNode;
  totalAmount: Scalars['Float'];
  deliveryStatus?: Maybe<DeliveryDeliveryStatus>;
  paymentStatus?: Maybe<DeliveryPaymentStatus>;
  orderNote?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  customer?: Maybe<UserProfile>;
  deliveryType?: Maybe<DeliveryDeliveryType>;
  shipper?: Maybe<UserProfile>;
  branchName?: Maybe<Scalars['String']>;
  shippingCost: Scalars['Float'];
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  distance?: Maybe<Scalars['Float']>;
  discount?: Maybe<Scalars['Float']>;
  branch?: Maybe<BranchNode>;
  deliveryNote?: Maybe<Scalars['String']>;
  cancelNote?: Maybe<Scalars['String']>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
  ship60DeliveryCode?: Maybe<Scalars['String']>;
  customerDeliveryCode?: Maybe<Scalars['String']>;
  lalamoveOrderId?: Maybe<Scalars['String']>;
  durationTime: Scalars['Int'];
  endTime?: Maybe<Scalars['DateTime']>;
  sortOrder: Scalars['Int'];
  details?: Maybe<Array<Maybe<DeliveryDetailNode>>>;
  deliveryAddress?: Maybe<DeliveryAddressNode>;
  deliveryHistory?: Maybe<Array<Maybe<DeliveryHistoryNode>>>;
  order?: Maybe<OrderNode>;
  deliveryReview?: Maybe<DeliveryReviewNode>;
};

export type DeliveryNodeConnection = {
  __typename?: 'DeliveryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DeliveryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DeliveryNode` and its cursor. */
export type DeliveryNodeEdge = {
  __typename?: 'DeliveryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DeliveryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum DeliveryPaymentStatus {
  /** Paid */
  Paid = 'PAID',
  /** Wait_for_pay */
  WaitForPay = 'WAIT_FOR_PAY',
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Fulfilled */
  Fulfilled = 'FULFILLED',
  /** Partially_fulfilled */
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  /** Unfulfilled */
  Unfulfilled = 'UNFULFILLED',
  /** Ready_to_capture */
  ReadyToCapture = 'READY_TO_CAPTURE',
  /** Ready_to_fulfill */
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export enum DeliveryPaymentStatusInput {
  Paid = 'PAID',
  WaitForPay = 'WAIT_FOR_PAY',
  Cancelled = 'CANCELLED',
  Fulfilled = 'FULFILLED',
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  Unfulfilled = 'UNFULFILLED',
  ReadyToCapture = 'READY_TO_CAPTURE',
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export type DeliveryReviewCreate = {
  __typename?: 'DeliveryReviewCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
};

export type DeliveryReviewCreateInput = {
  delivery: Scalars['String'];
  rating?: Maybe<Scalars['Float']>;
  comment?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
};

export type DeliveryReviewDelete = {
  __typename?: 'DeliveryReviewDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
};

export type DeliveryReviewNode = CustomizeInterface & {
  __typename?: 'DeliveryReviewNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  delivery: DeliveryNode;
  shipper: UserProfile;
  customer: UserProfile;
  rating: Scalars['Float'];
  comment?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
};

export type DeliveryReviewNodeConnection = {
  __typename?: 'DeliveryReviewNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DeliveryReviewNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DeliveryReviewNode` and its cursor. */
export type DeliveryReviewNodeEdge = {
  __typename?: 'DeliveryReviewNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DeliveryReviewNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum DeliveryStatusInput {
  Pending = 'PENDING',
  Delivering = 'DELIVERING',
  Delivered = 'DELIVERED',
  PickedUp = 'PICKED_UP',
  CustomerCancelled = 'CUSTOMER_CANCELLED',
  Cancelled = 'CANCELLED',
  Received = 'RECEIVED',
  FirstPickupAttemptFailed = 'FIRST_PICKUP_ATTEMPT_FAILED',
  SecondPickupAttemptFailed = 'SECOND_PICKUP_ATTEMPT_FAILED',
  ThirdPickupAttemptFailed = 'THIRD_PICKUP_ATTEMPT_FAILED',
  PickupFailed = 'PICKUP_FAILED',
  FirstSendAttemptFailed = 'FIRST_SEND_ATTEMPT_FAILED',
  SecondSendAttemptFailed = 'SECOND_SEND_ATTEMPT_FAILED',
  ThirdSendAttemptFailed = 'THIRD_SEND_ATTEMPT_FAILED',
  DeliveryFailed = 'DELIVERY_FAILED',
  BeingReturn = 'BEING_RETURN',
  FirstReturnAttemptFailed = 'FIRST_RETURN_ATTEMPT_FAILED',
  SecondReturnAttemptFailed = 'SECOND_RETURN_ATTEMPT_FAILED',
  ThirdReturnAttemptFailed = 'THIRD_RETURN_ATTEMPT_FAILED',
  ReturnFailed = 'RETURN_FAILED',
  PackageReturned = 'PACKAGE_RETURNED',
  RiderAssigned = 'RIDER_ASSIGNED',
  PackageLost = 'PACKAGE_LOST'
}

export type DeliveryTrackingCreate = {
  __typename?: 'DeliveryTrackingCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
};

export type DeliveryTrackingCreateInput = {
  delivery: Scalars['String'];
  latitude: Scalars['Float'];
  longtitude: Scalars['Float'];
  created?: Maybe<Scalars['DateTime']>;
};

export type DeliveryTrackingDelete = {
  __typename?: 'DeliveryTrackingDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
};

export type DeliveryTrackingNode = CustomizeInterface & {
  __typename?: 'DeliveryTrackingNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  delivery: DeliveryNode;
  shipper: UserProfile;
  latitude: Scalars['Float'];
  longtitude: Scalars['Float'];
  created?: Maybe<Scalars['DateTime']>;
};

export type DeliveryTrackingNodeConnection = {
  __typename?: 'DeliveryTrackingNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DeliveryTrackingNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DeliveryTrackingNode` and its cursor. */
export type DeliveryTrackingNodeEdge = {
  __typename?: 'DeliveryTrackingNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DeliveryTrackingNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum DeliveryTypeInput {
  Automatic = 'AUTOMATIC',
  Assign = 'ASSIGN'
}

export type DeteteCartInput = {
  cartDetailId: Scalars['String'];
};

export type DiscountInput = {
  discount: Scalars['Float'];
  unitDiscount: UnitDiscountInput;
  discountDescription?: Maybe<Scalars['String']>;
};

export type DistrictBranchCreateInput = {
  district: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DistrictBranchCreatePayload = {
  __typename?: 'DistrictBranchCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  districtBranch?: Maybe<DistrictBranchNode>;
};

export type DistrictBranchDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DistrictBranchDeletePayload = {
  __typename?: 'DistrictBranchDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  districtBranch?: Maybe<DistrictBranchNode>;
};

export type DistrictBranchNode = CustomizeInterface & {
  __typename?: 'DistrictBranchNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  district: DistrictNode;
};

export type DistrictBranchNodeConnection = {
  __typename?: 'DistrictBranchNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DistrictBranchNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DistrictBranchNode` and its cursor. */
export type DistrictBranchNodeEdge = {
  __typename?: 'DistrictBranchNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DistrictBranchNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type DistrictCreateInput = {
  nameDistrict: Scalars['String'];
  code: Scalars['String'];
  province: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DistrictCreatePayload = {
  __typename?: 'DistrictCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  district?: Maybe<DistrictNode>;
};

export type DistrictDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DistrictDeletePayload = {
  __typename?: 'DistrictDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  district?: Maybe<DistrictNode>;
};

export type DistrictNode = CustomizeInterface & {
  __typename?: 'DistrictNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  nameDistrict?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  province?: Maybe<ProvinceNode>;
  sortId: Scalars['Int'];
};

export type DistrictNodeConnection = {
  __typename?: 'DistrictNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<DistrictNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `DistrictNode` and its cursor. */
export type DistrictNodeEdge = {
  __typename?: 'DistrictNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<DistrictNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type DistrictUpdateInput = {
  nameDistrict?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  sortId?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  /** Set list of delivery addresss */
  deliveryAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of party infors */
  partyInfor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of wards */
  ward?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type DistrictUpdatePayload = {
  __typename?: 'DistrictUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  district?: Maybe<DistrictNode>;
};

/** Debugging information for the current query. */
export type DjangoDebug = {
  __typename?: 'DjangoDebug';
  /** Executed SQL queries for this API query. */
  sql?: Maybe<Array<Maybe<DjangoDebugSql>>>;
};

/** Represents a single database query made to a Django managed DB. */
export type DjangoDebugSql = {
  __typename?: 'DjangoDebugSQL';
  /** The type of database being used (e.g. postrgesql, mysql, sqlite). */
  vendor: Scalars['String'];
  /** The Django database alias (e.g. 'default'). */
  alias: Scalars['String'];
  /** The actual SQL sent to this database. */
  sql?: Maybe<Scalars['String']>;
  /** Duration of this database query in seconds. */
  duration: Scalars['Float'];
  /** The raw SQL of this query, without params. */
  rawSql: Scalars['String'];
  /** JSON encoded database query parameters. */
  params: Scalars['String'];
  /** Start time of this database query. */
  startTime: Scalars['Float'];
  /** Stop time of this database query. */
  stopTime: Scalars['Float'];
  /** Whether this database query took more than 10 seconds. */
  isSlow: Scalars['Boolean'];
  /** Whether this database query was a SELECT. */
  isSelect: Scalars['Boolean'];
  /** Postgres transaction ID if available. */
  transId?: Maybe<Scalars['String']>;
  /** Postgres transaction status if available. */
  transStatus?: Maybe<Scalars['String']>;
  /** Postgres isolation level if available. */
  isoLevel?: Maybe<Scalars['String']>;
  /** Postgres connection encoding if available. */
  encoding?: Maybe<Scalars['String']>;
};

export type EmployeeConnection = {
  __typename?: 'EmployeeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<EmployeeEdge>>;
  totalAmount?: Maybe<Scalars['Float']>;
};

/** A Relay edge containing a `Employee` and its cursor. */
export type EmployeeEdge = {
  __typename?: 'EmployeeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ByEmployee>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ErrorType = {
  __typename?: 'ErrorType';
  message?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
};


export type FaQsCategoryCreateInput = {
  name: Scalars['String'];
  parent?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsCategoryCreatePayload = {
  __typename?: 'FAQsCategoryCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQsCategory?: Maybe<FaQsCategoryNode>;
};

export type FaQsCategoryDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsCategoryDeletePayload = {
  __typename?: 'FAQsCategoryDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQsCategory?: Maybe<FaQsCategoryNode>;
};

export type FaQsCategoryNode = CustomizeInterface & {
  __typename?: 'FAQsCategoryNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  site?: Maybe<SiteNode>;
  child: FaQsCategoryNodeConnection;
};


export type FaQsCategoryNodeChildArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type FaQsCategoryNodeConnection = {
  __typename?: 'FAQsCategoryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FaQsCategoryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FAQsCategoryNode` and its cursor. */
export type FaQsCategoryNodeEdge = {
  __typename?: 'FAQsCategoryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FaQsCategoryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FaQsCategoryUpdateInput = {
  name?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  parent?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsCategoryUpdatePayload = {
  __typename?: 'FAQsCategoryUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQsCategory?: Maybe<FaQsCategoryNode>;
};

export type FaQsCreateInput = {
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  category: Scalars['ID'];
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsCreatePayload = {
  __typename?: 'FAQsCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQs?: Maybe<FaQsNode>;
};

export type FaQsDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsDeletePayload = {
  __typename?: 'FAQsDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQs?: Maybe<FaQsNode>;
};

export type FaQsNode = CustomizeInterface & {
  __typename?: 'FAQsNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  category: FaQsCategoryNode;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
};

export type FaQsNodeConnection = {
  __typename?: 'FAQsNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FaQsNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FAQsNode` and its cursor. */
export type FaQsNodeEdge = {
  __typename?: 'FAQsNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FaQsNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FaQsUpdateInput = {
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  category?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FaQsUpdatePayload = {
  __typename?: 'FAQsUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  fAQs?: Maybe<FaQsNode>;
};

export type FacebookLoginInput = {
  facebookId: Scalars['String'];
  accessToken: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FacebookLoginPayload = {
  __typename?: 'FacebookLoginPayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<MutationErrorType>>;
  token?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  facebookInformation?: Maybe<FacebookNode>;
};

export type FacebookNode = {
  __typename?: 'FacebookNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  user: UserProfile;
  facebookId: Scalars['String'];
  accessToken?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
};

export type FeatureCreateInput = {
  name: Scalars['String'];
  itemCode: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeatureCreatePayload = {
  __typename?: 'FeatureCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feature?: Maybe<FeatureNode>;
};

export type FeatureDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeatureDeletePayload = {
  __typename?: 'FeatureDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feature?: Maybe<FeatureNode>;
};

export type FeatureNode = CustomizeInterface & {
  __typename?: 'FeatureNode';
  name?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type FeatureNodeConnection = {
  __typename?: 'FeatureNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FeatureNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FeatureNode` and its cursor. */
export type FeatureNodeEdge = {
  __typename?: 'FeatureNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FeatureNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FeatureUpdateInput = {
  name?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeatureUpdatePayload = {
  __typename?: 'FeatureUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feature?: Maybe<FeatureNode>;
};

export type FeedbackCreateInput = {
  site?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['Upload']>;
  name: Scalars['String'];
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeedbackCreatePayload = {
  __typename?: 'FeedbackCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feedback?: Maybe<FeedbackNode>;
};

export type FeedbackDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeedbackDeletePayload = {
  __typename?: 'FeedbackDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feedback?: Maybe<FeedbackNode>;
};

export type FeedbackNode = CustomizeInterface & {
  __typename?: 'FeedbackNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  sortOrder: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
  position?: Maybe<Scalars['String']>;
};

export type FeedbackNodeConnection = {
  __typename?: 'FeedbackNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FeedbackNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FeedbackNode` and its cursor. */
export type FeedbackNodeEdge = {
  __typename?: 'FeedbackNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FeedbackNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FeedbackSort = {
  __typename?: 'FeedbackSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type FeedbackSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type FeedbackUpdateInput = {
  image?: Maybe<Scalars['Upload']>;
  name?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FeedbackUpdatePayload = {
  __typename?: 'FeedbackUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  feedback?: Maybe<FeedbackNode>;
};

export type FlashSaleActivate = {
  __typename?: 'FlashSaleActivate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  flashSale?: Maybe<FlashSaleNode>;
};

export type FlashSaleCreateInput = {
  site?: Maybe<Scalars['String']>;
  productList: Array<Maybe<ProductFlashSaleInput>>;
  image?: Maybe<Scalars['Upload']>;
  image2?: Maybe<Scalars['Upload']>;
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isActive: Scalars['Boolean'];
  image3?: Maybe<Scalars['Upload']>;
  image4?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FlashSaleCreatePayload = {
  __typename?: 'FlashSaleCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flashSale?: Maybe<FlashSaleNode>;
};

export type FlashSaleDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FlashSaleDeletePayload = {
  __typename?: 'FlashSaleDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flashSale?: Maybe<FlashSaleNode>;
};

export type FlashSaleInput = {
  id: Scalars['String'];
};

export type FlashSaleNode = CustomizeInterface & {
  __typename?: 'FlashSaleNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  isActive: Scalars['Boolean'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['String']>;
  image2?: Maybe<Scalars['String']>;
  image3?: Maybe<Scalars['String']>;
  image4?: Maybe<Scalars['String']>;
  productFlashSale: ProductFlashSaleNodeConnection;
};


export type FlashSaleNodeProductFlashSaleArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type FlashSaleNodeConnection = {
  __typename?: 'FlashSaleNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FlashSaleNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FlashSaleNode` and its cursor. */
export type FlashSaleNodeEdge = {
  __typename?: 'FlashSaleNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FlashSaleNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FlashSaleUpdateInput = {
  site?: Maybe<Scalars['String']>;
  productList?: Maybe<Array<Maybe<ProductFlashSaleInput>>>;
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  image?: Maybe<Scalars['Upload']>;
  image2?: Maybe<Scalars['Upload']>;
  image3?: Maybe<Scalars['Upload']>;
  image4?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FlashSaleUpdatePayload = {
  __typename?: 'FlashSaleUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flashSale?: Maybe<FlashSaleNode>;
};

export type Flatpage = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type FlatpageError = {
  __typename?: 'FlatpageError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type FlatpageNode = Flatpage & {
  __typename?: 'FlatpageNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  itemCode?: Maybe<Scalars['String']>;
  title: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
  publishDate?: Maybe<Scalars['String']>;
};

export type FlatpageNodeConnection = {
  __typename?: 'FlatpageNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<FlatpageNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `FlatpageNode` and its cursor. */
export type FlatpageNodeEdge = {
  __typename?: 'FlatpageNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<FlatpageNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type FoodProcessingTypeCreateInput = {
  site?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  code: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FoodProcessingTypeCreatePayload = {
  __typename?: 'FoodProcessingTypeCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  foodProcessingType?: Maybe<FoodProcessingTypeNode>;
};

export type FoodProcessingTypeDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FoodProcessingTypeDeletePayload = {
  __typename?: 'FoodProcessingTypeDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  foodProcessingType?: Maybe<FoodProcessingTypeNode>;
};

export type FoodProcessingTypeNode = {
  __typename?: 'FoodProcessingTypeNode';
  id: Scalars['ID'];
  name: Scalars['String'];
  code: Scalars['String'];
  site: SiteNode;
  product?: Maybe<ProductForBuyNode>;
};

export type FoodProcessingTypeUpdateInput = {
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type FoodProcessingTypeUpdatePayload = {
  __typename?: 'FoodProcessingTypeUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  foodProcessingType?: Maybe<FoodProcessingTypeNode>;
};

export type ForgotPasswordInput = {
  email: Scalars['String'];
};

export type ForgotPasswordToken = {
  __typename?: 'ForgotPasswordToken';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type ForgotPasswordVerify = {
  __typename?: 'ForgotPasswordVerify';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type FunctionNode = {
  __typename?: 'FunctionNode';
  add?: Maybe<Scalars['Boolean']>;
  change?: Maybe<Scalars['Boolean']>;
  view?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  export?: Maybe<Scalars['Boolean']>;
  changeDiscount?: Maybe<Scalars['Boolean']>;
  addTopping?: Maybe<Scalars['Boolean']>;
  viewAll?: Maybe<Scalars['Boolean']>;
};

export type GalleryCreateInput = {
  description?: Maybe<Scalars['String']>;
  file: Scalars['Upload'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type GalleryCreatePayload = {
  __typename?: 'GalleryCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  gallery?: Maybe<GalleryNode>;
};

export type GalleryDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type GalleryDeletePayload = {
  __typename?: 'GalleryDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  gallery?: Maybe<GalleryNode>;
};

export type GalleryNode = CustomizeInterface & {
  __typename?: 'GalleryNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  user: UserProfile;
  name?: Maybe<Scalars['String']>;
  file: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
};

export type GalleryNodeConnection = {
  __typename?: 'GalleryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<GalleryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `GalleryNode` and its cursor. */
export type GalleryNodeEdge = {
  __typename?: 'GalleryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<GalleryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type GalleryUpdateInput = {
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type GalleryUpdatePayload = {
  __typename?: 'GalleryUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  gallery?: Maybe<GalleryNode>;
};


export type GetLalamoveShippingCost = {
  __typename?: 'GetLalamoveShippingCost';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  shippingCost?: Maybe<Scalars['Float']>;
  currency?: Maybe<Scalars['String']>;
};

export type GetLalamoveShippingCostAddress = {
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  longtitude: Scalars['String'];
  latitude: Scalars['String'];
};

export type GetLalamoveShippingCostInput = {
  totalAmount: Scalars['Float'];
  lalabag?: Maybe<Scalars['Boolean']>;
  cod?: Maybe<Scalars['Boolean']>;
  address: GetLalamoveShippingCostAddress;
};

export type GetLalamoveStatus = {
  __typename?: 'GetLalamoveStatus';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  delivery?: Maybe<DeliveryNode>;
};

export type GetLalamoveStatusInput = {
  deliveryId: Scalars['String'];
};

export type GetLocation = {
  __typename?: 'GetLocation';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<SiteConfigError>>>;
};

export type GoodReceiptDetailUpdateInput = {
  goodsReceiptDetailId: Scalars['String'];
  productID?: Maybe<Scalars['String']>;
  variantID?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Float']>;
  costPrices?: Maybe<Scalars['Float']>;
  note?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
};

export type GoodReceiptReturnDetailUpdateInput = {
  id: Scalars['String'];
  quantity?: Maybe<Scalars['Float']>;
  costPrices?: Maybe<Scalars['Float']>;
  note?: Maybe<Scalars['String']>;
};

export type GoodsReceiptCreate = {
  __typename?: 'GoodsReceiptCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceipt?: Maybe<GoodsReceiptNode>;
};

export type GoodsReceiptDelete = {
  __typename?: 'GoodsReceiptDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type GoodsReceiptDetailNode = {
  __typename?: 'GoodsReceiptDetailNode';
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  quantity?: Maybe<Scalars['Float']>;
  discount: Scalars['Float'];
  costPrices: Scalars['Float'];
  unit?: Maybe<UnitNode>;
  note?: Maybe<Scalars['String']>;
  product?: Maybe<ProductForBuyNode>;
  listUnit?: Maybe<Array<Maybe<ProductUnitNode>>>;
};

export type GoodsReceiptInput = {
  productVariant: Array<Maybe<ProductVariantReceiptInput>>;
  supplier?: Maybe<Scalars['String']>;
  warehouse?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  receiptType: ReceiptTypeInput;
  status: StatusInput;
  costPrices?: Maybe<Scalars['Float']>;
};

export type GoodsReceiptNode = CustomizeInterface & {
  __typename?: 'GoodsReceiptNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  warehouse: WarehouseNode;
  createdBy: UserProfile;
  site?: Maybe<SiteNode>;
  supplier?: Maybe<SupplierNode>;
  receiptDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  status?: Maybe<GoodsReceiptStatus>;
  itemCode?: Maybe<Scalars['String']>;
  discount: Scalars['Float'];
  costPrices: Scalars['Float'];
  order?: Maybe<OrderNode>;
  details?: Maybe<Array<Maybe<GoodsReceiptDetailNode>>>;
};

export type GoodsReceiptNodeConnection = {
  __typename?: 'GoodsReceiptNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<GoodsReceiptNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `GoodsReceiptNode` and its cursor. */
export type GoodsReceiptNodeEdge = {
  __typename?: 'GoodsReceiptNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<GoodsReceiptNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type GoodsReceiptReturnCreate = {
  __typename?: 'GoodsReceiptReturnCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceiptReturn?: Maybe<GoodsReceiptReturnNode>;
};

export type GoodsReceiptReturnDelete = {
  __typename?: 'GoodsReceiptReturnDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type GoodsReceiptReturnDetailNode = {
  __typename?: 'GoodsReceiptReturnDetailNode';
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  warehouse?: Maybe<WarehouseNode>;
  quantity: Scalars['Float'];
  discount: Scalars['Float'];
  costPrices: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  product?: Maybe<ProductForBuyNode>;
  quantityMax?: Maybe<Scalars['Float']>;
};

export type GoodsReceiptReturnInput = {
  productVariant: Array<Maybe<ProductVariantReceiptReturnInput>>;
  note?: Maybe<Scalars['String']>;
  status: StatusInput;
  costPrices?: Maybe<Scalars['Float']>;
  order: Scalars['String'];
};

export type GoodsReceiptReturnNode = CustomizeInterface & {
  __typename?: 'GoodsReceiptReturnNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  createdBy: UserProfile;
  site: SiteNode;
  receiptDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  status: GoodsReceiptReturnStatus;
  itemCode: Scalars['String'];
  discount: Scalars['Float'];
  costPrices: Scalars['Float'];
  order: OrderNode;
  details?: Maybe<Array<Maybe<GoodsReceiptReturnDetailNode>>>;
};

export type GoodsReceiptReturnNodeConnection = {
  __typename?: 'GoodsReceiptReturnNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<GoodsReceiptReturnNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `GoodsReceiptReturnNode` and its cursor. */
export type GoodsReceiptReturnNodeEdge = {
  __typename?: 'GoodsReceiptReturnNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<GoodsReceiptReturnNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum GoodsReceiptReturnStatus {
  /** draft */
  Draft = 'DRAFT',
  /** completed */
  Completed = 'COMPLETED',
  /** cancelled */
  Cancelled = 'CANCELLED',
  /** transferring */
  Transferring = 'TRANSFERRING'
}

export type GoodsReceiptReturnUpdate = {
  __typename?: 'GoodsReceiptReturnUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceiptReturn?: Maybe<GoodsReceiptReturnNode>;
};

export type GoodsReceiptReturnUpdateInput = {
  id: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  status?: Maybe<StatusInput>;
  costPrices?: Maybe<Scalars['Float']>;
  goodsReceiptRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  goodsReceiptUpdate?: Maybe<Array<Maybe<GoodReceiptReturnDetailUpdateInput>>>;
};

/** An enumeration. */
export enum GoodsReceiptStatus {
  /** draft */
  Draft = 'DRAFT',
  /** completed */
  Completed = 'COMPLETED',
  /** cancelled */
  Cancelled = 'CANCELLED',
  /** transferring */
  Transferring = 'TRANSFERRING'
}

export type GoodsReceiptTransferCreate = {
  __typename?: 'GoodsReceiptTransferCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceiptTransfer?: Maybe<GoodsReceiptTransferNode>;
};

export type GoodsReceiptTransferDelete = {
  __typename?: 'GoodsReceiptTransferDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type GoodsReceiptTransferDetailNode = {
  __typename?: 'GoodsReceiptTransferDetailNode';
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  unit?: Maybe<UnitNode>;
  note?: Maybe<Scalars['String']>;
  costPrices: Scalars['Float'];
  quantityTransfer?: Maybe<Scalars['Float']>;
  quantityReceive?: Maybe<Scalars['Float']>;
  product?: Maybe<ProductForBuyNode>;
  warehouse?: Maybe<Array<Maybe<GoodsReceiptTransferDetailWarehouse>>>;
};

export type GoodsReceiptTransferDetailUpdateInput = {
  id: Scalars['String'];
  quantity: Scalars['Float'];
  costPrices?: Maybe<Scalars['Float']>;
};

export type GoodsReceiptTransferDetailWarehouse = {
  __typename?: 'GoodsReceiptTransferDetailWarehouse';
  quantity?: Maybe<Scalars['Float']>;
  unitId?: Maybe<Scalars['String']>;
  unitName?: Maybe<Scalars['String']>;
  isMain?: Maybe<Scalars['Boolean']>;
  quantityAvailable?: Maybe<Scalars['Float']>;
};

export type GoodsReceiptTransferInput = {
  warehouseTransfer: Scalars['String'];
  warehouseReceive: Scalars['String'];
  productVariant: Array<Maybe<ProductVariantReceiptInput>>;
  noteTransfer?: Maybe<Scalars['String']>;
  status: StatusInput;
};

export type GoodsReceiptTransferNode = CustomizeInterface & {
  __typename?: 'GoodsReceiptTransferNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  warehouseTransfer: WarehouseNode;
  warehouseReceive: WarehouseNode;
  transferer: UserProfile;
  receiver?: Maybe<UserProfile>;
  site: SiteNode;
  itemCode: Scalars['String'];
  status?: Maybe<GoodsReceiptTransferStatus>;
  totalQuantityTransfer?: Maybe<Scalars['Float']>;
  totalQuantityReceive?: Maybe<Scalars['Float']>;
  dateTransfer?: Maybe<Scalars['DateTime']>;
  dateReceive?: Maybe<Scalars['DateTime']>;
  noteTransfer?: Maybe<Scalars['String']>;
  noteReceive?: Maybe<Scalars['String']>;
  costPrices: Scalars['Float'];
  details?: Maybe<Array<Maybe<GoodsReceiptTransferDetailNode>>>;
};

export type GoodsReceiptTransferNodeConnection = {
  __typename?: 'GoodsReceiptTransferNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<GoodsReceiptTransferNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `GoodsReceiptTransferNode` and its cursor. */
export type GoodsReceiptTransferNodeEdge = {
  __typename?: 'GoodsReceiptTransferNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<GoodsReceiptTransferNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum GoodsReceiptTransferStatus {
  /** draft */
  Draft = 'DRAFT',
  /** completed */
  Completed = 'COMPLETED',
  /** cancelled */
  Cancelled = 'CANCELLED',
  /** transferring */
  Transferring = 'TRANSFERRING'
}

export type GoodsReceiptTransferUpdate = {
  __typename?: 'GoodsReceiptTransferUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceiptTransfer?: Maybe<GoodsReceiptTransferNode>;
};

export type GoodsReceiptTransferUpdateInput = {
  id: Scalars['String'];
  warehouseReceive?: Maybe<Scalars['String']>;
  productVariant?: Maybe<Array<Maybe<ProductVariantReceiptInput>>>;
  noteTransfer?: Maybe<Scalars['String']>;
  noteReceive?: Maybe<Scalars['String']>;
  status: StatusInput;
  goodsReceiptRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  goodsReceiptUpdateTranfer?: Maybe<Array<Maybe<GoodsReceiptTransferDetailUpdateInput>>>;
  goodsReceiptUpdateReceive?: Maybe<Array<Maybe<GoodsReceiptTransferDetailUpdateInput>>>;
};

export type GoodsReceiptUpdate = {
  __typename?: 'GoodsReceiptUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  goodsReceipt?: Maybe<GoodsReceiptNode>;
};

export type GoodsReceiptUpdateInput = {
  goodsReceiptId: Scalars['String'];
  productVariant?: Maybe<Array<Maybe<ProductVariantReceiptInput>>>;
  supplier?: Maybe<Scalars['String']>;
  warehouse?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  status?: Maybe<StatusInput>;
  costPrices?: Maybe<Scalars['Float']>;
  goodsReceiptRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  goodsReceiptUpdate?: Maybe<Array<Maybe<GoodReceiptDetailUpdateInput>>>;
};

export type GroupByDay = {
  __typename?: 'GroupByDay';
  date?: Maybe<Scalars['Date']>;
  amount?: Maybe<Scalars['Float']>;
};

export type GroupByMonth = {
  __typename?: 'GroupByMonth';
  month?: Maybe<Scalars['Int']>;
  year?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Float']>;
};

export type GroupByQuarter = {
  __typename?: 'GroupByQuarter';
  quarter?: Maybe<Scalars['Int']>;
  year?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Float']>;
};

export type GroupByWeek = {
  __typename?: 'GroupByWeek';
  firstDate?: Maybe<Scalars['Date']>;
  amount?: Maybe<Scalars['Float']>;
};

export type GroupByYear = {
  __typename?: 'GroupByYear';
  year?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Float']>;
};

export type GroupPermissionCreate = {
  __typename?: 'GroupPermissionCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
};

export type GroupPermissionDelete = {
  __typename?: 'GroupPermissionDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
};

export type GroupPermissionInput = {
  name: Scalars['String'];
  permission?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type GuestOrderCreate = {
  __typename?: 'GuestOrderCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  order?: Maybe<OrderNode>;
};

export type HashtagCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HashtagCreatePayload = {
  __typename?: 'HashtagCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  hashtag?: Maybe<HashtagNode>;
};

export type HashtagDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HashtagDeletePayload = {
  __typename?: 'HashtagDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  hashtag?: Maybe<HashtagNode>;
};

export type HashtagNode = CustomizeInterface & {
  __typename?: 'HashtagNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  news: Array<NewsHashtagNode>;
};

export type HashtagNodeConnection = {
  __typename?: 'HashtagNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<HashtagNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `HashtagNode` and its cursor. */
export type HashtagNodeEdge = {
  __typename?: 'HashtagNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<HashtagNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type HashtagUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HashtagUpdatePayload = {
  __typename?: 'HashtagUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  hashtag?: Maybe<HashtagNode>;
};

export type HeaderNewsCreateInput = {
  title: Scalars['String'];
  beginTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HeaderNewsCreatePayload = {
  __typename?: 'HeaderNewsCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  headerNews?: Maybe<HeaderNewsNode>;
};

export type HeaderNewsDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HeaderNewsDeletePayload = {
  __typename?: 'HeaderNewsDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  headerNews?: Maybe<HeaderNewsNode>;
};

export type HeaderNewsNode = CustomizeInterface & {
  __typename?: 'HeaderNewsNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  title: Scalars['String'];
  beginTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
};

export type HeaderNewsNodeConnection = {
  __typename?: 'HeaderNewsNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<HeaderNewsNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `HeaderNewsNode` and its cursor. */
export type HeaderNewsNodeEdge = {
  __typename?: 'HeaderNewsNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<HeaderNewsNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type HeaderNewsUpdateInput = {
  title?: Maybe<Scalars['String']>;
  beginTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type HeaderNewsUpdatePayload = {
  __typename?: 'HeaderNewsUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  headerNews?: Maybe<HeaderNewsNode>;
};

export type IncomingPayment = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type IncomingPaymentAddressNode = {
  __typename?: 'IncomingPaymentAddressNode';
  id: Scalars['ID'];
  address?: Maybe<UserAddressNode>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type IncomingPaymentDetailNode = {
  __typename?: 'IncomingPaymentDetailNode';
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  productName: Scalars['String'];
  quantity: Scalars['Int'];
  unitPrice: Scalars['Float'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  isReviewed: Scalars['Boolean'];
};

export type IncomingPaymentError = {
  __typename?: 'IncomingPaymentError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type IncomingPaymentHistoryNode = {
  __typename?: 'IncomingPaymentHistoryNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  status?: Maybe<IncomingPaymentHistoryStatus>;
  description?: Maybe<Scalars['String']>;
  user?: Maybe<UserProfile>;
};

/** An enumeration. */
export enum IncomingPaymentHistoryStatus {
  /** Comment */
  Comment = 'COMMENT',
  /** Action */
  Action = 'ACTION'
}

export type IncomingPaymentNode = IncomingPayment & {
  __typename?: 'IncomingPaymentNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  createdBy?: Maybe<UserProfile>;
  totalAmount: Scalars['Float'];
  details?: Maybe<Array<Maybe<IncomingPaymentDetailNode>>>;
  incomingPaymentAddress?: Maybe<IncomingPaymentAddressNode>;
  incomingPaymentHistory?: Maybe<Array<Maybe<IncomingPaymentHistoryNode>>>;
};

export type IncomingPaymentNodeConnection = {
  __typename?: 'IncomingPaymentNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<IncomingPaymentNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `IncomingPaymentNode` and its cursor. */
export type IncomingPaymentNodeEdge = {
  __typename?: 'IncomingPaymentNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<IncomingPaymentNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum IncomingPaymentPaymentStatusInput {
  Paid = 'PAID',
  WaitForPay = 'WAIT_FOR_PAY',
  Cancelled = 'CANCELLED',
  Fulfilled = 'FULFILLED',
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  Unfulfilled = 'UNFULFILLED',
  ReadyToCapture = 'READY_TO_CAPTURE',
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export type IncomingPaymentProductInput = {
  productId?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export enum IncomingStatusInput {
  Shipping = 'SHIPPING',
  Cancelled = 'CANCELLED',
  Delivered = 'DELIVERED',
  Pending = 'PENDING'
}

export type InventoryCountCreate = {
  __typename?: 'InventoryCountCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  inventoryCount?: Maybe<InventoryCountNode>;
};

export type InventoryCountDetailNode = {
  __typename?: 'InventoryCountDetailNode';
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  unit?: Maybe<UnitNode>;
  actualQuantity: Scalars['Float'];
  stockQuantity: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  product?: Maybe<ProductForBuyNode>;
  warehouse?: Maybe<Array<Maybe<GoodsReceiptTransferDetailWarehouse>>>;
};

export type InventoryCountInput = {
  productVariant: Array<Maybe<InventoryCountProductInput>>;
  warehouse: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  status: StatusInput;
};

export type InventoryCountNode = CustomizeInterface & {
  __typename?: 'InventoryCountNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  warehouse: WarehouseNode;
  createdBy: UserProfile;
  site: SiteNode;
  receiptDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  status?: Maybe<InventoryCountStatus>;
  itemCode?: Maybe<Scalars['String']>;
  completedBy?: Maybe<UserProfile>;
  details?: Maybe<Array<Maybe<InventoryCountDetailNode>>>;
};

export type InventoryCountNodeConnection = {
  __typename?: 'InventoryCountNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<InventoryCountNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `InventoryCountNode` and its cursor. */
export type InventoryCountNodeEdge = {
  __typename?: 'InventoryCountNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<InventoryCountNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type InventoryCountProductInput = {
  productID: Scalars['String'];
  variantID?: Maybe<Scalars['String']>;
  actualQuantity: Scalars['Float'];
  stockQuantity?: Maybe<Scalars['Float']>;
  note?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
};

export type InventoryCountProductUpdateInput = {
  id: Scalars['String'];
  actualQuantity: Scalars['Float'];
  stockQuantity?: Maybe<Scalars['Float']>;
  note?: Maybe<Scalars['String']>;
};

/** An enumeration. */
export enum InventoryCountStatus {
  /** draft */
  Draft = 'DRAFT',
  /** completed */
  Completed = 'COMPLETED',
  /** cancelled */
  Cancelled = 'CANCELLED',
  /** transferring */
  Transferring = 'TRANSFERRING'
}

export type InventoryCountUpdate = {
  __typename?: 'InventoryCountUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
  inventoryCount?: Maybe<InventoryCountNode>;
};

export type InventoryCountUpdateInput = {
  id: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  status: StatusInput;
  productVariant?: Maybe<Array<Maybe<InventoryCountProductInput>>>;
  productVariantUpdate?: Maybe<Array<Maybe<InventoryCountProductUpdateInput>>>;
  productVariantRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type InventoryNode = CustomizeInterface & {
  __typename?: 'InventoryNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  warehouse: WarehouseNode;
  variant?: Maybe<VariantNode>;
  quantity?: Maybe<Scalars['Float']>;
  site?: Maybe<SiteNode>;
  unit?: Maybe<UnitNode>;
  isVariant: Scalars['Boolean'];
  costPrices?: Maybe<Scalars['Float']>;
  product?: Maybe<ProductForBuyNode>;
};

export type InventoryNodeConnection = {
  __typename?: 'InventoryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<InventoryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `InventoryNode` and its cursor. */
export type InventoryNodeEdge = {
  __typename?: 'InventoryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<InventoryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type Invoice = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type InvoiceAddressNode = {
  __typename?: 'InvoiceAddressNode';
  id: Scalars['ID'];
  address?: Maybe<UserAddressNode>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type InvoiceDetailNode = {
  __typename?: 'InvoiceDetailNode';
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  productName: Scalars['String'];
  quantity: Scalars['Int'];
  unitPrice: Scalars['Float'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  isReviewed: Scalars['Boolean'];
};

export type InvoiceError = {
  __typename?: 'InvoiceError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type InvoiceHistoryNode = {
  __typename?: 'InvoiceHistoryNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  status?: Maybe<InvoiceHistoryStatus>;
  description?: Maybe<Scalars['String']>;
  user?: Maybe<UserProfile>;
};

/** An enumeration. */
export enum InvoiceHistoryStatus {
  /** Comment */
  Comment = 'COMMENT',
  /** Action */
  Action = 'ACTION'
}

export type InvoiceNode = Invoice & {
  __typename?: 'InvoiceNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  createdBy?: Maybe<UserProfile>;
  totalAmount: Scalars['Float'];
  details?: Maybe<Array<Maybe<InvoiceDetailNode>>>;
  invoiceAddress?: Maybe<InvoiceAddressNode>;
  invoiceHistory?: Maybe<Array<Maybe<InvoiceHistoryNode>>>;
};

export type InvoiceNodeConnection = {
  __typename?: 'InvoiceNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<InvoiceNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `InvoiceNode` and its cursor. */
export type InvoiceNodeEdge = {
  __typename?: 'InvoiceNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<InvoiceNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum InvoicePaymentStatusInput {
  Paid = 'PAID',
  WaitForPay = 'WAIT_FOR_PAY',
  Cancelled = 'CANCELLED',
  Fulfilled = 'FULFILLED',
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  Unfulfilled = 'UNFULFILLED',
  ReadyToCapture = 'READY_TO_CAPTURE',
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export type InvoiceProductInput = {
  productId?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export enum InvoiceStatusInput {
  Shipping = 'SHIPPING',
  Cancelled = 'CANCELLED',
  Delivered = 'DELIVERED',
  Pending = 'PENDING'
}

export type KiotVietInfoCreateInput = {
  clientId: Scalars['String'];
  clientSecret: Scalars['String'];
  retailer: Scalars['String'];
  token: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type KiotVietInfoCreatePayload = {
  __typename?: 'KiotVietInfoCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  kiotViet?: Maybe<KiotVietInformationNode>;
};

export type KiotVietInfoUpdateInput = {
  clientId?: Maybe<Scalars['String']>;
  clientSecret?: Maybe<Scalars['String']>;
  retailer?: Maybe<Scalars['String']>;
  token?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type KiotVietInfoUpdatePayload = {
  __typename?: 'KiotVietInfoUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  kiotViet?: Maybe<KiotVietInformationNode>;
};

export type KiotVietInformationNode = {
  __typename?: 'KiotVietInformationNode';
  id: Scalars['ID'];
  clientId: Scalars['String'];
  clientSecret: Scalars['String'];
  retailer: Scalars['String'];
  token: Scalars['String'];
};

export type KitchenOrderAdminUpdate = {
  __typename?: 'KitchenOrderAdminUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  kitchenOrder?: Maybe<KitchenOrderNode>;
};

export type KitchenOrderChefUpdate = {
  __typename?: 'KitchenOrderChefUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  kitchenOrder?: Maybe<KitchenOrderNode>;
};

export type KitchenOrderChefUpdateInput = {
  kitchenOrderId?: Maybe<Scalars['String']>;
  kitchenOrderDetailId?: Maybe<Scalars['String']>;
  status: KitchenOrderStatusInput;
  cancelNote?: Maybe<Scalars['String']>;
  chefId?: Maybe<Scalars['String']>;
};

export type KitchenOrderDetailNode = CustomizeInterface & {
  __typename?: 'KitchenOrderDetailNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  chef?: Maybe<UserProfile>;
  status: KitchenOrderDetailStatus;
  product?: Maybe<ProductForBuyNode>;
  foodProcessingType?: Maybe<FoodProcessingTypeNode>;
  quantity: Scalars['Float'];
  numberOfFood: Scalars['Float'];
  numberOfFoodCompleted: Scalars['Float'];
  cancelNote?: Maybe<Scalars['String']>;
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
};

export type KitchenOrderDetailNodeConnection = {
  __typename?: 'KitchenOrderDetailNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<KitchenOrderDetailNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `KitchenOrderDetailNode` and its cursor. */
export type KitchenOrderDetailNodeEdge = {
  __typename?: 'KitchenOrderDetailNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<KitchenOrderDetailNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum KitchenOrderDetailStatus {
  /** Unallocated */
  Unallocated = 'UNALLOCATED',
  /** Pending */
  Pending = 'PENDING',
  /** Received */
  Received = 'RECEIVED',
  /** Processing */
  Processing = 'PROCESSING',
  /** Partially_finished */
  PartiallyFinished = 'PARTIALLY_FINISHED',
  /** Finished */
  Finished = 'FINISHED',
  /** Cancelled */
  Cancelled = 'CANCELLED'
}

export type KitchenOrderInput = {
  kitchenOrderId: Scalars['String'];
  quantity?: Maybe<Scalars['Float']>;
  isPriority?: Maybe<Scalars['Boolean']>;
  isAppointment?: Maybe<Scalars['Boolean']>;
  timeAppointment?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  cancelNote?: Maybe<Scalars['String']>;
  productUpdate?: Maybe<Array<Maybe<KitchenProductUpdateInput>>>;
  productAdd?: Maybe<Array<Maybe<KitchenProductInput>>>;
  productRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  status?: Maybe<KitchenOrderStatusInput>;
};

export type KitchenOrderNode = CustomizeInterface & {
  __typename?: 'KitchenOrderNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  code: Scalars['String'];
  order?: Maybe<OrderNode>;
  customer: UserProfile;
  quantity: Scalars['Float'];
  numberOfFood: Scalars['Float'];
  numberOfFoodCompleted: Scalars['Float'];
  isPriority: Scalars['Boolean'];
  isAppointment: Scalars['Boolean'];
  timeAppointment?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  cancelNote?: Maybe<Scalars['String']>;
  site: SiteNode;
  status?: Maybe<KitchenOrderStatus>;
  details?: Maybe<KitchenOrderDetailNodeConnection>;
};


export type KitchenOrderNodeDetailsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  status?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type KitchenOrderNodeConnection = {
  __typename?: 'KitchenOrderNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<KitchenOrderNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `KitchenOrderNode` and its cursor. */
export type KitchenOrderNodeEdge = {
  __typename?: 'KitchenOrderNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<KitchenOrderNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum KitchenOrderStatus {
  /** Unallocated */
  Unallocated = 'UNALLOCATED',
  /** Pending */
  Pending = 'PENDING',
  /** Received */
  Received = 'RECEIVED',
  /** Processing */
  Processing = 'PROCESSING',
  /** Partially_finished */
  PartiallyFinished = 'PARTIALLY_FINISHED',
  /** Finished */
  Finished = 'FINISHED',
  /** Cancelled */
  Cancelled = 'CANCELLED'
}

export enum KitchenOrderStatusInput {
  Received = 'received',
  Processing = 'processing',
  PartiallyFinished = 'partially_finished',
  Finished = 'finished',
  Cancelled = 'cancelled'
}

export type KitchenProductInput = {
  productId: Scalars['String'];
  numberOfFood?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Float']>;
  foodProcessingTypeId?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
};

export type KitchenProductUpdateInput = {
  id: Scalars['String'];
  productId?: Maybe<Scalars['String']>;
  numberOfFood?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Float']>;
  foodProcessingTypeId?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
};

export type LoadData = {
  __typename?: 'LoadData';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type LoadPaymentMethod = {
  __typename?: 'LoadPaymentMethod';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type LoadPermission = {
  __typename?: 'LoadPermission';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type LocationDelete = {
  __typename?: 'LocationDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<SiteConfigError>>>;
};

export type LocationSort = {
  __typename?: 'LocationSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<SiteConfigError>>>;
};

export enum LoginStatusInput {
  System = 'SYSTEM',
  Facebook = 'FACEBOOK',
  Zalo = 'ZALO',
  Google = 'GOOGLE'
}

export type MigrateData = {
  __typename?: 'MigrateData';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  partyInfoCreate?: Maybe<PartyInforCreatePayload>;
  partyInfoUpdate?: Maybe<PartyInforUpdatePayload>;
  partyInfoDelete?: Maybe<PartyInforDeletePayload>;
  permissionCreate?: Maybe<PermissionCreate>;
  permissionUpdate?: Maybe<PermissionUpdate>;
  permissionDelete?: Maybe<PermissionDelete>;
  userPermissionUpdate?: Maybe<UserPermissionUpdate>;
  groupPermissionCreate?: Maybe<GroupPermissionCreate>;
  groupPermissionDelete?: Maybe<GroupPermissionDelete>;
  authLoadPermission?: Maybe<LoadPermission>;
  deliveryMethodCreate?: Maybe<DeliveryMethodCreate>;
  deliveryMethodUpdate?: Maybe<DeliveryMethodUpdate>;
  deliveryMethodDelete?: Maybe<DeliveryMethodDelete>;
  siteDeliveryMethodUpdate?: Maybe<SiteDeliveryMethodUpdate>;
  kitchenOrderAdminUpdate?: Maybe<KitchenOrderAdminUpdate>;
  kitchenOrderChefUpdate?: Maybe<KitchenOrderChefUpdate>;
  foodProcessingTypeCreate?: Maybe<FoodProcessingTypeCreatePayload>;
  foodProcessingTypeUpdate?: Maybe<FoodProcessingTypeUpdatePayload>;
  foodProcessingTypeDelete?: Maybe<FoodProcessingTypeDeletePayload>;
  siteConfigUpdate?: Maybe<SiteConfigUpdate>;
  siteConfigShip60Update?: Maybe<SiteConfigShip60Update>;
  locationDelete?: Maybe<LocationDelete>;
  getLocation?: Maybe<GetLocation>;
  locationSort?: Maybe<LocationSort>;
  flashSaleCreate?: Maybe<FlashSaleCreatePayload>;
  flashSaleUpdate?: Maybe<FlashSaleUpdatePayload>;
  flashSaleDelete?: Maybe<FlashSaleDeletePayload>;
  flashSaleActivate?: Maybe<FlashSaleActivate>;
  promotionCreate?: Maybe<PromotionCreatePayload>;
  promotionUpdate?: Maybe<PromotionUpdatePayload>;
  promotionDelete?: Maybe<PromotionDeletePayload>;
  promotionActivate?: Maybe<PromotionActivate>;
  promotionCodeCreate?: Maybe<PromotionCodeCreatePayload>;
  promotionCodeUpdate?: Maybe<PromotionCodeUpdatePayload>;
  promotionCodeDelete?: Maybe<PromotionCodeDeletePayload>;
  promotionCodeActivate?: Maybe<PromotionCodeActivate>;
  ourPartnersCreate?: Maybe<OurPartnerCreatePayload>;
  ourPartnersUpdate?: Maybe<OurPartnerUpdatePayload>;
  ourPartnersDelete?: Maybe<OurPartnerDeletePayload>;
  ourPartnersSort?: Maybe<OurPartnerSort>;
  customerReviewCreate?: Maybe<CustomerReviewCreatePayload>;
  customerReviewUpdate?: Maybe<CustomerReviewUpdatePayload>;
  customerReviewDelete?: Maybe<CustomerReviewDeletePayload>;
  customerReviewSort?: Maybe<CustomerReviewSort>;
  outgoingPaymentAdminCreate?: Maybe<AdminOutgoingPaymentCreate>;
  outgoingPaymentAdminDelete?: Maybe<AdminOutgoingPaymentDelete>;
  outgoingPaymentDetailAdminDelete?: Maybe<AdminOutgoingPaymentProductDelete>;
  outgoingPaymentAdminUpdate?: Maybe<AdminOutgoingPaymentUpdate>;
  incomingPaymentAdminCreate?: Maybe<AdminIncomingPaymentCreate>;
  incomingPaymentAdminDelete?: Maybe<AdminIncomingPaymentDelete>;
  incomingPaymentDetailAdminDelete?: Maybe<AdminIncomingPaymentProductDelete>;
  incomingPaymentAdminUpdate?: Maybe<AdminIncomingPaymentUpdate>;
  invoiceAdminCreate?: Maybe<AdminInvoiceCreate>;
  invoiceAdminDelete?: Maybe<AdminInvoiceDelete>;
  invoiceDetailAdminDelete?: Maybe<AdminInvoiceProductDelete>;
  invoiceAdminUpdate?: Maybe<AdminInvoiceUpdate>;
  deliveryAdminBatchCreate?: Maybe<AdminDeliveryBatchCreate>;
  deliveryAdminCreate?: Maybe<AdminDeliveryCreate>;
  deliveryAdminUpdate?: Maybe<AdminDeliveryUpdate>;
  deliveryAdminDelete?: Maybe<AdminDeliveryDelete>;
  deliveryDetailAdminDelete?: Maybe<AdminDeliveryProductDelete>;
  kiotvietInformationCreate?: Maybe<KiotVietInfoCreatePayload>;
  kiotvietInformationUpdate?: Maybe<KiotVietInfoUpdatePayload>;
  updateDeliveryStatus?: Maybe<UserUpdateDeliveryStatus>;
  deliveryTrackingCreate?: Maybe<DeliveryTrackingCreate>;
  deliveryTrackingDelete?: Maybe<DeliveryTrackingDelete>;
  deliveryReviewCreate?: Maybe<DeliveryReviewCreate>;
  deliveryReviewDelete?: Maybe<DeliveryReviewDelete>;
  deliveryBranchCreate?: Maybe<DeliveryBranchCreatePayload>;
  deliveryBranchUpdate?: Maybe<DeliveryBranchUpdatePayload>;
  deliveryBranchDelete?: Maybe<DeliveryBranchDeletePayload>;
  deliveryHistoryDelete?: Maybe<DeliveryHistoryDeletePayload>;
  collectionCreate?: Maybe<CollectionCreatePayload>;
  collectionUpdate?: Maybe<CollectionUpdatePayload>;
  collectionDelete?: Maybe<CollectionDeletePayload>;
  collectionSort?: Maybe<CollectionSort>;
  contactUsCreate?: Maybe<ContactUsCreate>;
  contactUsUpdate?: Maybe<ContactUsUpdate>;
  contactUsDelete?: Maybe<ContactUsDelete>;
  feedbackCreate?: Maybe<FeedbackCreatePayload>;
  feedbackUpdate?: Maybe<FeedbackUpdatePayload>;
  feedbackSort?: Maybe<FeedbackSort>;
  feedbackDelete?: Maybe<FeedbackDeletePayload>;
  productFeedbackCreate?: Maybe<ProductFeedbackCreatePayload>;
  productFeedbackUpdate?: Maybe<ProductFeedbackUpdatePayload>;
  productFeedbackSort?: Maybe<ProductFeedbackSort>;
  productFeedbackDelete?: Maybe<ProductFeedbackDeletePayload>;
  warehouseCreate?: Maybe<WarehouseCreatePayload>;
  warehouseUpdate?: Maybe<WarehouseUpdatePayload>;
  warehouseDelete?: Maybe<WarehouseDelete>;
  goodsReceiptCreate?: Maybe<GoodsReceiptCreate>;
  goodsReceiptUpdate?: Maybe<GoodsReceiptUpdate>;
  goodsReceiptDelete?: Maybe<GoodsReceiptDelete>;
  goodsReceiptReturnCreate?: Maybe<GoodsReceiptReturnCreate>;
  goodsReceiptReturnUpdate?: Maybe<GoodsReceiptReturnUpdate>;
  goodsReceiptReturnDelete?: Maybe<GoodsReceiptReturnDelete>;
  goodsReceiptTransferCreate?: Maybe<GoodsReceiptTransferCreate>;
  goodsReceiptTransferUpdate?: Maybe<GoodsReceiptTransferUpdate>;
  goodsReceiptTransferDelete?: Maybe<GoodsReceiptTransferDelete>;
  inventoryCountCreate?: Maybe<InventoryCountCreate>;
  inventoryCountUpdate?: Maybe<InventoryCountUpdate>;
  unitCreate?: Maybe<UnitCreatePayload>;
  unitUpdate?: Maybe<UnitUpdatePayload>;
  unitDelete?: Maybe<UnitDelete>;
  branchWarehouseUpdate?: Maybe<BranchWarehouseUpdate>;
  branchWarehouseSort?: Maybe<BranchWarehouseSort>;
  FAQsCategoryCreate?: Maybe<FaQsCategoryCreatePayload>;
  FAQsCategoryUpdate?: Maybe<FaQsCategoryUpdatePayload>;
  FAQsCategoryDelete?: Maybe<FaQsCategoryDeletePayload>;
  FAQsCreate?: Maybe<FaQsCreatePayload>;
  FAQsUpdate?: Maybe<FaQsUpdatePayload>;
  FAQsDelete?: Maybe<FaQsDeletePayload>;
  bannerGroupCreate?: Maybe<BannerGroupCreatePayload>;
  bannerGroupUpdate?: Maybe<BannerGroupUpdatePayload>;
  bannerGroupDelete?: Maybe<BannerGroupDeletePayload>;
  bannerCreate?: Maybe<BannerCreatePayload>;
  bannerUpdate?: Maybe<BannerUpdatePayload>;
  bannerDelete?: Maybe<BannerDeletePayload>;
  bannerSort?: Maybe<BannerSort>;
  bannerGroupBannerCreate?: Maybe<BannerGroupBannerCreatePayload>;
  bannerGroupBannerUpdate?: Maybe<BannerGroupBannerUpdatePayload>;
  bannerGroupBannerDetele?: Maybe<BannerGroupBannerDeletePayload>;
  galleryCreate?: Maybe<GalleryCreatePayload>;
  galleryUpdate?: Maybe<GalleryUpdatePayload>;
  galleryDelete?: Maybe<GalleryDeletePayload>;
  reviewForProduct?: Maybe<UserReviewCreate>;
  reviewUserEdit?: Maybe<UserEditReview>;
  reviewDelete?: Maybe<DeleteReviewPayload>;
  reviewUserActive?: Maybe<UserActiveReview>;
  newsCateCreate?: Maybe<NewsCateCreatePayload>;
  newsCateUpdate?: Maybe<NewsCateUpdatePayload>;
  newsCateDelete?: Maybe<NewsCateDeletePayload>;
  newsCreate?: Maybe<NewsCreatePayload>;
  newsUpdate?: Maybe<NewsUpdatePayload>;
  newsDelete?: Maybe<NewsDeletePayload>;
  newsCommentsCreate?: Maybe<NewsCommentsCreatePayload>;
  newsCommentsUpdate?: Maybe<NewsCommentsUpdatePayload>;
  newsCommentsDelete?: Maybe<NewsCommentsDeletePayload>;
  newsHashtagCreate?: Maybe<HashtagCreatePayload>;
  newsHashtagUpdate?: Maybe<HashtagUpdatePayload>;
  newsHashtagDelete?: Maybe<HashtagDeletePayload>;
  headerNewsCreate?: Maybe<HeaderNewsCreatePayload>;
  headerNewsUpdate?: Maybe<HeaderNewsUpdatePayload>;
  headerNewsDelete?: Maybe<HeaderNewsDeletePayload>;
  newsLikeAction?: Maybe<NewsLikeAction>;
  flatpageCreate?: Maybe<CreateFlatpagePayload>;
  flatpageUpdate?: Maybe<UpdateFlatpagePayload>;
  flatpageDelete?: Maybe<DeleteFlatpagePayload>;
  cartAdd?: Maybe<AddToCart>;
  cartUpdate?: Maybe<UpdateCart>;
  cartDelete?: Maybe<DeleteCart>;
  orderAdminCreate?: Maybe<AdminOrderCreate>;
  orderUserCreate?: Maybe<UserOrderCreate>;
  orderGuestCreate?: Maybe<GuestOrderCreate>;
  orderAdminUpdate?: Maybe<OrderUpdate>;
  orderCustomerUpdate?: Maybe<OrderCustomerUpdate>;
  orderAdminDelete?: Maybe<OrderDelete>;
  orderWarehouseUpdate?: Maybe<AdminOrderWarehouseUpdate>;
  otherRevenueCreate?: Maybe<OtherRevenueCreatePayload>;
  otherRevenueUpdate?: Maybe<OtherRevenueUpdatePayload>;
  otherRevenueDelete?: Maybe<OtherRevenueDeletePayload>;
  orderEmptyDelete?: Maybe<OrderEmptyDelete>;
  getLalamoveShippingCost?: Maybe<GetLalamoveShippingCost>;
  getLalamoveStatus?: Maybe<GetLalamoveStatus>;
  countryCreate?: Maybe<CountryCreatePayload>;
  countryUpdate?: Maybe<CountryUpdatePayload>;
  countryDelete?: Maybe<CountryDeletePayload>;
  userTopUp?: Maybe<UserTopUp>;
  topUpPackageCreate?: Maybe<TopUpPackageCreatePayload>;
  topUpPackageUpdate?: Maybe<TopUpPackageUpdatePayload>;
  topUpPackageDelete?: Maybe<TopUpPackageDeletePayload>;
  paymentMethodCreate?: Maybe<PaymentMethodCreatePayload>;
  paymentMethodUpdate?: Maybe<PaymentMethodUpdatePayload>;
  paymentMethodDelete?: Maybe<PaymentMethodDeletePayload>;
  authLoadPaymentMethod?: Maybe<LoadPaymentMethod>;
  productForBuyCreate?: Maybe<ProductForBuyCreatePayload>;
  productForBetCreate?: Maybe<ProductForBetCreatePayload>;
  productForBuyUpdate?: Maybe<ProductForBuyUpdatePayload>;
  productForBetUpdate?: Maybe<ProductForBetUpdatePayload>;
  productForBetTransaction?: Maybe<UserBetTransaction>;
  productForBetResult?: Maybe<ProductForBetResult>;
  productForBuyDelete?: Maybe<ProductForBuyDeletePayload>;
  attributeCreate?: Maybe<AttributeCreatePayload>;
  attributeUpdate?: Maybe<AttributeUpdatePayload>;
  attributeDelete?: Maybe<AttributeDeletePayload>;
  variantCreate?: Maybe<VariantCreate>;
  variantUpdate?: Maybe<VariantUpdate>;
  variantDelete?: Maybe<VariantDelete>;
  brandCreate?: Maybe<BrandCreatePayload>;
  brandUpdate?: Maybe<BrandUpdatePayload>;
  brandDelete?: Maybe<BrandDeletePayload>;
  productComboCreate?: Maybe<ProductComboCreate>;
  productComboUpdate?: Maybe<ProductComboUpdate>;
  productComboDelete?: Maybe<ProductComboDelete>;
  productTagCreate?: Maybe<ProductTagCreatePayload>;
  productTagUpdate?: Maybe<ProductTagUpdatePayload>;
  productTagDelete?: Maybe<ProductTagDeletePayload>;
  productDescriptionCreate?: Maybe<ProductDescriptionCreatePayload>;
  productDescriptionUpdate?: Maybe<ProductDescriptionUpdatePayload>;
  productDescriptionDelete?: Maybe<ProductDescriptionDeletePayload>;
  productSort?: Maybe<ProductSort>;
  productPricesListCreate?: Maybe<ProductPricesListCreatePayload>;
  productPricesListUpdate?: Maybe<ProductPricesListUpdatePayload>;
  productPricesListDelete?: Maybe<ProductPricesListDeletePayload>;
  productPricesCreate?: Maybe<ProductPricesCreatePayload>;
  productPricesUpdate?: Maybe<ProductPricesUpdatePayload>;
  productPricesDelete?: Maybe<ProductPricesDeletePayload>;
  productToppingsCreate?: Maybe<ProductToppingCreatePayload>;
  productToppingsUpdate?: Maybe<ProductToppingUpdatePayload>;
  productToppingsDelete?: Maybe<ProductToppingDeletePayload>;
  productVariantDelete?: Maybe<ProductVariantDelete>;
  specificationCreate?: Maybe<SpecificationCreatePayload>;
  specificationUpdate?: Maybe<SpecificationUpdatePayload>;
  specificationDelete?: Maybe<SpecificationDeletePayload>;
  productLikeAction?: Maybe<ProductLikeAction>;
  authLogin?: Maybe<AuthencationLoginPayload>;
  authLoginWithZalo?: Maybe<ZaloLoginPayload>;
  authLoginWithFacebook?: Maybe<FacebookLoginPayload>;
  /** Same as `grapgql_jwt` implementation, with standard output. */
  authVerifyToken?: Maybe<VerifyTokenPayload>;
  authRegisterToken?: Maybe<RegisterAccountTokenPayload>;
  authRegisterVerify?: Maybe<RegisterAccountVerify>;
  authForgotPasswordToken?: Maybe<ForgotPasswordToken>;
  authForgotPasswordVerify?: Maybe<ForgotPasswordVerify>;
  authCreateNewPassword?: Maybe<CreateNewPassword>;
  authCreateSuperAdmin?: Maybe<CreateSuperAdminPayload>;
  authChangePassword?: Maybe<ChangePassword>;
  authMigrateData?: Maybe<MigrateData>;
  authLoadData?: Maybe<LoadData>;
  userChangeProfile?: Maybe<ChangeProfile>;
  userAddressAdd?: Maybe<AddAddressPayload>;
  userAddressUpdate?: Maybe<UpdateAddressPayload>;
  userAddressDelete?: Maybe<DeleteAddressPayload>;
  userAddressSetDefault?: Maybe<SetDefaultAddress>;
  adminCreateCustomer?: Maybe<CreateCustomerPayload>;
  adminUpdateCustomer?: Maybe<UpdateCustomerPayload>;
  siteCreate?: Maybe<SiteCreatePayload>;
  siteUpdate?: Maybe<SiteUpdatePayload>;
  siteDeactivate?: Maybe<SiteDeactivate>;
  siteUploadConfigurationData?: Maybe<SiteUploadConfigurationData>;
  adminCreateAdminSite?: Maybe<CreateAdminSitePayload>;
  adminUpdateAdminSite?: Maybe<UpdateAdminSitePayload>;
  adminDeleteAdminSite?: Maybe<DeleteAdminSitePayload>;
  adminChangeAdminSitePassword?: Maybe<AdminChangePassword>;
  supplierCreate?: Maybe<SupplierCreatePayload>;
  supplierUpdate?: Maybe<SupplierUpdatePayload>;
  supplierDelete?: Maybe<SupplierDelete>;
  adminCreateUserAddress?: Maybe<AdminCreateUserAddressPayload>;
  adminUpdateUserAddress?: Maybe<AdminUpdateUserAddressPayload>;
  adminDeleteUserAddress?: Maybe<AdminDeleteUserAddressPayload>;
  userTagCreate?: Maybe<TagUserCreatePayload>;
  userTagUpdate?: Maybe<TagUserUpdatePayload>;
  userTagDelete?: Maybe<TagUserDeletePayload>;
  siteFeatureCreate?: Maybe<SiteFeatureCreatePayload>;
  siteFeatureUpdate?: Maybe<SiteFeatureUpdate>;
  siteFeatureActive?: Maybe<SiteFeatureActive>;
  featureCreate?: Maybe<FeatureCreatePayload>;
  featureUpdate?: Maybe<FeatureUpdatePayload>;
  featureDelete?: Maybe<FeatureDeletePayload>;
  userTokenNotificationCreate?: Maybe<UserTokenNotificationCreate>;
  userTokenNotificationDelete?: Maybe<UserTokenNotificationDelete>;
  siteSmtpCreate?: Maybe<SiteSmtpCreatePayload>;
  siteSmtpUpdate?: Maybe<SiteSmtpUpdatePayload>;
  siteSmtpDelete?: Maybe<SiteSmtpDeletePayload>;
  siteTokenOnepayCreate?: Maybe<SiteTokenOnepayCreatePayload>;
  siteTokenOnepayUpdate?: Maybe<SiteTokenOnepayUpdatePayload>;
  siteTokenOnepayDelete?: Maybe<SiteTokenOnepayDeletePayload>;
  siteTokenFacebookCreate?: Maybe<SiteTokenFacebookCreatePayload>;
  siteTokenFacebookUpdate?: Maybe<SiteTokenFacebookUpdatePayload>;
  siteTokenFacebookDelete?: Maybe<SiteTokenFacebookDeletePayload>;
  siteTokenZaloCreate?: Maybe<SiteTokenZaloCreatePayload>;
  siteTokenZaloUpdate?: Maybe<SiteTokenZaloUpdatePayload>;
  siteTokenZaloDelete?: Maybe<SiteTokenZaloDeletePayload>;
  branchCreate?: Maybe<BranchCreatePayload>;
  branchUpdate?: Maybe<BranchUpdatePayload>;
  branchDelete?: Maybe<BranchDeletePayload>;
  staffGroupCreate?: Maybe<StaffGroupCreatePayload>;
  staffGroupUpdate?: Maybe<StaffGroupUpdatePayload>;
  staffGroupDelete?: Maybe<StaffGroupDeletePayload>;
  customerGroupCreate?: Maybe<CustomerGroupCreatePayload>;
  customerGroupUpdate?: Maybe<CustomerGroupUpdatePayload>;
  customerGroupDelete?: Maybe<CustomerGroupDeletePayload>;
  adminCreatePosCustomer?: Maybe<CreatePosCustomer>;
  adminUpdatePosCustomer?: Maybe<UpdatePosCustomer>;
  provinceBranchCreate?: Maybe<ProvinceBranchCreatePayload>;
  provinceBranchDelete?: Maybe<ProvinceBranchDeletePayload>;
  districtBranchCreate?: Maybe<DistrictBranchCreatePayload>;
  districtBranchDelete?: Maybe<DistrictBranchDeletePayload>;
  checkLalamove?: Maybe<CheckLalamove>;
  wardCreate?: Maybe<WardCreatePayload>;
  wardUpdate?: Maybe<WardUpdatePayload>;
  wardDelete?: Maybe<WardDeletePayload>;
  siteTokenLalamoveCreate?: Maybe<SiteTokenLalamoveCreatePayload>;
  siteTokenLalamoveUpdate?: Maybe<SiteTokenLalamoveUpdatePayload>;
  siteTokenLalamoveDelete?: Maybe<SiteTokenLalamoveDeletePayload>;
  districtCreate?: Maybe<DistrictCreatePayload>;
  districtUpdate?: Maybe<DistrictUpdatePayload>;
  districtDelete?: Maybe<DistrictDeletePayload>;
  authUpdateLocation?: Maybe<UpdateLocation>;
  siteTokenVnpayCreate?: Maybe<SiteTokenVnPayCreatePayload>;
  siteTokenVnpayUpdate?: Maybe<SiteTokenVnPayUpdatePayload>;
  siteTokenVnpayDelete?: Maybe<SiteTokenVnPayDeletePayload>;
  userLocationTrackingCreate?: Maybe<UserLocationTrackingCreate>;
  userLocationTrackingDelete?: Maybe<UserLocationTrackingDelete>;
  categoryCreate?: Maybe<CategoryCreatePayload>;
  categoryUpdate?: Maybe<CategoryUpdatePayload>;
  categoryDelete?: Maybe<CategoryDeletePayload>;
};


export type MutationPartyInfoCreateArgs = {
  input: PartyInforCreateInput;
};


export type MutationPartyInfoUpdateArgs = {
  input: PartyInforUpdateInput;
};


export type MutationPartyInfoDeleteArgs = {
  input: PartyInforDeleteInput;
};


export type MutationPermissionCreateArgs = {
  input: PermissionCreateInput;
};


export type MutationPermissionUpdateArgs = {
  input: PermissionUpdateInput;
};


export type MutationPermissionDeleteArgs = {
  input: PermissionDeleteInput;
};


export type MutationUserPermissionUpdateArgs = {
  input: UserPermissionUpdateInput;
};


export type MutationGroupPermissionCreateArgs = {
  input: GroupPermissionInput;
};


export type MutationGroupPermissionDeleteArgs = {
  input: PermissionDeleteInput;
};


export type MutationDeliveryMethodCreateArgs = {
  input: DeliveryMethodCreateInput;
};


export type MutationDeliveryMethodUpdateArgs = {
  input: DeliveryMethodUpdateInput;
};


export type MutationDeliveryMethodDeleteArgs = {
  input: DeliveryMethodDeleteInput;
};


export type MutationSiteDeliveryMethodUpdateArgs = {
  input: SiteDeliveryMethodUpdateInput;
};


export type MutationKitchenOrderAdminUpdateArgs = {
  input: KitchenOrderInput;
};


export type MutationKitchenOrderChefUpdateArgs = {
  input: KitchenOrderChefUpdateInput;
};


export type MutationFoodProcessingTypeCreateArgs = {
  input: FoodProcessingTypeCreateInput;
};


export type MutationFoodProcessingTypeUpdateArgs = {
  input: FoodProcessingTypeUpdateInput;
};


export type MutationFoodProcessingTypeDeleteArgs = {
  input: FoodProcessingTypeDeleteInput;
};


export type MutationSiteConfigUpdateArgs = {
  input: SiteConfigUpdateInput;
};


export type MutationSiteConfigShip60UpdateArgs = {
  input: SiteConfigShip60UpdateInput;
};


export type MutationFlashSaleCreateArgs = {
  input: FlashSaleCreateInput;
};


export type MutationFlashSaleUpdateArgs = {
  input: FlashSaleUpdateInput;
};


export type MutationFlashSaleDeleteArgs = {
  input: FlashSaleDeleteInput;
};


export type MutationFlashSaleActivateArgs = {
  input?: Maybe<FlashSaleInput>;
};


export type MutationPromotionCreateArgs = {
  input: PromotionCreateInput;
};


export type MutationPromotionUpdateArgs = {
  input: PromotionUpdateInput;
};


export type MutationPromotionDeleteArgs = {
  input: PromotionDeleteInput;
};


export type MutationPromotionActivateArgs = {
  input?: Maybe<PromotionInput>;
};


export type MutationPromotionCodeCreateArgs = {
  input: PromotionCodeCreateInput;
};


export type MutationPromotionCodeUpdateArgs = {
  input: PromotionCodeUpdateInput;
};


export type MutationPromotionCodeDeleteArgs = {
  input: PromotionCodeDeleteInput;
};


export type MutationPromotionCodeActivateArgs = {
  input: PromotionCodeInput;
};


export type MutationOurPartnersCreateArgs = {
  input: OurPartnerCreateInput;
};


export type MutationOurPartnersUpdateArgs = {
  input: OurPartnerUpdateInput;
};


export type MutationOurPartnersDeleteArgs = {
  input: OurPartnerDeleteInput;
};


export type MutationOurPartnersSortArgs = {
  input?: Maybe<Array<Maybe<OurPartnerSortInput>>>;
};


export type MutationCustomerReviewCreateArgs = {
  input: CustomerReviewCreateInput;
};


export type MutationCustomerReviewUpdateArgs = {
  input: CustomerReviewUpdateInput;
};


export type MutationCustomerReviewDeleteArgs = {
  input: CustomerReviewDeleteInput;
};


export type MutationCustomerReviewSortArgs = {
  input?: Maybe<Array<Maybe<CustomerReviewSortInput>>>;
};


export type MutationOutgoingPaymentAdminCreateArgs = {
  input: AdminOutgoingPaymentCreateInput;
};


export type MutationOutgoingPaymentAdminDeleteArgs = {
  input: AdminOutgoingPaymentDeleteInput;
};


export type MutationOutgoingPaymentDetailAdminDeleteArgs = {
  input: AdminOutgoingPaymentProductDeleteInput;
};


export type MutationOutgoingPaymentAdminUpdateArgs = {
  input: AdminOutgoingPaymentUpdateInput;
};


export type MutationIncomingPaymentAdminCreateArgs = {
  input: AdminIncomingPaymentCreateInput;
};


export type MutationIncomingPaymentAdminDeleteArgs = {
  input: AdminIncomingPaymentDeleteInput;
};


export type MutationIncomingPaymentDetailAdminDeleteArgs = {
  input: AdminIncomingPaymentProductDeleteInput;
};


export type MutationIncomingPaymentAdminUpdateArgs = {
  input: AdminIncomingPaymentUpdateInput;
};


export type MutationInvoiceAdminCreateArgs = {
  input: AdminInvoiceCreateInput;
};


export type MutationInvoiceAdminDeleteArgs = {
  input: AdminInvoiceDeleteInput;
};


export type MutationInvoiceDetailAdminDeleteArgs = {
  input: AdminInvoiceProductDeleteInput;
};


export type MutationInvoiceAdminUpdateArgs = {
  input: AdminInvoiceUpdateInput;
};


export type MutationDeliveryAdminBatchCreateArgs = {
  inputs: Array<Maybe<AdminDeliveryCreateInput>>;
};


export type MutationDeliveryAdminCreateArgs = {
  input: AdminDeliveryCreateInput;
};


export type MutationDeliveryAdminUpdateArgs = {
  input: AdminDeliveryUpdateInput;
};


export type MutationDeliveryAdminDeleteArgs = {
  input: AdminDeliveryDeleteInput;
};


export type MutationDeliveryDetailAdminDeleteArgs = {
  input: AdminDeliveryProductDeleteInput;
};


export type MutationKiotvietInformationCreateArgs = {
  input: KiotVietInfoCreateInput;
};


export type MutationKiotvietInformationUpdateArgs = {
  input: KiotVietInfoUpdateInput;
};


export type MutationUpdateDeliveryStatusArgs = {
  input: UpdateDeliveryStatusInput;
};


export type MutationDeliveryTrackingCreateArgs = {
  input: DeliveryTrackingCreateInput;
};


export type MutationDeliveryTrackingDeleteArgs = {
  id: Scalars['String'];
};


export type MutationDeliveryReviewCreateArgs = {
  input: DeliveryReviewCreateInput;
};


export type MutationDeliveryReviewDeleteArgs = {
  id: Scalars['String'];
};


export type MutationDeliveryBranchCreateArgs = {
  input: DeliveryBranchCreateInput;
};


export type MutationDeliveryBranchUpdateArgs = {
  input: DeliveryBranchUpdateInput;
};


export type MutationDeliveryBranchDeleteArgs = {
  input: DeliveryBranchDeleteInput;
};


export type MutationDeliveryHistoryDeleteArgs = {
  input: DeliveryHistoryDeleteInput;
};


export type MutationCollectionCreateArgs = {
  input: CollectionCreateInput;
};


export type MutationCollectionUpdateArgs = {
  input: CollectionUpdateInput;
};


export type MutationCollectionDeleteArgs = {
  input: CollectionDeleteInput;
};


export type MutationCollectionSortArgs = {
  input?: Maybe<Array<Maybe<CollectionSortInput>>>;
};


export type MutationContactUsCreateArgs = {
  input: ContactUsInput;
};


export type MutationContactUsUpdateArgs = {
  input: ContactUsUpdateInput;
};


export type MutationContactUsDeleteArgs = {
  input: ContactUsDeleteInput;
};


export type MutationFeedbackCreateArgs = {
  input: FeedbackCreateInput;
};


export type MutationFeedbackUpdateArgs = {
  input: FeedbackUpdateInput;
};


export type MutationFeedbackSortArgs = {
  input?: Maybe<Array<Maybe<FeedbackSortInput>>>;
};


export type MutationFeedbackDeleteArgs = {
  input: FeedbackDeleteInput;
};


export type MutationProductFeedbackCreateArgs = {
  input: ProductFeedbackCreateInput;
};


export type MutationProductFeedbackUpdateArgs = {
  input: ProductFeedbackUpdateInput;
};


export type MutationProductFeedbackSortArgs = {
  input?: Maybe<Array<Maybe<FeedbackSortInput>>>;
};


export type MutationProductFeedbackDeleteArgs = {
  input: ProductFeedbackDeleteInput;
};


export type MutationWarehouseCreateArgs = {
  input: WarehouseCreateInput;
};


export type MutationWarehouseUpdateArgs = {
  input: WarehouseUpdateInput;
};


export type MutationWarehouseDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationGoodsReceiptCreateArgs = {
  input: GoodsReceiptInput;
};


export type MutationGoodsReceiptUpdateArgs = {
  input: GoodsReceiptUpdateInput;
};


export type MutationGoodsReceiptDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationGoodsReceiptReturnCreateArgs = {
  input: GoodsReceiptReturnInput;
};


export type MutationGoodsReceiptReturnUpdateArgs = {
  input: GoodsReceiptReturnUpdateInput;
};


export type MutationGoodsReceiptReturnDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationGoodsReceiptTransferCreateArgs = {
  input: GoodsReceiptTransferInput;
};


export type MutationGoodsReceiptTransferUpdateArgs = {
  input: GoodsReceiptTransferUpdateInput;
};


export type MutationGoodsReceiptTransferDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationInventoryCountCreateArgs = {
  input: InventoryCountInput;
};


export type MutationInventoryCountUpdateArgs = {
  input: InventoryCountUpdateInput;
};


export type MutationUnitCreateArgs = {
  input: UnitCreateInput;
};


export type MutationUnitUpdateArgs = {
  input: UnitUpdateInput;
};


export type MutationUnitDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationBranchWarehouseUpdateArgs = {
  input: BranchWarehouseUpdateInput;
};


export type MutationBranchWarehouseSortArgs = {
  input?: Maybe<Array<Maybe<BranchWarehouseSortInput>>>;
};


export type MutationFaQsCategoryCreateArgs = {
  input: FaQsCategoryCreateInput;
};


export type MutationFaQsCategoryUpdateArgs = {
  input: FaQsCategoryUpdateInput;
};


export type MutationFaQsCategoryDeleteArgs = {
  input: FaQsCategoryDeleteInput;
};


export type MutationFaQsCreateArgs = {
  input: FaQsCreateInput;
};


export type MutationFaQsUpdateArgs = {
  input: FaQsUpdateInput;
};


export type MutationFaQsDeleteArgs = {
  input: FaQsDeleteInput;
};


export type MutationBannerGroupCreateArgs = {
  input: BannerGroupCreateInput;
};


export type MutationBannerGroupUpdateArgs = {
  input: BannerGroupUpdateInput;
};


export type MutationBannerGroupDeleteArgs = {
  input: BannerGroupDeleteInput;
};


export type MutationBannerCreateArgs = {
  input: BannerCreateInput;
};


export type MutationBannerUpdateArgs = {
  input: BannerUpdateInput;
};


export type MutationBannerDeleteArgs = {
  input: BannerDeleteInput;
};


export type MutationBannerSortArgs = {
  input?: Maybe<Array<Maybe<BannerSortInput>>>;
};


export type MutationBannerGroupBannerCreateArgs = {
  input: BannerGroupBannerCreateInput;
};


export type MutationBannerGroupBannerUpdateArgs = {
  input: BannerGroupBannerUpdateInput;
};


export type MutationBannerGroupBannerDeteleArgs = {
  input: BannerGroupBannerDeleteInput;
};


export type MutationGalleryCreateArgs = {
  input: GalleryCreateInput;
};


export type MutationGalleryUpdateArgs = {
  input: GalleryUpdateInput;
};


export type MutationGalleryDeleteArgs = {
  input: GalleryDeleteInput;
};


export type MutationReviewForProductArgs = {
  input: UserReviewInput;
};


export type MutationReviewUserEditArgs = {
  input: UserEditReviewInput;
};


export type MutationReviewDeleteArgs = {
  input: DeleteReviewInput;
};


export type MutationReviewUserActiveArgs = {
  input: UserActiveReviewInput;
};


export type MutationNewsCateCreateArgs = {
  input: NewsCateCreateInput;
};


export type MutationNewsCateUpdateArgs = {
  input: NewsCateUpdateInput;
};


export type MutationNewsCateDeleteArgs = {
  input: NewsCateDeleteInput;
};


export type MutationNewsCreateArgs = {
  input: NewsCreateInput;
};


export type MutationNewsUpdateArgs = {
  input: NewsUpdateInput;
};


export type MutationNewsDeleteArgs = {
  input: NewsDeleteInput;
};


export type MutationNewsCommentsCreateArgs = {
  input: NewsCommentsCreateInput;
};


export type MutationNewsCommentsUpdateArgs = {
  input: NewsCommentsUpdateInput;
};


export type MutationNewsCommentsDeleteArgs = {
  input: NewsCommentsDeleteInput;
};


export type MutationNewsHashtagCreateArgs = {
  input: HashtagCreateInput;
};


export type MutationNewsHashtagUpdateArgs = {
  input: HashtagUpdateInput;
};


export type MutationNewsHashtagDeleteArgs = {
  input: HashtagDeleteInput;
};


export type MutationHeaderNewsCreateArgs = {
  input: HeaderNewsCreateInput;
};


export type MutationHeaderNewsUpdateArgs = {
  input: HeaderNewsUpdateInput;
};


export type MutationHeaderNewsDeleteArgs = {
  input: HeaderNewsDeleteInput;
};


export type MutationNewsLikeActionArgs = {
  input: NewsLikeInput;
};


export type MutationFlatpageCreateArgs = {
  input: CreateFlatpageInput;
};


export type MutationFlatpageUpdateArgs = {
  input: UpdateFlatpageInput;
};


export type MutationFlatpageDeleteArgs = {
  input: DeleteFlatpageInput;
};


export type MutationCartAddArgs = {
  input: AddToCartInput;
};


export type MutationCartUpdateArgs = {
  input: UpdateCartInput;
};


export type MutationCartDeleteArgs = {
  input: DeteteCartInput;
};


export type MutationOrderAdminCreateArgs = {
  input: AdminOrderCreateInput;
};


export type MutationOrderUserCreateArgs = {
  input: UserOrderCreateInput;
};


export type MutationOrderGuestCreateArgs = {
  input: UserGuestInput;
};


export type MutationOrderAdminUpdateArgs = {
  input: OrderUpdateInput;
};


export type MutationOrderCustomerUpdateArgs = {
  input: OrderCustomerUpdateInput;
};


export type MutationOrderAdminDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationOrderWarehouseUpdateArgs = {
  input: Array<Maybe<AdminOrderWarehouseUpdateInput>>;
};


export type MutationOtherRevenueCreateArgs = {
  input: OtherRevenueCreateInput;
};


export type MutationOtherRevenueUpdateArgs = {
  input: OtherRevenueUpdateInput;
};


export type MutationOtherRevenueDeleteArgs = {
  input: OtherRevenueDeleteInput;
};


export type MutationGetLalamoveShippingCostArgs = {
  input: GetLalamoveShippingCostInput;
};


export type MutationGetLalamoveStatusArgs = {
  input: GetLalamoveStatusInput;
};


export type MutationCountryCreateArgs = {
  input: CountryCreateInput;
};


export type MutationCountryUpdateArgs = {
  input: CountryUpdateInput;
};


export type MutationCountryDeleteArgs = {
  input: CountryDeleteInput;
};


export type MutationUserTopUpArgs = {
  input: UserTopUpInput;
};


export type MutationTopUpPackageCreateArgs = {
  input: TopUpPackageCreateInput;
};


export type MutationTopUpPackageUpdateArgs = {
  input: TopUpPackageUpdateInput;
};


export type MutationTopUpPackageDeleteArgs = {
  input: TopUpPackageDeleteInput;
};


export type MutationPaymentMethodCreateArgs = {
  input: PaymentMethodCreateInput;
};


export type MutationPaymentMethodUpdateArgs = {
  input: PaymentMethodUpdateInput;
};


export type MutationPaymentMethodDeleteArgs = {
  input: PaymentMethodDeleteInput;
};


export type MutationProductForBuyCreateArgs = {
  input: ProductForBuyCreateInput;
};


export type MutationProductForBetCreateArgs = {
  input: ProductForBetCreateInput;
};


export type MutationProductForBuyUpdateArgs = {
  input: ProductForBuyUpdateInput;
};


export type MutationProductForBetUpdateArgs = {
  input: ProductForBetUpdateInput;
};


export type MutationProductForBetTransactionArgs = {
  input: BetTransactionInput;
};


export type MutationProductForBetResultArgs = {
  input: ProductForBetResultInput;
};


export type MutationProductForBuyDeleteArgs = {
  input: ProductForBuyDeleteInput;
};


export type MutationAttributeCreateArgs = {
  input: AttributeCreateInput;
};


export type MutationAttributeUpdateArgs = {
  input: AttributeUpdateInput;
};


export type MutationAttributeDeleteArgs = {
  input: AttributeDeleteInput;
};


export type MutationVariantCreateArgs = {
  input: VariantCreateInput;
};


export type MutationVariantUpdateArgs = {
  input: VariantUpdateInput;
};


export type MutationVariantDeleteArgs = {
  input: VariantDeleteInput;
};


export type MutationBrandCreateArgs = {
  input: BrandCreateInput;
};


export type MutationBrandUpdateArgs = {
  input: BrandUpdateInput;
};


export type MutationBrandDeleteArgs = {
  input: BrandDeleteInput;
};


export type MutationProductComboCreateArgs = {
  input: ProductComboInput;
};


export type MutationProductComboUpdateArgs = {
  input: ProductComboUpdateInput;
};


export type MutationProductComboDeleteArgs = {
  input: ProductComboDeleteInput;
};


export type MutationProductTagCreateArgs = {
  input: ProductTagCreateInput;
};


export type MutationProductTagUpdateArgs = {
  input: ProductTagUpdateInput;
};


export type MutationProductTagDeleteArgs = {
  input: ProductTagDeleteInput;
};


export type MutationProductDescriptionCreateArgs = {
  input: ProductDescriptionCreateInput;
};


export type MutationProductDescriptionUpdateArgs = {
  input: ProductDescriptionUpdateInput;
};


export type MutationProductDescriptionDeleteArgs = {
  input: ProductDescriptionDeleteInput;
};


export type MutationProductSortArgs = {
  input?: Maybe<Array<Maybe<ProductSortInput>>>;
};


export type MutationProductPricesListCreateArgs = {
  input: ProductPricesListCreateInput;
};


export type MutationProductPricesListUpdateArgs = {
  input: ProductPricesListUpdateInput;
};


export type MutationProductPricesListDeleteArgs = {
  input: ProductPricesListDeleteInput;
};


export type MutationProductPricesCreateArgs = {
  input: ProductPricesCreateInput;
};


export type MutationProductPricesUpdateArgs = {
  input: ProductPricesUpdateInput;
};


export type MutationProductPricesDeleteArgs = {
  input: ProductPricesDeleteInput;
};


export type MutationProductToppingsCreateArgs = {
  input: ProductToppingCreateInput;
};


export type MutationProductToppingsUpdateArgs = {
  input: ProductToppingUpdateInput;
};


export type MutationProductToppingsDeleteArgs = {
  input: ProductToppingDeleteInput;
};


export type MutationProductVariantDeleteArgs = {
  input: ProductVariantDeleteInput;
};


export type MutationSpecificationCreateArgs = {
  input: SpecificationCreateInput;
};


export type MutationSpecificationUpdateArgs = {
  input: SpecificationUpdateInput;
};


export type MutationSpecificationDeleteArgs = {
  input: SpecificationDeleteInput;
};


export type MutationProductLikeActionArgs = {
  input: ProductVariantLikeInput;
};


export type MutationAuthLoginArgs = {
  input: AuthencationLoginInput;
};


export type MutationAuthLoginWithZaloArgs = {
  input: ZaloLoginInput;
};


export type MutationAuthLoginWithFacebookArgs = {
  input: FacebookLoginInput;
};


export type MutationAuthVerifyTokenArgs = {
  input: VerifyTokenInput;
};


export type MutationAuthRegisterTokenArgs = {
  input: RegisterAccountTokenInput;
};


export type MutationAuthRegisterVerifyArgs = {
  input: TokenVerifyInput;
};


export type MutationAuthForgotPasswordTokenArgs = {
  input: ForgotPasswordInput;
};


export type MutationAuthForgotPasswordVerifyArgs = {
  input: TokenVerifyInput;
};


export type MutationAuthCreateNewPasswordArgs = {
  input: CreateNewPasswordInput;
};


export type MutationAuthCreateSuperAdminArgs = {
  input: CreateSuperAdminInput;
};


export type MutationAuthChangePasswordArgs = {
  input: ChangePasswordInput;
};


export type MutationAuthLoadDataArgs = {
  isAccept: Scalars['Boolean'];
  name: Scalars['String'];
};


export type MutationUserChangeProfileArgs = {
  input?: Maybe<ChangeProfileInput>;
};


export type MutationUserAddressAddArgs = {
  input: AddAddressInput;
};


export type MutationUserAddressUpdateArgs = {
  input: UpdateAddressInput;
};


export type MutationUserAddressDeleteArgs = {
  input: DeleteAddressInput;
};


export type MutationUserAddressSetDefaultArgs = {
  input?: Maybe<SetDefaultAddressInput>;
};


export type MutationAdminCreateCustomerArgs = {
  input: CreateCustomerInput;
};


export type MutationAdminUpdateCustomerArgs = {
  input: UpdateCustomerInput;
};


export type MutationSiteCreateArgs = {
  input: SiteCreateInput;
};


export type MutationSiteUpdateArgs = {
  input: SiteUpdateInput;
};


export type MutationSiteDeactivateArgs = {
  input?: Maybe<SiteDeleteInput>;
};


export type MutationAdminCreateAdminSiteArgs = {
  input: CreateAdminSiteInput;
};


export type MutationAdminUpdateAdminSiteArgs = {
  input: UpdateAdminSiteInput;
};


export type MutationAdminDeleteAdminSiteArgs = {
  input: DeleteAdminSiteInput;
};


export type MutationAdminChangeAdminSitePasswordArgs = {
  input: AdminChangePasswordInput;
};


export type MutationSupplierCreateArgs = {
  input: SupplierCreateInput;
};


export type MutationSupplierUpdateArgs = {
  input: SupplierUpdateInput;
};


export type MutationSupplierDeleteArgs = {
  input: Array<Maybe<Scalars['String']>>;
};


export type MutationAdminCreateUserAddressArgs = {
  input: AdminCreateUserAddressInput;
};


export type MutationAdminUpdateUserAddressArgs = {
  input: AdminUpdateUserAddressInput;
};


export type MutationAdminDeleteUserAddressArgs = {
  input: AdminDeleteUserAddressInput;
};


export type MutationUserTagCreateArgs = {
  input: TagUserCreateInput;
};


export type MutationUserTagUpdateArgs = {
  input: TagUserUpdateInput;
};


export type MutationUserTagDeleteArgs = {
  input: TagUserDeleteInput;
};


export type MutationSiteFeatureCreateArgs = {
  input: SiteFeatureCreateInput;
};


export type MutationSiteFeatureUpdateArgs = {
  input: SiteFeatureUpdateInput;
};


export type MutationSiteFeatureActiveArgs = {
  site: Scalars['String'];
  siteFeatures: Array<Maybe<SiteFeatureActiveInput>>;
};


export type MutationFeatureCreateArgs = {
  input: FeatureCreateInput;
};


export type MutationFeatureUpdateArgs = {
  input: FeatureUpdateInput;
};


export type MutationFeatureDeleteArgs = {
  input: FeatureDeleteInput;
};


export type MutationUserTokenNotificationCreateArgs = {
  token: Scalars['String'];
};


export type MutationUserTokenNotificationDeleteArgs = {
  token: Scalars['String'];
};


export type MutationSiteSmtpCreateArgs = {
  input: SiteSmtpCreateInput;
};


export type MutationSiteSmtpUpdateArgs = {
  input: SiteSmtpUpdateInput;
};


export type MutationSiteSmtpDeleteArgs = {
  input: SiteSmtpDeleteInput;
};


export type MutationSiteTokenOnepayCreateArgs = {
  input: SiteTokenOnepayCreateInput;
};


export type MutationSiteTokenOnepayUpdateArgs = {
  input: SiteTokenOnepayUpdateInput;
};


export type MutationSiteTokenOnepayDeleteArgs = {
  input: SiteTokenOnepayDeleteInput;
};


export type MutationSiteTokenFacebookCreateArgs = {
  input: SiteTokenFacebookCreateInput;
};


export type MutationSiteTokenFacebookUpdateArgs = {
  input: SiteTokenFacebookUpdateInput;
};


export type MutationSiteTokenFacebookDeleteArgs = {
  input: SiteTokenFacebookDeleteInput;
};


export type MutationSiteTokenZaloCreateArgs = {
  input: SiteTokenZaloCreateInput;
};


export type MutationSiteTokenZaloUpdateArgs = {
  input: SiteTokenZaloUpdateInput;
};


export type MutationSiteTokenZaloDeleteArgs = {
  input: SiteTokenZaloDeleteInput;
};


export type MutationBranchCreateArgs = {
  input: BranchCreateInput;
};


export type MutationBranchUpdateArgs = {
  input: BranchUpdateInput;
};


export type MutationBranchDeleteArgs = {
  input: BranchDeleteInput;
};


export type MutationStaffGroupCreateArgs = {
  input: StaffGroupCreateInput;
};


export type MutationStaffGroupUpdateArgs = {
  input: StaffGroupUpdateInput;
};


export type MutationStaffGroupDeleteArgs = {
  input: StaffGroupDeleteInput;
};


export type MutationCustomerGroupCreateArgs = {
  input: CustomerGroupCreateInput;
};


export type MutationCustomerGroupUpdateArgs = {
  input: CustomerGroupUpdateInput;
};


export type MutationCustomerGroupDeleteArgs = {
  input: CustomerGroupDeleteInput;
};


export type MutationAdminCreatePosCustomerArgs = {
  input: CreatePosCustomerInput;
};


export type MutationAdminUpdatePosCustomerArgs = {
  input: UpdatePosCustomerInput;
};


export type MutationProvinceBranchCreateArgs = {
  input: ProvinceBranchCreateInput;
};


export type MutationProvinceBranchDeleteArgs = {
  input: ProvinceBranchDeleteInput;
};


export type MutationDistrictBranchCreateArgs = {
  input: DistrictBranchCreateInput;
};


export type MutationDistrictBranchDeleteArgs = {
  input: DistrictBranchDeleteInput;
};


export type MutationWardCreateArgs = {
  input: WardCreateInput;
};


export type MutationWardUpdateArgs = {
  input: WardUpdateInput;
};


export type MutationWardDeleteArgs = {
  input: WardDeleteInput;
};


export type MutationSiteTokenLalamoveCreateArgs = {
  input: SiteTokenLalamoveCreateInput;
};


export type MutationSiteTokenLalamoveUpdateArgs = {
  input: SiteTokenLalamoveUpdateInput;
};


export type MutationSiteTokenLalamoveDeleteArgs = {
  input: SiteTokenLalamoveDeleteInput;
};


export type MutationDistrictCreateArgs = {
  input: DistrictCreateInput;
};


export type MutationDistrictUpdateArgs = {
  input: DistrictUpdateInput;
};


export type MutationDistrictDeleteArgs = {
  input: DistrictDeleteInput;
};


export type MutationSiteTokenVnpayCreateArgs = {
  input: SiteTokenVnPayCreateInput;
};


export type MutationSiteTokenVnpayUpdateArgs = {
  input: SiteTokenVnPayUpdateInput;
};


export type MutationSiteTokenVnpayDeleteArgs = {
  input: SiteTokenVnPayDeleteInput;
};


export type MutationUserLocationTrackingCreateArgs = {
  input: UserLocationTrackingCreateInput;
};


export type MutationUserLocationTrackingDeleteArgs = {
  id: Scalars['String'];
};


export type MutationCategoryCreateArgs = {
  input: CategoryCreateInput;
};


export type MutationCategoryUpdateArgs = {
  input: CategoryUpdateInput;
};


export type MutationCategoryDeleteArgs = {
  input: CategoryDeleteInput;
};

/** An error that happened in a mutation. */
export type MutationErrorType = {
  __typename?: 'MutationErrorType';
  /** The field that caused the error, or `null` if it isn't associated with any particular field. */
  field?: Maybe<Scalars['String']>;
  /** The error message. */
  message?: Maybe<Scalars['String']>;
};

export type NewCustomerNode = {
  __typename?: 'NewCustomerNode';
  time?: Maybe<Scalars['DateTime']>;
  count?: Maybe<Scalars['Int']>;
};

export type News = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type NewsCateCreateInput = {
  icon?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCateCreatePayload = {
  __typename?: 'NewsCateCreatePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsCategory?: Maybe<NewsCateNode>;
};

export type NewsCateDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCateDeletePayload = {
  __typename?: 'NewsCateDeletePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsCategory?: Maybe<NewsCateNode>;
};

export type NewsCateNode = NewsCategory & {
  __typename?: 'NewsCateNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  icon?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  parent?: Maybe<NewsCateNode>;
  site: SiteNode;
  subCategory: NewsCateNodeConnection;
};


export type NewsCateNodeSubCategoryArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type NewsCateNodeConnection = {
  __typename?: 'NewsCateNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<NewsCateNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `NewsCateNode` and its cursor. */
export type NewsCateNodeEdge = {
  __typename?: 'NewsCateNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<NewsCateNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type NewsCateUpdateInput = {
  id: Scalars['String'];
  icon?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCateUpdatePayload = {
  __typename?: 'NewsCateUpdatePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsCategory?: Maybe<NewsCateNode>;
};

export type NewsCategory = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type NewsCommentsCreateInput = {
  comment: Scalars['String'];
  news: Scalars['ID'];
  parent?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCommentsCreatePayload = {
  __typename?: 'NewsCommentsCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsComments?: Maybe<NewsCommentsNode>;
};

export type NewsCommentsDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCommentsDeletePayload = {
  __typename?: 'NewsCommentsDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsComments?: Maybe<NewsCommentsNode>;
};

export type NewsCommentsNode = CustomizeInterface & {
  __typename?: 'NewsCommentsNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  news: NewsNode;
  user: UserProfile;
  parent?: Maybe<NewsCommentsNode>;
  comment: Scalars['String'];
  level: Scalars['Int'];
  reply: NewsCommentsNodeConnection;
};


export type NewsCommentsNodeReplyArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['String']>;
  news?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type NewsCommentsNodeConnection = {
  __typename?: 'NewsCommentsNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<NewsCommentsNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `NewsCommentsNode` and its cursor. */
export type NewsCommentsNodeEdge = {
  __typename?: 'NewsCommentsNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<NewsCommentsNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type NewsCommentsUpdateInput = {
  comment?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCommentsUpdatePayload = {
  __typename?: 'NewsCommentsUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  newsComments?: Maybe<NewsCommentsNode>;
};

export type NewsCreateInput = {
  thumbnail?: Maybe<Scalars['Upload']>;
  category: Scalars['String'];
  site?: Maybe<Scalars['String']>;
  hashtags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  slug?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  url?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsCreatePayload = {
  __typename?: 'NewsCreatePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  news?: Maybe<NewsNode>;
};

export type NewsDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsDeletePayload = {
  __typename?: 'NewsDeletePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  news?: Maybe<NewsNode>;
};

export type NewsError = {
  __typename?: 'NewsError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type NewsHashtagNode = {
  __typename?: 'NewsHashtagNode';
  id: Scalars['ID'];
  news: NewsNode;
  hashtag: HashtagNode;
};

export type NewsLikeAction = {
  __typename?: 'NewsLikeAction';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<NewsError>>>;
  news?: Maybe<NewsNode>;
};

export type NewsLikeInput = {
  newsId: Scalars['String'];
};

export type NewsNode = News & {
  __typename?: 'NewsNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  category: NewsCateNode;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  site: SiteNode;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<UserProfile>;
  slug?: Maybe<Scalars['String']>;
  created: Scalars['DateTime'];
  url?: Maybe<Scalars['String']>;
  comments: NewsCommentsNodeConnection;
  hashtag: Array<NewsHashtagNode>;
  newsRelated?: Maybe<Array<Maybe<NewsRelateNode>>>;
  likeCount?: Maybe<Scalars['Int']>;
};


export type NewsNodeCommentsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['String']>;
  news?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type NewsNodeNewsRelatedArgs = {
  amountNews?: Maybe<Scalars['Int']>;
};

export type NewsNodeConnection = {
  __typename?: 'NewsNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<NewsNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `NewsNode` and its cursor. */
export type NewsNodeEdge = {
  __typename?: 'NewsNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<NewsNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type NewsRelateNode = News & {
  __typename?: 'NewsRelateNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  category: NewsCateNode;
  title: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  site: SiteNode;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  createdBy?: Maybe<UserProfile>;
  slug?: Maybe<Scalars['String']>;
  created: Scalars['DateTime'];
  url?: Maybe<Scalars['String']>;
  comments: NewsCommentsNodeConnection;
  likeCount?: Maybe<Scalars['Int']>;
};


export type NewsRelateNodeCommentsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['String']>;
  news?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type NewsUpdateInput = {
  thumbnail?: Maybe<Scalars['Upload']>;
  site?: Maybe<Scalars['String']>;
  category?: Maybe<Scalars['String']>;
  hashtagRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  hashtags?: Maybe<Array<Maybe<Scalars['String']>>>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  slug?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  url?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility: Scalars['Boolean'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type NewsUpdatePayload = {
  __typename?: 'NewsUpdatePayload';
  errors?: Maybe<Array<Maybe<NewsError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  news?: Maybe<NewsNode>;
};

export type Order = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type OrderAddressNode = {
  __typename?: 'OrderAddressNode';
  id: Scalars['ID'];
  address?: Maybe<UserAddressNode>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  province?: Maybe<ProvinceNode>;
  district?: Maybe<DistrictNode>;
  ward?: Maybe<WardNode>;
  street?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
};

export type OrderBranchNode = {
  __typename?: 'OrderBranchNode';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  latitude?: Maybe<Scalars['Float']>;
  longtitude?: Maybe<Scalars['Float']>;
  key?: Maybe<Scalars['String']>;
};

export type OrderCustomerUpdate = {
  __typename?: 'OrderCustomerUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  order?: Maybe<OrderNode>;
};

export type OrderCustomerUpdateInput = {
  id: Scalars['String'];
  addressId?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<PaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  orderNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  paymentReturnData?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
};

export type OrderDelete = {
  __typename?: 'OrderDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
};

export type OrderDetailNode = {
  __typename?: 'OrderDetailNode';
  quantity: Scalars['Float'];
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  productName: Scalars['String'];
  unitPrice: Scalars['Float'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  isReviewed: Scalars['Boolean'];
  subTotal: Scalars['Float'];
  parent?: Maybe<OrderDetailNode>;
  orderDetailNote?: Maybe<Scalars['String']>;
  productTopping?: Maybe<ProductToppingNode>;
  totalAmountTopping: Scalars['Float'];
  level: Scalars['Int'];
  subOrderDetail: Array<OrderDetailNode>;
  variant?: Maybe<VariantNode>;
  productPricesList?: Maybe<ProductPricesListNode>;
  pricesList?: Maybe<Array<Maybe<ProductPricesNode>>>;
  inventory?: Maybe<Array<Maybe<WarehouseInventoryNode>>>;
  inventoryQuantity?: Maybe<Array<Maybe<ProductBranchWarehouse>>>;
  orderDetailWarehouse?: Maybe<Array<Maybe<OrderDetailWarehouseNode>>>;
  unitList?: Maybe<Array<Maybe<OrderDetailUnitNode>>>;
};

export type OrderDetailUnitNode = {
  __typename?: 'OrderDetailUnitNode';
  id: Scalars['ID'];
  orderDetail: OrderDetailNode;
  unit?: Maybe<UnitNode>;
  unitName: Scalars['String'];
  quantity: Scalars['Float'];
  isMain: Scalars['Boolean'];
};

export type OrderDetailWarehouseNode = {
  __typename?: 'OrderDetailWarehouseNode';
  id: Scalars['ID'];
  warehouse: WarehouseNode;
  quantity: Scalars['Float'];
  sortOrder: Scalars['Int'];
  quantityReturn: Scalars['Float'];
};

/** An enumeration. */
export enum OrderDiscountDiscountType {
  /** Discount */
  Discount = 'DISCOUNT',
  /** Promotion */
  Promotion = 'PROMOTION',
  /** Promotion_code */
  PromotionCode = 'PROMOTION_CODE'
}

export type OrderDiscountNode = CustomizeInterface & {
  __typename?: 'OrderDiscountNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  discount: Scalars['Float'];
  unitDiscount: OrderDiscountUnitDiscount;
  discountType: OrderDiscountDiscountType;
  promotion?: Maybe<PromotionNode>;
};

/** An enumeration. */
export enum OrderDiscountUnitDiscount {
  /** Percent */
  Percent = 'PERCENT',
  /** vnd */
  Vnd = 'VND'
}

export type OrderEmptyDelete = {
  __typename?: 'OrderEmptyDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
};

export type OrderError = {
  __typename?: 'OrderError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type OrderHistoryNode = {
  __typename?: 'OrderHistoryNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  status?: Maybe<OrderHistoryStatus>;
  description?: Maybe<Scalars['String']>;
  user?: Maybe<UserProfile>;
  modifiedName?: Maybe<Scalars['String']>;
};

/** An enumeration. */
export enum OrderHistoryStatus {
  /** Comment */
  Comment = 'COMMENT',
  /** Action */
  Action = 'ACTION'
}

export type OrderNode = Order & {
  __typename?: 'OrderNode';
  createdBy?: Maybe<UserProfile>;
  orderStatus?: Maybe<OrderOrderStatus>;
  salesChannel?: Maybe<OrderSalesChannel>;
  shippingCost: Scalars['Float'];
  deliveryMethod?: Maybe<DeliveryMethodNode>;
  totalDiscount: Scalars['Float'];
  totalOtherFee: Scalars['Float'];
  /** The ID of the object. */
  id: Scalars['ID'];
  paymentMethod: PaymentNode;
  totalAmount: Scalars['Float'];
  paymentStatus?: Maybe<OrderPaymentStatus>;
  deposit?: Maybe<Scalars['Float']>;
  orderNote?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  customer?: Maybe<UserProfile>;
  paymentReturnData?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  branchName?: Maybe<Scalars['String']>;
  otherFee: Scalars['Float'];
  discount?: Maybe<Scalars['Float']>;
  paid: Scalars['Float'];
  shippingTime?: Maybe<Scalars['DateTime']>;
  isDelivery: Scalars['Boolean'];
  isDeliveryNow: Scalars['Boolean'];
  customerGroup?: Maybe<CustomerGroupNode>;
  unitDiscount?: Maybe<OrderUnitDiscount>;
  branch?: Maybe<BranchNode>;
  isSendConfirmationMessage: Scalars['Boolean'];
  isSendNotificationMessage: Scalars['Boolean'];
  isSpam: Scalars['Boolean'];
  created: Scalars['DateTime'];
  createdDate: Scalars['DateTime'];
  bonusPoint: Scalars['Float'];
  orderOtherFee?: Maybe<Array<Maybe<OrderOtherFeeNode>>>;
  delivery: DeliveryNodeConnection;
  details?: Maybe<Array<Maybe<OrderDetailNode>>>;
  orderAddress?: Maybe<OrderAddressNode>;
  orderHistory?: Maybe<Array<Maybe<OrderHistoryNode>>>;
  orderBranch?: Maybe<OrderBranchNode>;
  productPricesList?: Maybe<ProductPricesListNode>;
  createdByName?: Maybe<Scalars['String']>;
  orderDiscount?: Maybe<Array<Maybe<OrderDiscountNode>>>;
};


export type OrderNodeDeliveryArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  shipper?: Maybe<Scalars['String']>;
  deliveryStatus?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  deliveryAddress?: Maybe<Scalars['String']>;
  receiveAddress?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  deliveryPhone?: Maybe<Scalars['String']>;
  receivePhone?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type OrderNodeConnection = {
  __typename?: 'OrderNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<OrderNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `OrderNode` and its cursor. */
export type OrderNodeEdge = {
  __typename?: 'OrderNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<OrderNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum OrderOrderStatus {
  /** Shipping */
  Shipping = 'SHIPPING',
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Received */
  Received = 'RECEIVED',
  /** Ordered */
  Ordered = 'ORDERED',
  /** Pending */
  Pending = 'PENDING',
  /** Picked_up */
  PickedUp = 'PICKED_UP',
  /** First_pickup_attempt_failed */
  FirstPickupAttemptFailed = 'FIRST_PICKUP_ATTEMPT_FAILED',
  /** Second_pickup_attempt_failed */
  SecondPickupAttemptFailed = 'SECOND_PICKUP_ATTEMPT_FAILED',
  /** Third_pickup_attempt_failed */
  ThirdPickupAttemptFailed = 'THIRD_PICKUP_ATTEMPT_FAILED',
  /** Pickup_failed */
  PickupFailed = 'PICKUP_FAILED',
  /** Delivering */
  Delivering = 'DELIVERING',
  /** First_send_attempt_failed */
  FirstSendAttemptFailed = 'FIRST_SEND_ATTEMPT_FAILED',
  /** Second_send_attempt_failed */
  SecondSendAttemptFailed = 'SECOND_SEND_ATTEMPT_FAILED',
  /** Third_send_attempt_failed */
  ThirdSendAttemptFailed = 'THIRD_SEND_ATTEMPT_FAILED',
  /** Delivery_failed */
  DeliveryFailed = 'DELIVERY_FAILED',
  /** Being_return */
  BeingReturn = 'BEING_RETURN',
  /** First_return_attempt_failed */
  FirstReturnAttemptFailed = 'FIRST_RETURN_ATTEMPT_FAILED',
  /** Second_return_attempt_failed */
  SecondReturnAttemptFailed = 'SECOND_RETURN_ATTEMPT_FAILED',
  /** Third_return_attempt_failed */
  ThirdReturnAttemptFailed = 'THIRD_RETURN_ATTEMPT_FAILED',
  /** Package_returned */
  PackageReturned = 'PACKAGE_RETURNED',
  /** Return_failed */
  ReturnFailed = 'RETURN_FAILED',
  /** Delivered */
  Delivered = 'DELIVERED',
  /** Rider_assigned */
  RiderAssigned = 'RIDER_ASSIGNED',
  /** package_lost */
  PackageLost = 'PACKAGE_LOST'
}

export type OrderOtherFee = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type OrderOtherFeeInput = {
  name: Scalars['String'];
  amount?: Maybe<Scalars['Float']>;
};

export type OrderOtherFeeNode = OrderOtherFee & {
  __typename?: 'OrderOtherFeeNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  feeName: Scalars['String'];
  amount: Scalars['Float'];
};

/** An enumeration. */
export enum OrderPaymentStatus {
  /** Paid */
  Paid = 'PAID',
  /** Wait_for_pay */
  WaitForPay = 'WAIT_FOR_PAY',
  /** Cancelled */
  Cancelled = 'CANCELLED',
  /** Fulfilled */
  Fulfilled = 'FULFILLED',
  /** Partially_fulfilled */
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  /** Unfulfilled */
  Unfulfilled = 'UNFULFILLED',
  /** Ready_to_capture */
  ReadyToCapture = 'READY_TO_CAPTURE',
  /** Ready_to_fulfill */
  ReadyToFulfill = 'READY_TO_FULFILL'
}

/** An enumeration. */
export enum OrderSalesChannel {
  /** Web */
  Web = 'WEB',
  /** Facebook */
  Facebook = 'FACEBOOK',
  /** ECommerce */
  Ecommerce = 'ECOMMERCE',
  /** Direct */
  Direct = 'DIRECT',
  /** Call_center */
  CallCenter = 'CALL_CENTER'
}

export enum OrderStatusInput {
  Pending = 'PENDING',
  Shipping = 'SHIPPING',
  Delivered = 'DELIVERED',
  Ordered = 'ORDERED',
  PickedUp = 'PICKED_UP',
  Cancelled = 'CANCELLED',
  FirstPickupAttemptFailed = 'FIRST_PICKUP_ATTEMPT_FAILED',
  SecondPickupAttemptFailed = 'SECOND_PICKUP_ATTEMPT_FAILED',
  ThirdPickupAttemptFailed = 'THIRD_PICKUP_ATTEMPT_FAILED',
  PickupFailed = 'PICKUP_FAILED',
  FirstSendAttemptFailed = 'FIRST_SEND_ATTEMPT_FAILED',
  SecondSendAttemptFailed = 'SECOND_SEND_ATTEMPT_FAILED',
  ThirdSendAttemptFailed = 'THIRD_SEND_ATTEMPT_FAILED',
  DeliveryFailed = 'DELIVERY_FAILED',
  BeingReturn = 'BEING_RETURN',
  FirstReturnAttemptFailed = 'FIRST_RETURN_ATTEMPT_FAILED',
  SecondReturnAttemptFailed = 'SECOND_RETURN_ATTEMPT_FAILED',
  ThirdReturnAttemptFailed = 'THIRD_RETURN_ATTEMPT_FAILED',
  ReturnFailed = 'RETURN_FAILED',
  PackageReturned = 'PACKAGE_RETURNED',
  PackageLost = 'PACKAGE_LOST',
  RiderAssigned = 'RIDER_ASSIGNED'
}

/** An enumeration. */
export enum OrderUnitDiscount {
  /** Percent */
  Percent = 'PERCENT',
  /** vnd */
  Vnd = 'VND'
}

export type OrderUpdate = {
  __typename?: 'OrderUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  order?: Maybe<OrderNode>;
};

export type OrderUpdateInput = {
  id: Scalars['String'];
  addressId?: Maybe<Scalars['String']>;
  receiver?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<OrderStatusInput>;
  paymentStatus?: Maybe<PaymentStatusInput>;
  paymentMethodId?: Maybe<Scalars['String']>;
  orderNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  product?: Maybe<Array<Maybe<ProductInput>>>;
  comment?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<SalesChannelInput>;
  paymentReturnData?: Maybe<Scalars['String']>;
  discounts?: Maybe<Array<Maybe<DiscountInput>>>;
  method?: Maybe<LoginStatusInput>;
  facebookId?: Maybe<Scalars['String']>;
  facebookName?: Maybe<Scalars['String']>;
  otherFee?: Maybe<Scalars['Float']>;
  shippingCost?: Maybe<Scalars['Float']>;
  paid?: Maybe<Scalars['Float']>;
  shippingTime?: Maybe<Scalars['DateTime']>;
  isDelivery?: Maybe<Scalars['Boolean']>;
  isDeliveryNow?: Maybe<Scalars['Boolean']>;
  deliveryNote?: Maybe<Scalars['String']>;
  productPricesListId?: Maybe<Scalars['String']>;
  customerGroupId?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  unitDiscount?: Maybe<UnitDiscountInput>;
  branch?: Maybe<Scalars['String']>;
  isSpam?: Maybe<Scalars['Boolean']>;
  created?: Maybe<Scalars['DateTime']>;
  orderOtherFee?: Maybe<Array<Maybe<OrderOtherFeeInput>>>;
};

export type OtherRevenueCreateInput = {
  name: Scalars['String'];
  price?: Maybe<Scalars['Float']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OtherRevenueCreatePayload = {
  __typename?: 'OtherRevenueCreatePayload';
  errors?: Maybe<Array<Maybe<OtherRevenueError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  otherRevenue?: Maybe<OtherRevenueNode>;
};

export type OtherRevenueDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OtherRevenueDeletePayload = {
  __typename?: 'OtherRevenueDeletePayload';
  errors?: Maybe<Array<Maybe<OtherRevenueError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  otherRevenue?: Maybe<OtherRevenueNode>;
};

export type OtherRevenueError = {
  __typename?: 'OtherRevenueError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type OtherRevenueNode = CustomizeInterface & {
  __typename?: 'OtherRevenueNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  name: Scalars['String'];
  price: Scalars['Float'];
};

export type OtherRevenueNodeConnection = {
  __typename?: 'OtherRevenueNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<OtherRevenueNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `OtherRevenueNode` and its cursor. */
export type OtherRevenueNodeEdge = {
  __typename?: 'OtherRevenueNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<OtherRevenueNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type OtherRevenueUpdateInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OtherRevenueUpdatePayload = {
  __typename?: 'OtherRevenueUpdatePayload';
  errors?: Maybe<Array<Maybe<OtherRevenueError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  otherRevenue?: Maybe<OtherRevenueNode>;
};

export type OurPartnerCreateInput = {
  site?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['Upload']>;
  name: Scalars['String'];
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OurPartnerCreatePayload = {
  __typename?: 'OurPartnerCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ourPartner?: Maybe<OurPartnerNode>;
};

export type OurPartnerDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OurPartnerDeletePayload = {
  __typename?: 'OurPartnerDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ourPartner?: Maybe<OurPartnerNode>;
};

export type OurPartnerNode = CustomizeInterface & {
  __typename?: 'OurPartnerNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  image?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  sortOrder: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  site: SiteNode;
  position?: Maybe<Scalars['String']>;
};

export type OurPartnerNodeConnection = {
  __typename?: 'OurPartnerNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<OurPartnerNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `OurPartnerNode` and its cursor. */
export type OurPartnerNodeEdge = {
  __typename?: 'OurPartnerNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<OurPartnerNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type OurPartnerSort = {
  __typename?: 'OurPartnerSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type OurPartnerSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type OurPartnerUpdateInput = {
  image?: Maybe<Scalars['Upload']>;
  name?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type OurPartnerUpdatePayload = {
  __typename?: 'OurPartnerUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ourPartner?: Maybe<OurPartnerNode>;
};

export type OutgoingPayment = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type OutgoingPaymentAddressNode = {
  __typename?: 'OutgoingPaymentAddressNode';
  id: Scalars['ID'];
  address?: Maybe<UserAddressNode>;
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
};

export type OutgoingPaymentDetailNode = {
  __typename?: 'OutgoingPaymentDetailNode';
  id: Scalars['ID'];
  product?: Maybe<ProductForBuyNode>;
  productName: Scalars['String'];
  quantity: Scalars['Int'];
  unitPrice: Scalars['Float'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['Float']>;
  isReviewed: Scalars['Boolean'];
};

export type OutgoingPaymentError = {
  __typename?: 'OutgoingPaymentError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type OutgoingPaymentHistoryNode = {
  __typename?: 'OutgoingPaymentHistoryNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  status?: Maybe<OutgoingPaymentHistoryStatus>;
  description?: Maybe<Scalars['String']>;
  user?: Maybe<UserProfile>;
};

/** An enumeration. */
export enum OutgoingPaymentHistoryStatus {
  /** Comment */
  Comment = 'COMMENT',
  /** Action */
  Action = 'ACTION'
}

export type OutgoingPaymentNode = OutgoingPayment & {
  __typename?: 'OutgoingPaymentNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  createdBy?: Maybe<UserProfile>;
  totalAmount: Scalars['Float'];
  details?: Maybe<Array<Maybe<OutgoingPaymentDetailNode>>>;
  outgoingPaymentAddress?: Maybe<OutgoingPaymentAddressNode>;
  outgoingPaymentHistory?: Maybe<Array<Maybe<OutgoingPaymentHistoryNode>>>;
};

export type OutgoingPaymentNodeConnection = {
  __typename?: 'OutgoingPaymentNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<OutgoingPaymentNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `OutgoingPaymentNode` and its cursor. */
export type OutgoingPaymentNodeEdge = {
  __typename?: 'OutgoingPaymentNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<OutgoingPaymentNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum OutgoingPaymentPaymentStatusInput {
  Paid = 'PAID',
  WaitForPay = 'WAIT_FOR_PAY',
  Cancelled = 'CANCELLED',
  Fulfilled = 'FULFILLED',
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  Unfulfilled = 'UNFULFILLED',
  ReadyToCapture = 'READY_TO_CAPTURE',
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export type OutgoingPaymentProductInput = {
  productId?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export enum OutgoingPaymentStatusInput {
  Shipping = 'SHIPPING',
  Cancelled = 'CANCELLED',
  Delivered = 'DELIVERED',
  Pending = 'PENDING'
}

/** The Relay compliant `PageInfo` type, containing data necessary to paginate this connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']>;
};

export type PartyInfoNode = CustomizeInterface & {
  __typename?: 'PartyInfoNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  customer?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<ProvinceNode>;
  district?: Maybe<DistrictNode>;
  ward?: Maybe<WardNode>;
  createdDate?: Maybe<Scalars['DateTime']>;
  invitedNumber: Scalars['Int'];
  formatAddress?: Maybe<Scalars['String']>;
};

export type PartyInfoNodeConnection = {
  __typename?: 'PartyInfoNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<PartyInfoNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `PartyInfoNode` and its cursor. */
export type PartyInfoNodeEdge = {
  __typename?: 'PartyInfoNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<PartyInfoNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type PartyInforCreateInput = {
  invitedNumber?: Maybe<Scalars['Int']>;
  customer?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PartyInforCreatePayload = {
  __typename?: 'PartyInforCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  partyInfor?: Maybe<PartyInfoNode>;
};

export type PartyInforDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PartyInforDeletePayload = {
  __typename?: 'PartyInforDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  partyInfor?: Maybe<PartyInfoNode>;
};

export type PartyInforUpdateInput = {
  customer?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['DateTime']>;
  invitedNumber?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PartyInforUpdatePayload = {
  __typename?: 'PartyInforUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  partyInfor?: Maybe<PartyInfoNode>;
};

export type Payment = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type PaymentError = {
  __typename?: 'PaymentError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type PaymentMethodCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  status: PaymentMethodStatus;
  /** Set list of incoming payments */
  paymentIncoming?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  paymentInvoice?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  paymentOutgoing?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PaymentMethodCreatePayload = {
  __typename?: 'PaymentMethodCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<MutationErrorType>>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  paymentMethod?: Maybe<PaymentNode>;
};

export type PaymentMethodDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PaymentMethodDeletePayload = {
  __typename?: 'PaymentMethodDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<MutationErrorType>>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  paymentMethod?: Maybe<PaymentNode>;
};

/** An enumeration. */
export enum PaymentMethodStatus {
  /** Active */
  Active = 'ACTIVE',
  /** Inactive */
  Inactive = 'INACTIVE'
}

export type PaymentMethodUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  status?: Maybe<PaymentMethodStatus>;
  /** The ID of the object. */
  id: Scalars['ID'];
  /** Set list of incoming payments */
  paymentIncoming?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  paymentInvoice?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  paymentOutgoing?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PaymentMethodUpdatePayload = {
  __typename?: 'PaymentMethodUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<MutationErrorType>>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  paymentMethod?: Maybe<PaymentNode>;
};

export type PaymentNode = Payment & {
  __typename?: 'PaymentNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};

export type PaymentNodeConnection = {
  __typename?: 'PaymentNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<PaymentNodeEdge>>;
};

/** A Relay edge containing a `PaymentNode` and its cursor. */
export type PaymentNodeEdge = {
  __typename?: 'PaymentNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<PaymentNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum PaymentStatusInput {
  Paid = 'PAID',
  WaitForPay = 'WAIT_FOR_PAY',
  Cancelled = 'CANCELLED',
  Fulfilled = 'FULFILLED',
  PartiallyFulfilled = 'PARTIALLY_FULFILLED',
  Unfulfilled = 'UNFULFILLED',
  ReadyToCapture = 'READY_TO_CAPTURE',
  ReadyToFulfill = 'READY_TO_FULFILL'
}

export type PerError = {
  __typename?: 'PerError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type PermissionContent = {
  __typename?: 'PermissionContent';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type PermissionCreate = {
  __typename?: 'PermissionCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
};

export type PermissionCreateInput = {
  name: Scalars['String'];
  permission?: Maybe<PermissionTypeInput>;
};

export type PermissionDelete = {
  __typename?: 'PermissionDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
};

export type PermissionDeleteInput = {
  id: Scalars['String'];
};

export type PermissionFieldInput = {
  id: Scalars['String'];
  permission: PermissionTypeInput;
};

export type PermissionFieldNode = {
  __typename?: 'PermissionFieldNode';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  group?: Maybe<PermissionGroup>;
  permission?: Maybe<FunctionNode>;
};

export type PermissionGroup = {
  __typename?: 'PermissionGroup';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
};

export type PermissionTypeInput = {
  add?: Maybe<Scalars['Boolean']>;
  change?: Maybe<Scalars['Boolean']>;
  view?: Maybe<Scalars['Boolean']>;
  delete?: Maybe<Scalars['Boolean']>;
  export?: Maybe<Scalars['Boolean']>;
  changeDiscount?: Maybe<Scalars['Boolean']>;
  addTopping?: Maybe<Scalars['Boolean']>;
  viewAll?: Maybe<Scalars['Boolean']>;
};

export type PermissionUpdate = {
  __typename?: 'PermissionUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
};

export type PermissionUpdateInput = {
  id: Scalars['String'];
  addPermission?: Maybe<PermissionTypeInput>;
  removePermission?: Maybe<PermissionTypeInput>;
};

export type ProductAndVariantConnection = {
  __typename?: 'ProductAndVariantConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductAndVariantEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductAndVariant` and its cursor. */
export type ProductAndVariantEdge = {
  __typename?: 'ProductAndVariantEdge';
  /** The item at the end of the edge */
  node?: Maybe<ByProductAndVariant>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductBranchBranchNode = {
  __typename?: 'ProductBranchBranchNode';
  id: Scalars['ID'];
  branch: BranchNode;
  price: Scalars['Float'];
};

export type ProductBranchInput = {
  branchId: Scalars['String'];
  price?: Maybe<Scalars['Float']>;
};

export type ProductBranchInventory = {
  __typename?: 'ProductBranchInventory';
  warehouseId?: Maybe<Scalars['String']>;
  warehouseName?: Maybe<Scalars['String']>;
  productBranchOrdered?: Maybe<Array<Maybe<ProductBranchOrdered>>>;
};

export type ProductBranchOrdered = {
  __typename?: 'ProductBranchOrdered';
  branchName?: Maybe<Scalars['String']>;
  customerOrder?: Maybe<Scalars['Float']>;
};

export type ProductBranchProductNode = CustomizeInterface & {
  __typename?: 'ProductBranchProductNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  price: Scalars['Float'];
  product: ProductForBuyNode;
  variant?: Maybe<VariantNode>;
};

export type ProductBranchWarehouse = {
  __typename?: 'ProductBranchWarehouse';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Float']>;
  customerOrder?: Maybe<Scalars['Float']>;
};

export type ProductComboCreate = {
  __typename?: 'ProductComboCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
  productCombo?: Maybe<ProductForBuyNode>;
};

export type ProductComboDelete = {
  __typename?: 'ProductComboDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
};

export type ProductComboDeleteInput = {
  id: Scalars['String'];
};

export type ProductComboDetailInput = {
  product: Scalars['String'];
  quantity?: Maybe<Scalars['Int']>;
};

export type ProductComboDetailNode = {
  __typename?: 'ProductComboDetailNode';
  product: ProductForBuyNode;
  quantity: Scalars['Int'];
};

export type ProductComboInput = {
  product: Array<Maybe<ProductComboDetailInput>>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  thumbnail?: Maybe<Scalars['Upload']>;
  price: Scalars['Float'];
  site?: Maybe<Scalars['String']>;
  category: Scalars['String'];
};

export type ProductComboUpdate = {
  __typename?: 'ProductComboUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
  productCombo?: Maybe<ProductForBuyNode>;
};

export type ProductComboUpdateInput = {
  id: Scalars['String'];
  product?: Maybe<Array<Maybe<ProductComboDetailInput>>>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  visibility?: Maybe<Scalars['Boolean']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  thumbnail?: Maybe<Scalars['Upload']>;
  price?: Maybe<Scalars['Float']>;
  category?: Maybe<Scalars['String']>;
};

export type ProductConnection = {
  __typename?: 'ProductConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductEdge>>;
  totalAmount?: Maybe<Scalars['Float']>;
  totalQuantity?: Maybe<Scalars['Int']>;
};

export type ProductDeliveryInput = {
  productID?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export type ProductDescriptionCreateInput = {
  site?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  product: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductDescriptionCreatePayload = {
  __typename?: 'ProductDescriptionCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productDescription?: Maybe<ProductDescriptionNode>;
};

export type ProductDescriptionDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductDescriptionDeletePayload = {
  __typename?: 'ProductDescriptionDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productDescription?: Maybe<ProductDescriptionNode>;
};

export type ProductDescriptionDetailNode = {
  __typename?: 'ProductDescriptionDetailNode';
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type ProductDescriptionInput = {
  name: Scalars['String'];
  description: Scalars['String'];
};

export type ProductDescriptionNode = CustomizeInterface & {
  __typename?: 'ProductDescriptionNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  product?: Maybe<ProductForBuyNode>;
};

export type ProductDescriptionNodeConnection = {
  __typename?: 'ProductDescriptionNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductDescriptionNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductDescriptionNode` and its cursor. */
export type ProductDescriptionNodeEdge = {
  __typename?: 'ProductDescriptionNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductDescriptionNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductDescriptionUpdate = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
};

export type ProductDescriptionUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductDescriptionUpdatePayload = {
  __typename?: 'ProductDescriptionUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productDescription?: Maybe<ProductDescriptionNode>;
};

/** A Relay edge containing a `Product` and its cursor. */
export type ProductEdge = {
  __typename?: 'ProductEdge';
  /** The item at the end of the edge */
  node?: Maybe<ByProduct>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductError = {
  __typename?: 'ProductError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type ProductFeedbackCount = {
  __typename?: 'ProductFeedbackCount';
  ratingOne?: Maybe<Scalars['Int']>;
  ratingTwo?: Maybe<Scalars['Int']>;
  ratingThree?: Maybe<Scalars['Int']>;
  ratingFour?: Maybe<Scalars['Int']>;
  ratingFive?: Maybe<Scalars['Int']>;
  ratingAverage?: Maybe<Scalars['Float']>;
  totalRatingCount?: Maybe<Scalars['Int']>;
};

export type ProductFeedbackCreateInput = {
  createdTime?: Maybe<Scalars['DateTime']>;
  content?: Maybe<Scalars['String']>;
  userName: Scalars['String'];
  rating: Scalars['Float'];
  product: Scalars['ID'];
  variant?: Maybe<Scalars['ID']>;
  visibility: Scalars['Boolean'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductFeedbackCreatePayload = {
  __typename?: 'ProductFeedbackCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productFeedback?: Maybe<ProductFeedbackNode>;
};

export type ProductFeedbackDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductFeedbackDeletePayload = {
  __typename?: 'ProductFeedbackDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productFeedback?: Maybe<ProductFeedbackNode>;
};

export type ProductFeedbackNode = CustomizeInterface & {
  __typename?: 'ProductFeedbackNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  content?: Maybe<Scalars['String']>;
  userName: Scalars['String'];
  createdTime: Scalars['DateTime'];
  rating: Scalars['Float'];
  visibility: Scalars['Boolean'];
  sortOrder: Scalars['Int'];
};

export type ProductFeedbackNodeConnection = {
  __typename?: 'ProductFeedbackNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductFeedbackNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductFeedbackNode` and its cursor. */
export type ProductFeedbackNodeEdge = {
  __typename?: 'ProductFeedbackNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductFeedbackNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductFeedbackSort = {
  __typename?: 'ProductFeedbackSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type ProductFeedbackUpdateInput = {
  content?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
  createdTime?: Maybe<Scalars['DateTime']>;
  rating?: Maybe<Scalars['Float']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductFeedbackUpdatePayload = {
  __typename?: 'ProductFeedbackUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productFeedback?: Maybe<ProductFeedbackNode>;
};

export type ProductFlashSaleInput = {
  product: Scalars['String'];
  price: Scalars['Float'];
};

export type ProductFlashSaleNode = CustomizeInterface & {
  __typename?: 'ProductFlashSaleNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  flashSale?: Maybe<FlashSaleNode>;
  product?: Maybe<ProductForBuyNode>;
  price: Scalars['Float'];
};

export type ProductFlashSaleNodeConnection = {
  __typename?: 'ProductFlashSaleNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductFlashSaleNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductFlashSaleNode` and its cursor. */
export type ProductFlashSaleNodeEdge = {
  __typename?: 'ProductFlashSaleNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductFlashSaleNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductForBetCreateInput = {
  category: Scalars['String'];
  images?: Maybe<Array<Maybe<Scalars['Upload']>>>;
  maxTicket: Scalars['Int'];
  tradability: Scalars['Boolean'];
  tradabilityDate?: Maybe<Scalars['DateTime']>;
  drawDate?: Maybe<Scalars['DateTime']>;
  site?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sku?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  barcode?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  gif?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  brand?: Maybe<Scalars['ID']>;
  isStock?: Maybe<Scalars['Boolean']>;
  isManageInventory: Scalars['Boolean'];
  isSellOutOfStock: Scalars['Boolean'];
  tagImage?: Maybe<Scalars['UploadType']>;
  gifFile?: Maybe<Scalars['UploadType']>;
  /** Set list of product flash sales */
  flashSaleProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of food processing types */
  foodTypeProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of products */
  subProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBetCreatePayload = {
  __typename?: 'ProductForBetCreatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  product?: Maybe<ProductForBuyNode>;
};

export type ProductForBetHistoryNode = {
  __typename?: 'ProductForBetHistoryNode';
  id?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  visibility?: Maybe<Scalars['Boolean']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  tradability?: Maybe<Scalars['Boolean']>;
  tradabilityDate?: Maybe<Scalars['DateTime']>;
  category?: Maybe<CategoryNode>;
  costPerBet?: Maybe<Scalars['Int']>;
  maxTicket?: Maybe<Scalars['Int']>;
  drawDate?: Maybe<Scalars['DateTime']>;
  isDrawed?: Maybe<Scalars['Boolean']>;
  luckyNumber?: Maybe<Array<Maybe<Scalars['Int']>>>;
  status?: Maybe<Scalars['String']>;
  lotteryResult?: Maybe<Scalars['String']>;
  winner?: Maybe<UserProfile>;
};

export type ProductForBetNode = ProductNode & {
  __typename?: 'ProductForBetNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  created: Scalars['DateTime'];
  category: CategoryNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibility: Scalars['Boolean'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  productType: ProductProductType;
  sku?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate: Scalars['Float'];
  totalReview: Scalars['Int'];
  barcode?: Maybe<Scalars['String']>;
  isTopping: Scalars['Boolean'];
  isToppingGroup: Scalars['Boolean'];
  isToppingGroupMandatory: Scalars['Boolean'];
  parent?: Maybe<ProductForBuyNode>;
  productImage: Array<ProductImageNode>;
  tags: Array<ProductTagRelatedNode>;
  brand?: Maybe<BrandNode>;
  isManageInventory: Scalars['Boolean'];
  isSellOutOfStock: Scalars['Boolean'];
  advertisingPrice?: Maybe<Scalars['Float']>;
  tag?: Maybe<Scalars['String']>;
  supplier: Array<ProductSupplierNode>;
  costPerBet?: Maybe<Scalars['Float']>;
  maxTicket?: Maybe<Scalars['Int']>;
  tradability?: Maybe<Scalars['Boolean']>;
  tradabilityDate?: Maybe<Scalars['DateTime']>;
  bettors?: Maybe<Array<Maybe<ProductForBetTransactionNode>>>;
  drawDate?: Maybe<Scalars['DateTime']>;
  isDrawed?: Maybe<Scalars['Boolean']>;
  winner?: Maybe<ProductForBetTransactionNode>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionDetailNode>>>;
  unit?: Maybe<Array<Maybe<ProductUnitNode>>>;
  unitExchange?: Maybe<Array<Maybe<ProductUnitNode>>>;
  unitParallel?: Maybe<Array<Maybe<ProductUnitNode>>>;
};

export type ProductForBetNodeConnection = {
  __typename?: 'ProductForBetNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductForBetNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductForBetNode` and its cursor. */
export type ProductForBetNodeEdge = {
  __typename?: 'ProductForBetNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductForBetNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductForBetResult = {
  __typename?: 'ProductForBetResult';
  status?: Maybe<Scalars['Boolean']>;
  result?: Maybe<Scalars['String']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
};

export type ProductForBetResultInput = {
  product: Scalars['String'];
};

export type ProductForBetTransactionNode = BetHistory & {
  __typename?: 'ProductForBetTransactionNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  product: ProductForBuyNode;
  user: UserProfile;
  luckyNumber: Scalars['Int'];
  status: ProductForBetTransactionStatus;
};

export type ProductForBetTransactionNodeConnection = {
  __typename?: 'ProductForBetTransactionNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductForBetTransactionNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductForBetTransactionNode` and its cursor. */
export type ProductForBetTransactionNodeEdge = {
  __typename?: 'ProductForBetTransactionNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductForBetTransactionNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum ProductForBetTransactionStatus {
  /** Won */
  Won = 'WON',
  /** Pending */
  Pending = 'PENDING',
  /** Missed */
  Missed = 'MISSED'
}

export type ProductForBetUpdateInput = {
  category?: Maybe<Scalars['String']>;
  images?: Maybe<Array<Maybe<Scalars['Upload']>>>;
  imagesDelete?: Maybe<Array<Maybe<Scalars['String']>>>;
  tradability: Scalars['Boolean'];
  maxTicket?: Maybe<Scalars['Int']>;
  tradabilityDate?: Maybe<Scalars['DateTime']>;
  drawDate?: Maybe<Scalars['DateTime']>;
  site?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  collectionsRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sku?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate?: Maybe<Scalars['Float']>;
  totalReview?: Maybe<Scalars['Int']>;
  barcode?: Maybe<Scalars['String']>;
  advertisingPrice?: Maybe<Scalars['Float']>;
  tag?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  gif?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility: Scalars['Boolean'];
  brand?: Maybe<Scalars['ID']>;
  isStock?: Maybe<Scalars['Boolean']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  isToppingGroupMandatory?: Maybe<Scalars['Boolean']>;
  parent?: Maybe<Scalars['ID']>;
  isManageInventory?: Maybe<Scalars['Boolean']>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  tagImage?: Maybe<Scalars['UploadType']>;
  gifFile?: Maybe<Scalars['UploadType']>;
  lft?: Maybe<Scalars['Int']>;
  rght?: Maybe<Scalars['Int']>;
  treeId?: Maybe<Scalars['Int']>;
  level?: Maybe<Scalars['Int']>;
  /** Set list of product flash sales */
  flashSaleProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of food processing types */
  foodTypeProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of products */
  subProduct?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBetUpdatePayload = {
  __typename?: 'ProductForBetUpdatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  product?: Maybe<ProductForBuyNode>;
};

export type ProductForBuyCreateInput = {
  category?: Maybe<Scalars['String']>;
  images?: Maybe<Array<Maybe<Scalars['Upload']>>>;
  supplierId?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  tag?: Maybe<Scalars['String']>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionInput>>>;
  unitAdd?: Maybe<Array<Maybe<ProductUnitInput>>>;
  unitMain?: Maybe<Scalars['String']>;
  variants?: Maybe<Array<Maybe<VariantInput>>>;
  parentId?: Maybe<Scalars['String']>;
  relatedProduct?: Maybe<Array<Maybe<Scalars['String']>>>;
  branchList?: Maybe<Array<Maybe<ProductBranchInput>>>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sku?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate?: Maybe<Scalars['Float']>;
  totalReview?: Maybe<Scalars['Int']>;
  barcode?: Maybe<Scalars['String']>;
  advertisingPrice?: Maybe<Scalars['Float']>;
  video?: Maybe<Scalars['String']>;
  gif?: Maybe<Scalars['String']>;
  visibility: Scalars['Boolean'];
  brand?: Maybe<Scalars['ID']>;
  isStock?: Maybe<Scalars['Boolean']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  isToppingGroupMandatory?: Maybe<Scalars['Boolean']>;
  isManageInventory?: Maybe<Scalars['Boolean']>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  tagImage?: Maybe<Scalars['Upload']>;
  gifFile?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBuyCreatePayload = {
  __typename?: 'ProductForBuyCreatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  /** The mutated object. */
  product?: Maybe<ProductForBuyNode>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBuyDeleteInput = {
  site?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBuyDeletePayload = {
  __typename?: 'ProductForBuyDeletePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  product?: Maybe<ProductForBuyNode>;
};

export type ProductForBuyNode = ProductNode & {
  __typename?: 'ProductForBuyNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  created: Scalars['DateTime'];
  category: CategoryNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibility: Scalars['Boolean'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  productType: ProductProductType;
  sku?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  isStock?: Maybe<Scalars['Boolean']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate: Scalars['Float'];
  totalReview: Scalars['Int'];
  barcode?: Maybe<Scalars['String']>;
  isTopping: Scalars['Boolean'];
  isToppingGroup: Scalars['Boolean'];
  isToppingGroupMandatory: Scalars['Boolean'];
  parent?: Maybe<ProductForBuyNode>;
  productImage: Array<ProductImageNode>;
  tags: Array<ProductTagRelatedNode>;
  flashSaleProduct: ProductFlashSaleNodeConnection;
  isManageInventory: Scalars['Boolean'];
  isSellOutOfStock: Scalars['Boolean'];
  advertisingPrice?: Maybe<Scalars['Float']>;
  tag?: Maybe<Scalars['String']>;
  video?: Maybe<Scalars['String']>;
  tagImage?: Maybe<Scalars['String']>;
  gif?: Maybe<Scalars['String']>;
  gifFile?: Maybe<Scalars['String']>;
  variant?: Maybe<VariantNodeConnection>;
  productCombo?: Maybe<Array<Maybe<ProductComboDetailNode>>>;
  collections?: Maybe<Array<Maybe<CollectionNode>>>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionDetailNode>>>;
  productPricesList?: Maybe<Array<Maybe<ProductPricesListNode>>>;
  unit?: Maybe<Array<Maybe<ProductUnitNode>>>;
  toppingList?: Maybe<Array<Maybe<ToppingNode>>>;
  inventory?: Maybe<Array<Maybe<ProductBranchWarehouse>>>;
  totalQuantityInventory?: Maybe<Scalars['Float']>;
  totalOrderCustomer?: Maybe<Scalars['Float']>;
  unitExchange?: Maybe<Array<Maybe<ProductUnitNode>>>;
  unitParallel?: Maybe<Array<Maybe<ProductUnitNode>>>;
  inventoryDetail?: Maybe<Array<Maybe<ProductInventory>>>;
  inventoryBranchDetail?: Maybe<Array<Maybe<ProductBranchInventory>>>;
  productFeedback?: Maybe<Array<Maybe<ProductFeedbackNode>>>;
  productFeedbackCount?: Maybe<ProductFeedbackCount>;
  relatedProduct?: Maybe<Array<Maybe<ProductForBuyNode>>>;
  branchList?: Maybe<Array<Maybe<ProductBranchBranchNode>>>;
  likeCount?: Maybe<Scalars['Int']>;
};


export type ProductForBuyNodeFlashSaleProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ProductForBuyNodeVariantArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  attribute?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type ProductForBuyNodeConnection = {
  __typename?: 'ProductForBuyNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductForBuyNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductForBuyNode` and its cursor. */
export type ProductForBuyNodeEdge = {
  __typename?: 'ProductForBuyNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductForBuyNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductForBuyUpdateInput = {
  category?: Maybe<Scalars['String']>;
  images?: Maybe<Array<Maybe<Scalars['Upload']>>>;
  imagesDelete?: Maybe<Array<Maybe<Scalars['String']>>>;
  supplierId?: Maybe<Scalars['String']>;
  supplierRemove?: Maybe<Scalars['String']>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  collectionsRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  tagRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  tag?: Maybe<Scalars['String']>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionUpdate>>>;
  unitAdd?: Maybe<Array<Maybe<ProductUnitInput>>>;
  unitUpdate?: Maybe<Array<Maybe<ProductUnitInput>>>;
  unitRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  unitMain?: Maybe<Scalars['String']>;
  variants?: Maybe<Array<Maybe<VariantInput>>>;
  parentId?: Maybe<Scalars['String']>;
  variantDetailId?: Maybe<Scalars['String']>;
  relatedProduct?: Maybe<Array<Maybe<Scalars['String']>>>;
  branchList?: Maybe<Array<Maybe<ProductBranchInput>>>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sku?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate?: Maybe<Scalars['Float']>;
  totalReview?: Maybe<Scalars['Int']>;
  barcode?: Maybe<Scalars['String']>;
  advertisingPrice?: Maybe<Scalars['Float']>;
  video?: Maybe<Scalars['String']>;
  gif?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  visibility?: Maybe<Scalars['Boolean']>;
  brand?: Maybe<Scalars['ID']>;
  isStock?: Maybe<Scalars['Boolean']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  isToppingGroupMandatory?: Maybe<Scalars['Boolean']>;
  isManageInventory?: Maybe<Scalars['Boolean']>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  tagImage?: Maybe<Scalars['Upload']>;
  gifFile?: Maybe<Scalars['Upload']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductForBuyUpdatePayload = {
  __typename?: 'ProductForBuyUpdatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  product?: Maybe<ProductForBuyNode>;
};

export type ProductImageNode = {
  __typename?: 'ProductImageNode';
  id: Scalars['ID'];
  image?: Maybe<Scalars['String']>;
};

export type ProductInput = {
  productID: Scalars['String'];
  variantID?: Maybe<Scalars['String']>;
  quantity: Scalars['Int'];
  topping?: Maybe<Array<Maybe<ToppingInput>>>;
  orderDetailNote?: Maybe<Scalars['String']>;
  productPricesListId?: Maybe<Scalars['String']>;
  unitList?: Maybe<Array<Maybe<ProductUnitQuantityInput>>>;
};

export type ProductInventory = {
  __typename?: 'ProductInventory';
  warehouseId?: Maybe<Scalars['String']>;
  warehouseName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Float']>;
  productBranchOrdered?: Maybe<Array<Maybe<ProductBranchOrdered>>>;
};

export type ProductLikeAction = {
  __typename?: 'ProductLikeAction';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
  product?: Maybe<ProductForBuyNode>;
  variant?: Maybe<VariantNode>;
};

export type ProductNode = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ProductPricesCreateInput = {
  price?: Maybe<Scalars['Float']>;
  productPricesList: Scalars['ID'];
  product: Scalars['ID'];
  variant?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesCreatePayload = {
  __typename?: 'ProductPricesCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPrices?: Maybe<ProductPricesNode>;
};

export type ProductPricesDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesDeletePayload = {
  __typename?: 'ProductPricesDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPrices?: Maybe<ProductPricesNode>;
};

export type ProductPricesListBranchNode = CustomizeInterface & {
  __typename?: 'ProductPricesListBranchNode';
  productPricesList: ProductPricesListNode;
  branch: BranchNode;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ProductPricesListBranchNodeConnection = {
  __typename?: 'ProductPricesListBranchNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesListBranchNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesListBranchNode` and its cursor. */
export type ProductPricesListBranchNodeEdge = {
  __typename?: 'ProductPricesListBranchNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesListBranchNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductPricesListCreateInput = {
  user?: Maybe<Array<Maybe<Scalars['String']>>>;
  userGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  branch?: Maybe<Array<Maybe<Scalars['String']>>>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  name: Scalars['String'];
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  userScope?: Maybe<ProductPricesListUserScope>;
  isActiveAllUser?: Maybe<Scalars['Boolean']>;
  isActiveAllBranch?: Maybe<Scalars['Boolean']>;
  isActiveAllCustomer?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesListCreatePayload = {
  __typename?: 'ProductPricesListCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPricesList?: Maybe<ProductPricesListNode>;
};

export type ProductPricesListCustomerGroupNode = CustomizeInterface & {
  __typename?: 'ProductPricesListCustomerGroupNode';
  productPricesList: ProductPricesListNode;
  customerGroup: CustomerGroupNode;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ProductPricesListCustomerGroupNodeConnection = {
  __typename?: 'ProductPricesListCustomerGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesListCustomerGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesListCustomerGroupNode` and its cursor. */
export type ProductPricesListCustomerGroupNodeEdge = {
  __typename?: 'ProductPricesListCustomerGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesListCustomerGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductPricesListDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesListDeletePayload = {
  __typename?: 'ProductPricesListDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPricesList?: Maybe<ProductPricesListNode>;
};

export type ProductPricesListNode = CustomizeInterface & {
  __typename?: 'ProductPricesListNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  name: Scalars['String'];
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  isActiveAllUser: Scalars['Boolean'];
  userScope: ProductPricesListUserScope;
  isActiveAllBranch: Scalars['Boolean'];
  isActiveAllCustomer: Scalars['Boolean'];
  itemCode?: Maybe<Scalars['String']>;
  userList?: Maybe<ProductPricesListUserNodeConnection>;
  userGroupList?: Maybe<ProductPricesListUserGroupNodeConnection>;
  branchList?: Maybe<ProductPricesListBranchNodeConnection>;
  customerGroupList?: Maybe<ProductPricesListCustomerGroupNodeConnection>;
};


export type ProductPricesListNodeUserListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ProductPricesListNodeUserGroupListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ProductPricesListNodeBranchListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ProductPricesListNodeCustomerGroupListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type ProductPricesListNodeConnection = {
  __typename?: 'ProductPricesListNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesListNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesListNode` and its cursor. */
export type ProductPricesListNodeEdge = {
  __typename?: 'ProductPricesListNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesListNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductPricesListUpdateInput = {
  user?: Maybe<Array<Maybe<Scalars['String']>>>;
  userGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  branch?: Maybe<Array<Maybe<Scalars['String']>>>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  name?: Maybe<Scalars['String']>;
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  userScope?: Maybe<ProductPricesListUserScope>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActiveAllUser?: Maybe<Scalars['Boolean']>;
  isActiveAllBranch?: Maybe<Scalars['Boolean']>;
  isActiveAllCustomer?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesListUpdatePayload = {
  __typename?: 'ProductPricesListUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPricesList?: Maybe<ProductPricesListNode>;
};

export type ProductPricesListUserGroupNode = CustomizeInterface & {
  __typename?: 'ProductPricesListUserGroupNode';
  productPricesList: ProductPricesListNode;
  userGroup: StaffGroupNode;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ProductPricesListUserGroupNodeConnection = {
  __typename?: 'ProductPricesListUserGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesListUserGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesListUserGroupNode` and its cursor. */
export type ProductPricesListUserGroupNodeEdge = {
  __typename?: 'ProductPricesListUserGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesListUserGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductPricesListUserNode = CustomizeInterface & {
  __typename?: 'ProductPricesListUserNode';
  productPricesList: ProductPricesListNode;
  user: UserProfile;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ProductPricesListUserNodeConnection = {
  __typename?: 'ProductPricesListUserNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesListUserNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesListUserNode` and its cursor. */
export type ProductPricesListUserNodeEdge = {
  __typename?: 'ProductPricesListUserNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesListUserNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum ProductPricesListUserScope {
  /** All_user */
  AllUser = 'ALL_USER',
  /** Staff */
  Staff = 'STAFF',
  /** Staff_group */
  StaffGroup = 'STAFF_GROUP',
  /** Staff_and_staff_group */
  StaffAndStaffGroup = 'STAFF_AND_STAFF_GROUP'
}

export type ProductPricesNode = CustomizeInterface & {
  __typename?: 'ProductPricesNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  productPricesList: ProductPricesListNode;
  price: Scalars['Float'];
  variant?: Maybe<VariantNode>;
  product?: Maybe<ProductForBuyNode>;
};

export type ProductPricesNodeConnection = {
  __typename?: 'ProductPricesNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductPricesNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductPricesNode` and its cursor. */
export type ProductPricesNodeEdge = {
  __typename?: 'ProductPricesNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductPricesNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductPricesUpdateInput = {
  price?: Maybe<Scalars['Float']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductPricesUpdatePayload = {
  __typename?: 'ProductPricesUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productPrices?: Maybe<ProductPricesNode>;
};

/** An enumeration. */
export enum ProductProductType {
  /** Bet */
  Bet = 'BET',
  /** Buy */
  Buy = 'BUY',
  /** Combo */
  Combo = 'COMBO'
}

export type ProductRelatedResult = {
  __typename?: 'ProductRelatedResult';
  product?: Maybe<ProductForBetNode>;
};

export type ProductSort = {
  __typename?: 'ProductSort';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
};

export type ProductSortInput = {
  id: Scalars['String'];
  sortOrder: Scalars['Int'];
};

export type ProductSupplierNode = {
  __typename?: 'ProductSupplierNode';
  product: ProductForBuyNode;
  supplier: SupplierNode;
};

export type ProductTagCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductTagCreatePayload = {
  __typename?: 'ProductTagCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<ProductTagNode>;
};

export type ProductTagDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductTagDeletePayload = {
  __typename?: 'ProductTagDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<ProductTagNode>;
};

export type ProductTagNode = CustomizeInterface & {
  __typename?: 'ProductTagNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  product: Array<ProductTagRelatedNode>;
};

export type ProductTagNodeConnection = {
  __typename?: 'ProductTagNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductTagNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductTagNode` and its cursor. */
export type ProductTagNodeEdge = {
  __typename?: 'ProductTagNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductTagNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductTagRelatedNode = {
  __typename?: 'ProductTagRelatedNode';
  id: Scalars['ID'];
  product: ProductForBuyNode;
  tag: ProductTagNode;
};

export type ProductTagUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductTagUpdatePayload = {
  __typename?: 'ProductTagUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<ProductTagNode>;
};

export type ProductToppingCreateInput = {
  price?: Maybe<Scalars['Float']>;
  product: Scalars['ID'];
  variant?: Maybe<Scalars['ID']>;
  topping: Scalars['ID'];
  isMandatory?: Maybe<Scalars['Boolean']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductToppingCreatePayload = {
  __typename?: 'ProductToppingCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productToppings?: Maybe<ProductToppingNode>;
};

export type ProductToppingDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductToppingDeletePayload = {
  __typename?: 'ProductToppingDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productToppings?: Maybe<ProductToppingNode>;
};

export type ProductToppingNode = CustomizeInterface & {
  __typename?: 'ProductToppingNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  price: Scalars['Float'];
  isMandatory: Scalars['Boolean'];
  isDefault: Scalars['Boolean'];
  product?: Maybe<ProductForBuyNode>;
  topping?: Maybe<ProductForBuyNode>;
};

export type ProductToppingNodeConnection = {
  __typename?: 'ProductToppingNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductToppingNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
  toppingGroup?: Maybe<Array<Maybe<ProductForBuyNode>>>;
};

/** A Relay edge containing a `ProductToppingNode` and its cursor. */
export type ProductToppingNodeEdge = {
  __typename?: 'ProductToppingNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductToppingNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductToppingUpdateInput = {
  price?: Maybe<Scalars['Float']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isMandatory?: Maybe<Scalars['Boolean']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProductToppingUpdatePayload = {
  __typename?: 'ProductToppingUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  productToppings?: Maybe<ProductToppingNode>;
};

export type ProductUnitInput = {
  id: Scalars['String'];
  exchangeValue?: Maybe<Scalars['Float']>;
  price?: Maybe<Scalars['Float']>;
  productUnitType?: Maybe<ProductUnitTypeInput>;
};

export type ProductUnitNode = CustomizeInterface & {
  __typename?: 'ProductUnitNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  unit: UnitNode;
  isMain: Scalars['Boolean'];
  exchangeValue: Scalars['Float'];
  price: Scalars['Float'];
  variant?: Maybe<VariantNode>;
  productUnitType?: Maybe<ProductUnitProductUnitType>;
};

/** An enumeration. */
export enum ProductUnitProductUnitType {
  /** Exchange */
  Exchange = 'EXCHANGE',
  /** Parallel */
  Parallel = 'PARALLEL',
  /** Parallel_and_exchange */
  ParallelAndExchange = 'PARALLEL_AND_EXCHANGE'
}

export type ProductUnitQuantityInput = {
  unitId: Scalars['String'];
  quantity: Scalars['Int'];
};

export enum ProductUnitTypeInput {
  Exchange = 'EXCHANGE',
  Parallel = 'PARALLEL',
  ParallelAndExchange = 'PARALLEL_AND_EXCHANGE'
}

export type ProductUserConnection = {
  __typename?: 'ProductUserConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductUserEdge>>;
  totalQuantity?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
};

/** A Relay edge containing a `ProductUser` and its cursor. */
export type ProductUserEdge = {
  __typename?: 'ProductUserEdge';
  /** The item at the end of the edge */
  node?: Maybe<ByUserByProduct>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductVariantDelete = {
  __typename?: 'ProductVariantDelete';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
};

export type ProductVariantDeleteInput = {
  productId?: Maybe<Scalars['String']>;
  variantId?: Maybe<Scalars['String']>;
};

export type ProductVariantInput = {
  productID: Scalars['String'];
  variantID?: Maybe<Scalars['String']>;
};

export type ProductVariantLikeInput = {
  productId: Scalars['String'];
  variantId?: Maybe<Scalars['String']>;
};

export type ProductVariantPriceInput = {
  productId: Scalars['String'];
  variantId?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
};

export type ProductVariantReceiptInput = {
  productID: Scalars['String'];
  variantID?: Maybe<Scalars['String']>;
  quantity: Scalars['Float'];
  costPrices: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
};

export type ProductVariantReceiptReturnInput = {
  productID: Scalars['String'];
  variantID?: Maybe<Scalars['String']>;
  quantity: Scalars['Float'];
  costPrices: Scalars['Float'];
  note?: Maybe<Scalars['String']>;
  warehouse: Scalars['String'];
};

export type ProductViewedRecentNode = {
  __typename?: 'ProductViewedRecentNode';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProductViewedRecentNodeEdge>>;
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProductViewedRecentNode` and its cursor. */
export type ProductViewedRecentNodeEdge = {
  __typename?: 'ProductViewedRecentNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProductForBetNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProductsRecentViewedNode = {
  __typename?: 'ProductsRecentViewedNode';
  product?: Maybe<ProductViewedRecentNode>;
  id?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
};


export type ProductsRecentViewedNodeProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type PromotionActivate = {
  __typename?: 'PromotionActivate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  promotion?: Maybe<PromotionNode>;
};

export type PromotionCodeActivate = {
  __typename?: 'PromotionCodeActivate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
  promotionCode?: Maybe<PromotionCodeNode>;
};

export type PromotionCodeCreateInput = {
  image?: Maybe<Scalars['Upload']>;
  discount?: Maybe<Scalars['Float']>;
  endTime?: Maybe<Scalars['DateTime']>;
  startTime?: Maybe<Scalars['DateTime']>;
  quantity?: Maybe<Scalars['Int']>;
  isActive?: Maybe<Scalars['Boolean']>;
  unitDiscount?: Maybe<UnitDiscountInput>;
  limitQuantity?: Maybe<Scalars['Boolean']>;
  isPublic?: Maybe<Scalars['Boolean']>;
  title?: Maybe<Scalars['String']>;
  code: Scalars['String'];
  limitDiscount?: Maybe<Scalars['Float']>;
  description?: Maybe<Scalars['String']>;
  limitAmount?: Maybe<Scalars['Float']>;
  /** Set list of order discounts */
  orderDiscount?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionCodeCreatePayload = {
  __typename?: 'PromotionCodeCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotionCode?: Maybe<PromotionCodeNode>;
};

export type PromotionCodeDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionCodeDeletePayload = {
  __typename?: 'PromotionCodeDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotionCode?: Maybe<PromotionCodeNode>;
};

export type PromotionCodeInput = {
  id: Scalars['String'];
};

export type PromotionCodeNode = CustomizeInterface & {
  __typename?: 'PromotionCodeNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  title?: Maybe<Scalars['String']>;
  code: Scalars['String'];
  unitDiscount: PromotionCodeUnitDiscount;
  discount: Scalars['Float'];
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  limitDiscount?: Maybe<Scalars['Float']>;
  quantity: Scalars['Int'];
  isActive: Scalars['Boolean'];
  description?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['String']>;
  limitAmount?: Maybe<Scalars['Float']>;
  limitQuantity: Scalars['Boolean'];
  isPublic: Scalars['Boolean'];
};

export type PromotionCodeNodeConnection = {
  __typename?: 'PromotionCodeNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<PromotionCodeNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `PromotionCodeNode` and its cursor. */
export type PromotionCodeNodeEdge = {
  __typename?: 'PromotionCodeNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<PromotionCodeNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum PromotionCodeUnitDiscount {
  /** Percent */
  Percent = 'PERCENT',
  /** vnd */
  Vnd = 'VND'
}

export type PromotionCodeUpdateInput = {
  title?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  unitDiscount?: Maybe<PromotionCodeUnitDiscount>;
  discount?: Maybe<Scalars['Float']>;
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  limitDiscount?: Maybe<Scalars['Float']>;
  quantity?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  limitAmount?: Maybe<Scalars['Float']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  image?: Maybe<Scalars['Upload']>;
  limitQuantity?: Maybe<Scalars['Boolean']>;
  isPublic?: Maybe<Scalars['Boolean']>;
  /** Set list of order discounts */
  orderDiscount?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionCodeUpdatePayload = {
  __typename?: 'PromotionCodeUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotionCode?: Maybe<PromotionCodeNode>;
};

export type PromotionCreateInput = {
  image?: Maybe<Scalars['Upload']>;
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  percent: Scalars['Int'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isActive: Scalars['Boolean'];
  /** Set list of order discounts */
  orderDiscount?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionCreatePayload = {
  __typename?: 'PromotionCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotion?: Maybe<PromotionNode>;
};

export type PromotionDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionDeletePayload = {
  __typename?: 'PromotionDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotion?: Maybe<PromotionNode>;
};

export type PromotionInput = {
  id: Scalars['String'];
};

export type PromotionNode = CustomizeInterface & {
  __typename?: 'PromotionNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  startTime: Scalars['DateTime'];
  endTime: Scalars['DateTime'];
  percent: Scalars['Int'];
  isActive: Scalars['Boolean'];
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  image?: Maybe<Scalars['String']>;
};

export type PromotionNodeConnection = {
  __typename?: 'PromotionNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<PromotionNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `PromotionNode` and its cursor. */
export type PromotionNodeEdge = {
  __typename?: 'PromotionNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<PromotionNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type PromotionUpdateInput = {
  startTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  percent?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  image?: Maybe<Scalars['Upload']>;
  /** Set list of order discounts */
  orderDiscount?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type PromotionUpdatePayload = {
  __typename?: 'PromotionUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  promotion?: Maybe<PromotionNode>;
};

export type ProvinceBranchCreateInput = {
  province: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProvinceBranchCreatePayload = {
  __typename?: 'ProvinceBranchCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  provinceBranch?: Maybe<ProvinceBranchNode>;
};

export type ProvinceBranchDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ProvinceBranchDeletePayload = {
  __typename?: 'ProvinceBranchDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  provinceBranch?: Maybe<ProvinceBranchNode>;
};

export type ProvinceBranchNode = CustomizeInterface & {
  __typename?: 'ProvinceBranchNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  province: ProvinceNode;
};

export type ProvinceBranchNodeConnection = {
  __typename?: 'ProvinceBranchNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProvinceBranchNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProvinceBranchNode` and its cursor. */
export type ProvinceBranchNodeEdge = {
  __typename?: 'ProvinceBranchNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProvinceBranchNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ProvinceNode = CustomizeInterface & {
  __typename?: 'ProvinceNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  nameProvince?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  sortId: Scalars['Int'];
};

export type ProvinceNodeConnection = {
  __typename?: 'ProvinceNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ProvinceNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ProvinceNode` and its cursor. */
export type ProvinceNodeEdge = {
  __typename?: 'ProvinceNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ProvinceNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  partyInfos?: Maybe<PartyInfoNodeConnection>;
  partyInfo?: Maybe<PartyInfoNode>;
  permission?: Maybe<Array<Maybe<ContentTypeNode>>>;
  groupPermission?: Maybe<Array<Maybe<UserGroupPermissionNode>>>;
  userBranchPermission?: Maybe<Array<Maybe<UserBranchPermissionNode>>>;
  siteDeliveryDetailView?: Maybe<Array<Maybe<SiteDeliveryMethodNode>>>;
  deliveryMethods?: Maybe<DeliveryMethodNodeConnection>;
  siteDeliveriesMethod?: Maybe<SiteDeliveryMethodNodeConnection>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
  kitchenOrderView?: Maybe<KitchenOrderNodeConnection>;
  kitchenOrderDetail?: Maybe<KitchenOrderNode>;
  foodProcessingTypeView?: Maybe<Array<Maybe<FoodProcessingTypeNode>>>;
  siteConfigView?: Maybe<SiteConfigNodeConnection>;
  siteConfigDetailView?: Maybe<SiteConfigNode>;
  siteConfigShip60View?: Maybe<SiteConfigShip60NodeConnection>;
  siteConfigShip60DetailView?: Maybe<SiteConfigShip60Node>;
  flashSale?: Maybe<FlashSaleNodeConnection>;
  flashSaleDetail?: Maybe<FlashSaleNode>;
  promotion?: Maybe<PromotionNodeConnection>;
  promotionDetail?: Maybe<PromotionNode>;
  promotionCodes?: Maybe<PromotionCodeNodeConnection>;
  promotionCodeDetail?: Maybe<PromotionCodeNode>;
  ourPartners?: Maybe<OurPartnerNodeConnection>;
  ourPartner?: Maybe<OurPartnerNode>;
  customerReviews?: Maybe<CustomerReviewNodeConnection>;
  customerReview?: Maybe<CustomerReviewNode>;
  outgoingPaymentsView?: Maybe<OutgoingPaymentNodeConnection>;
  /** The ID of the object */
  outgoingPaymentDetailView?: Maybe<OutgoingPaymentNode>;
  incomingPaymentsView?: Maybe<IncomingPaymentNodeConnection>;
  /** The ID of the object */
  incomingPaymentDetailView?: Maybe<IncomingPaymentNode>;
  invoicesView?: Maybe<InvoiceNodeConnection>;
  /** The ID of the object */
  invoiceDetailView?: Maybe<InvoiceNode>;
  deliveryGroups?: Maybe<DeliveryGroupNodeConnection>;
  deliveryGroupDetail?: Maybe<DeliveryGroupNode>;
  deliveriesView?: Maybe<DeliveryNodeConnection>;
  /** The ID of the object */
  deliveryDetailView?: Maybe<DeliveryNode>;
  kiotvietInformation?: Maybe<Array<Maybe<KiotVietInformationNode>>>;
  deliveryTracking?: Maybe<DeliveryTrackingNodeConnection>;
  deliveryReview?: Maybe<DeliveryReviewNodeConnection>;
  deliveryBranch?: Maybe<Array<Maybe<DeliveryBranchNode>>>;
  deliveryBranchDetail?: Maybe<DeliveryBranchNode>;
  collections?: Maybe<CollectionNodeConnection>;
  collection?: Maybe<CollectionNode>;
  contactUs?: Maybe<ContactUsNodeConnection>;
  contactUsDetail?: Maybe<ContactUsNode>;
  feedbacks?: Maybe<FeedbackNodeConnection>;
  feedback?: Maybe<FeedbackNode>;
  productFeedbacks?: Maybe<ProductFeedbackNodeConnection>;
  productFeedback?: Maybe<ProductFeedbackNode>;
  warehouses?: Maybe<WarehouseNodeConnection>;
  warehouse?: Maybe<WarehouseNode>;
  inventories?: Maybe<InventoryNodeConnection>;
  inventory?: Maybe<InventoryNode>;
  goodsReceipts?: Maybe<GoodsReceiptNodeConnection>;
  goodsReceiptsDetail?: Maybe<GoodsReceiptNode>;
  goodsReceiptsReturn?: Maybe<GoodsReceiptReturnNodeConnection>;
  goodsReceiptsReturnDetail?: Maybe<GoodsReceiptReturnNode>;
  goodsReceiptsTransfer?: Maybe<GoodsReceiptTransferNodeConnection>;
  goodsReceiptsTransferDetail?: Maybe<GoodsReceiptTransferNode>;
  inventoryCount?: Maybe<InventoryCountNodeConnection>;
  inventoryCountDetail?: Maybe<InventoryCountNode>;
  unitsUnit?: Maybe<UnitNodeConnection>;
  unitsUnitDetail?: Maybe<UnitNode>;
  FAQsCategories?: Maybe<FaQsCategoryNodeConnection>;
  FAQsCategory?: Maybe<FaQsCategoryNode>;
  FAQuestions?: Maybe<FaQsNodeConnection>;
  FAQuestion?: Maybe<FaQsNode>;
  bannerGroups?: Maybe<BannerGroupNodeConnection>;
  bannerGroup?: Maybe<BannerGroupNode>;
  banners?: Maybe<BannerNodeConnection>;
  banner?: Maybe<BannerNode>;
  galleries?: Maybe<GalleryNodeConnection>;
  gallery?: Maybe<GalleryNode>;
  reviewForProduct?: Maybe<UserReviewNodeConnection>;
  /** The ID of the object */
  newsCateById?: Maybe<NewsCateNode>;
  newsCategory?: Maybe<NewsCateNodeConnection>;
  news?: Maybe<NewsNodeConnection>;
  /** The ID of the object */
  newsForDetail?: Maybe<NewsNode>;
  newsComments?: Maybe<NewsCommentsNodeConnection>;
  newsCommentById?: Maybe<NewsCommentsNode>;
  newsHashtags?: Maybe<HashtagNodeConnection>;
  newsHashtag?: Maybe<HashtagNode>;
  headerNews?: Maybe<HeaderNewsNodeConnection>;
  headerNewsDetail?: Maybe<HeaderNewsNode>;
  /** The ID of the object */
  flatpageById?: Maybe<FlatpageNode>;
  flatpages?: Maybe<FlatpageNodeConnection>;
  cartView?: Maybe<Array<Maybe<CartNode>>>;
  ordersView?: Maybe<OrderNodeConnection>;
  /** The ID of the object */
  orderDetailView?: Maybe<OrderNode>;
  saleReport?: Maybe<ReportNodeConnection>;
  saleReportByProduct?: Maybe<ReportByProductNodeConnection>;
  saleEmployeeReport?: Maybe<SaleEmployeesReportNodeConnection>;
  saleEmployeeByProductReport?: Maybe<SaleEmployeesByProductReportNodeConnection>;
  saleCustomerReport?: Maybe<SaleCustomersReportNodeConnection>;
  saleCustomerByProductReport?: Maybe<SaleCustomersByProductReportNodeConnection>;
  saleProductReport?: Maybe<SaleProductsReportNodeConnection>;
  saleProductByCustomerReport?: Maybe<SaleProductsByCustomerReportNodeConnection>;
  dashboard?: Maybe<Array<Maybe<DashBoardNode>>>;
  topSaleProduct?: Maybe<Array<Maybe<ByProduct>>>;
  topSaleCustomer?: Maybe<Array<Maybe<ByCustomer>>>;
  otherRevenueDetail?: Maybe<OtherRevenueNode>;
  otherRevenues?: Maybe<OtherRevenueNodeConnection>;
  branch?: Maybe<BranchNodeConnection>;
  branchDetail?: Maybe<BranchNode>;
  /** The ID of the object */
  country?: Maybe<CountryNode>;
  countries?: Maybe<CountryNodeConnection>;
  topUpPackage?: Maybe<TopUpPackageNodeConnection>;
  paymentMethods?: Maybe<PaymentNodeConnection>;
  topUpHistories?: Maybe<TopUpHistoryNodeConnection>;
  sitePaymentMethod?: Maybe<SitePaymentMethodNodeConnection>;
  userProfile?: Maybe<UserProfile>;
  userAddress?: Maybe<Array<Maybe<UserAddressNode>>>;
  userCustomer?: Maybe<UserCustomerNodeConnection>;
  customerDetail?: Maybe<CustomerDetailNode>;
  userHistoryBet?: Maybe<BetHistoryConnection>;
  userRecentlyProductViewed?: Maybe<Array<Maybe<UserProductRecentViewedNode>>>;
  sites?: Maybe<SiteNodeConnection>;
  site?: Maybe<SiteNode>;
  userAdmin?: Maybe<UserCustomerNodeConnection>;
  userAdminSite?: Maybe<UserCustomerNodeConnection>;
  userOwner?: Maybe<UserCustomerNodeConnection>;
  userManager?: Maybe<UserCustomerNodeConnection>;
  userStaff?: Maybe<UserCustomerNodeConnection>;
  adminSiteDetail?: Maybe<AdminSiteDetailNode>;
  suppliers?: Maybe<SupplierNodeConnection>;
  supplier?: Maybe<SupplierNode>;
  userFacebook?: Maybe<UserFacebookNodeConnection>;
  userShipper?: Maybe<Array<Maybe<UserCustomerNode>>>;
  userTokenNotification?: Maybe<Array<Maybe<UserTokenNotificationNode>>>;
  userTags?: Maybe<UserTagNodeConnection>;
  userTag?: Maybe<UserTagNode>;
  siteFeatures?: Maybe<SiteFeatureNodeConnection>;
  siteFeatureDetail?: Maybe<Array<Maybe<SiteFeatureNode>>>;
  newCustomer?: Maybe<Array<Maybe<NewCustomerNode>>>;
  siteSmtps?: Maybe<SiteSmtpNodeConnection>;
  siteSmtpDetail?: Maybe<SiteSmtpNode>;
  features?: Maybe<FeatureNodeConnection>;
  feature?: Maybe<FeatureNode>;
  siteTokenOnepays?: Maybe<SiteTokenOnePayNodeConnection>;
  siteTokenOnepayDetail?: Maybe<SiteTokenOnePayNode>;
  siteTokenFacebookId?: Maybe<Scalars['String']>;
  siteTokenFacebooks?: Maybe<SiteTokenFacebookNodeConnection>;
  siteTokenFacebookDetail?: Maybe<SiteTokenFacebookNode>;
  siteTokenZaloId?: Maybe<Scalars['String']>;
  siteTokenZalos?: Maybe<SiteTokenZaloNodeConnection>;
  siteTokenZaloDetail?: Maybe<SiteTokenZaloNode>;
  siteEmailContentDetail?: Maybe<SiteEmailContentNode>;
  siteEmailContents?: Maybe<SiteEmailContentNodeConnection>;
  provinces?: Maybe<ProvinceNodeConnection>;
  districts?: Maybe<DistrictNodeConnection>;
  wards?: Maybe<WardNodeConnection>;
  staffGroup?: Maybe<StaffGroupNodeConnection>;
  staffGroupDetail?: Maybe<StaffGroupNode>;
  customerGroup?: Maybe<CustomerGroupNodeConnection>;
  customerGroupDetail?: Maybe<CustomerGroupNode>;
  provinceBranches?: Maybe<ProvinceBranchNodeConnection>;
  provinceBranchDetail?: Maybe<ProvinceBranchNode>;
  districtBranches?: Maybe<DistrictBranchNodeConnection>;
  districtBranchDetail?: Maybe<DistrictBranchNode>;
  siteTokenLalamoves?: Maybe<SiteTokenLalamoveNodeConnection>;
  siteTokenLalamoveDetail?: Maybe<SiteTokenLalamoveNode>;
  siteTokenVnpays?: Maybe<SiteTokenVnPayNodeConnection>;
  siteTokenVnpayDetail?: Maybe<SiteTokenVnPayNode>;
  gmailClientId?: Maybe<Scalars['String']>;
  userLocationTracking?: Maybe<UserLocationTrackingNodeConnection>;
  /** The ID of the object */
  productForBetDetail?: Maybe<ProductForBetNode>;
  productsForBet?: Maybe<ProductForBetNodeConnection>;
  /** The ID of the object */
  productForBuyDetail?: Maybe<ProductForBuyNode>;
  productsForBuy?: Maybe<ProductForBuyNodeConnection>;
  productForBetHistory?: Maybe<ProductForBetTransactionNodeConnection>;
  /** The ID of the object */
  productForBetHistoryDetail?: Maybe<ProductForBetTransactionNode>;
  productForBuyBestSeller?: Maybe<Array<Maybe<ProductForBuyNode>>>;
  productRelated?: Maybe<Array<Maybe<ProductRelatedResult>>>;
  attributes?: Maybe<AttributeNodeConnection>;
  /** The ID of the object */
  attribute?: Maybe<AttributeNode>;
  specifications?: Maybe<SpecificationNodeConnection>;
  /** The ID of the object */
  specificationDetail?: Maybe<SpecificationNode>;
  variants?: Maybe<VariantNodeConnection>;
  /** The ID of the object */
  variant?: Maybe<VariantNode>;
  productAll?: Maybe<ProductForBetNodeConnection>;
  brands?: Maybe<BrandNodeConnection>;
  brand?: Maybe<BrandNode>;
  productCombo?: Maybe<ProductForBuyNodeConnection>;
  productComboDetail?: Maybe<ProductForBuyNode>;
  productTags?: Maybe<ProductTagNodeConnection>;
  productTag?: Maybe<ProductTagNode>;
  productDescription?: Maybe<ProductDescriptionNode>;
  productDescriptions?: Maybe<ProductDescriptionNodeConnection>;
  productForBuyWithVariant?: Maybe<ProductAndVariantConnection>;
  productPricesLists?: Maybe<ProductPricesListNodeConnection>;
  productPricesListDetail?: Maybe<ProductPricesListNode>;
  productPrices?: Maybe<ProductPricesNodeConnection>;
  productToppings?: Maybe<ProductToppingNodeConnection>;
  variantDescription?: Maybe<VariantDescriptionNode>;
  variantDescriptions?: Maybe<VariantDescriptionNodeConnection>;
  toppingGroup?: Maybe<Array<Maybe<ProductForBuyNode>>>;
  relatedProductList?: Maybe<RelatedProductNodeConnection>;
  /** The ID of the object */
  categoryById?: Maybe<CategoryNode>;
  categories?: Maybe<CategoryNodeConnection>;
  _debug?: Maybe<DjangoDebug>;
};


export type QueryPartyInfosArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  phone?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryPartyInfoArgs = {
  id: Scalars['String'];
};


export type QueryDeliveryMethodsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteDeliveriesMethodArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  isActive?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDeliveryMethodArgs = {
  id: Scalars['String'];
};


export type QueryKitchenOrderViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  status?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  isPriority?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['String']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  chefStatus?: Maybe<Scalars['String']>;
  sortPriority?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryKitchenOrderDetailArgs = {
  id: Scalars['String'];
};


export type QuerySiteConfigViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteConfigShip60ViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFlashSaleArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  willStart?: Maybe<Scalars['DateTime']>;
  willEnd?: Maybe<Scalars['DateTime']>;
  exitsInTime?: Maybe<Scalars['DateTime']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFlashSaleDetailArgs = {
  id: Scalars['String'];
};


export type QueryPromotionArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  beforeStartTime?: Maybe<Scalars['DateTime']>;
  afterEndTime?: Maybe<Scalars['DateTime']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryPromotionDetailArgs = {
  id: Scalars['String'];
};


export type QueryPromotionCodesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  beforeStartTime?: Maybe<Scalars['DateTime']>;
  afterEndTime?: Maybe<Scalars['DateTime']>;
  limitAmount?: Maybe<Scalars['Float']>;
  code?: Maybe<Scalars['String']>;
  hasLimitAmount?: Maybe<Scalars['Boolean']>;
  isPublic?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryPromotionCodeDetailArgs = {
  id: Scalars['String'];
};


export type QueryOurPartnersArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryOurPartnerArgs = {
  id: Scalars['String'];
};


export type QueryCustomerReviewsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryCustomerReviewArgs = {
  id: Scalars['String'];
};


export type QueryOutgoingPaymentsViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  outgoingPaymentStatus?: Maybe<Scalars['String']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryOutgoingPaymentDetailViewArgs = {
  id: Scalars['ID'];
};


export type QueryIncomingPaymentsViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  incomingPaymentStatus?: Maybe<Scalars['String']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryIncomingPaymentDetailViewArgs = {
  id: Scalars['ID'];
};


export type QueryInvoicesViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  invoiceStatus?: Maybe<Scalars['String']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryInvoiceDetailViewArgs = {
  id: Scalars['ID'];
};


export type QueryDeliveryGroupsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['ID']>;
  deliveryGroupStatus?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  created?: Maybe<Scalars['DateTime']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDeliveryGroupDetailArgs = {
  id: Scalars['String'];
};


export type QueryDeliveriesViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  shipper?: Maybe<Scalars['String']>;
  deliveryStatus?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  deliveryAddress?: Maybe<Scalars['String']>;
  receiveAddress?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['String']>;
  deliveryPhone?: Maybe<Scalars['String']>;
  receivePhone?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDeliveryDetailViewArgs = {
  id: Scalars['ID'];
};


export type QueryDeliveryTrackingArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  delivery?: Maybe<Scalars['String']>;
  shipper?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDeliveryReviewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  delivery?: Maybe<Scalars['String']>;
  shipper?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDeliveryBranchDetailArgs = {
  id: Scalars['String'];
};


export type QueryCollectionsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryCollectionArgs = {
  id: Scalars['String'];
};


export type QueryContactUsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  subject?: Maybe<Scalars['String']>;
  isReplied?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryContactUsDetailArgs = {
  id: Scalars['String'];
};


export type QueryFeedbacksArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFeedbackArgs = {
  id: Scalars['String'];
};


export type QueryProductFeedbacksArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  rating?: Maybe<Scalars['Float']>;
  visibility?: Maybe<Scalars['Boolean']>;
  productId?: Maybe<Scalars['String']>;
  variantId?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductFeedbackArgs = {
  id: Scalars['String'];
};


export type QueryWarehousesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  isPrimary?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['String']>;
  branch?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryWarehouseArgs = {
  id: Scalars['String'];
};


export type QueryInventoriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  warehouse?: Maybe<Scalars['String']>;
  product?: Maybe<Scalars['String']>;
  variant?: Maybe<Scalars['String']>;
  unit?: Maybe<Scalars['String']>;
  productMainUnit?: Maybe<Scalars['String']>;
  branch?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryInventoryArgs = {
  id: Scalars['String'];
};


export type QueryGoodsReceiptsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  warehouse?: Maybe<Scalars['String']>;
  createdBy?: Maybe<Scalars['String']>;
  supplier?: Maybe<Scalars['String']>;
  receiptDate?: Maybe<Scalars['DateTime']>;
  itemCode?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  receiptType?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryGoodsReceiptsDetailArgs = {
  id: Scalars['String'];
};


export type QueryGoodsReceiptsReturnArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  createdBy?: Maybe<Scalars['String']>;
  receiptDate?: Maybe<Scalars['DateTime']>;
  itemCode?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryGoodsReceiptsReturnDetailArgs = {
  id: Scalars['String'];
};


export type QueryGoodsReceiptsTransferArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  warehouseTransfer?: Maybe<Scalars['String']>;
  warehouseReceive?: Maybe<Scalars['String']>;
  dateReceive?: Maybe<Scalars['DateTime']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryGoodsReceiptsTransferDetailArgs = {
  id: Scalars['String'];
};


export type QueryInventoryCountArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  warehouse?: Maybe<Scalars['String']>;
  receiptDate?: Maybe<Scalars['DateTime']>;
  status?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryInventoryCountDetailArgs = {
  id: Scalars['String'];
};


export type QueryUnitsUnitArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  id?: Maybe<Scalars['ID']>;
  site?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  product?: Maybe<Scalars['String']>;
  productMainUnit?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUnitsUnitDetailArgs = {
  id: Scalars['String'];
};


export type QueryFaQsCategoriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parentId?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFaQsCategoryArgs = {
  id: Scalars['String'];
};


export type QueryFaQuestionsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFaQuestionArgs = {
  id: Scalars['String'];
};


export type QueryBannerGroupsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  itemCode?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBannerGroupArgs = {
  id?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};


export type QueryBannersArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  groupId?: Maybe<Scalars['String']>;
  siteId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBannerArgs = {
  id: Scalars['String'];
};


export type QueryGalleriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
  file?: Maybe<Scalars['String']>;
  siteId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryGalleryArgs = {
  id: Scalars['String'];
};


export type QueryReviewForProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  rate?: Maybe<Scalars['Float']>;
  comment?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryNewsCateByIdArgs = {
  id: Scalars['ID'];
};


export type QueryNewsCategoryArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryNewsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  category?: Maybe<Scalars['String']>;
  hashtagName?: Maybe<Scalars['String']>;
  hashtagId?: Maybe<Scalars['String']>;
  categoryName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryNewsForDetailArgs = {
  id: Scalars['ID'];
};


export type QueryNewsCommentsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  parent?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['String']>;
  news?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryNewsCommentByIdArgs = {
  id: Scalars['String'];
};


export type QueryNewsHashtagsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryNewsHashtagArgs = {
  id: Scalars['String'];
};


export type QueryHeaderNewsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  siteId?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  beginTime?: Maybe<Scalars['DateTime']>;
  endTime?: Maybe<Scalars['DateTime']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryHeaderNewsDetailArgs = {
  id: Scalars['String'];
};


export type QueryFlatpageByIdArgs = {
  id: Scalars['ID'];
};


export type QueryFlatpagesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  title?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};


export type QueryOrdersViewArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['ID']>;
  createdBy?: Maybe<Scalars['String']>;
  paymentStatus?: Maybe<Scalars['String']>;
  customer?: Maybe<Scalars['ID']>;
  isDeliveryNow?: Maybe<Scalars['Boolean']>;
  id?: Maybe<Scalars['String']>;
  product?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  customerLastName?: Maybe<Scalars['String']>;
  customerFirstName?: Maybe<Scalars['String']>;
  orderCode?: Maybe<Scalars['String']>;
  isDelivery?: Maybe<Scalars['Boolean']>;
  orderStatus?: Maybe<Scalars['String']>;
  orderInfomation?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  isCancelled?: Maybe<Scalars['Boolean']>;
  isPending?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryOrderDetailViewArgs = {
  id: Scalars['ID'];
};


export type QuerySaleReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleReportByProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleEmployeeReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleEmployeeByProductReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  employeeId?: Maybe<Scalars['String']>;
  employeeName?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleCustomerReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleCustomerByProductReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleProductReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySaleProductByCustomerReportArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['DateTime']>;
  dateTo?: Maybe<Scalars['DateTime']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  orderId?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDashboardArgs = {
  dateFrom: Scalars['DateTime'];
  dateTo: Scalars['DateTime'];
  groupBy: DashBoardGroupBy;
};


export type QueryTopSaleProductArgs = {
  topNumber: Scalars['Int'];
  dateFrom: Scalars['DateTime'];
  dateTo: Scalars['DateTime'];
  orderBy: TopProductOrderBy;
};


export type QueryTopSaleCustomerArgs = {
  topNumber: Scalars['Int'];
  dateFrom: Scalars['DateTime'];
  dateTo: Scalars['DateTime'];
};


export type QueryOtherRevenueDetailArgs = {
  id: Scalars['String'];
};


export type QueryOtherRevenuesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBranchArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  provinceId?: Maybe<Scalars['String']>;
  districtId?: Maybe<Scalars['String']>;
  wardId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBranchDetailArgs = {
  id: Scalars['String'];
};


export type QueryCountryArgs = {
  id: Scalars['ID'];
};


export type QueryCountriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryTopUpPackageArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  depositAmount?: Maybe<Scalars['Float']>;
  credit?: Maybe<Scalars['Float']>;
  unit?: Maybe<Scalars['String']>;
  bonus?: Maybe<Scalars['Float']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryPaymentMethodsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryTopUpHistoriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  created?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['String']>;
  topUpId?: Maybe<Scalars['String']>;
  userId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySitePaymentMethodArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  isActive?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['String']>;
  paymentMethod?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserCustomerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryCustomerDetailArgs = {
  id: Scalars['String'];
};


export type QueryUserHistoryBetArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QuerySitesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  domain?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  isMainSite?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  accessKey?: Maybe<Scalars['String']>;
  orderBuy?: Maybe<Scalars['String']>;
};


export type QuerySiteArgs = {
  id: Scalars['String'];
};


export type QueryUserAdminArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserAdminSiteArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserOwnerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserManagerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserStaffArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  fullName?: Maybe<Scalars['String']>;
  facebookId?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  role?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryAdminSiteDetailArgs = {
  id: Scalars['String'];
};


export type QuerySuppliersArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  siteId?: Maybe<Scalars['String']>;
  branchId?: Maybe<Scalars['String']>;
  isActive?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySupplierArgs = {
  id: Scalars['String'];
};


export type QueryUserFacebookArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserTagsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserTagArgs = {
  id: Scalars['String'];
};


export type QuerySiteFeaturesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
};


export type QuerySiteFeatureDetailArgs = {
  siteId: Scalars['String'];
};


export type QueryNewCustomerArgs = {
  dateFrom: Scalars['DateTime'];
  dateTo: Scalars['DateTime'];
  groupBy: DashBoardGroupBy;
};


export type QuerySiteSmtpsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFeaturesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryFeatureArgs = {
  id: Scalars['String'];
};


export type QuerySiteTokenOnepaysArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteTokenFacebooksArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteTokenZalosArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteEmailContentDetailArgs = {
  id: Scalars['String'];
};


export type QuerySiteEmailContentsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProvincesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  nameProvince?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDistrictsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  nameDistrict?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryWardsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  nameWard?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryStaffGroupArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryStaffGroupDetailArgs = {
  id: Scalars['String'];
};


export type QueryCustomerGroupArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryCustomerGroupDetailArgs = {
  id: Scalars['String'];
};


export type QueryProvinceBranchesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProvinceBranchDetailArgs = {
  id: Scalars['String'];
};


export type QueryDistrictBranchesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  provinceId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryDistrictBranchDetailArgs = {
  id: Scalars['String'];
};


export type QuerySiteTokenLalamovesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySiteTokenVnpaysArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  site?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryUserLocationTrackingArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  user?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductForBetDetailArgs = {
  id: Scalars['ID'];
};


export type QueryProductsForBetArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  tradability?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductForBuyDetailArgs = {
  id: Scalars['ID'];
};


export type QueryProductsForBuyArgs = {
  categoryWithSub?: Maybe<SubCategoryInput>;
  categoryList?: Maybe<Array<Maybe<Scalars['String']>>>;
  attributeList?: Maybe<Array<Maybe<Scalars['String']>>>;
  hasToppingGroup?: Maybe<Scalars['Boolean']>;
  productIdList?: Maybe<Array<Maybe<Scalars['String']>>>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  isStock?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  collection?: Maybe<Scalars['String']>;
  collectionName?: Maybe<Scalars['String']>;
  hasFlashSale?: Maybe<Scalars['Boolean']>;
  collectionItemCode?: Maybe<Scalars['String']>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductForBetHistoryArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  created?: Maybe<Scalars['String']>;
  luckyNumber?: Maybe<Scalars['Int']>;
  status?: Maybe<Scalars['String']>;
  drawedDate?: Maybe<Scalars['DateTime']>;
  product?: Maybe<Scalars['String']>;
  user?: Maybe<Scalars['String']>;
  productByName?: Maybe<Scalars['String']>;
  userByName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductForBetHistoryDetailArgs = {
  id: Scalars['ID'];
};


export type QueryProductRelatedArgs = {
  input: RelatedProductInput;
};


export type QueryAttributesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  specificationId?: Maybe<Scalars['String']>;
  specificationName?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryAttributeArgs = {
  id: Scalars['ID'];
};


export type QuerySpecificationsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QuerySpecificationDetailArgs = {
  id: Scalars['ID'];
};


export type QueryVariantsArgs = {
  attributeList?: Maybe<Array<Maybe<Scalars['String']>>>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  attribute?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['Float']>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryVariantArgs = {
  id: Scalars['ID'];
};


export type QueryProductAllArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  tradability?: Maybe<Scalars['Boolean']>;
  site?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBrandsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryBrandArgs = {
  id: Scalars['String'];
};


export type QueryProductComboArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  visibility?: Maybe<Scalars['Boolean']>;
  isStock?: Maybe<Scalars['Boolean']>;
  category?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  collection?: Maybe<Scalars['String']>;
  collectionName?: Maybe<Scalars['String']>;
  hasFlashSale?: Maybe<Scalars['Boolean']>;
  collectionItemCode?: Maybe<Scalars['String']>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  sku?: Maybe<Scalars['String']>;
  barcode?: Maybe<Scalars['String']>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductComboDetailArgs = {
  id: Scalars['String'];
};


export type QueryProductTagsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};


export type QueryProductTagArgs = {
  id: Scalars['String'];
};


export type QueryProductDescriptionArgs = {
  id: Scalars['String'];
};


export type QueryProductDescriptionsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['String']>;
};


export type QueryProductForBuyWithVariantArgs = {
  categoryList?: Maybe<Array<Maybe<Scalars['String']>>>;
  categoryName?: Maybe<Scalars['String']>;
  attributeList?: Maybe<Array<Maybe<Scalars['String']>>>;
  productVariantBarcode?: Maybe<Scalars['String']>;
  productPricesListId?: Maybe<Scalars['String']>;
  hasToppingGroup?: Maybe<Scalars['Boolean']>;
  hasVisibility?: Maybe<Scalars['Boolean']>;
  hasTopping?: Maybe<Scalars['Boolean']>;
  collectionsName?: Maybe<Scalars['String']>;
  branchId?: Maybe<Scalars['String']>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  isManageInventory?: Maybe<Scalars['Boolean']>;
  warehouseId?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  collectionId?: Maybe<Scalars['String']>;
  categoryWithSub?: Maybe<SubCategoryInput>;
  isTopping?: Maybe<Scalars['Boolean']>;
  isToppingGroup?: Maybe<Scalars['Boolean']>;
  productIdList?: Maybe<Array<Maybe<Scalars['String']>>>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryProductPricesListsArgs = {
  customerId?: Maybe<Scalars['String']>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  userScope?: Maybe<Scalars['String']>;
};


export type QueryProductPricesListDetailArgs = {
  id: Scalars['String'];
};


export type QueryProductPricesArgs = {
  productPricesListId: Array<Maybe<Scalars['String']>>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryProductToppingsArgs = {
  productVariant?: Maybe<ProductVariantInput>;
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type QueryVariantDescriptionArgs = {
  id: Scalars['String'];
};


export type QueryVariantDescriptionsArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  variantId?: Maybe<Scalars['String']>;
};


export type QueryRelatedProductListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  productId?: Maybe<Scalars['String']>;
  relatedProductId?: Maybe<Scalars['String']>;
};


export type QueryCategoryByIdArgs = {
  id: Scalars['ID'];
};


export type QueryCategoriesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  level?: Maybe<Scalars['String']>;
  parentName?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export enum ReceiptTypeInput {
  Purchase = 'PURCHASE',
  Returns = 'RETURNS',
  Transfer = 'TRANSFER'
}

export type RegisterAccountTokenInput = {
  avatar?: Maybe<Scalars['Upload']>;
  code?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  password: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type RegisterAccountTokenPayload = {
  __typename?: 'RegisterAccountTokenPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type RegisterAccountVerify = {
  __typename?: 'RegisterAccountVerify';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type RelatedProductInput = {
  productId: Scalars['String'];
};

export type RelatedProductNode = CustomizeInterface & {
  __typename?: 'RelatedProductNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  variant?: Maybe<VariantNode>;
  product?: Maybe<ProductForBuyNode>;
  relatedProduct?: Maybe<ProductForBuyNode>;
};

export type RelatedProductNodeConnection = {
  __typename?: 'RelatedProductNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<RelatedProductNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `RelatedProductNode` and its cursor. */
export type RelatedProductNodeEdge = {
  __typename?: 'RelatedProductNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<RelatedProductNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type Report = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type ReportByProductNode = Report & {
  __typename?: 'ReportByProductNode';
  quantity: Scalars['Float'];
  /** The ID of the object. */
  id: Scalars['ID'];
  date?: Maybe<Scalars['Date']>;
  orderId?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  orderStatus?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<Scalars['String']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  deliveryMethod?: Maybe<DeliveryMethodNode>;
  orderDiscounts?: Maybe<Array<Maybe<OrderDiscountNode>>>;
};

export type ReportByProductNodeConnection = {
  __typename?: 'ReportByProductNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ReportByProductNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
  byProduct?: Maybe<ProductConnection>;
  byEmployee?: Maybe<EmployeeConnection>;
  byCustomer?: Maybe<CustomerConnection>;
  bySalesChannel?: Maybe<SalesChannelConnection>;
  byEmployeeByProduct?: Maybe<ProductUserConnection>;
  byCustomerByProduct?: Maybe<ProductUserConnection>;
  byProductByCustomer?: Maybe<ProductUserConnection>;
  bySalesChannelByProduct?: Maybe<SalesChannelProductConnection>;
};


export type ReportByProductNodeConnectionByProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionByEmployeeArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionByCustomerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionBySalesChannelArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionByEmployeeByProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionByCustomerByProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionByProductByCustomerArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};


export type ReportByProductNodeConnectionBySalesChannelByProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ReportByProductNode` and its cursor. */
export type ReportByProductNodeEdge = {
  __typename?: 'ReportByProductNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ReportByProductNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ReportNode = Order & {
  __typename?: 'ReportNode';
  createdBy?: Maybe<UserProfile>;
  orderStatus?: Maybe<OrderOrderStatus>;
  salesChannel?: Maybe<OrderSalesChannel>;
  shippingCost: Scalars['Float'];
  deliveryMethod?: Maybe<DeliveryMethodNode>;
  totalDiscount: Scalars['Float'];
  totalOtherFee: Scalars['Float'];
  /** The ID of the object. */
  id: Scalars['ID'];
  date?: Maybe<Scalars['Date']>;
  orderId?: Maybe<Scalars['String']>;
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
  createdById?: Maybe<Scalars['String']>;
  createdByName?: Maybe<Scalars['String']>;
  orderDiscounts?: Maybe<Array<Maybe<OrderDiscountNode>>>;
  subTotal?: Maybe<Scalars['Float']>;
};

export type ReportNodeConnection = {
  __typename?: 'ReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<ReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `ReportNode` and its cursor. */
export type ReportNodeEdge = {
  __typename?: 'ReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<ReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type ReviewError = {
  __typename?: 'ReviewError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type SaleCustomersByProductReportNode = Report & {
  __typename?: 'SaleCustomersByProductReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export type SaleCustomersByProductReportNodeConnection = {
  __typename?: 'SaleCustomersByProductReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleCustomersByProductReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleCustomersByProductReportNode` and its cursor. */
export type SaleCustomersByProductReportNodeEdge = {
  __typename?: 'SaleCustomersByProductReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleCustomersByProductReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SaleCustomersReportNode = Report & {
  __typename?: 'SaleCustomersReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
};

export type SaleCustomersReportNodeConnection = {
  __typename?: 'SaleCustomersReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleCustomersReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleCustomersReportNode` and its cursor. */
export type SaleCustomersReportNodeEdge = {
  __typename?: 'SaleCustomersReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleCustomersReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SaleEmployeesByProductReportNode = Report & {
  __typename?: 'SaleEmployeesByProductReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  employeeId?: Maybe<Scalars['String']>;
  employeeName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export type SaleEmployeesByProductReportNodeConnection = {
  __typename?: 'SaleEmployeesByProductReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleEmployeesByProductReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleEmployeesByProductReportNode` and its cursor. */
export type SaleEmployeesByProductReportNodeEdge = {
  __typename?: 'SaleEmployeesByProductReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleEmployeesByProductReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SaleEmployeesReportNode = Report & {
  __typename?: 'SaleEmployeesReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  employeeId?: Maybe<Scalars['String']>;
  employeeName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
};

export type SaleEmployeesReportNodeConnection = {
  __typename?: 'SaleEmployeesReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleEmployeesReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleEmployeesReportNode` and its cursor. */
export type SaleEmployeesReportNodeEdge = {
  __typename?: 'SaleEmployeesReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleEmployeesReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SaleProductsByCustomerReportNode = Report & {
  __typename?: 'SaleProductsByCustomerReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  customerId?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  amount?: Maybe<Scalars['Float']>;
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
};

export type SaleProductsByCustomerReportNodeConnection = {
  __typename?: 'SaleProductsByCustomerReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleProductsByCustomerReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleProductsByCustomerReportNode` and its cursor. */
export type SaleProductsByCustomerReportNodeEdge = {
  __typename?: 'SaleProductsByCustomerReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleProductsByCustomerReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SaleProductsReportNode = Report & {
  __typename?: 'SaleProductsReportNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  productId?: Maybe<Scalars['String']>;
  productName?: Maybe<Scalars['String']>;
  quantity?: Maybe<Scalars['Int']>;
  amount?: Maybe<Scalars['Float']>;
};

export type SaleProductsReportNodeConnection = {
  __typename?: 'SaleProductsReportNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SaleProductsReportNodeEdge>>;
  /** The total amount of objects in this query. */
  totalAmount?: Maybe<Scalars['Float']>;
  groupByDay?: Maybe<Array<Maybe<GroupByDay>>>;
  groupByWeek?: Maybe<Array<Maybe<GroupByWeek>>>;
  groupByMonth?: Maybe<Array<Maybe<GroupByMonth>>>;
  groupByQuarter?: Maybe<Array<Maybe<GroupByQuarter>>>;
  groupByYear?: Maybe<Array<Maybe<GroupByYear>>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
  /** The total quantity of objects in this query. */
  totalQuantity?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SaleProductsReportNode` and its cursor. */
export type SaleProductsReportNodeEdge = {
  __typename?: 'SaleProductsReportNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SaleProductsReportNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SalesChannelConnection = {
  __typename?: 'SalesChannelConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SalesChannelEdge>>;
  totalAmount?: Maybe<Scalars['Float']>;
};

/** A Relay edge containing a `SalesChannel` and its cursor. */
export type SalesChannelEdge = {
  __typename?: 'SalesChannelEdge';
  /** The item at the end of the edge */
  node?: Maybe<BySalesChannel>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export enum SalesChannelInput {
  Facebook = 'FACEBOOK',
  Web = 'WEB',
  Ecommerce = 'ECOMMERCE',
  Direct = 'DIRECT',
  CallCenter = 'CALL_CENTER'
}

export type SalesChannelProductConnection = {
  __typename?: 'SalesChannelProductConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SalesChannelProductEdge>>;
  totalQuantity?: Maybe<Scalars['Int']>;
  totalAmount?: Maybe<Scalars['Float']>;
};

/** A Relay edge containing a `SalesChannelProduct` and its cursor. */
export type SalesChannelProductEdge = {
  __typename?: 'SalesChannelProductEdge';
  /** The item at the end of the edge */
  node?: Maybe<BySalesChannelByProduct>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SetDefaultAddress = {
  __typename?: 'SetDefaultAddress';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type SetDefaultAddressInput = {
  addressId: Scalars['String'];
};

export type SiteConfigError = {
  __typename?: 'SiteConfigError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type SiteConfigNode = CustomizeInterface & {
  __typename?: 'SiteConfigNode';
  addressSite?: Maybe<Scalars['String']>;
  nameSite?: Maybe<Scalars['String']>;
  logoSite?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type SiteConfigNodeConnection = {
  __typename?: 'SiteConfigNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteConfigNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteConfigNode` and its cursor. */
export type SiteConfigNodeEdge = {
  __typename?: 'SiteConfigNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteConfigNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteConfigShip60Node = CustomizeInterface & {
  __typename?: 'SiteConfigShip60Node';
  grantType?: Maybe<Scalars['String']>;
  clientId?: Maybe<Scalars['String']>;
  urlApi?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type SiteConfigShip60NodeConnection = {
  __typename?: 'SiteConfigShip60NodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteConfigShip60NodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteConfigShip60Node` and its cursor. */
export type SiteConfigShip60NodeEdge = {
  __typename?: 'SiteConfigShip60NodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteConfigShip60Node>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteConfigShip60Update = {
  __typename?: 'SiteConfigShip60Update';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<SiteConfigError>>>;
  siteConfigShip60?: Maybe<SiteConfigShip60Node>;
};

export type SiteConfigShip60UpdateInput = {
  grantType: Scalars['String'];
  clientId: Scalars['String'];
  urlApi: Scalars['String'];
};

export type SiteConfigUpdate = {
  __typename?: 'SiteConfigUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<SiteConfigError>>>;
  siteConfig?: Maybe<SiteConfigNode>;
};

export type SiteConfigUpdateInput = {
  addressSite?: Maybe<Scalars['String']>;
  nameSite?: Maybe<Scalars['String']>;
  logoSite?: Maybe<Scalars['Upload']>;
};

export type SiteCreateInput = {
  emailContacts?: Maybe<Array<Maybe<Scalars['String']>>>;
  emailOrders?: Maybe<Array<Maybe<Scalars['String']>>>;
  headerNewsTime?: Maybe<Scalars['String']>;
  isBranchLocationManagement?: Maybe<Scalars['Boolean']>;
  domain: Scalars['String'];
  name: Scalars['String'];
  accessKey?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  tranferInfomation?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  typeHosting?: Maybe<Scalars['String']>;
  isMainSite?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteCreatePayload = {
  __typename?: 'SiteCreatePayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  site?: Maybe<SiteNode>;
};

export type SiteDeactivate = {
  __typename?: 'SiteDeactivate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  site?: Maybe<SiteNode>;
};

export type SiteDeleteInput = {
  id: Scalars['String'];
};

export type SiteDeliveryMethodNode = CustomizeInterface & {
  __typename?: 'SiteDeliveryMethodNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  deliveryMethod: DeliveryMethodNode;
  isActive: Scalars['Boolean'];
};

export type SiteDeliveryMethodNodeConnection = {
  __typename?: 'SiteDeliveryMethodNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteDeliveryMethodNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteDeliveryMethodNode` and its cursor. */
export type SiteDeliveryMethodNodeEdge = {
  __typename?: 'SiteDeliveryMethodNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteDeliveryMethodNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteDeliveryMethodUpdate = {
  __typename?: 'SiteDeliveryMethodUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryMethodError>>>;
  siteDeliveryMethod?: Maybe<Array<Maybe<SiteDeliveryMethodNode>>>;
};

export type SiteDeliveryMethodUpdateInput = {
  deliveryMethod?: Maybe<Array<Maybe<DeliveryMethodInput>>>;
};

export type SiteEmailContactNode = CustomizeInterface & {
  __typename?: 'SiteEmailContactNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  email: Scalars['String'];
};

export type SiteEmailContentInput = {
  content?: Maybe<Scalars['String']>;
  itemCode: Scalars['String'];
  contentFile?: Maybe<Scalars['Upload']>;
};

export type SiteEmailContentNode = CustomizeInterface & {
  __typename?: 'SiteEmailContentNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  content?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
};

export type SiteEmailContentNodeConnection = {
  __typename?: 'SiteEmailContentNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteEmailContentNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteEmailContentNode` and its cursor. */
export type SiteEmailContentNodeEdge = {
  __typename?: 'SiteEmailContentNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteEmailContentNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteEmailContentUpdateInput = {
  id: Scalars['String'];
  content?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  contentFile?: Maybe<Scalars['Upload']>;
};

export type SiteEmailOrderNode = CustomizeInterface & {
  __typename?: 'SiteEmailOrderNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  email: Scalars['String'];
};

export type SiteEmailUpdateInput = {
  id: Scalars['String'];
  email?: Maybe<Scalars['String']>;
};

export type SiteFeatureActive = {
  __typename?: 'SiteFeatureActive';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  siteFeatures?: Maybe<Array<Maybe<SiteFeatureNode>>>;
};

export type SiteFeatureActiveInput = {
  id: Scalars['String'];
  isActive: Scalars['Boolean'];
};

export type SiteFeatureCreateInput = {
  site: Scalars['ID'];
  feature: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteFeatureCreatePayload = {
  __typename?: 'SiteFeatureCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteFeature?: Maybe<SiteFeatureNode>;
};

export type SiteFeatureNode = CustomizeInterface & {
  __typename?: 'SiteFeatureNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  feature: FeatureNode;
  isActive: Scalars['Boolean'];
};

export type SiteFeatureNodeConnection = {
  __typename?: 'SiteFeatureNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteFeatureNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteFeatureNode` and its cursor. */
export type SiteFeatureNodeEdge = {
  __typename?: 'SiteFeatureNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteFeatureNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteFeatureUpdate = {
  __typename?: 'SiteFeatureUpdate';
  status?: Maybe<Scalars['Boolean']>;
  siteFeature?: Maybe<Array<Maybe<SiteFeatureNode>>>;
  errors?: Maybe<Array<Maybe<ErrorType>>>;
};

export type SiteFeatureUpdateInput = {
  site: Scalars['String'];
  features: Array<Maybe<Scalars['String']>>;
  isActive?: Maybe<Scalars['Boolean']>;
};

export type SiteNode = CustomizeInterface & {
  __typename?: 'SiteNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  domain: Scalars['String'];
  name: Scalars['String'];
  isMainSite?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  accessKey?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  tranferInfomation?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  typeHosting?: Maybe<Scalars['String']>;
  headerNewsTime: Scalars['Int'];
  isBranchLocationManagement: Scalars['Boolean'];
  emailContact?: Maybe<Array<Maybe<SiteEmailContactNode>>>;
  emailOrder?: Maybe<Array<Maybe<SiteEmailOrderNode>>>;
  paymentMethod?: Maybe<Array<Maybe<SitePaymentNode>>>;
  emailContent?: Maybe<Array<Maybe<SiteEmailContentNode>>>;
};

export type SiteNodeConnection = {
  __typename?: 'SiteNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteNode` and its cursor. */
export type SiteNodeEdge = {
  __typename?: 'SiteNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SitePaymentMethodInput = {
  paymentMethod: Scalars['String'];
  isActive: Scalars['Boolean'];
};

export type SitePaymentMethodNode = CustomizeInterface & {
  __typename?: 'SitePaymentMethodNode';
  paymentMethod: PaymentNode;
  isActive: Scalars['Boolean'];
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
};

export type SitePaymentMethodNodeConnection = {
  __typename?: 'SitePaymentMethodNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SitePaymentMethodNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SitePaymentMethodNode` and its cursor. */
export type SitePaymentMethodNodeEdge = {
  __typename?: 'SitePaymentMethodNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SitePaymentMethodNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SitePaymentNode = CustomizeInterface & {
  __typename?: 'SitePaymentNode';
  paymentMethod: PaymentNode;
  isActive: Scalars['Boolean'];
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type SiteSmtpCreateInput = {
  smtpSendmail?: Maybe<Scalars['String']>;
  smtpHost?: Maybe<Scalars['String']>;
  smtpSecure?: Maybe<SiteSmtpSmtpSecure>;
  smtpGate?: Maybe<Scalars['Int']>;
  smtpUsername?: Maybe<Scalars['String']>;
  smtpPassword?: Maybe<Scalars['String']>;
  site: Scalars['ID'];
  isActive: Scalars['Boolean'];
  smtpIsCertified?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteSmtpCreatePayload = {
  __typename?: 'SiteSMTPCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteSMTP?: Maybe<SiteSmtpNode>;
};

export type SiteSmtpDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteSmtpDeletePayload = {
  __typename?: 'SiteSMTPDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteSMTP?: Maybe<SiteSmtpNode>;
};

export type SiteSmtpNode = CustomizeInterface & {
  __typename?: 'SiteSMTPNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  smtpSendmail?: Maybe<Scalars['String']>;
  isActive: Scalars['Boolean'];
  smtpHost?: Maybe<Scalars['String']>;
  smtpSecure: SiteSmtpSmtpSecure;
  smtpGate: Scalars['Int'];
  smtpIsCertified: Scalars['Boolean'];
  smtpUsername?: Maybe<Scalars['String']>;
  smtpPassword?: Maybe<Scalars['String']>;
};

export type SiteSmtpNodeConnection = {
  __typename?: 'SiteSMTPNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteSmtpNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteSMTPNode` and its cursor. */
export type SiteSmtpNodeEdge = {
  __typename?: 'SiteSMTPNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteSmtpNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum SiteSmtpSmtpSecure {
  /** None */
  None = 'NONE',
  /** Ssl */
  Ssl = 'SSL',
  /** Tls */
  Tls = 'TLS'
}

export type SiteSmtpUpdateInput = {
  smtpSendmail?: Maybe<Scalars['String']>;
  smtpHost?: Maybe<Scalars['String']>;
  smtpSecure?: Maybe<SiteSmtpSmtpSecure>;
  smtpGate?: Maybe<Scalars['Int']>;
  smtpUsername?: Maybe<Scalars['String']>;
  smtpPassword?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<Scalars['ID']>;
  isActive?: Maybe<Scalars['Boolean']>;
  smtpIsCertified?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteSmtpUpdatePayload = {
  __typename?: 'SiteSMTPUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteSMTP?: Maybe<SiteSmtpNode>;
};

export type SiteTokenFacebookCreateInput = {
  facebookAppId?: Maybe<Scalars['String']>;
  facebookAppSecret?: Maybe<Scalars['String']>;
  site: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenFacebookCreatePayload = {
  __typename?: 'SiteTokenFacebookCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenFacebook?: Maybe<SiteTokenFacebookNode>;
};

export type SiteTokenFacebookDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenFacebookDeletePayload = {
  __typename?: 'SiteTokenFacebookDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenFacebook?: Maybe<SiteTokenFacebookNode>;
};

export type SiteTokenFacebookNode = CustomizeInterface & {
  __typename?: 'SiteTokenFacebookNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  facebookAppId?: Maybe<Scalars['String']>;
  facebookAppSecret?: Maybe<Scalars['String']>;
};

export type SiteTokenFacebookNodeConnection = {
  __typename?: 'SiteTokenFacebookNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteTokenFacebookNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteTokenFacebookNode` and its cursor. */
export type SiteTokenFacebookNodeEdge = {
  __typename?: 'SiteTokenFacebookNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteTokenFacebookNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteTokenFacebookUpdateInput = {
  facebookAppId?: Maybe<Scalars['String']>;
  facebookAppSecret?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenFacebookUpdatePayload = {
  __typename?: 'SiteTokenFacebookUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenFacebook?: Maybe<SiteTokenFacebookNode>;
};

export type SiteTokenLalamoveCreateInput = {
  lalamoveApiKey?: Maybe<Scalars['String']>;
  lalamoveAppSecret?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenLalamoveCreatePayload = {
  __typename?: 'SiteTokenLalamoveCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenLalamove?: Maybe<SiteTokenLalamoveNode>;
};

export type SiteTokenLalamoveDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenLalamoveDeletePayload = {
  __typename?: 'SiteTokenLalamoveDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenLalamove?: Maybe<SiteTokenLalamoveNode>;
};

export type SiteTokenLalamoveNode = CustomizeInterface & {
  __typename?: 'SiteTokenLalamoveNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  lalamoveApiKey?: Maybe<Scalars['String']>;
  lalamoveAppSecret?: Maybe<Scalars['String']>;
};

export type SiteTokenLalamoveNodeConnection = {
  __typename?: 'SiteTokenLalamoveNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteTokenLalamoveNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteTokenLalamoveNode` and its cursor. */
export type SiteTokenLalamoveNodeEdge = {
  __typename?: 'SiteTokenLalamoveNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteTokenLalamoveNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteTokenLalamoveUpdateInput = {
  lalamoveApiKey?: Maybe<Scalars['String']>;
  lalamoveAppSecret?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenLalamoveUpdatePayload = {
  __typename?: 'SiteTokenLalamoveUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenLalamove?: Maybe<SiteTokenLalamoveNode>;
};

export type SiteTokenOnePayNode = CustomizeInterface & {
  __typename?: 'SiteTokenOnePayNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  vpcVersion: Scalars['Int'];
  vpcCurrency: Scalars['String'];
  vpcCommand: Scalars['String'];
  vpcAccesscode?: Maybe<Scalars['String']>;
  vpcMerchant?: Maybe<Scalars['String']>;
  vpcLocale: Scalars['String'];
  vpcReturnUrl: Scalars['String'];
  vpcUser?: Maybe<Scalars['String']>;
  vpcPassword?: Maybe<Scalars['String']>;
};

export type SiteTokenOnePayNodeConnection = {
  __typename?: 'SiteTokenOnePayNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteTokenOnePayNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteTokenOnePayNode` and its cursor. */
export type SiteTokenOnePayNodeEdge = {
  __typename?: 'SiteTokenOnePayNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteTokenOnePayNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteTokenOnepayCreateInput = {
  vpcVersion?: Maybe<Scalars['Int']>;
  vpcCurrency?: Maybe<Scalars['String']>;
  vpcCommand?: Maybe<Scalars['String']>;
  vpcAccesscode?: Maybe<Scalars['String']>;
  vpcMerchant?: Maybe<Scalars['String']>;
  vpcLocale?: Maybe<Scalars['String']>;
  vpcReturnUrl: Scalars['String'];
  vpcUser?: Maybe<Scalars['String']>;
  vpcPassword?: Maybe<Scalars['String']>;
  site: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenOnepayCreatePayload = {
  __typename?: 'SiteTokenOnepayCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenOnepay?: Maybe<SiteTokenOnePayNode>;
};

export type SiteTokenOnepayDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenOnepayDeletePayload = {
  __typename?: 'SiteTokenOnepayDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenOnepay?: Maybe<SiteTokenOnePayNode>;
};

export type SiteTokenOnepayUpdateInput = {
  vpcVersion?: Maybe<Scalars['Int']>;
  vpcCurrency?: Maybe<Scalars['String']>;
  vpcCommand?: Maybe<Scalars['String']>;
  vpcAccesscode?: Maybe<Scalars['String']>;
  vpcMerchant?: Maybe<Scalars['String']>;
  vpcLocale?: Maybe<Scalars['String']>;
  vpcReturnUrl?: Maybe<Scalars['String']>;
  vpcUser?: Maybe<Scalars['String']>;
  vpcPassword?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenOnepayUpdatePayload = {
  __typename?: 'SiteTokenOnepayUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenOnepay?: Maybe<SiteTokenOnePayNode>;
};

export type SiteTokenVnPayCreateInput = {
  vnpayTmnKey?: Maybe<Scalars['String']>;
  vnpaySecret?: Maybe<Scalars['String']>;
  paymentGateway?: Maybe<Scalars['String']>;
  vpnApiUrl?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenVnPayCreatePayload = {
  __typename?: 'SiteTokenVNPayCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenVNPay?: Maybe<SiteTokenVnPayNode>;
};

export type SiteTokenVnPayDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenVnPayDeletePayload = {
  __typename?: 'SiteTokenVNPayDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenVNPay?: Maybe<SiteTokenVnPayNode>;
};

export type SiteTokenVnPayNode = CustomizeInterface & {
  __typename?: 'SiteTokenVNPayNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  vnpayTmnKey?: Maybe<Scalars['String']>;
  vnpaySecret?: Maybe<Scalars['String']>;
  paymentGateway?: Maybe<Scalars['String']>;
  vpnApiUrl?: Maybe<Scalars['String']>;
};

export type SiteTokenVnPayNodeConnection = {
  __typename?: 'SiteTokenVNPayNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteTokenVnPayNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteTokenVNPayNode` and its cursor. */
export type SiteTokenVnPayNodeEdge = {
  __typename?: 'SiteTokenVNPayNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteTokenVnPayNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteTokenVnPayUpdateInput = {
  vnpayTmnKey?: Maybe<Scalars['String']>;
  vnpaySecret?: Maybe<Scalars['String']>;
  paymentGateway?: Maybe<Scalars['String']>;
  vpnApiUrl?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenVnPayUpdatePayload = {
  __typename?: 'SiteTokenVNPayUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenVNPay?: Maybe<SiteTokenVnPayNode>;
};

export type SiteTokenZaloCreateInput = {
  zaloAppId?: Maybe<Scalars['String']>;
  zaloAppSecret?: Maybe<Scalars['String']>;
  site: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenZaloCreatePayload = {
  __typename?: 'SiteTokenZaloCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenZalo?: Maybe<SiteTokenZaloNode>;
};

export type SiteTokenZaloDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenZaloDeletePayload = {
  __typename?: 'SiteTokenZaloDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenZalo?: Maybe<SiteTokenZaloNode>;
};

export type SiteTokenZaloNode = CustomizeInterface & {
  __typename?: 'SiteTokenZaloNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  zaloAppId?: Maybe<Scalars['String']>;
  zaloAppSecret?: Maybe<Scalars['String']>;
};

export type SiteTokenZaloNodeConnection = {
  __typename?: 'SiteTokenZaloNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SiteTokenZaloNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SiteTokenZaloNode` and its cursor. */
export type SiteTokenZaloNodeEdge = {
  __typename?: 'SiteTokenZaloNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SiteTokenZaloNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SiteTokenZaloUpdateInput = {
  zaloAppId?: Maybe<Scalars['String']>;
  zaloAppSecret?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteTokenZaloUpdatePayload = {
  __typename?: 'SiteTokenZaloUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  siteTokenZalo?: Maybe<SiteTokenZaloNode>;
};

export type SiteUpdateInput = {
  emailContactsAdd?: Maybe<Array<Maybe<Scalars['String']>>>;
  emailContactsUpdate?: Maybe<Array<Maybe<SiteEmailUpdateInput>>>;
  emailContactsRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  emailOrdersAdd?: Maybe<Array<Maybe<Scalars['String']>>>;
  emailOrdersUpdate?: Maybe<Array<Maybe<SiteEmailUpdateInput>>>;
  emailOrdersRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  paymentMethodsAdd?: Maybe<Array<Maybe<SitePaymentMethodInput>>>;
  paymentMethodsUpdate?: Maybe<Array<Maybe<SitePaymentMethodInput>>>;
  paymentMethodsRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  emailContentsAdd?: Maybe<Array<Maybe<SiteEmailContentInput>>>;
  emailContentsUpdate?: Maybe<Array<Maybe<SiteEmailContentUpdateInput>>>;
  emailContentsRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  domain?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  accessKey?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  tranferInfomation?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  note?: Maybe<Scalars['String']>;
  typeHosting?: Maybe<Scalars['String']>;
  headerNewsTime?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isMainSite?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  isBranchLocationManagement?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SiteUpdatePayload = {
  __typename?: 'SiteUpdatePayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  site?: Maybe<SiteNode>;
};

export type SiteUploadConfigurationData = {
  __typename?: 'SiteUploadConfigurationData';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type SpecificationCreateInput = {
  name: Scalars['String'];
  itemCode?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SpecificationCreatePayload = {
  __typename?: 'SpecificationCreatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  specification?: Maybe<SpecificationNode>;
};

export type SpecificationDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SpecificationDeletePayload = {
  __typename?: 'SpecificationDeletePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  specification?: Maybe<SpecificationNode>;
};

export type SpecificationInterface = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type SpecificationNode = SpecificationInterface & {
  __typename?: 'SpecificationNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  name: Scalars['String'];
  itemCode?: Maybe<Scalars['String']>;
};

export type SpecificationNodeConnection = {
  __typename?: 'SpecificationNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SpecificationNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SpecificationNode` and its cursor. */
export type SpecificationNodeEdge = {
  __typename?: 'SpecificationNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SpecificationNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SpecificationUpdateInput = {
  name?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SpecificationUpdatePayload = {
  __typename?: 'SpecificationUpdatePayload';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  specification?: Maybe<SpecificationNode>;
};

export type StaffGroupCreateInput = {
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type StaffGroupCreatePayload = {
  __typename?: 'StaffGroupCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  staffGroup?: Maybe<StaffGroupNode>;
};

export type StaffGroupDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type StaffGroupDeletePayload = {
  __typename?: 'StaffGroupDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  staffGroup?: Maybe<StaffGroupNode>;
};

export type StaffGroupNode = CustomizeInterface & {
  __typename?: 'StaffGroupNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site?: Maybe<SiteNode>;
  name: Scalars['String'];
  note?: Maybe<Scalars['String']>;
  staffGroupUser?: Maybe<Array<Maybe<StaffGroupStaffNode>>>;
  productPricesList: ProductPricesListUserGroupNodeConnection;
};


export type StaffGroupNodeProductPricesListArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type StaffGroupNodeConnection = {
  __typename?: 'StaffGroupNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<StaffGroupNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `StaffGroupNode` and its cursor. */
export type StaffGroupNodeEdge = {
  __typename?: 'StaffGroupNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<StaffGroupNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type StaffGroupStaffNode = CustomizeInterface & {
  __typename?: 'StaffGroupStaffNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  staffGroup: StaffGroupNode;
  staff: UserProfile;
};

export type StaffGroupStaffNodeConnection = {
  __typename?: 'StaffGroupStaffNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<StaffGroupStaffNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `StaffGroupStaffNode` and its cursor. */
export type StaffGroupStaffNodeEdge = {
  __typename?: 'StaffGroupStaffNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<StaffGroupStaffNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type StaffGroupUpdateInput = {
  name?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type StaffGroupUpdatePayload = {
  __typename?: 'StaffGroupUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  staffGroup?: Maybe<StaffGroupNode>;
};

export enum StatusInput {
  Draft = 'DRAFT',
  Completed = 'COMPLETED',
  Cancelled = 'CANCELLED',
  Transferring = 'TRANSFERRING'
}

export type SubCategoryInput = {
  id: Scalars['String'];
  includeSubCategory?: Maybe<Scalars['Boolean']>;
};

export type SupplierCreateInput = {
  branch?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SupplierCreatePayload = {
  __typename?: 'SupplierCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  supplier?: Maybe<SupplierNode>;
};

export type SupplierDelete = {
  __typename?: 'SupplierDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type SupplierNode = CustomizeInterface & {
  __typename?: 'SupplierNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  branch?: Maybe<BranchNode>;
  isActive?: Maybe<Scalars['Boolean']>;
  purchaseReceipt?: Maybe<Array<Maybe<GoodsReceiptNode>>>;
};

export type SupplierNodeConnection = {
  __typename?: 'SupplierNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<SupplierNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `SupplierNode` and its cursor. */
export type SupplierNodeEdge = {
  __typename?: 'SupplierNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<SupplierNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type SupplierUpdateInput = {
  branch?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type SupplierUpdatePayload = {
  __typename?: 'SupplierUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  supplier?: Maybe<SupplierNode>;
};

export type TagUserCreateInput = {
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TagUserCreatePayload = {
  __typename?: 'TagUserCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<UserTagNode>;
};

export type TagUserDeleteInput = {
  id?: Maybe<Array<Maybe<Scalars['String']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TagUserDeletePayload = {
  __typename?: 'TagUserDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<UserTagNode>;
};

export type TagUserUpdateInput = {
  name?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TagUserUpdatePayload = {
  __typename?: 'TagUserUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  tag?: Maybe<UserTagNode>;
};

export type TokenVerifyInput = {
  code: Scalars['String'];
};

export enum TopProductOrderBy {
  Quantity = 'QUANTITY',
  Amount = 'AMOUNT'
}

export type TopUpHistoryNode = Payment & {
  __typename?: 'TopUpHistoryNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  user: UserProfile;
  topUp: TopUpPackageNode;
  status: UserTopUpHistoryStatus;
};

export type TopUpHistoryNodeConnection = {
  __typename?: 'TopUpHistoryNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<TopUpHistoryNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `TopUpHistoryNode` and its cursor. */
export type TopUpHistoryNodeEdge = {
  __typename?: 'TopUpHistoryNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<TopUpHistoryNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type TopUpPackageCreateInput = {
  bonus?: Maybe<Scalars['Float']>;
  depositAmount: Scalars['Float'];
  credit: Scalars['Float'];
  unit: TopUpPackageUnit;
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TopUpPackageCreatePayload = {
  __typename?: 'TopUpPackageCreatePayload';
  errors?: Maybe<Array<Maybe<PaymentError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  topUpPackage?: Maybe<TopUpPackageNode>;
};

export type TopUpPackageDeleteInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TopUpPackageDeletePayload = {
  __typename?: 'TopUpPackageDeletePayload';
  errors?: Maybe<Array<Maybe<PaymentError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  topUpPackage?: Maybe<TopUpPackageNode>;
};

export type TopUpPackageNode = Payment & {
  __typename?: 'TopUpPackageNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  depositAmount: Scalars['Float'];
  credit: Scalars['Float'];
  unit: TopUpPackageUnit;
  bonus: Scalars['Float'];
  description?: Maybe<Scalars['String']>;
};

export type TopUpPackageNodeConnection = {
  __typename?: 'TopUpPackageNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<TopUpPackageNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `TopUpPackageNode` and its cursor. */
export type TopUpPackageNodeEdge = {
  __typename?: 'TopUpPackageNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<TopUpPackageNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum TopUpPackageUnit {
  /** Money */
  Money = 'MONEY',
  /** Ticket */
  Ticket = 'TICKET'
}

export type TopUpPackageUpdateInput = {
  /** The ID of the object. */
  id: Scalars['ID'];
  depositAmount?: Maybe<Scalars['Float']>;
  credit?: Maybe<Scalars['Float']>;
  unit?: Maybe<TopUpPackageUnit>;
  bonus?: Maybe<Scalars['Float']>;
  description?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type TopUpPackageUpdatePayload = {
  __typename?: 'TopUpPackageUpdatePayload';
  errors?: Maybe<Array<Maybe<PaymentError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  topUpPackage?: Maybe<TopUpPackageNode>;
};

export type ToppingInput = {
  toppingID: Scalars['String'];
  quantity: Scalars['Int'];
  note?: Maybe<Scalars['String']>;
};

export type ToppingNode = ProductNode & {
  __typename?: 'ToppingNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  created: Scalars['DateTime'];
  category: CategoryNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  description2?: Maybe<Scalars['String']>;
  description3?: Maybe<Scalars['String']>;
  thumbnail?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibility: Scalars['Boolean'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  productType: ProductProductType;
  sku?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  isStock?: Maybe<Scalars['Boolean']>;
  slug?: Maybe<Scalars['String']>;
  sortOrder?: Maybe<Scalars['Int']>;
  sortOrderCategory?: Maybe<Scalars['Int']>;
  totalRate: Scalars['Float'];
  totalReview: Scalars['Int'];
  barcode?: Maybe<Scalars['String']>;
  isTopping: Scalars['Boolean'];
  isToppingGroup: Scalars['Boolean'];
  isToppingGroupMandatory: Scalars['Boolean'];
  parent?: Maybe<ProductForBuyNode>;
  productImage: Array<ProductImageNode>;
  tags: Array<ProductTagRelatedNode>;
  flashSaleProduct: ProductFlashSaleNodeConnection;
  unit?: Maybe<Array<Maybe<ProductUnitNode>>>;
  productPricesList?: Maybe<Array<Maybe<ProductPricesListNode>>>;
  collections?: Maybe<Array<Maybe<CollectionNode>>>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionDetailNode>>>;
};


export type ToppingNodeFlashSaleProductArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type UnitCreateInput = {
  site?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UnitCreatePayload = {
  __typename?: 'UnitCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  unit?: Maybe<UnitNode>;
};

export type UnitDelete = {
  __typename?: 'UnitDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export enum UnitDiscountInput {
  Vnd = 'VND',
  Percent = 'PERCENT'
}

export type UnitNode = CustomizeInterface & {
  __typename?: 'UnitNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  site: SiteNode;
};

export type UnitNodeConnection = {
  __typename?: 'UnitNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UnitNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UnitNode` and its cursor. */
export type UnitNodeEdge = {
  __typename?: 'UnitNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UnitNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UnitUpdateInput = {
  name?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UnitUpdatePayload = {
  __typename?: 'UnitUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  unit?: Maybe<UnitNode>;
};

export type UpdateAddressInput = {
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  province?: Maybe<Scalars['ID']>;
  district?: Maybe<Scalars['ID']>;
  ward?: Maybe<Scalars['ID']>;
  /** Set list of delivery addresss */
  deliveryAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payment addresss */
  incomingPaymentAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoice addresss */
  invoiceAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of order addresss */
  orderAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payment addresss */
  outgoingPaymentAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateAddressPayload = {
  __typename?: 'UpdateAddressPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  userAddress?: Maybe<UserAddressNode>;
};

export type UpdateAdminSiteInput = {
  id: Scalars['String'];
  avatar?: Maybe<Scalars['UploadType']>;
  site?: Maybe<Scalars['String']>;
  role?: Maybe<AdminSiteRoleInput>;
  staffGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  branch?: Maybe<Array<Maybe<Scalars['String']>>>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  loginMethod?: Maybe<UserLoginMethod>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  isSpam?: Maybe<Scalars['Boolean']>;
  /** Set list of orders */
  adminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  delivery?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  deliveryAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery groups */
  deliveryGroup?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery groups */
  deliveryGroups?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of deliverys */
  deliveryShipper?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of delivery historys */
  deliveryUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of goods receipt transfers */
  goodReceiptTransferReceiver?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payments */
  incomingPayment?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payments */
  incomingPaymentAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of incoming payment historys */
  incomingPaymentUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of inventory counts */
  inventoryCountUserCompleted?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  invoice?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoices */
  invoiceAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of invoice historys */
  invoiceUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of kitchen order details */
  orderKitchenChef?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  outgoingPayment?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payments */
  outgoingPaymentAdminCustomer?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of outgoing payment historys */
  outgoingPaymentUserHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of order historys */
  userHistory?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of newss */
  userNews?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateAdminSitePayload = {
  __typename?: 'UpdateAdminSitePayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type UpdateCart = {
  __typename?: 'UpdateCart';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  cart?: Maybe<CartNode>;
};

export type UpdateCartInput = {
  cartDetailId: Scalars['String'];
  topping?: Maybe<Array<Maybe<ToppingInput>>>;
  quantity?: Maybe<Scalars['Int']>;
  note?: Maybe<Scalars['String']>;
};

export type UpdateCustomerInput = {
  avatar?: Maybe<Scalars['Upload']>;
  tagRemove?: Maybe<Array<Maybe<Scalars['String']>>>;
  tag?: Maybe<Array<Maybe<Scalars['String']>>>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive?: Maybe<Scalars['Boolean']>;
  isSpam?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateCustomerPayload = {
  __typename?: 'UpdateCustomerPayload';
  errors?: Maybe<Array<Maybe<UserError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  user?: Maybe<UserProfile>;
};

export type UpdateDeliveryStatusInput = {
  delivery: Scalars['String'];
  deliveryStatus: DeliveryStatusInput;
};

export type UpdateFlatpageInput = {
  site?: Maybe<Scalars['String']>;
  itemCode?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type UpdateFlatpagePayload = {
  __typename?: 'UpdateFlatpagePayload';
  errors?: Maybe<Array<Maybe<FlatpageError>>>;
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  flatpage?: Maybe<FlatpageNode>;
};

export type UpdateLocation = {
  __typename?: 'UpdateLocation';
  status?: Maybe<Scalars['Boolean']>;
  message?: Maybe<Scalars['String']>;
};

export type UpdatePosCustomer = {
  __typename?: 'UpdatePosCustomer';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
  customer?: Maybe<CustomerDetailNode>;
};

export type UpdatePosCustomerInput = {
  id: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  customerGroup?: Maybe<Array<Maybe<Scalars['String']>>>;
  note?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  branch?: Maybe<Scalars['String']>;
  tax?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['Upload']>;
  receiver?: Maybe<Scalars['String']>;
  addressId?: Maybe<Scalars['String']>;
  isSpam?: Maybe<Scalars['Boolean']>;
  password?: Maybe<Scalars['String']>;
};



export type UserActiveReview = {
  __typename?: 'UserActiveReview';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ReviewError>>>;
  review?: Maybe<UserReviewNode>;
};

export type UserActiveReviewInput = {
  reviewId: Scalars['String'];
  isActive?: Maybe<Scalars['Boolean']>;
};

export type UserAddress = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type UserAddressNode = UserAddress & {
  __typename?: 'UserAddressNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  receiver?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  formatAddress?: Maybe<Scalars['String']>;
  isDefault: Scalars['Boolean'];
  province?: Maybe<ProvinceNode>;
  district?: Maybe<DistrictNode>;
  ward?: Maybe<WardNode>;
  street?: Maybe<Scalars['String']>;
};

export type UserBetTransaction = {
  __typename?: 'UserBetTransaction';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ProductError>>>;
  luckyNumber?: Maybe<Scalars['String']>;
};

export type UserBranchPermissionNode = {
  __typename?: 'UserBranchPermissionNode';
  id: Scalars['ID'];
  user: UserProfile;
  branch: BranchNode;
};

export type UserCustomerNode = CustomizeInterface & {
  __typename?: 'UserCustomerNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  isActive: Scalars['Boolean'];
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  address?: Maybe<Array<Maybe<UserAddressNode>>>;
  site?: Maybe<SiteNode>;
  loginMethod: UserLoginMethod;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  isSpam: Scalars['Boolean'];
  customerGroupUser: CustomerGroupCustomerNodeConnection;
  role?: Maybe<Scalars['String']>;
  branch?: Maybe<Array<Maybe<BranchNode>>>;
  tax?: Maybe<Scalars['String']>;
  customerType?: Maybe<Scalars['String']>;
  debt?: Maybe<Scalars['Float']>;
  permission?: Maybe<Array<Maybe<BranchPermissionFieldNode>>>;
  bonusPoint?: Maybe<Scalars['Float']>;
  rank?: Maybe<Scalars['String']>;
};


export type UserCustomerNodeCustomerGroupUserArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type UserCustomerNodeConnection = {
  __typename?: 'UserCustomerNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserCustomerNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UserCustomerNode` and its cursor. */
export type UserCustomerNodeEdge = {
  __typename?: 'UserCustomerNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserCustomerNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UserEditReview = {
  __typename?: 'UserEditReview';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ReviewError>>>;
  review?: Maybe<UserReviewNode>;
};

export type UserEditReviewInput = {
  reviewId: Scalars['String'];
  comment?: Maybe<Scalars['String']>;
  rate?: Maybe<Scalars['Float']>;
  imagesDelete?: Maybe<Array<Maybe<Scalars['String']>>>;
  newImages?: Maybe<Array<Maybe<Scalars['Upload']>>>;
};

export type UserError = {
  __typename?: 'UserError';
  field?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type UserFacebookNode = CustomizeInterface & {
  __typename?: 'UserFacebookNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  user: UserProfile;
  facebookId: Scalars['String'];
  name?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
};

export type UserFacebookNodeConnection = {
  __typename?: 'UserFacebookNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserFacebookNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UserFacebookNode` and its cursor. */
export type UserFacebookNodeEdge = {
  __typename?: 'UserFacebookNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserFacebookNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UserGroupPermissionNode = {
  __typename?: 'UserGroupPermissionNode';
  id: Scalars['ID'];
  name: Scalars['String'];
  /** The groups this user belongs to. A user will get all permissions granted to each of their groups. */
  userSet: UserProfileConnection;
  permission?: Maybe<Array<Maybe<PermissionContent>>>;
};


export type UserGroupPermissionNodeUserSetArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
};

export type UserGuestInput = {
  fullName: Scalars['String'];
  phone: Scalars['String'];
  email: Scalars['String'];
  product: Array<Maybe<ProductInput>>;
  paymentStatus?: Maybe<PaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  orderNote?: Maybe<Scalars['String']>;
  province?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  ward?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<SalesChannelInput>;
  paymentReturnData?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  promotionCodeCode?: Maybe<Scalars['String']>;
};

export type UserLocationTrackingCreate = {
  __typename?: 'UserLocationTrackingCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type UserLocationTrackingCreateInput = {
  latitude: Scalars['Float'];
  longtitude: Scalars['Float'];
  created?: Maybe<Scalars['DateTime']>;
};

export type UserLocationTrackingDelete = {
  __typename?: 'UserLocationTrackingDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<UserError>>>;
};

export type UserLocationTrackingNode = CustomizeInterface & {
  __typename?: 'UserLocationTrackingNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  latitude: Scalars['Float'];
  longtitude: Scalars['Float'];
  created?: Maybe<Scalars['DateTime']>;
  user: UserProfile;
};

export type UserLocationTrackingNodeConnection = {
  __typename?: 'UserLocationTrackingNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserLocationTrackingNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UserLocationTrackingNode` and its cursor. */
export type UserLocationTrackingNodeEdge = {
  __typename?: 'UserLocationTrackingNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserLocationTrackingNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

/** An enumeration. */
export enum UserLoginMethod {
  /** System */
  System = 'SYSTEM',
  /** Facebook */
  Facebook = 'FACEBOOK',
  /** Zalo */
  Zalo = 'ZALO',
  /** Google */
  Google = 'GOOGLE'
}

export type UserOrderCreate = {
  __typename?: 'UserOrderCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<OrderError>>>;
  order?: Maybe<OrderNode>;
};

export type UserOrderCreateInput = {
  cartId: Scalars['String'];
  promotionCode?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  addressId?: Maybe<Scalars['String']>;
  orderStatus: OrderStatusInput;
  paymentStatus?: Maybe<PaymentStatusInput>;
  paymentMethodId: Scalars['String'];
  orderNote?: Maybe<Scalars['String']>;
  addressNote?: Maybe<Scalars['String']>;
  salesChannel?: Maybe<SalesChannelInput>;
  paymentReturnData?: Maybe<Scalars['String']>;
  deliveryMethodId?: Maybe<Scalars['String']>;
  promotionCodeCode?: Maybe<Scalars['String']>;
};

export type UserPermissionUpdate = {
  __typename?: 'UserPermissionUpdate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PerError>>>;
  user?: Maybe<AdminSiteDetailNode>;
};

export type UserPermissionUpdateInput = {
  userId: Scalars['String'];
  branchId: Scalars['String'];
  permissions?: Maybe<Array<Maybe<PermissionFieldInput>>>;
};

export type UserProductRecentViewedNode = {
  __typename?: 'UserProductRecentViewedNode';
  user?: Maybe<ProductsRecentViewedNode>;
};

export type UserProfile = CustomizeInterface & {
  __typename?: 'UserProfile';
  /** The ID of the object. */
  id: Scalars['ID'];
  email: Scalars['String'];
  lastName?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  avatar?: Maybe<Scalars['String']>;
  site?: Maybe<SiteNode>;
  loginMethod: UserLoginMethod;
  birthday?: Maybe<Scalars['DateTime']>;
  gender?: Maybe<Scalars['String']>;
  facebookProfile?: Maybe<Scalars['String']>;
  note?: Maybe<Scalars['String']>;
  isSpam: Scalars['Boolean'];
  staffGroupUser: StaffGroupStaffNodeConnection;
  userType?: Maybe<UserUserType>;
  code?: Maybe<Scalars['String']>;
  wallet?: Maybe<Scalars['Float']>;
  ticket?: Maybe<Scalars['Int']>;
  role?: Maybe<Scalars['String']>;
  debt?: Maybe<Scalars['Float']>;
  address?: Maybe<Array<Maybe<UserAddressNode>>>;
  branch?: Maybe<Array<Maybe<BranchNode>>>;
  tax?: Maybe<Scalars['String']>;
  customerType?: Maybe<Scalars['String']>;
  permission?: Maybe<Array<Maybe<BranchPermissionFieldNode>>>;
  bonusPoint?: Maybe<Scalars['Float']>;
  rank?: Maybe<Scalars['String']>;
};


export type UserProfileStaffGroupUserArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type UserProfileConnection = {
  __typename?: 'UserProfileConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserProfileEdge>>;
};

/** A Relay edge containing a `UserProfile` and its cursor. */
export type UserProfileEdge = {
  __typename?: 'UserProfileEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserProfile>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UserReviewCreate = {
  __typename?: 'UserReviewCreate';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<ReviewError>>>;
  review?: Maybe<UserReviewNode>;
};

export type UserReviewImageNode = {
  __typename?: 'UserReviewImageNode';
  id: Scalars['ID'];
  image: Scalars['String'];
};

export type UserReviewInput = {
  productId: Scalars['String'];
  comment?: Maybe<Scalars['String']>;
  rate?: Maybe<Scalars['Float']>;
  images?: Maybe<Array<Maybe<Scalars['Upload']>>>;
  site?: Maybe<Scalars['String']>;
};

export type UserReviewNode = CustomizeInterface & {
  __typename?: 'UserReviewNode';
  created: Scalars['DateTime'];
  /** The ID of the object. */
  id: Scalars['ID'];
  comment?: Maybe<Scalars['String']>;
  rate: Scalars['Float'];
  isActive: Scalars['Boolean'];
  images?: Maybe<Array<Maybe<UserReviewImageNode>>>;
  user?: Maybe<UserProfile>;
  product?: Maybe<ProductForBuyNode>;
};

export type UserReviewNodeConnection = {
  __typename?: 'UserReviewNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserReviewNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UserReviewNode` and its cursor. */
export type UserReviewNodeEdge = {
  __typename?: 'UserReviewNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserReviewNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UserTagNode = CustomizeInterface & {
  __typename?: 'UserTagNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  site: SiteNode;
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  user: Array<UserTagRelatedNode>;
};

export type UserTagNodeConnection = {
  __typename?: 'UserTagNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<UserTagNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `UserTagNode` and its cursor. */
export type UserTagNodeEdge = {
  __typename?: 'UserTagNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<UserTagNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type UserTagRelatedNode = {
  __typename?: 'UserTagRelatedNode';
  id: Scalars['ID'];
  user: UserProfile;
  tag: UserTagNode;
};

export type UserTokenNotificationCreate = {
  __typename?: 'UserTokenNotificationCreate';
  status?: Maybe<Scalars['Boolean']>;
};

export type UserTokenNotificationDelete = {
  __typename?: 'UserTokenNotificationDelete';
  status?: Maybe<Scalars['Boolean']>;
};

export type UserTokenNotificationNode = {
  __typename?: 'UserTokenNotificationNode';
  id: Scalars['ID'];
  user: UserProfile;
  token: Scalars['String'];
};

export type UserTopUp = {
  __typename?: 'UserTopUp';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<PaymentError>>>;
};

/** An enumeration. */
export enum UserTopUpHistoryStatus {
  /** Success */
  Success = 'SUCCESS',
  /** Failed */
  Failed = 'FAILED'
}

export type UserTopUpInput = {
  userId: Scalars['String'];
  topUpPackage: Scalars['String'];
};

export type UserUpdateDeliveryStatus = {
  __typename?: 'UserUpdateDeliveryStatus';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<DeliveryError>>>;
  delivery?: Maybe<DeliveryNode>;
};

/** An enumeration. */
export enum UserUserType {
  /** Customer */
  Customer = 'CUSTOMER',
  /** Admin */
  Admin = 'ADMIN',
  /** AdminSite */
  AdminSite = 'ADMIN_SITE',
  /** Guest */
  Guest = 'GUEST',
  /** Shipper */
  Shipper = 'SHIPPER'
}

export type VariantCreate = {
  __typename?: 'VariantCreate';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  variant?: Maybe<VariantNode>;
};

export type VariantCreateInput = {
  product: Scalars['String'];
  attributes: Array<Maybe<Scalars['String']>>;
  price?: Maybe<Scalars['Float']>;
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  barcode?: Maybe<Scalars['String']>;
  visibility?: Maybe<Scalars['Boolean']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sortOrder?: Maybe<Scalars['Int']>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionInput>>>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  relatedProduct?: Maybe<Array<Maybe<Scalars['String']>>>;
  video?: Maybe<Scalars['String']>;
  branchList?: Maybe<Array<Maybe<ProductBranchInput>>>;
};

export type VariantDelete = {
  __typename?: 'VariantDelete';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
};

export type VariantDeleteInput = {
  id: Scalars['String'];
};

export type VariantDescriptionDetailNode = {
  __typename?: 'VariantDescriptionDetailNode';
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
};

export type VariantDescriptionNode = CustomizeInterface & {
  __typename?: 'VariantDescriptionNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  variant?: Maybe<VariantNode>;
};

export type VariantDescriptionNodeConnection = {
  __typename?: 'VariantDescriptionNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<VariantDescriptionNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `VariantDescriptionNode` and its cursor. */
export type VariantDescriptionNodeEdge = {
  __typename?: 'VariantDescriptionNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<VariantDescriptionNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type VariantInput = {
  attributes: Array<Maybe<Scalars['String']>>;
  price?: Maybe<Scalars['Float']>;
  barcode?: Maybe<Scalars['String']>;
};

export type VariantInterface = {
  /** The ID of the object. */
  id: Scalars['ID'];
};

export type VariantNode = VariantInterface & {
  __typename?: 'VariantNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  product: ProductForBuyNode;
  thumbnail?: Maybe<Scalars['String']>;
  price: Scalars['Float'];
  visibility: Scalars['Boolean'];
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sortOrder?: Maybe<Scalars['Int']>;
  isSellOutOfStock: Scalars['Boolean'];
  video?: Maybe<Scalars['String']>;
  images?: Maybe<Array<Maybe<ProductImageNode>>>;
  attributes?: Maybe<AttributeNodeConnection>;
  specification?: Maybe<Array<Maybe<SpecificationNode>>>;
  descriptions?: Maybe<Array<Maybe<VariantDescriptionDetailNode>>>;
  collections?: Maybe<Array<Maybe<CollectionNode>>>;
  variantName?: Maybe<Scalars['String']>;
  pricesList?: Maybe<Array<Maybe<VariantProductPricesNode>>>;
  inventory?: Maybe<Array<Maybe<ProductBranchWarehouse>>>;
  totalQuantityInventory?: Maybe<Scalars['Float']>;
  totalOrderCustomer?: Maybe<Scalars['Float']>;
  inventoryDetail?: Maybe<Array<Maybe<ProductInventory>>>;
  relatedProduct?: Maybe<Array<Maybe<ProductForBuyNode>>>;
  productFeedback?: Maybe<Array<Maybe<ProductFeedbackNode>>>;
  productFeedbackCount?: Maybe<ProductFeedbackCount>;
  branchList?: Maybe<Array<Maybe<ProductBranchBranchNode>>>;
  likeCount?: Maybe<Scalars['Int']>;
};


export type VariantNodeAttributesArgs = {
  before?: Maybe<Scalars['String']>;
  after?: Maybe<Scalars['String']>;
  first?: Maybe<Scalars['Int']>;
  last?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  specificationId?: Maybe<Scalars['String']>;
  specificationName?: Maybe<Scalars['String']>;
  productId?: Maybe<Scalars['String']>;
  orderBy?: Maybe<Scalars['String']>;
};

export type VariantNodeConnection = {
  __typename?: 'VariantNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<VariantNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `VariantNode` and its cursor. */
export type VariantNodeEdge = {
  __typename?: 'VariantNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<VariantNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type VariantProductPricesNode = CustomizeInterface & {
  __typename?: 'VariantProductPricesNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  productPricesList: ProductPricesListNode;
  price: Scalars['Float'];
};

export type VariantUpdate = {
  __typename?: 'VariantUpdate';
  errors?: Maybe<Array<Maybe<ProductError>>>;
  status?: Maybe<Scalars['Boolean']>;
  variant?: Maybe<VariantNode>;
};

export type VariantUpdateInput = {
  id: Scalars['String'];
  price?: Maybe<Scalars['Float']>;
  images?: Maybe<Array<Maybe<Scalars['String']>>>;
  attributes?: Maybe<Array<Maybe<Scalars['String']>>>;
  barcode?: Maybe<Scalars['String']>;
  visibility?: Maybe<Scalars['Boolean']>;
  visibilityDate?: Maybe<Scalars['DateTime']>;
  sortOrder?: Maybe<Scalars['Int']>;
  descriptions?: Maybe<Array<Maybe<ProductDescriptionUpdate>>>;
  collections?: Maybe<Array<Maybe<Scalars['String']>>>;
  isSellOutOfStock?: Maybe<Scalars['Boolean']>;
  relatedProduct?: Maybe<Array<Maybe<Scalars['String']>>>;
  video?: Maybe<Scalars['String']>;
  branchList?: Maybe<Array<Maybe<ProductBranchInput>>>;
};

export type VerifyTokenInput = {
  token: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

/** Same as `grapgql_jwt` implementation, with standard output. */
export type VerifyTokenPayload = {
  __typename?: 'VerifyTokenPayload';
  payload?: Maybe<Scalars['GenericScalar']>;
  success?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Scalars['ExpectedErrorType']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WardCreateInput = {
  nameWard: Scalars['String'];
  code: Scalars['String'];
  district: Scalars['ID'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WardCreatePayload = {
  __typename?: 'WardCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ward?: Maybe<WardNode>;
};

export type WardDeleteInput = {
  id: Array<Maybe<Scalars['String']>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WardDeletePayload = {
  __typename?: 'WardDeletePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ward?: Maybe<WardNode>;
};

export type WardNode = CustomizeInterface & {
  __typename?: 'WardNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  nameWard?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  district?: Maybe<DistrictNode>;
  sortId: Scalars['Int'];
};

export type WardNodeConnection = {
  __typename?: 'WardNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<WardNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `WardNode` and its cursor. */
export type WardNodeEdge = {
  __typename?: 'WardNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<WardNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type WardUpdateInput = {
  nameWard?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  sortId?: Maybe<Scalars['Int']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  /** Set list of delivery addresss */
  deliveryAddress?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Set list of party infors */
  partyInfor?: Maybe<Array<Maybe<Scalars['ID']>>>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WardUpdatePayload = {
  __typename?: 'WardUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  ward?: Maybe<WardNode>;
};

export type WareError = {
  __typename?: 'WareError';
  message?: Maybe<Scalars['String']>;
  field?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
};

export type WarehouseCreateInput = {
  name: Scalars['String'];
  address?: Maybe<Scalars['String']>;
  isPrimary: Scalars['Boolean'];
  site?: Maybe<Scalars['ID']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WarehouseCreatePayload = {
  __typename?: 'WarehouseCreatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  warehouse?: Maybe<WarehouseNode>;
};

export type WarehouseDelete = {
  __typename?: 'WarehouseDelete';
  status?: Maybe<Scalars['Boolean']>;
  errors?: Maybe<Array<Maybe<WareError>>>;
};

export type WarehouseInventoryNode = {
  __typename?: 'WarehouseInventoryNode';
  orderDetailWarehouseId?: Maybe<Scalars['String']>;
  warehouse?: Maybe<WarehouseNode>;
  quantity?: Maybe<Scalars['Float']>;
  quantityMain?: Maybe<Scalars['Float']>;
  quantityRemain?: Maybe<Scalars['Float']>;
  orderDetail?: Maybe<Scalars['String']>;
};

export type WarehouseNode = CustomizeInterface & {
  __typename?: 'WarehouseNode';
  /** The ID of the object. */
  id: Scalars['ID'];
  name: Scalars['String'];
  address?: Maybe<Scalars['String']>;
  isPrimary: Scalars['Boolean'];
  site?: Maybe<SiteNode>;
};

export type WarehouseNodeConnection = {
  __typename?: 'WarehouseNodeConnection';
  /** Pagination data for this connection. */
  pageInfo: PageInfo;
  /** Contains the nodes in this connection. */
  edges: Array<Maybe<WarehouseNodeEdge>>;
  /** The total count of objects in this query. */
  totalCount?: Maybe<Scalars['Int']>;
};

/** A Relay edge containing a `WarehouseNode` and its cursor. */
export type WarehouseNodeEdge = {
  __typename?: 'WarehouseNodeEdge';
  /** The item at the end of the edge */
  node?: Maybe<WarehouseNode>;
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
};

export type WarehouseUnitInventory = {
  __typename?: 'WarehouseUnitInventory';
  quantity?: Maybe<Scalars['Float']>;
  unitId?: Maybe<Scalars['String']>;
  unitName?: Maybe<Scalars['String']>;
  isMain?: Maybe<Scalars['Boolean']>;
  quantityAvailable?: Maybe<Scalars['Float']>;
};

export type WarehouseUpdateInput = {
  name?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  /** The ID of the object. */
  id: Scalars['ID'];
  isPrimary?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
};

export type WarehouseUpdatePayload = {
  __typename?: 'WarehouseUpdatePayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<CustomizeMutationErrorType>>;
  /** Status of the mutation. */
  status?: Maybe<Scalars['Boolean']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  warehouse?: Maybe<WarehouseNode>;
};

export type ZaloLoginInput = {
  oAuthCode: Scalars['String'];
  clientMutationId?: Maybe<Scalars['String']>;
};

export type ZaloLoginPayload = {
  __typename?: 'ZaloLoginPayload';
  /** List of errors that occurred while executing the mutation. */
  errors?: Maybe<Array<MutationErrorType>>;
  token?: Maybe<Scalars['String']>;
  clientMutationId?: Maybe<Scalars['String']>;
  /** The mutated object. */
  zaloInformation?: Maybe<ZaloNode>;
};

export type ZaloNode = {
  __typename?: 'ZaloNode';
  created: Scalars['DateTime'];
  id: Scalars['ID'];
  user: UserProfile;
  zaloId: Scalars['String'];
  accessToken?: Maybe<Scalars['String']>;
  birthday?: Maybe<Scalars['Date']>;
  gender?: Maybe<Scalars['String']>;
};
