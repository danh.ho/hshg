import { FetchResult } from 'apollo-link';
import { Mutation } from './graphql.models';
import { Observable } from 'rxjs';

export type MutationResultObservable = Observable<FetchResult<Mutation>>;