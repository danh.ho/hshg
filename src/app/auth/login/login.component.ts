import { Route } from '@angular/compiler/src/core';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services';
import { AuthState, GetSiteTokenFacebook, GetSiteTokenGmail, Login, LoginFailed, LoginSuccessfully, SetToken } from 'src/app/shared/store/auth';
import { SubSink } from 'subsink';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';
import { FacebookLoginService } from '../facebook-login.service';
import { ConfigService } from 'src/app/shared/config/config.service';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
    selector: 'cos-frontend-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    private _subscription = new Subscription();
    secondFormGroup: FormGroup;
    subsink = new SubSink();
    err: any;
    facebookId='';
    gmailId='';
    facebookLogin;
    gmailLogin;
    error_code;
    error;
    // gmailLogin = `https://accounts.google.com/o/oauth2/v2/auth?scope=${environment.gmailScope}&client_id=${environment.gmailAppId}&response_type=code&redirect_uri=${this.config.getConfig().URL+environment.gmailRedirectURL}&state=${environment.SITE_ID}`
    // gmailLogin = `https://accounts.google.com/o/oauth2/v2/auth?scope=${environment.gmailScope}&client_id=${environment.gmailAppId}&response_type=code&redirect_uri=${environment.gmailRedirectURL}&state=${environment.SITE_ID}`
    codeAuth: string;
    translate: string;
    loggedIn: boolean;
    token: string;
    return: string;
    cartItemsNo: number;
    constructor(
        private config : ConfigService,
        private _formBuilder: FormBuilder,
        private _store: Store,
        private _actions: Actions,
        private router: Router,
        private route: ActivatedRoute,
        private _notify: NotificationService,
        private _facebookService: FacebookLoginService,
        @Inject(DOCUMENT) private document: Document
    ) { }

    ngOnInit(): void {
        this.secondFormGroup = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
        });
        this.return = this.route.snapshot.queryParamMap.get('returnUrl');
        this.route.queryParams.subscribe(params => { 
            this.token = params['token']; 
            this.error_code = params['error_code'];
            this.error = params['error'];
        })
        if (this.token) {
            this._store.dispatch(new SetToken({ token: this.token })).subscribe(() => {
                this.router.navigate(['/home'])
            });
        }
        if (this.error_code || this.error) {
            this.router.navigate(['/home'])
        }
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
            }));
    }

    login() {
        this._store.dispatch(new Login(this.secondFormGroup.value));
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(LoginSuccessfully))
            .subscribe((translation) => {
                if (translation) {
                    if (this.return) {
                        this.router.navigate([`/${this.return}`]);
                    } else {
                        this.router.navigate(['/home']);
                    }

                }
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(LoginFailed))
            .subscribe((res) => {
                this.err = res;
                this.codeAuth = this.err.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;

            });
    }
    signInWithGmail() {
        this._store.dispatch(new GetSiteTokenGmail);
        this._subscription.add(this._store.select(AuthState.getGmailId).pipe(
            tap(res => {
                if(res){
                   this.gmailId = res;
                }})
        ).subscribe(()=>{
                this.gmailLogin = `https://accounts.google.com/o/oauth2/v2/auth?scope=${environment.gmailScope}&client_id=${this.gmailId}&response_type=code&redirect_uri=${this.config.getConfig().URL+environment.gmailRedirectURL}&state=${environment.SITE_ID}`
                this.document.location.href = this.gmailLogin;
        }
        ))
    }
    ngOnDestroy() {
        this._subscription.unsubscribe();
        this.subsink.unsubscribe();
    }

    signInWithFB() {
        this._store.dispatch(new GetSiteTokenFacebook);
        this._subscription.add(this._store.select(AuthState.getFacebookId).pipe(
            tap(res => {
                if(res){
                   this.facebookId = res;
                }})
        ).subscribe(()=>{
                this.facebookLogin = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${this.facebookId}&response_type=code&redirect_uri=${this.config.getConfig().URL+ environment.facebookRedirectURL}&state=${environment.SITE_ID}&scope=email`
                // this.facebookLogin = `https://www.facebook.com/v10.0/dialog/oauth?client_id=${this.facebookId}&response_type=code&redirect_uri=${environment.facebookRedirectURL}&state=${environment.SITE_ID}`
                this.document.location.href = this.facebookLogin;
        }
        ))
        
    }


}
