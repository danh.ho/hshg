import { Injectable } from '@angular/core';

let facebookAuth: {
    access_token: string,
    data_access_expiration_time: string,
    state: string,
}
@Injectable({
    providedIn: 'root'
})
export class FacebookLoginService {

    constructor() { }
    public setFacebookAuth(auth) {
        facebookAuth = auth;
    }
    public getFacebookAuth() {
        return facebookAuth;
    }
}
