import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofActionDispatched, ofActionSuccessful, Store } from '@ngxs/store';
import { ForgotPasswordInput } from 'src/app/shared/models/graphql.models';
import { AuthState, ForgotPasswordToken, ForgotPasswordTokenFailed, SendCodeSuccessfully } from 'src/app/shared/store/auth';
import { SubSink } from 'subsink';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

@Component({
    selector: 'cos-frontend-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

    forgotPasswordFormGroup: FormGroup;
    title = 'forgotpassword';
    email: string;
    subsink = new SubSink();
    err: any;
    isLoading = true;
    codeAuth: string;
    translate: string;
    cartItemsNo: number;
    private _subscription = new Subscription();

    constructor(
        private _formBuilder: FormBuilder,
        private router: Router,
        private _store: Store,
        private _actions: Actions,
        private _spinner: NgxSpinnerService
    ) { }

    ngOnInit(): void {
        this.forgotPasswordFormGroup = this._formBuilder.group({
            email: ['', [Validators.email, Validators.required]]
        })
        this.subsink.sink = this._actions.pipe(ofActionDispatched(ForgotPasswordToken)).subscribe(() => { this._spinner.show() })
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(SendCodeSuccessfully))
            .subscribe((res) => {
                if (res) {
                    this._spinner.hide();
                    this.router.navigate(['/auth/verify-code'], { queryParams: { email: this.email, title: this.title } })
                }
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(ForgotPasswordTokenFailed))
            .subscribe((err) => {
                this.err = err;
                this._spinner.hide()
                this.codeAuth = this.err.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            });
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
                })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }
    forgot() {
        this.email = this.forgotPasswordFormGroup.get('email').value;
        const email = this.email;
        const data = { email } as ForgotPasswordInput;
        this._store.dispatch(new ForgotPasswordToken(data))

    }
    checkLoading(email: string) {
        if (this.forgotPasswordFormGroup.get('email').invalid) {
            this.isLoading = true
        } else {
            this.isLoading = false
        }
    }
}
