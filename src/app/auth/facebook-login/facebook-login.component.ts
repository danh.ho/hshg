import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FacebookLoginService } from '../facebook-login.service';

@Component({
    selector: 'cos-frontend-facebook-login',
    templateUrl: './facebook-login.component.html',
    styleUrls: ['./facebook-login.component.scss']
})
export class FacebookLoginComponent implements OnInit {

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _facebookLoginService: FacebookLoginService) { }

    ngOnInit(): void {
        this._route.fragment.subscribe(fragments => {
            let params = fragments.split('&');
            this._facebookLoginService.setFacebookAuth({
                access_token: params[0].split('=')[1],
                data_access_expiration_time: params[1].split('=')[1],
                state: params[2].split('=')[1]
            })
        })
        this._router.navigateByUrl('/auth/login')
    }
}
