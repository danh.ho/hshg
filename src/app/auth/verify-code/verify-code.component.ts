import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofActionDispatched, ofActionSuccessful, Store } from '@ngxs/store';
import { Config } from 'ng-otp-input/lib/models/config';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { Action } from 'rxjs/internal/scheduler/Action';
import { switchMap } from 'rxjs/operators';
import { ForgotPasswordInput } from 'src/app/shared/models/graphql.models';
import { NotificationService } from 'src/app/shared/services';
import { AuthState, ForgotPasswordToken, ForgotPasswordVerify, ForgotPasswordVerifyFailed, ForgotPasswordVerifySuccessfully, Register, SendCode, VerifyCode, VerifyCodeFailed, VerifyCodeSuccessfully } from 'src/app/shared/store/auth';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';
import { SubSink } from 'subsink';
import { Code, SendVerifyCode } from './verify-code.model';

@Component({
    selector: 'cos-frontend-verify-code',
    templateUrl: './verify-code.component.html',
    styleUrls: ['./verify-code.component.scss']
})
export class VerifyCodeComponent implements OnInit {
    subsink = new SubSink();
    err: any;
    codeAuth: string;
    translate: string;
    formGroup: FormGroup;
    email: string;
    password: string;
    title: string;
    isLoading = true;
    cartItemsNo: number;
    private _subscription = new Subscription();
    constructor(
        private _formBuilder: FormBuilder,
        private _store: Store,
        private _actions: Actions,
        private router: Router,
        private _notify: NotificationService,
        private route: ActivatedRoute,
        private _spinner: NgxSpinnerService
    ) { }

    ngOnInit(): void {
        this.formGroup = this._formBuilder.group({
            code: ['0',
                Validators.required]
        })
        this.email = this.route.snapshot.queryParamMap.get('email');
        this.password = this.route.snapshot.queryParamMap.get('password');
        this.title = this.route.snapshot.queryParamMap.get('title');
        this.subsink.sink = this._actions.pipe(ofActionDispatched(SendCode)).subscribe(() => this._spinner.show())
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(VerifyCodeSuccessfully))
            .subscribe((res) => {
                if (res) {
                    this._spinner.hide();
                    this._notify.success('', 'Đăng ký thành công')
                    this.router.navigate(['/auth/login'])
                }
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(VerifyCodeFailed))
            .subscribe((res) => {
                this._spinner.hide();
                this.err = res;
                this.codeAuth = this.err.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            });
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
        }));
    }

    onInputEntry(event, nextInput) {
        let input = event.target;
        let length = input.value.length;
        let maxLength = input.attributes.maxlength.value;

        if (length >= maxLength) {
            nextInput.focus();
        }
    }
    sendVerifyCode() {
        const email = this.email;
        const password = this.password;
        const sendCode = { email, password } as SendVerifyCode;
        this._store.dispatch(new SendCode(sendCode));
    }
    getCode() {
        const code = this.formGroup.get('code').value;
        const newCode = { code } as Code;
        this._store.dispatch(new VerifyCode(newCode));
    }
    getCodeForForgetPassword() {
        const code = this.formGroup.get('code').value;
        const data = { code } as Code;
        this._store.dispatch(new ForgotPasswordVerify(data));
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(ForgotPasswordVerifySuccessfully))
            .subscribe((res) => {
                if (res) {
                    this.router.navigate(['/auth/change-password'], { queryParams: { code: code } })
                }
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(ForgotPasswordVerifyFailed))
            .subscribe((res) => {
                this.err = res;
                this.codeAuth = this.err.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            });
    }
    sendVerifyCodeForgot() {
        const email = this.email;
        const data = { email } as ForgotPasswordInput;
        this._store.dispatch(new ForgotPasswordToken(data));
    }
    get config(): Object {
        return {
            allowNumbersOnly: true,
            inputStyles: { width: '2rem', height: '2rem' },
            length: 6
        }
    }
    onOtpChange(otp: string) {
        if (otp.length < 6) {
            this.isLoading = true;
        } else {
            this.isLoading = false
        }
        this.formGroup.get('code').setValue(otp);
    }
}
