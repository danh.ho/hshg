export interface Code{
    code: string;
}
export interface SendVerifyCode{
    email: string;
    password: string;
}