import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import { VerifyCodeComponent } from './verify-code/verify-code.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { FacebookLoginComponent } from './facebook-login/facebook-login.component';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
    declarations: [LoginComponent, RegisterComponent, VerifyCodeComponent, ForgotPasswordComponent, ChangePasswordComponent, FacebookLoginComponent],
    imports: [
        CommonModule,
        AuthRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        IonicModule,
        NgOtpInputModule
    ]
})
export class AuthModule { }
