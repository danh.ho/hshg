import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofActionSuccessful, Store } from '@ngxs/store';
import { CreateNewPasswordInput, TokenVerifyInput } from 'src/app/shared/models/graphql.models';
import { NotificationService } from 'src/app/shared/services';
import { CreateNewPassword, CreateNewPasswordFailed, CreateNewPasswordSuccessfully } from 'src/app/shared/store/auth';
import { SubSink } from 'subsink';
import { Location } from '@angular/common'
@Component({
    selector: 'cos-frontend-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

    changPasswordFormGroup: FormGroup;
    code: string;
    subsink = new SubSink();
    err: any;
    codeAuth: string;
    translate
    constructor(
        private _formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private _store: Store,
        private _actions: Actions,
        private router: Router,
        private _notify: NotificationService,
        private location: Location
    ) { }

    ngOnInit(): void {
        this.code = this.route.snapshot.queryParamMap.get('code');
        this.changPasswordFormGroup = this._formBuilder.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        })
    }
    changePass() {
        const code = this.code;
        const dataCode = { code } as TokenVerifyInput;
        const data = Object.assign({}, dataCode, this.changPasswordFormGroup.value);
        this._store.dispatch(new CreateNewPassword(data));
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(CreateNewPasswordSuccessfully))
            .subscribe((res) => {
                this._notify.success('', 'Tạo mật khẩu mới thành công')
                this.router.navigate(['/auth/login'])
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(CreateNewPasswordFailed))
            .subscribe((err) => {
                this.err = err;
                this.codeAuth = this.err.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            });
    }
    back() {
        this.location.back()
    }

}
