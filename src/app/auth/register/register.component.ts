import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { AnyTxtRecord } from 'dns';
import { Observable, Subscription } from 'rxjs';
import { LoadDistrict, LoadProvince, LoadWard, ProfileState } from 'src/app/my-account/shared/states';
import { DistrictNodeConnection, ProvinceNodeConnection, WardNodeConnection } from 'src/app/shared/models/graphql.models';
import { AuthState, Register, RegisterFailed, RegisterSuccessfully } from 'src/app/shared/store/auth';
import { SubSink } from 'subsink';
import { pass } from './register.model';
import * as moment from 'moment'
import { switchMap } from 'rxjs/operators';
import { ShoppingCartState } from 'src/app/shared/store/shopping-cart';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
        const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

        return (invalidCtrl || invalidParent);
    }
}


@Component({
    selector: 'cos-frontend-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    @Select(ProfileState.getNodeConnectionProvince) provinces$: Observable<ProvinceNodeConnection>;
    @Select(ProfileState.getNodeConnectionDistrict) districts$: Observable<DistrictNodeConnection>;
    @Select(ProfileState.getNodeConnectionWard) wards$: Observable<WardNodeConnection>;
    private _subscription = new Subscription();
    secondFormGroup: FormGroup;
    passGroup: FormGroup;
    matcher = new MyErrorStateMatcher();
    subsink = new SubSink();
    codeAuth: string;
    mess: any;
    translate: string;
    email: string;
    title = 'register';
    dateGroup: FormGroup;
    cartItemsNo: number;
    constructor(
        private router: Router,
        private _formBuilder: FormBuilder,
        private _store: Store,
        private _actions: Actions
    ) { }

    ngOnInit(): void {
        this.secondFormGroup = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            phone: ['', [Validators.pattern("^[0-9]*$"), Validators.minLength(10), Validators.maxLength(10), Validators.required]],
            email: ['', [Validators.required, Validators.email]],
        })
        this.dateGroup = this._formBuilder.group({
            birthday: [new Date(), Validators.required],
            street: [""],
            district: [""],
            province: [""],
            ward: [""],
            gender: ['NAM', Validators.required],
        })
        this.passGroup = this._formBuilder.group({
            pass: ['', [Validators.required, Validators.minLength(6)]],
            confirmPass: ['']
        }, { validator: this.checkPasswords })
        this._subscription.add(
            this._store.select(AuthState.isLogin).pipe(
              switchMap((isLogin) => {
                if (isLogin) {
                  return this._store.select(ShoppingCartState.getMemberCart)
                } else {
                  return this._store.select(ShoppingCartState.getGuestCart)
                }
              })
            ).subscribe(cart => {
              this.cartItemsNo = cart.length;
            }));
        this._initDataProvince()
    }
    register(password) {
        const newPass = { password } as pass;
        const data = Object.assign({}, newPass, this.secondFormGroup.value);
        const {
            birthday,
            street,
            province,
            district,
            ward
        } = this.dateGroup.value
        const dataRegister = {
            ...data,
            street: (street && street != '') ? street : undefined,
            province: (province && province != '') ? province : undefined,
            district: (district && district != '') ? district : undefined,
            ward: (ward && ward != '') ? ward : undefined,
            birthday: moment(birthday).toISOString()
        }
        this._store.dispatch(new Register(dataRegister));
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(RegisterFailed))
            .subscribe((translation) => {
                this.mess = translation;
                this.codeAuth = this.mess.error.code;
                this.translate = `FAILED.USER_ERRORS.${this.codeAuth}`;
            });
        this.subsink.sink = this._actions
            .pipe(ofActionSuccessful(RegisterSuccessfully))
            .subscribe((res) => {
                this.email = this.secondFormGroup.get('email').value;
                this.router.navigate(['/auth/verify-code'], { queryParams: { email: this.email, password: password, title: this.title } })
            });
    }
    checkPasswords(group: FormGroup) {
        let pass = group.controls.pass.value;
        let confirmPass = group.controls.confirmPass.value;

        return pass === confirmPass ? null : { notSame: true }
    }

    _initDataProvince(){
        this._store.dispatch(new LoadProvince())
    }

    _initDataDistrict(province){
        this._store.dispatch(new LoadDistrict({province}))
    }

    _initDataWard(district){
        this._store.dispatch(new LoadWard({district}))
    }

    changeProvince(event, province){
        if (event.source.selected) {
            this._initDataDistrict(province?.node?.id)
        }
    }

    changeDistrict(event, district){
        if (event.source.selected) {
            this._initDataWard(district?.node?.id)
        }
    }
}

