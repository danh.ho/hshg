# The builder from node image
FROM node:12-alpine as builder

RUN apk update && apk add --no-cache make git

# Move our files into directory name "app"
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

# Build with $env variable from outside
RUN npx ionic build --prod

# Build a small nginx image with static website
FROM nginx:1.21-alpine as prod-staged
RUN rm -rf /usr/share/nginx/html/*
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/www/ /usr/share/nginx/html/
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


# FROM staticfloat/nginx-certbot
# RUN rm -rf /usr/share/nginx/html/*
# COPY --from=builder /cos-frontend/dist/cos-frontend/ /usr/share/nginx/html/
