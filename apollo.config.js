module.exports = {
    client: {
        service: {
            name: '2usd.nng.bz',
            // url: 'https://2usd.nng.bz/graphql/',
            url: 'https://dev.hshg.nng.bz/graphql/',
        },
        skipSSLValidation: true,
        excludes: ['node_modules/**/*'],
        includes: ['src/**/*.{ts,gql,tsx,js,jsx,graphql}'],
    },
};
